<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Non-Tenure-Track Adjunct Faculty | VPAFS | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		  <h1>Non-Tenure-Track (Adjunct) Faculty at UTSA</h1>

<p>The University of Texas at San Antonio is dedicated to providing all faculty with the support they need to be successful teachers in order to help our students succeed. Non-Tenure-Track is the overarching name for academic titles in which tenure cannot be granted. NTT titles at UTSA include Lecturers, Instructors, TAs, Professors of Research, Visiting Professors, Adjunct Professors, Adjoint Professors, Clinical Professors, Professors in Practice, and more. For more information about NTT titles, read the <a href="http://www.utsa.edu/hop/chapter2/2-2.html" target="_blank">UTSA Handbook of Operating Procedures (HOP) policy 2.02 <em>Faculty Appointments and Titles</em></a>.
	
		<p>Colloquially, NTT faculty are often referred to (or identify themselves as) <strong>"adjunct" faculty</strong>. At UTSA, adjunct faculty make up 53% of the total faculty population.
			
<h2>Information for New Adjunct Faculty</h2>
<ul>
	<li><a href="http://www.utsa.edu/tlc/tls/Online%20Faculty%20Orientation/index.html#/?_k=f4nlyg" target="_blank">New Faculty Orientation</a> - This online faculty orientation is designed to give new faculty a crash course overview of working and teaching at UTSA. It also provides valuable information to help you get a great start in building your teaching portfolio and research.</li>
	<li><a href="documents/ntt-faculty-handbook.pdf" target="_blank">New Faculty Handbook</a> - an onboarding resource for new non-tenure-track faculty</li>      
	<li><a href="http://teaching.utsa.edu/about/" target="_blank">Teaching & Learning Services</a> - TLS offers programs, services and resources that support instructional excellence, innovative teaching, and outstanding educational experiences for faculty and students.</li>
	<li><a href="http://faculty.utsa.edu/" target="_blank">Faculty Center</a> - The Faculty Center is a centralized hub for professional development that serves all UTSA faculty. Located on the fourth floor of the John Peace Library, it's a space for faculty to get work done, attend workshops, and meet with other faculty.</li>
	<li><a href="faculty-support-groups.asp">Faculty Associations and Support Groups</a></li>
	
</ul>

<h2>Information for Continuing Adjunct Faculty</h2>
       <ul>
		  <li>Workload, Duties, Evaluation and Promotion - <a href="http://www.utsa.edu/hop/chapter2/2-50.html" target="_blank">HOP policy 2.50 <em>Non-Tenure-Track Faculty Recruitment, Evaluation, and Promotion Processes</em></a> outlines workload expectations, who must undergo annual performance evaluation, and the process for promotion.</li>
		  <li>Performance Evaluation - According to <a href="http://www.utsa.edu/hop/chapter2/2-11.html" target="_blank">HOP policy 2.11 <em>Annual Faculty Performance Appraisal for Merit Consideration</em></a>, all full-time and continuing part-time faculty members must undergo annual evaluation. Every January, eligible faculty must complete their annual report (for activity in the previous calendar year) in <a href="http://provost.utsa.edu/vpafs/dm/index.asp" target="_blank">Digital Measures</a>. Read the HOP policy for details on the process, responsibility of the faculty member, deadlines and more.</li>
		  <li><a href="http://www.utsa.edu/gov/" target="_blank">University Governance</a> - including opportunities to get involved and/or stay in-the-know via the <a href="http://www.utsa.edu/senate/" target="_blank">Faculty Senate</a></li>
		  <li><a href="http://www.utsa.edu/facultyawards/" target="_blank">Faculty Awards</a> - There are several university-wide awards as well as the Regents' Outstanding Teaching Award for which NTT faculty are eligible.
    </ul>  
          
          <h2>Information for Departments with Adjunct Faculty</h2>
         
      <ul>
		  <li>Hiring NTT Faculty - NTT positions at UTSA may be filled by promotion through the ranks or through recruitment (as outlined in <a href="http://www.utsa.edu/hop/chapter2/2-50.html" target="_blank">HOP 2.50</a>). All NTT positions are recruited through STARS. The complete process is outlined in the <a href="http://provost.utsa.edu/VPAFS/documents/NTT-Faculty-Recruitment-Manual.pdf" target="_blank">NTT Faculty Recruitment Manual</a>.</li>
		  <li>Performance Evaluation - According to <a href="http://www.utsa.edu/hop/chapter2/2-11.html" target="_blank">HOP policy 2.11 <em>Annual Faculty Performance Appraisal for Merit Consideration</em></a>, all full-time and continuing part-time faculty members must undergo annual evaluation. Read the HOP policy for details on the process, responsibility of the department, timeline and more.</li>
		  
    </ul>
     
            </dd>
          </dl>
        </blockquote>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
