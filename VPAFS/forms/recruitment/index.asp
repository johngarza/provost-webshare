Tags: academics, curriculum <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Recruitment Forms | Academic and Faculty Support | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
  <style type="text/css">
  	h2 {border-bottom: 1px solid #002a5c}
	.office p span.title{color: #002a5c;}
	.office p span.name {font-weight: bold;}
  </style>
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
  <script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           		<!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
             <h1>Faculty Recruitment Forms</h1>



       <!-- <p><a href="2009/2009%20Recruitment%20Forms.zip">All forms in compressed file</a></p>-->
        <h3>Tenure/Tenure-Track Recruitment Forms</h3>
<ul  type="1">
 <li><b><a href="../../documents/Packet-Checklist-TT.pdf">Packet Checklist - TT</a></b></li>
 <li><b><a href="Appt-ltr-TT.doc">Appointment Letter Tenure Track</a></b></li>
  <li><b><a href="../../documents/Recommendation-for-Employment-(FACULTY).pdf">Recommendation for Employment (FACULTY)</a></b></li>
 <li><b><a href="../../documents/Checklist-for-Search-Committee.pdf">Checklist for Search Committee</a></b></li>
	<li><b><a href="../../documents/diversity-advocate-faculty-search-report.doc">Diversity Access and Equity Advocate Faculty Search Report</a></b></li>
 <li><b><a href="../../documents/Faculty-Recruitment-Report.pdf">Faculty Recruitment Report</a></b></li>
 <li><b><a href="2009/Faculty_Applicant_Pool.doc">Faculty Applicant Pool</a></b></li>
   <li><b><a href="../../documents/sample-offer-letter.doc">Offer Letter Template</a> (updated 2/11/2019)</b></li>
   <li><b><a href="2010/Source%20of%20Funds.xls">Source of Funds</a></b></li>
          <li><b><a href="../../documents/English-Language-Proficiency-Form.pdf">English Language Proficiency Form</a></b></li>
</ul>

        <h3>NTT Recruitment Forms</h3>
<ul   type="1">
  <li><b><a href="2010/NTT%20Checklist.doc">Packet Checklist - NTT</a></b></li>
  <li><b><a href="2010/Memorandum%20of%20appointment%20-%20lecturers1.doc">Memorandum of Appointment - Lecturers</a></b></li>
  <li><b><a href="2010/Application%20for%20NTT%20faculty%20positions.doc">Application for Non Tenure-Track Faculty Positions</a></b></li>
          <li><b><a href="../../documents/English-Language-Proficiency-Form.pdf">English Language Proficiency Form</a></b></li>
 </ul>

        <h3>Visiting, Adjunct, Research & Specialist (NTT)</h3>
<ul  type="1">
   <li><b><a href="../../documents/Packet-Checklist-Visiting-Adj-Res.pdf">Packet Checklist - Visiting, Adjunct, Research &amp; Specialist (NTT)</a></b></li>
   <li><b><a href="2010/Appt ltr TT.doc">Memorandum of Appointment</a></b></li>
    <li><b><a href="2010/Application%20for%20NTT%20faculty%20positions.doc">Application for Non Tenure-Track Faculty Positions</a></b></li>
          <li><b><a href="../../documents/English-Language-Proficiency-Form.pdf">English Language Proficiency Form</a></b></li>
</ul>

        <h3>Adjoint Final</h3>
<ul  type="1">
          <li><b><a href="../../documents/English-Language-Proficiency-Form.pdf">English Language Proficiency Form</a></b></li>
</ul>

        <h3>Special Opportunity Hire</h3>
        <ul  type="1">
          <li><b><a href="../../documents/Packet-Checklist-Sp-Opp.pdf">Packet Checklist - Special Opportunity</a></b></li>
          <li><b><a href="../../documents/English-Language-Proficiency-Form.pdf">English Language Proficiency Form</a></b></li>
        </ul>


        </ol>

        <p>If any forms that you are looking for are missing, please notify Jenny De Los Santos at (210) 458-2898.</p>


        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
