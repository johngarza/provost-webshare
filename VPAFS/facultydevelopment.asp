<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Development | VPAFS | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		  <h1>Faculty Development</h1>
<p>
<h2>Definition of Faculty Development at UTSA</h2>
			<p>UTSA defines faculty development as a process by which faculty work to improve their skills in the following areas: (1) pedagogical skills, (2) leadership skills, (3) skills necessary to engage in scholarly activities, (4) personal development, and (5) professional development. Faculty development activities are successful when individuals' goals in these areas are being met and when simultaneously the goals of the organization are being met.</p>

<p>Areas of Faculty Development
<ul>
	<li>Teaching</li>
	<li>Instructional Design and Curriculum Development</li>
	<li>Scholarly Activity including writing, conducting research, presenting at conferences, etc.</li>
	<li>Leadership, Administration, and Organizational Development</li>
	<li>Personal and Professional Development</li></ul>

<p>One thing that sets UTSA apart with respect to faculty development is its <a href="http://faculty.utsa.edu/" target="_blank">Faculty Center</a>. The Faculty Center is a virtual and physical space that serves as a centralized hub for faculty development. Located on the fourth floor of the John Peace Library, its rooms can be reserved for faculty workshops, meetings and events. It also serves as an informal gathering place for the university's faculty, who are encouraged to drop in at any time to chat with colleagues or get work done while away from the office.</p> 

<h2>Partners in Faculty Development</h2>
<p>
<p><strong><a href="http://teaching.utsa.edu/" target="_blank">Teaching & Learning Services</a> (TLS)</strong>

<br>TLS offers programs, services and resources that support instructional excellence, innovative teaching, and outstanding educational experiences for faculty and students.

<p><strong><a href="http://research.utsa.edu/texas-university-research/utsa-research/research-support-officer/" target="_blank">Office of Research Support</a> (ORS)</strong>

<br>ORS offers a comprehensive program for faculty in the area of research skills development. ORS also provides faculty development funding to UTSA researchers through institutional seed grant programs and builds strategic research partnerships through collaborative funding programs with research based organizations and institutions in the San Antonio area.


<p><strong><a href="http://odl.utsa.edu/online/" target="_blank">Office of Online Learning</a></strong>

<br>Online Learning sponsors a variety of workshops and training on topics related to educational technologies, online and hybrid/blended learning, Blackboard Learn, and emerging technologies for teaching and learning.
</p>

        <h2>Types of Faculty Development Programs and Services</h2>
<p>
        <ul>
			<li>Workshops and Seminars (examples below)</li>
			<ul>
				<li>Teaching Skills Development</li>
				<ul>
					<li>Designing Effective: Assessments; Discussions; Group Work</li>
					<li>Academic Integrity</li>
					<li>Cooperative Learning</li>
					<li>Using iClickers</li>
					<li>Gamification</li>
					<li>Lecture Skills</li>
					<li>Power Point Design</li>
					<li>Teaching Large Classes</li>
					<li>Syllabus Design</li>
				</ul>
				<li>Research Skills Development</li>
				<ul>
					<li>Foundations of Grant Seeking</li>
					<li>Grant Writing & the Proposal Development Process</li>
					<li>Project Summary, Goals, Objectives & Significance</li>
					<li>Background, Preliminary Work & Budget</li>
					<li>Strategy & Methodology</li>
					<li>Broader Impacts & Project Evaluation</li>
				</ul>
			</ul>
			<li>Online Course Design & Development</li>
			<li>Forums hosted by the Academy of Distinguished Teaching Scholars</li>
			<li>Provost Academy for Teaching Excellence</li>
			<li>Faculty Learning Communities</li>
			<li>Classroom Observations </li>
			<li>Quick Course Diagnosis (QCD)</li>
			<li>Individual Consultations</li>
			<li>Online Faculty Development Materials</li>
			<li>Funding for Innovative Teaching and Research</li>
			<li>Leadership UTSA</li>
			<li>Peer Mentoring</li>
			<li>Promotion & Tenure Forums</li>
			<li>Networking Opportunities</li>
			<li>Faculty Support Groups</li>
			<li>Notification to Faculty of Faculty Development offerings at both the Main Campus and Downtown Campus </li>
			</ul>

        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
