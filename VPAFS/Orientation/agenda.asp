<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
<style>
#content table {border:none;}
#content table th {border:none;}
#content table tr {border:none;}
#content table td {border:none;}
</style></head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a><a href="/home/orientation"></a></div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!--#include virtual="/home/includes/nav_orientation.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a>
        <br /><img src="images/nfo-collage-web.jpg" width="700" height="140" />
        <img src="images/orientation-logo.png" width="700" height="53" /><br />
<h1 align="center">&nbsp;</h1>
<h1 align="center">AGENDA<br />
  Aug. 14-15, 2018
</h1>
		  <p><a href="docs/nfo18-agenda.pdf" target="_blank">Download the detailed final agenda (PDF)</a></p>
		  
        <h2 align="left"><u>First Day - Aug. 14
       </u></h2>
         <h3>Faculty Center Assembly Room, John Peace Library, 4th Floor (JPL 4.04.22)</h3>
        <table width="650" style="border:hidden" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="108"><strong>8:30 - 9:15</strong></td>
            <td width="528"><strong>Breakfast</strong></td>
          </tr>
          <tr>
            <td width="108"><strong>9:15 - 9:20</strong></td>
            <td><strong>Welcome</strong></td>
          </tr>
          <tr>
            <td width="108"><strong>9:20 - 9:50</strong></td>
            <td><strong>Introduction of New Faculty </strong></td>
          </tr>
          <tr >
            <td width="108"><strong>9:50 - 10:20</strong></td>
            <td><strong>Provost's Overview of Orientation; Role of Faculty at UTSA</strong>
            </td>
          </tr>
          <tr>
            <td width="108"><strong>10:20 &ndash; 11:30</strong></td>
            <td><strong>Research</strong> | <a href="docs/Research-2018.pdf" target="_blank">Download presentation</a>
			  </td>
          </tr>
         
          <tr>
            <td width="108"><strong>11:30 - 11:45</strong></td>
            <td><strong>Break</strong></td>
          </tr>
      <tr>
            <td width="108"><strong>11:45 - 12:00</strong></td>
            <td><strong>President's Welcome to New Faculty</strong></td>
          </tr>
         <tr>
            <td width="108"><strong>12:00 - 1:05</strong></td>
            <td><strong>Lunch - What I Wish I Had Known My First Year at UTSA </strong><br />
				<em>A Conversation with Second-Year Faculty</em></td>
          </tr>
          <tr>
            <td width="108"><strong>1:15 &ndash; 1:35</strong></td>
            <td><strong>The Graduate School</strong>
            </td>
          </tr>
<tr>
            <td width="108"><strong>1:35 &ndash; 1:55</strong></td>
            <td><strong>Opportunities for International Collaboration</strong> | <a href="docs/International-2018.pdf" target="_blank">Download presentation</a></td>
          </tr>
          <tr>
            <td width="108"><strong>1:55 - 2:05</strong></td>
            <td><strong>Break</strong></td>
          </tr>
          <td width="108"><strong>2:05 &ndash; 2:45</strong></td>
            <td><strong>Faculty Role in Addressing Mental Health</strong>
           </td>
          </tr>
           <tr>
            <td width="108"><strong>2:45 - 3:10</strong></td>
            <td><strong>Faculty Involvement</strong> | <a href="docs/Faculty-Involvement-2018.pdf" target="_blank">Download presentation</a></td>
          </tr>
           <tr>
          <tr>
            <td width="108"><strong>3:10 - 3:20</strong></td>
            <td><strong>Break</strong></td>
          </tr>
          
          <tr>
            <td width="108"><strong>3:20 &ndash; 4:30</strong></td>
            <td><strong>Promotion and Tenure at UTSA</strong></td>
          </tr>
          <tr>
            <td width="108"><strong>4:30 - 5:00</strong></td>
            <td><strong>Library Resources</strong> | <a href="docs/Library-Resources-2018.pdf" target="_blank">Download presentation</a></td>
          </tr>
        <tr>
            <td width="108"><strong>5:00 - 6:00</strong></td>
            <td><p><strong>Provost's Reception in the Faculty Center</strong></td>
          </tr>
     <td colspan="2">        </td></tr></table>
         
         
         <h2 align="left"><u>Second Day - Aug. 15</u></h2>
  <h3>Faculty Center Assembly Room, John Peace Library, 4th Floor (JPL 4.04.22)</h3>
    <table width="650" border="none" cellspacing="2" cellpadding="2">
  <tr>
    <td width="108"><strong>8:30 &ndash; 9:00</strong></td>
    <td width="528"><strong>Breakfast</strong></td>
  </tr>
  <tr>
    <td width="108"><strong>9:00 &ndash; 9:15</strong></td>
    <td width="528"><strong>Welcome and Introduction of New Non-Tenure-Track Faculty</strong></td>
  </tr>
  <tr>
    <td width="108"><strong>9:15 &ndash; 9:45</strong></td>
    <td width="528"><strong>Student Success is Our Collective Priority</strong> | <a href="docs/Student-Success-2018.pdf" target="_blank">Download presentation</a> | <a href="docs/2017_12 Student Success _Ventajas_Assets_2014.pdf" target="_blank">Download supporting article</a>
    </td>
  </tr>
     <tr>
    <td width="108"><strong>9:45 &ndash; 10:15</strong></td>
    <td width="528"><strong>Campus Climate Team and Faculty Actions</strong> | <a href="docs/Campus-Climate-Team-2018.pdf" target="_blank">Download presentation</a>
    </td>
  </tr>
    <tr>
    <td width="108"><strong>10:15 &ndash; 10:30 </strong></td>
    <td><strong>Break</strong></td>
  </tr>
     <tr>
    <td width="108"><strong>10:30 &ndash; 11:30</strong></td>
    <td width="528"><strong>Navigating Our Relational Roles with Students: Boundaries and Policies</strong>
    </td>
  </tr>
        <tr>
    <td width="108"><strong>11:30 &ndash; 11:45 </strong></td>
    <td><strong>An Informational Resource to Problem Solving: Meet Your Ombuds!</strong>
			<p><a href="docs/Ombuds-2018.pdf" target="_blank">Download presentation</a></p>
			</td>
  </tr>
          
          <tr>
    <td width="108"><strong>11:45 &ndash; 12:30 </strong></td>
    <td><strong>Lunch</strong></td>
  </tr>
          
          <tr>
    <td width="108"><strong>12:30 &ndash; 1:00 </strong></td>
    <td><strong>Undergraduate Education</strong> | <a href="docs/Undergraduate-Education-2018.pdf" target="_blank">Download presentation</a></p></td>
  </tr>
            <tr>
    <td width="108"><strong>1:00 &ndash; 1:30 </strong></td>
    <td><strong>Promoting Teaching Excellence: An Introduction to Teaching & Learning Services</strong>
				<p><a href="docs/TLS-2018.pdf" target="_blank">Download presentation</a></p></td>
  </tr>
  <tr>
    <td width="108"><strong>1:30 &ndash; 2:00 </strong></td>
    <td><strong>Online Learning</strong> | <a href="docs/OnlineLearning-Faculty-Resources-2018.pdf" target="_blank">Download materials</a></p></td>
  </tr>

</table>
   
<p>&nbsp;</p>
   <!-- <p align="right">All the information here also is available in <a href="New_Faculty_Orientation_2013.pdf">PDF format</a>.</p>-->

       
         


      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</body>
</html>
