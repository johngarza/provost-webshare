<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css">
<style>
#noborder table {border:none;}
#noborder table th {border:none;}
#noborder table tr {border:none;}
#noborder table td {border:none;}
</style>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a></div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!--#include virtual="/home/includes/nav_orientation.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <p><a name="acces-content" class="screen-reader" id="acces-content"></a><br /><img src="images/nfo-collage-web.jpg" width="700" height="140" /><img src="images/orientation-logo.png" width="700" height="53" /><br />
        <h1 align="center">August 13-18, 2018</h1>
<p>&nbsp;</p>
        <h2 align="center">Welcome to UTSA!</h2>
        <!--<p><img src="../../images/Agrawal-web2.jpg" alt="agrawal" width="164" height="224" align="right" style="margin: 0 0 0 10px;"/>-->
        <strong>Welcome to The University of Texas at San Antonio, and congratulations on your appointment as a member of the UTSA faculty.</strong></p>
        <p><strong>As a faculty member, you play a critical role in the success of our students, as well as in UTSA&rsquo;s vision as a premier public research university. As such, we are committed to your development as a teacher and scholar throughout your academic career at UTSA&mdash;New Faculty Orientation is just the beginning. To introduce you to the university, we have designed an agenda to help you get a great start in building your teaching portfolio and research program here at UTSA, with ample opportunity to get to know your new faculty and administrative colleagues across campus. We encourage you to browse this website as it includes pertinent information and resources to assist your transition to your new city and university.</strong></p>
        <p><!--I look forward to meeting you at New Faculty Orientation.</strong></p>
        <p><strong><em>C. Mauli Agrawal, Ph.D., P.E.<br />
        Interim Provost and Vice President for Academic Affairs
        </em></strong></p>-->
     
<table width="700" border="0" cellpadding="10">
  <tbody>
    <tr>
      <th scope="col" width="350" align="left"><h3>For New Tenure-Track/Tenured Faculty</h3>
         <br>
          <p>Aug. 13 - <a href="http://www.utsa.edu/hr/Employment/DayOne/" target="_blank">Day O.N.E. (Orientation for New Employees) with Human Resources</a>*: On this day, you will be provided with information about your employment and benefits, receive your UTSA ID Card, Parking Permit and MyUTSA ID account setup.</p>
        <p>Aug. 14-15 - <a href="http://provost.utsa.edu/vpafs/orientation/agenda.asp">Provost's Orientation</a>*: This two-day training will introduce you to UTSA leadership, culture and important resources to help you hit the ground running. You will also have your professional photo taken.</p>
        <p>Aug. 16-17 - College and/or department orientations (check with your college and department)</p>
		  <p>Aug. 18 - President's Dinner and Tour</p>
     
		
        <p align="center"><a href="https://utsafacultycenter.wufoo.com/forms/s1233hsx02ckl48/" target="_blank"><img src="images/registerbutton-tt.png" width="200" height="69" alt=""/></a>        </p></th>
      
      
      <th scope="col" width="350" align="left"><h3>For New Full-Time Non-Tenure-Track Faculty</h3>
         <br>
        <p>Aug. 14, 5-6 p.m. - Provost's Reception - A chance to meet and mingle with UTSA leaders, staff, and other new faculty. Drinks and hors d'oeuvres will be served.
        <p>Aug. 15, 8:30 a.m.-2 p.m. - New full-time NTT faculty are invited to attend the 2nd day of the <a href="http://provost.utsa.edu/vpafs/orientation/agenda.asp">Provost's Orientation</a>, which focuses on student success, university policies and resources, and teaching support.</p>
        <p>Aug. 16-17 - College and/or department orientations (check with your college and department)</p>
        <br>
     <p>
       
        <p align="center"><a href="https://utsafacultycenter.wufoo.com/forms/sqlej4y0ez2x8v/" target="_blank"><img src="images/registerbutton-ntt.png" width="200" height="69" alt=""/></a>
        </p>
    </tr>
  </tbody>
</table>
        
<p><em>*Required for tenure/tenure-track faculty members who are joining UTSA in Summer or Fall 2018 or those who joined UTSA mid-year 2017-2018 and did not attend the 2017 New Faculty Orientation.</em></p>
      
    
            
      

      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
  
 <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</body>
</html>
