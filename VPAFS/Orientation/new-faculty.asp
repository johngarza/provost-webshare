<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
<style>
#noborder table {border:none;}
#noborder table th {border:none;}
#noborder table tr {border:none;}
#noborder table td {border:none;}
</style>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a></div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!--#include virtual="/home/includes/nav_orientation.asp" -->
          </div>
      </div><!-- end of col-navigation -->
   
      <div id="col-main">
        <p><a name="acces-content" class="screen-reader" id="acces-content"></a></p>
        <p><img src="images/nfo-collage-web.jpg" width="700" height="140" alt="nfo-collage" /><br />
          <img src="images/orientation-logo.png" width="700" height="53" /><br />
          
        </p>
        <h1 align="center">&nbsp;</h1>
        <h1 align="center">NEW TENURE-TRACK & TENURED FACULTY FOR 2018</h1>
		  <p align="center"><em>as of 8/1/18</em></p>
      
              <div id="noborder"> <table width="700" border="1">
          <tr>
            <td colspan="3"><h2>College of Architecture, Construction and Planning (CACP)</h2></td>
          </tr>
          <tr>
             <td width="233"><strong>Ibukun Awolusi</strong><br />
            Assistant Professor<br>
            Architecture<br>
			  Ph.D., The University of Alabama</td>
			  
			  <td width="233"><strong>Antonio Martinez-Molina</strong><br />
            Assistant Professor<br>
            Construction Science<br>
			  Ph.D., Polytechnic University of Valencia UPV</td>
			  
			  <td width="233"><strong></strong><br />
            <br>
            </td>
            

        </table></div>
         
         
         <div id="noborder"> <table width="700" border="1">
          <tr>
            <td colspan="3"><h2>College of Business (COB)</h2></td>
          </tr>
          <tr>
            <td width="233"><strong>Shannon L. Marlow</strong><br />
            Assistant Professor<br>
            Management<br>
			  ABD, Rice University</td>
            
            <td width="233"><strong>Yeongjoo Park</strong><br />
            Assistant Professor<br>
            Management Science and Statistics<br>
			  Ph.D., University of Illinois at Urbana-Champaign</td>

			  <td width="233"><strong>Anthony Rios</strong><br />
            Assistant Professor<br>
            Information Systems and Cyber Security<br>
			  ABD, University of Kentucky</td>
            
            </tr>
			 <tr>
            <td width="225"><strong>Arkajyoti Roy</strong><br />
			Assistant Professor<br>
         	Management Science and Statistics<br>
			  Ph.D., Purdue University</td>

            <td width="225"><strong>Dian Wang</strong><br />
            Assistant Professor<br>
            Marketing<br>
			  ABD, Texas A&M University</td>
            
            <td width="225"><strong>Wenbo Wu</strong><br />
			Assistant Professor<br>
         	Management Science and Statistics<br>
			  Ph.D., University of Georgia</td>
            
          </tr>
          
        </table></div>
        
<div id="noborder"> <table width="700" border="1">
    <tr>
            <td colspan="2"><h2>College of Education and Human Development (COEHD)</h2></td>
          </tr>
     <tr>
      <td width="233"><strong>Amarie Carnett</strong><br />
            Assistant Professor<br>
            Educational Psychology<br>
			  Ph.D., Victoria University of Wellington</td>
           
           <td width="233"><strong>Kelly Cheever</strong><br />
            Assistant Professor<br>
            Kinesiology, Health and Nutrition<br>
			  Ph.D., Temple University</td>
            
		 <td width="233"><strong>Samuel Ray DeJulio</strong><br />
            Assistant Professor<br>
            Interdisciplinary Learning and Teaching<br>
			  ABD, The University of Texas at Austin</td>
    </tr>
    <tr>

           <td width="233"><strong>Jeffrey Howard</strong><br />
            Assistant Professor<br>
           Kinesiology, Health and Nutrition<br>
			  Ph.D., The University of Texas at San Antonio</td>
            
		<td width="233"><strong>Claudia G. Interiano</strong><br />
            Assistant Professor<br>
            Counseling<br>
			  ABD, University of North Carolina-Charlotte</td>
            
            <td width="233"><strong>Olesya Kisselev</strong><br />
            Assistant Professor<br>
            Bicultural-Bilingual Studies<br>
			  Ph.D., Penn State University</td>
         
    </tr>
    <tr>
          <td width="233"><strong>Devon Romero</strong><br />
            Assistant Professor<br>
            Counseling<br>
			  ABD, University of Alabama</td>  
            
		<td width="233"><strong>Masataka Umeda</strong><br />
            Assistant Professor<br>
            Kinesiology, Health and Nutrition<br>
			  Ph.D., University of Wisconsin-Madison</td>
            
		<td width="225"><strong>Mike Villarreal</strong><br />
            Assistant Professor<br>
				Educational Leadership and Policy Studies<br>
			  Ph.D., The University of Texas at Austin</td>
    </tr>

    <tr>
            <td width="233"><strong>Tianou Zhang</strong><br />
            Assistant Professor<br>
				Kinesiology, Health and Nutrition<br>
			  Ph.D., University of Minnesota-Twin Cities</td></tr>        
    
        </table></div>
        
       <div id="noborder"> <table width="700" border="1">
    <tr>
            <td colspan="3"><h2>College of Engineering (COE)</h2></td>
          </tr>
     <tr>
		 <td width="233"><strong>Nehal Abu-Lail</strong><br>
           Associate Professor<br>
           Chemical Engineering<br>
			  Ph.D., Worcester Polytechnic Institute</td>
		 
		 <td width="233"><strong>Miltos Alamaniotis</strong><br>
           Assistant Professor<br>
           Electrical and Computer Engineering<br>
			  Ph.D., Purdue University</td>
		 
		 <td width="233"><strong>Taposh Banerjee</strong><br>
           Assistant Professor<br>
           Electrical and Computer Engineering<br>
			  Ph.D., University of Illinois at Urbana-Champaign</td>
    </tr>
    
    <tr>
		<td width="233"><strong>Christopher Combs</strong><br>
            Assistant Professor<br>
            Mechanical Engineering<br>
			  Ph.D., The University of Texas at Austin</td>
		
		<td width="233"><strong>Hugo Giambini</strong><br>
           Assistant Professor<br>
           Biomedical Engineering<br>
			  Ph.D., Mayo Graduate School, Mayo Clinic</td>
            
		<td width="233"><strong>Amit Kumar</strong><br>
   Assistant Professor<br>
   Civil and Environmental Engineering<br>
			  Ph.D., Purdue University</td>
    
    </tr>
    
    <tr>
		<td width="233"><strong>Amina Qutub</strong><br>
   Associate Professor<br>
   Biomedical Engineering<br>
			  Ph.D., University of California, Berkeley</td>
		
		<td width="233"><strong>Abelardo Ramirez-Hernandez</strong><br>
           Assistant Professor<br>
           Chemical Engineering<br>
			  Ph.D., Universidad Autonoma del Estado de Morelos</td>
            
		<td width="233"><strong>David Restrepo</strong><br>
   Assistant Professor<br>
   Mechanical Engineering<br>
			  Ph.D., Purdue University</td>
    
    </tr>
    
   
        </table></div>
        
        
                     <div id="noborder"> <table width="700" border="1">
          <tr>
            <td colspan="3"><h2>College of Liberal and Fine Arts (COLFA)</h2></td>
          </tr>
          <tr>
            <td width="233"><strong>Fernando A. Campos</strong><br />
            Assistant Professor<br>
            Anthropology<br>
			  Ph.D., University of Calgary</td>
            
            <td width="233"><strong>Tracy Cowden</strong><br />
			Department Chair and Professor<br>
         	Music<br>
			  D.M.A., Eastman School of Music</td>
			  
			  <td width="233"><strong>Neil A. Debbage</strong><br />
            Assistant Professor<br>
            Political Science and Geography<br>
			  ABD, University of Georgia</td>
						 </tr>
                 <tr>

            <td width="233"><strong>Marcus Hamilton</strong><br />
			Associate Professor<br>
         	Anthropology<br>
			  Ph.D., University of New Mexico</td>
					 
			<td width="233"><strong>Andrew Lloyd</strong><br />
            Assistant Professor<br>
            Music<br>
			  D.M.A., University of North Texas</td>
            
            <td width="233"><strong>Nathan Richardson</strong><br />
			Department Chair and Professor<br>
         	Modern Languages and Literatures<br>
			  Ph.D., University of Kansas</td>		 
						 </tr>
                
                 <tr>
            <td width="233"><strong>Andrea L. Ruiz</strong><br />
            Assistant Professor<br>
            Sociology<br>
			  Ph.D., Penn State University</td>
            
            <td width="233"><strong>Kerry Sinanan</strong><br />
			Assistant Professor<br>
         	English<br>
			  Ph.D., Trinity College, Dublin</td>
					 
			<td width="233"><strong>Alicia Swan</strong><br />
			Assistant Professor<br>
         	Psychology<br>
			  Ph.D., Southern Illinois University</td>
						 </tr>
                 <tr>
            
            <td width="233"><strong>Serife Tekin</strong><br />
            Assistant Professor<br>
            Philosophy and Classics<br>
			  Ph.D., York University, Canada</td>
				
            <td width="233"><strong>Crystal Webster</strong><br />
			Assistant Professor<br>
         	History<br>
			  Ph.D., University of Massachusetts, Amherst</td>
            
            <td width="233"><strong>Eva C. Wikberg</strong><br />
            Assistant Professor<br>
            Anthropology<br>
			  Ph.D., University of Calgary</td>
						 </tr>
            
            <tr>
            <td width="225"><strong>Jessica Wright</strong><br />
			Assistant Professor<br>
         	Philosophy and Classics<br>
			  Ph.D., Princeton University</td>
						 </tr>
                  

        </table></div>
        
        
                             <div id="noborder"> <table width="700" border="1">
          <tr>
            <td colspan="3"><h2>College of Public Policy (COPP)</h2></td>
          </tr>
          <tr>
            <td width="233"><strong>Cashen Boccio</strong><br />
            Assistant Professor<br>
            Criminal Justice<br>
			  ABD, Florida State University</td>
            
            <td width="233"><strong>Chantal Fahmy</strong><br />
			Assistant Professor<br>
				Criminal Justice<br>
			  ABD, Arizona State University</td>
			  
			<td width="233"><strong>Ying Huang</strong><br />
            Assistant Professor<br>
            Demography<br>
			  Ph.D., University at Albany, SUNY</td>
								 </tr>
          <tr>
            
            <td width="233"><strong>Megan Hayes Piel</strong><br />
			Assistant Professor<br>
				Social Work<br>
			  Ph.D., Arizona State University</td>
          
            <td width="233"><strong>Bonita Sharma</strong><br />
            Assistant Professor<br>
            Social Work<br>
			  Ph.D., The University of Texas at Arlington</td>
            
            <td width="233"><strong>Alexander Testa</strong><br />
			Assistant Professor<br>
				Criminal Justice<br>
			  ABD, University of Maryland, College Park</td>
								 
								 </tr>
          <tr>
            <td width="233"><strong>Jolena Todic</strong><br />
            Assistant Professor<br>
            Social Work<br>
			  ABD, The University of Texas at Austin</td>
            
            

        </table></div>
        
              <div id="noborder"> <table width="700" border="1">
    <tr>
            <td colspan="3"><h2>College of Sciences (COS)</h2></td>
          </tr>
     <tr>
           <td width="233"><strong>Kamal Al Nasr</strong> (begins Jan. 2019)<br />
            Associate Professor<br>
            Computer Science<br>
			  Ph.D., Old Dominion University</td>
           
           <td width="233"><strong>Jennifer Smith</strong><br />
            Assistant Professor<br>
            Environmental Science and Ecology<br>
			  Ph.D., University of Birmingham</td>

           <td width="233"><strong>Hui Zhang</strong> (begins Jan. 2019)<br />
            Associate Professor<br>
            Computer Science<br>
			  Ph.D., Indiana University</td>
    </tr>
   
        </table></div>
        <p>
        <br>
		  <p>See something that needs to be changed? Please email us at <a href="mailto:vpafs@utsa.edu">vpafs@utsa.edu</a> with any corrections.</p>
        
       <!-- <p align="right">All the information here also is available in <a href="New_Faculty_Orientation_2013.pdf">PDF format</a>.</p>-->
            

         


      </div><!-- end col-main-->
    </div>

  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</body>
</html>
