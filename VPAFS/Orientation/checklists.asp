<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a></div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!--#include virtual="/home/includes/nav_orientation.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a><br /><img src="images/nfo-collage-web.jpg" width="700" height="140" /><img src="images/orientation-logo.png" width="700" height="53" /><br />

        <h1 align="center">&nbsp;</h1>
        <h1 align="center">CHECKLISTS</h1>

        <p>These handy checklists will help you stay on track from the moment you accept our offer to the end of your first semester at UTSA.</p>
        
        <h2>Prior to Arrival <a href="docs/checklist-prior-to-arrival.pdf" style="text-decoration:none"><img src="images/Adobe_PDF_file_icon_24x24.png" width="24" height="24" alt="pdf" /></a></h2>
        <h4>&#x274f; Register for New Faculty Orientation.</h4>
        &nbsp;
          <h4> &#x274f; Touch base with your department chair to discuss your arrival date, office location, equipment and supply needs, class schedule, and any required department or college orientations.</h4>&nbsp;
          <h4> &#x274f; Get to know your department administrative staff.</h4>&nbsp;
          <h4> &#x274f; Explore the <a href="https://www.utsa.edu/visit/main-campus.html" target="_blank">UTSA campus maps</a> to familiarize yourself with the community and parking.</h4> &nbsp;
          <h4> &#x274f; Familiarize yourself with San Antonio if you are new to the area.</h4> &nbsp;
          <h4> &#x274f; Familiarize yourself with the <a href="http://faculty.utsa.edu/" target="_blank">UTSA Faculty Center</a> and its many resources.</h4> &nbsp;
          <h4> &#x274f; Work with your department to <a href="http://www.bkstr.com/webapp/wcs/stores/servlet/StoreCatalogDisplay?langId=-1&amp;storeId=15178&amp;demoKey=d&amp;catalogId=10001" target="_blank">order your textbooks</a> from Follett.</h4> &nbsp;
          <h4> &#x274f; Familiarize yourself with the <a href="http://www.utsa.edu/HOP/" target="_blank">UTSA Handbook of Operating Procedures</a> (HOP).</h4> &nbsp;
          <h4> &#x274f; Parking permits are generally purchased at Day O.N.E. orientation. If you need a permit before Aug. 13, contact your department.</h4> &nbsp;
          <h4> &#x274f; If you require UTSA email/network access more than 15 days prior to your contract start date, please make arrangements with your department.</h4> &nbsp;
          <h4> &#x274f; Send your list of required equipment/supplies to your department administrative staff. Follow up to ensure the order has been placed.</h4> &nbsp;
          <h4> &#x274f; Make sure that you have received communications from Human Resources about Day O.N.E. orientation.
          </h4>
        <hr />
        
        <h2>Upon Arrival <a href="docs/checklist-upon-arrival.pdf"><img src="images/Adobe_PDF_file_icon_24x24.png" width="24" height="24" alt="pdf" /></a></h2>
        <h4>&#x274f; Attend Day O.N.E. (new employee orientation) hosted by UTSA Human Resources, Aug. 13, 2018. On this day, you will be provided with information about your employment and benefits, receive your UTSA ID Card, Parking Permit, and MyUTSA ID account setup.</h4>&nbsp;
      <h4>&#x274f; Attend the Provost's New Faculty Orientation, Aug. 14-15, 2018. This orientation is required for all new tenure/tenure-track faculty. </h4>&nbsp;
          <!--<h4> &#x274f; Have your professional photo taken at New Faculty Orientation. See schedule for times.</h4>&nbsp;-->
          <h4> &#x274f; Attend your college and/or department orientation, Aug. 16-17, 2018. Check with your department for details.</h4>&nbsp;
          <h4> &#x274f; Attend the President&rsquo;s Tour and Dinner, Saturday, Aug. 18, 2018.</h4>&nbsp;
          <h4> &#x274f; Make sure your university email is working. Contact <a href="http://www.utsa.edu/oit/FacultyAndStaffServices/Support/OITConnect.html" target="_blank">OIT Connect</a> at 458-5555 if you have any trouble.</h4>&nbsp;
          <h4> &#x274f; Make sure your office telephone is working. Set up your voice messaging system. Contact <a href="http://www.utsa.edu/oit/FacultyAndStaffServices/Support/OITConnect.html" target="_blank">OIT Connect</a> at 458-5555 if you have any trouble.</h4>&nbsp;
          <h4> &#x274f; Check on the status of your book orders at the campus bookstore. If they have not arrived, find out expected date/time so that you can share this information with students on the first day of class.</h4>&nbsp;
          <h4> &#x274f; Meet with your department chair; inquire about the department&rsquo;s mentoring program.</h4>&nbsp;
          <h4> &#x274f; Meet your department&rsquo;s administrative staff.</h4>&nbsp;
          <h4> &#x274f; Complete the required compliance training. Check your UTSA email for instructions and the deadline.         </h4>&nbsp;
        <hr />
        
        <h2>Before First Day of Class <a href="docs/checklist-before-first-class.pdf"><img src="images/Adobe_PDF_file_icon_24x24.png" width="24" height="24" alt="pdf" /></a></h2>
        <h4>&#x274f; Prepare your syllabus for each class. Find syllabus resources on the <a href="http://teaching.utsa.edu/resources/class-preparation/">Teaching & Learning Services website</a>. If you need assistance or have questions, contact your <a href="https://lib.utsa.edu/services/find-your-librarian/" target="_blank">subject specialist librarian</a>. </h4>&nbsp;
        <h4> &#x274f; Upload each class syllabus to <a href="https://bluebook.utsa.edu/" target="_blank">Bluebook</a> (UT System requirement).</h4>&nbsp;
        
        <h4>&#x274f; Upload each class syllabus to <a href="https://utsa.blackboard.com" target="_blank">Blackboard</a> so that is available to students by the first day of class.</h4>&nbsp;
        <h4>&#x274f; Connect with your <a href="https://lib.utsa.edu/services/find-your-librarian/" target="_blank">subject specialist librarian</a> to discuss your own information or library research needs and to learn about library support for your teaching.</h4>&nbsp;
        <h4> &#x274f; Obtain your class roster(s) via ASAP:<br />
          - Go to <a href="http://www.utsa.edu" target="_blank">http://www.utsa.edu</a><br />
          - Click on &ldquo;myUTSA,&rdquo; then click on &ldquo;ASAP&rdquo;<br />
          - Log in to ASAP with your abc123<br />
        - Click on &ldquo;Faculty Services&rdquo; and scroll down to &ldquo;Summary Class List.&rdquo; You can also retrieve a Class Photo List for each class within the Faculty Services page.</h4>&nbsp; 
        <h4> &#x274f; Visit your classrooms to check the layout and technology. Make sure you&rsquo;re familiar with the teaching technology available in each classroom. Contact <a href="http://www.utsa.edu/oit/services/classroom-support-and-digital-learning/index.html" target="_blank">Learning Technologies</a> (458-5555 or oitconnect@utsa.edu) if you need assistance.   Log on to the classroom computers before class since the first time takes longer than usual.</h4>&nbsp;
<hr />
        
        <h2>Before the End of Your First Semester <a href="docs/checklist-before-semester-end.pdf"><img src="images/Adobe_PDF_file_icon_24x24.png" width="24" height="24" /></a></h2>
        <h4>&#x274f; Respond to the 2018 New Faculty Orientation Survey to provide feedback about your orientation experience.</h4>&nbsp;
          <h4>&#x274f; Discuss your workload with your department chair.</h4>&nbsp;
          <h4>&#x274f; Meet with your mentor(s).</h4>&nbsp;
          <h4>&#x274f; Attend the faculty development workshops that are part of the <a href="../newfaculty/institute.asp" target="_blank">New Faculty Institute</a> and any others you find interesting via the <a href="http://faculty.utsa.edu" target="_blank">Faculty Center</a>.</h4>&nbsp;
          <h4>&#x274f; Check your <a href="http://provost.utsa.edu/VPIE/dm/workload-reporting.asp" target="_blank">UTSA Faculty Workload Detail report</a> for accuracy.</h4>&nbsp;
        <h4>&#x274f; Keep up with entering your professional accomplishments and activities in <a href="http://provost.utsa.edu/vpaie/dm/" target="_blank">Digital Measures</a>. </h4>
        <p>&nbsp;</p>
     <!-- <p align="right">All the information here also is available in <a href="New_Faculty_Orientation_2013.pdf">PDF format</a>.</p>-->
            

         


      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div><!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</html>
