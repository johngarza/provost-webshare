<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a></div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!--#include virtual="/home/includes/nav_orientation.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a><br /><img src="images/nfo-collage-web.jpg" width="700" height="140" /><img src="images/orientation-logo.png" width="700" height="53" /><br />

        <h1 align="center">&nbsp;</h1>
        <h1 align="center">RESOURCES</h1>

        <p>This page provides a list of some resources that will help prepare you for success at UTSA. You can also explore www.utsa.edu and the <a href="http://www.utsa.edu/hop/">Handbook of Operating Procedures</a> for additional information.</p>
        
        <!--<h2>Orientation Materials</h2>
        <p><a href="docs/Digital-Measures-NFO16.pdf">Digital Measures</a><br />
          <a href="docs/Faculty-Involvement-NFO16.pdf">Faculty Involvement</a><br />
          <a href="docs/How-Faculty-Help-UTSA-Students-Succeed-NFO16.pdf">How Faculty Help UTSA Students Succeed</a><br />
        <a href="docs/Library-Resources-NFO16.pdf">Library Resources</a></p> -->
        
        <h2>About UTSA</h2>
        <p>Learn about UTSA&rsquo;s history, mission/vision, strategic goals and more.</p>
        <p><a href="http://www.utsa.edu/about/" target="_blank">UTSA Overview</a><br />
          <a href="http://www.utsa.edu/about/leadership/index.html" target="_blank">Leadership</a><br />
          <a href="http://www.utsa.edu/strategicplan/" target="_blank">Strategic Plan</a><br />
          <a href="http://www.utsa.edu/academics/" target="_blank">Academics</a><br />
          <a href="http://www.utsa.edu/about/glance/marks-of-excellence.html" target="_blank">Accolades</a><br />
          <a href="http://goutsa.com/" target="_blank">Athletics</a><br />
          <a href="http://www.utsa.edu/campuscarry/" target="_blank">Campus Carry</a><br />
          <a href="http://www.utsa.edu/visit/main-campus.html" target="_blank">Campus Maps</a><br />
          <a href="https://campusrec.utsa.edu/" target="_blank">Campus Recreation</a><br />
          <a href="http://www.utsa.edu/community/" target="_blank">Community Services</a><br />
          <a href="http://utsa.campusdish.com/" target="_blank">Dining Services</a><br />
          <a href="https://www.utsa.edu/hr/Employee.html" target="_blank">Human Resources</a> (benefits, compensation, leave administration, employee discount program) <br />
          <a href="https://lib.utsa.edu/" target="_blank">Libraries</a><br />
          <a href="http://www.utsa.edu/auxiliary/parking.html" target="_blank">Parking</a><br />
          <a href="http://research.utsa.edu/" target="_blank">Research</a><br />
          <a href="http://www.utsa.edu/calendar/" target="_blank">UTSA Calendar </a><br />
          <a href="http://alerts.utsa.edu/" target="_blank">UTSA Campus Alerts</a><br />
          <a href="http://www.utsa.edu/cdc/" target="_blank">UTSA Child Development Center</a> (located at the UTSA Main Campus) <br />
          <a href="http://www.utsa.edu/directory/" target="_blank">UTSA Directory</a><br />
          <a href="http://www.utsa.edu/today/" target="_blank">UTSA Today</a><br />
        </p>
        <h2>About San Antonio</h2>
        <p>Learn about San Antonio, its neighborhoods, school districts, and other resources to assist your move.</p>
        <p><a href="https://www.bexar.org/" target="_blank">Bexar County</a><br />
          <a href="http://www.sanantonio.gov/" target="_blank">City of San Antonio</a><br />
          <a href="http://www.sabor.com" target="_blank">San Antonio Board of Realtors</a><br />
          <a href="https://www.sachamber.org/contact-us/resources/relocation/" target="_blank">San Antonio Chamber of Commerce - Relocation Guide</a><br />
          <a href="http://visitsanantonio.com/" target="_blank">San Antonio Convention &amp; Visitors Bureau</a><br />
          <a href="http://www.sanantonio.gov/ParksAndRec/Home.aspx" target="_blank">San Antonio Parks &amp; Recreation</a><br />
          <a href="https://www.mymove.com/guide/moving-to-san-antonio.html" target="_blank">My Move - Independent Guide to Moving to San Antonio</a><br />
          <a href="http://www.viainfo.net" target="_blank">VIA Metropolitan Transit</a>
        </p>
        <h2>Faculty Support</h2>
        <p>Here are some resources, services and policies that you may find most helpful to be aware of before you get to UTSA.</p>
         <a href="http://www.utsa.edu/bit/tips.cfm" target="_blank">Behavioral Concerns Assistance - Tips for Faculty</a><br />
          <a href="https://utsa.blackboard.com" target="_blank">Blackboard Learn</a><br />
		  <a href="http://www.utsa.edu/campusclimate/" target="_blank">Campus Climate Team</a><br>
          <a href="http://provost.utsa.edu/VPAFS/dm/index.asp" target="_blank">Digital Measures</a> (documenting your academic career) <br />
          <a href="http://provost.utsa.edu/home/Evaluation/" target="_blank">Evaluation, Promotion &amp; Tenure, Third-Year Review  </a><br />
          <a href="http://faculty.utsa.edu" target="_blank">Faculty Center</a><br />
          <a href="http://www.utsa.edu/facultyawards/" target="_blank">Faculty Awards</a><br />
          <a href="https://www.utsa.edu/hop/chapter2/2-34.html" target="_blank">Faculty Grievance Policy</a><br />
          <a href="http://provost.utsa.edu/vpafs/newfaculty/mentoring.asp" target="_blank">Faculty Peer Mentoring</a><br />
          <a href="http://www.utsa.edu/hop/chapter4/4-2.html" target="_blank">Faculty Rights and Responsibilities Policy</a><br />
          <a href="http://www.utsa.edu/hop/chapter2/2-14.html" target="_blank">Faculty Workload Policy</a><br />
          <a href="http://www.utsa.edu/registrar/files/FERPA_faculty_info.pdf" target="_blank">Family Educational Rights and Privacy Act</a> (FERPA) <br />
          <a href="http://odl.utsa.edu/online/" target="_blank">Online Learning</a><br />
          <a href="http://provost.utsa.edu/vpafs/newfaculty/institute.asp" target="_blank">New Faculty Institute</a><br />
          <a href="http://research.utsa.edu/texas-university-research/utsa-research/research-support-officer/" target="_blank">Research Support</a><br />
          <a href="http://www.utsa.edu/disability/resources/facultyguide.html" target="_blank">Student Disability Services - Faculty Resource Guide</a><br />
          <a href="http://teaching.utsa.edu" target="_blank">Teaching & Learning Services</a><br />
          <a href="http://www.utsa.edu/financialaffairs/dts/" target="_blank">Travel Guidelines</a><br />
          
        
        <h2>Student Support</h2>
        <p>UTSA prides itself on its diverse student population. Learn more about UTSA student demographics in <a href="https://www.utsa.edu/about/glance/" target="_blank">UTSA Fast Facts</a>. </p>
        <p><a href="http://www.utsa.edu/campuslife/index.html" target="_blank">Campus Life</a><br />
        <a href="http://careercenter.utsa.edu" target="_blank">Career Center</a><br />
         <a href="http://www.utsa.edu/eos/discrimination.html" target="_blank">Discrimination and Sexual Harassment</a><br>
			<a href="https://www.utsa.edu/dreamers/" target="_blank">Dreamers Resource Center</a><br>
         <a href="https://pivot.utsa.edu/f2gg/" target="_blank">First-Gen and Transfer Student Support</a><br>
          <a href="http://www.utsa.edu/info/students.html" target="_blank">Information for Students</a><br />
          <a href="http://www.utsa.edu/studentunion/roadrunnerpantry" target="_blank">Roadrunner Pantry</a><br />
          <a href="http://www.utsa.edu/counsel/" target="_blank">Student Counseling Services</a><br>
          <a href="http://www.utsa.edu/disability/" target="_blank">Student Disability Services</a><br>
        <a href="http://www.utsa.edu/sga/" target="_blank">Student Government Association</a><br />
        <a href="http://www.utsa.edu/health/" target="_blank">Student Health Services</a><br>
        <a href="http://www.utsa.edu/trcss/" target="_blank">Tutoring & Supplemental Instruction</a><br />
			<a href="http://www.utsa.edu/advise/" target="_blank">Undergraduate Advising</a><br>
			<a href="http://www.utsa.edu/va" target="_blank">Veterans Certification Office</a><br>
			<a href="http://www.utsa.edu/twc" target="_blank">Writing Center</a></p>
    

    
       <!-- <p align="right">All the information here also is available in <a href="New_Faculty_Orientation_2013.pdf">PDF format</a>.</p>-->
            

         


      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</body>
</html>
