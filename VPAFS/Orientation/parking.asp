<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a></div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!--#include virtual="/home/includes/nav_orientation.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a><br /><img src="images/nfo-collage-web.jpg" width="700" height="140" /><img src="images/orientation-logo.png" width="700" height="53" /><br />

        <h1 align="center">&nbsp;</h1>
        <h1 align="center">DIRECTIONS &amp; PARKING</h1>

        <p>New Faculty Orientation will take place primarily at the Faculty Center in the John Peace Library (JPL) at the UTSA Main Campus, located near I-10 and Loop 1604 in Northwest San Antonio.</p>
        <p>UTSA Main Campus<br />
          1 UTSA Circle<br />
          San Antonio, TX 78249<p/>
           <p><a href="https://www.google.com/maps/dir//1+UTSA+Circle,+San+Antonio,+TX/@29.5845373,-98.6193569,17z/data=!3m1!4b1!4m8!4m7!1m0!1m5!1m1!1s0x865c66620458aab3:0x34927311bc2b744!2m2!1d-98.6171682!2d29.5845327">Directions</a> (Google Maps) </p>
           
          <p><a href="http://faculty.utsa.edu/wp-content/uploads/2016/12/UTSA-Faculty-Center-campus-map-8.5x11-2016.jpg">UTSA Faculty Center Map</a></p>
        
       
        <p><a href="http://www.utsa.edu/auxiliary/docs/Park-Trans_Map.pdf">Parking Map</a></p>
        <p>You will purchase and receive your UTSA parking permit on Aug. 13 during your Day O.N.E. Human Resources orientation. If you need parking access before Aug. 13, please contact your department.</p>
<h2>&nbsp;</h2>

       <p>&nbsp;</p>
       <!-- <p align="right">All the information here also is available in <a href="New_Faculty_Orientation_2013.pdf">PDF format</a>.</p>-->
            

         


      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</body>
</html>
