<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>New Faculty Orientation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
<style>
#noborder table {border:none;}
#noborder table th {border:none;}
#noborder table tr {border:none;}
#noborder table td {border:none;}
</style>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
    <!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle"><a href="/home/">Academic Affairs</a></div>

     
      <p><a name="acces-content" class="screen-reader" id="acces-content"></a></p>
        
          <p align="center"><img src="images/newfacultybanner.png" width="950" height="150" alt=""/>
		 <br>
      
              <div id="noborder"> <table width="950" border="1">
          <tr>
            <td width="233"><img src="images/faculty18/abu-lail.jpg" width="200" height="200" alt=""/>
              <h3>Nehal Abu-Lail</h3>
           <strong>Associate Professor<br>
           Chemical Engineering<br />
           College of Engineering
         <br>
			  Ph.D., Worcester Polytechnic Institute</strong><br><hr>
			  My research focuses on how cells interact with surfaces within stressful environments. I am also interested in tissue engineering of articular cartilage using co-cultures of chondrocytes and stem cells under synergistic chemical and mechanical stimuli. Furthermore, I am particularly interested in why women are minorities in engineering.
		    </td>
			  
			  
			  <td width="233"><img src="images/faculty18/alamaniotis3.jpg" width="200" height="200" alt=""/>
			    <h3>Miltos Alamaniotis</h3>
           <strong>Assistant Professor<br>
           Electrical and Computer Engineering<br />
           College of Engineering         <br>
			  Ph.D., Purdue University</strong><br><hr>
		    My research is interdisciplinary and focuses on the use of artificial intelligence, pattern recognition and machine learning in the development of intelligent signal processing and data analytics applied to i) smart electric power systems management and control, and ii) signal detection, processing and sensing for national security.</td>
			  
			  <td width="233"><img src="images/faculty18/awolusi2.jpg" width="200" height="200" alt=""/>
			    <h3>Ibukun Awolusi</h3>
            <strong>Assistant Professor<br>
            Construction Science<br />
            College of Architecture, Construction and Planning
          <br>
			  Ph.D., The University of Alabama<br></strong><hr>
			  My research involves the implementation of proactive safety performance measurement strategies and leveraging technology to collect, analyze, and disseminate safety leading indicator information to improve worker safety performance on construction sites; and the investigation of strategies for developing smart, sustainable, and healthy built environments.
            </td>
           <td width="233"><img src="images/faculty18/banerjee2.jpg" width="200" height="200" alt=""/>
             <h3>Taposh Banerjee</h3>
           <strong>Assistant Professor<br>
           Electrical & Computer Engineering<br />
           College of Engineering         <br>
		     Ph.D., University of Illinois at Urbana-Champaign</strong><br><hr>
			  I work in the areas of Machine Learning and Signal Processing. I am interested in developing new algorithms or techniques in these areas and apply them to different applications. Applications of interest include Data Science, Cyber-physical systems, and Biology.
            </td>
            </tr>

        <tr>
            <td width="233"><img src="images/faculty18/boccio.jpg" width="200" height="200" alt=""/>
              <h3>Cashen Boccio</h3>
            <strong>Assistant Professor<br>
            Criminal Justice<br />
            College of Public Policy
          <br>
			  ABD, Florida State University</strong><br><hr>
          My research focuses on individual-level causes of criminal and delinquent behavior.  Specifically, my research focuses on three main areas of interest: 1) the etiology of antisocial traits, 2) the consequences of antisocial traits across the life course, and 3) the integration of individual-level characteristics into mainstream criminological theories. 
				
          </td>
            
			  <td width="233"><img src="images/faculty18/campos.jpg" width="200" height="200" alt=""/>
			    <h3>Fernando A. Campos</h3>
            <strong>Assistant Professor<br>
            Anthropology<br />
            College of Liberal and Fine Arts
          <br>
			  Ph.D., University of Calgary</strong><br><hr>
          My research addresses evolutionary questions about how organisms respond to changing environments, and how differences between individuals in these responses are linked to important life outcomes, including health, reproduction, and survival. I'm interested not only in the effects of external environmental challenges, including climate change and landscape change, but also in the effects of social adversity.
				  
				  
		  </td>

			  <td width="233"><img src="images/faculty18/carnett.jpg" width="200" height="200" alt=""/>
			    <h3>Amarie Carnett</h3>
            <strong>Assistant Professor<br>
            Educational Psychology<br />
            College of Education and Human Development          <br>
			  Ph.D., Victoria University of Wellington</strong><br><hr>
         My research is focused on developing interventions to help children with autism spectrum disorder (ASD) to develop autonomy within their environments and increase their overall quality of life. My research interest can be classified into two overlapping categories (1) communication interventions, and (2) adaptive functioning.
				  
				  
          </td>
            <td width="233"><img src="images/faculty18/cheever.jpg" width="200" height="200" alt=""/>
              <h3>Kelly Cheever</h3>
            <strong>Assistant Professor<br>
            Kinesiology, Health and Nutrition<br />
            College of Education and Human Development          <br>
			  Ph.D., Temple University</strong><br><hr>
          I study the cumulative effect of contact sport participation on long term outcomes of quality of life such as cervical osteoarthritis, cognitive  decline, and motor control. 
            </td>
           </tr>
			 <tr>
				 <td width="225"><img src="images/faculty18/combs.jpg" width="200" height="200" alt=""/>
				   <h3>Christopher Combs</h3>
            <strong>Assistant Professor<br>
            Mechanical Engineering<br />
            College of Engineering          <br>
			  Ph.D., The University of Texas at Austin</strong><br><hr>
			  My research is focused on high-speed aerodynamics and propulsion. The types of problems my research group will study impact the design of high-speed vehicles, spacecraft undergoing atmospheric re-entry, and next-generation propulsion systems. We will investigate the fundamental physics of these high-speed air flows in UTSA's state-of-the-art wind tunnel. The results will influence vehicle design decisions and help ensure that these systems are efficient and safe.
            </td>

            <td width="225"><img src="images/faculty18/cowden.jpg" width="200" height="200" alt=""/>
              <h3>Tracy Cowden</h3>
			<strong>Department Chair and Professor<br>
         	Music<br />
         	College of Liberal and Fine Arts       	  <br>
			  D.M.A., Eastman School of Music</strong><br><hr>
			  I am a pianist, and my research includes the performance and interpretation of new and old works, primarily in collaboration with singers, instrumentalists, chamber ensembles, or in the context of an orchestra. I also publish and present topics in the pedagogy of music. 
           </td>
            
            <td width="225"><img src="images/faculty18/debbage.jpg" width="200" height="200" alt=""/>
              <h3>Neil A. Debbage</h3>
            <strong>Assistant Professor<br>
            Political Science and Geography<br />
            College of Liberal and Fine Arts          <br>
			  ABD, University of Georgia</strong><br><hr>
			  My research has generally focused on applying GIS and other forms of geospatial analysis to better understand the impacts of urbanization on the natural environment. More specifically, I am interested in urban climatology, urban hydrology, the vulnerability of urban residents to hazards, and sustainable development. My most recent research projects explore the multiple facets of urban flooding vulnerability throughout the Atlanta, Greenville, and Charlotte metropolitan areas.
           </td>
            <td width="233"><img src="images/faculty18/dejulio.jpg" width="200" height="200" alt=""/>
              <h3>Samuel DeJulio</h3>
            <strong>Assistant Professor<br>
            Interdisciplinary Learning and Teaching<br />
            College of Education and Human Development          <br>
			  ABD, The University of Texas at Austin</strong><br><hr>
			  My research focuses primarily on literacy teacher preparation. As a teacher educator, I also do research that focuses on the work with my students in field-based literacy courses and literacy tutoring. Generally, my work is focused on elementary literacy teaching, but I work across content and grade levels.
            </td>
          </tr>
          
       
    <tr>
           
           <td width="233"><img src="images/faculty18/fahmy.jpg" width="200" height="200" alt=""/>
             <h3>Chantal Fahmy</h3>
			<strong>Assistant Professor<br>
				Criminal Justice<br />
				College of Public Policy			  <br>
		     ABD, Arizona State University</strong><br><hr>
			  I focus on prisoner reentry and reintegration after release from prison in a way that takes into consideration many aspects of an ex-prisoner's challenges including physical health, mental health, and social support from family and friends. More broadly, I'm interested in making changes to our prison system and how prisoner reintegration can become a smoother process.
          </td>
            
		<td width="233"><img src="images/faculty18/giambini.jpg" width="200" height="200" alt=""/>
		  <h3>Hugo Giambini</h3>
           <strong>Assistant Professor<br>
           Biomedical Engineering<br />
           College of Engineering         <br>
			  Ph.D., Mayo Graduate School, Mayo Clinic</strong><br><hr>
          My biomedical engineering and orthopedic research addresses the mechanical and biomechanical factors influencing hard and soft tissue integrity and performance, as well as non-invasive tissue assessment and modeling using medical imaging. My research interests lie in using biomechanics and imaging tools to improve predictive methods and better understand pathogenesis of musculoskeletal conditions.
          </td>
		
		
		<td width="233"><img src="images/faculty18/hamilton.jpg" width="200" height="200" alt=""/>
		  <h3>Marcus Hamilton</h3>
			<strong>Associate Professor<br>
         	Anthropology<br />
         	College of Liberal and Fine Arts       	  <br>
			  Ph.D., University of New Mexico</strong><br><hr>
		  I research the evolution of human ecology and social organization. </td>
		
		 <td width="233"><img src="images/faculty18/howard.jpg" width="200" height="200" alt=""/>
		   <h3>Jeffrey Howard</h3>
            <strong>Assistant Professor<br>
           Kinesiology, Health and Nutrition<br />
           College of Education and Human Development         <br>
			  Ph.D., The University of Texas at San Antonio</strong><br><hr>
          I am a traumatic injury epidemiologist and health demographer. Generally, I study how exposures to traumatic injury and chronic stress are related to subsequent risks of morbidity and mortality over the life course. More specifically, I study how these exposures cause changes in psychological health, health behaviors, and increased inflammatory responses.
          </td>
          
    </tr>
    <tr>
		<td width="233"><img src="images/faculty18/huang.jpg" width="200" height="200" alt=""/>
		  <h3>Ying Huang</h3>
            <strong>Assistant Professor<br>
            Demography<br />
            College of Public Policy          <br>
 Ph.D., University at Albany, SUNY</strong><br><hr>
						 My research focuses on the causes and consequences of social stratification, with an emphasis on the inequalities in health, class, and place. Specifically, my research interests include demography, population health, and urban sociology. My current research projects focus on the relationship between health and social stratification over the life course and across generations in the U.S., with an emphasis on child wellbeing.
	      </td>
		
		<td width="233"><img src="images/faculty18/kisselev.jpg" width="200" height="200" alt=""/>
		  <h3>Olesya Kisselev</h3>
            <strong>Assistant Professor<br>
            Bicultural-Bilingual Studies<br />
            College of Education and Human Development          <br>
			  Ph.D., Penn State University</strong><br><hr>
       In my research, I study how children and adults acquire second language proficiency and what material and societal circumstances foster or hinder the process of acquisition.
        </td>
		
		<td width="233"><img src="images/faculty18/kumar.jpg" width="200" height="200" alt=""/>
		  <h3>Amit Kumar</h3>
 <strong>  Assistant Professor<br>
   Civil and Environmental Engineering<br />
   College of Engineering <br>
		    Ph.D., Purdue University</strong><br><hr>
			  My primary area of research is transportation engineering and planning. In particular, my research interest and expertise include connected and autonomous vehicles, electrified transportation systems, crowdsourcing, smart cities, sustainable transportation, intelligent transportation system, megaregions, and relation between transportation and health.
		  </td>
		
		<td width="233"><img src="images/faculty18/lloyd.jpg" width="200" height="200" alt=""/>
		  <h3>S. Andrew Lloyd</h3>
            <strong>Assistant Professor<br>
            Music<br />
            College of Liberal and Fine Arts          <br>
			  D.M.A., University of North Texas</strong><br><hr>
          I am an organist and composer, with compositions having been performed all over the world including the National Cathedral, and the Cathedrale de Notre Dame de Paris.
          </td>
		</tr>
    <tr>
		<td width="233"><img src="images/faculty18/marlow.jpg" width="200" height="200" alt=""/>
		  <h3>Shannon L. Marlow</h3>
            <strong>Assistant Professor<br>
            Management<br />
            College of Business          <br>
			  ABD, Rice University</strong><br><hr>
          My research addresses how to improve performance in teams. I focus on uncovering understudied aspects of teamwork, particularly in the area of team processes and dynamics, that can increase understanding of teamwork. My program of research also addresses other conditions such as leadership within the team and organizational factors, like training, that can lead to greater team success.
           </td>
		
		<td width="233"><img src="images/faculty18/martinez.jpg" width="200" height="200" alt=""/>
		  <h3>Antonio Martinez-Molina</h3>
            <strong>Assistant Professor<br>
            Architecture<br />
            College of Architecture, Construction and Planning          <br>
		    Ph.D., Polytechnic University of Valencia UPV</strong><br><hr>
			  My research generally focuses on the question of how to enhance building occupant satisfaction, health and well-being with sustainable design. Particularly, I am interested in measuring and understanding the actual performance of buildings, whether they are new-build, existing, retrofitted or historic, and optimizing their performance with sustainable measures and innovative technologies.
		  </td>
		
<td width="233"><img src="images/faculty18/park.jpg" width="200" height="200" alt=""/>
  <h3>Yeonjoo Park</h3>
            <strong>Assistant Professor<br>
            Management Science and Statistics<br />
            College of Business          <br>
		    Ph.D., University of Illinois at Urbana-Champaign</strong><br><hr>
			  My research interest is in the analysis of functional data, where information is collected over fine grid points, e.g., every hour or every minute etc. I am especially interested in proposing testing tools for functional data and studying their theoretic back-ups. Also I am working on extending functional data analysis technique to spatial data analysis.
            </td>
		
<td width="233"><img src="images/faculty18/piel.jpg" width="200" height="200" alt=""/>
  <h3>Megan Hayes Piel</h3>
			<strong>Assistant Professor<br>
				Social Work<br />
				College of Public Policy
          <br>
			  Ph.D., Arizona State University</strong><br><hr>
        My research is broadly focused in the area of child welfare, specifically related to improving outcomes for youth in foster care. I am particularly interested in the mental health and well-being of youth aging out of foster care, as well as their access and success in higher education. I also conduct research related to the resilience and cultural competency of foster families. 
         </td>
    </tr>
    <tr>
		<td width="233"><img src="images/faculty18/qutub.jpg" width="200" height="200" alt=""/>
		  <h3>Amina Qutub</h3>
   <strong>Associate Professor<br>
   Biomedical Engineering<br />
   College of Engineering <br>
			  Ph.D., University of California, Berkeley<br></strong><hr>
			  The Qutub Lab develops computational and experimental technologies to uncover how human cells communicate during processes of growth in order to impact human health. In our core projects, we're discovering how functional human neural networks develop from the communication of single neural stem cells, and how protein signaling determines how hematopoietic stem cells become cancerous. Applications of our work include treating blood cancers and understand mechanisms of neurological disorders. 
	      </td>
		
		<td width="233"><img src="images/faculty18/ramirez.jpg" width="200" height="200" alt=""/>
		  <h3>Abelardo Ramirez-Hernandez</h3>
           <strong>Assistant Professor<br>
           Chemical Engineering<br />
           College of Engineering         <br>
			  Ph.D., Universidad Autonoma del Estado de Morelos<br></strong><hr>
           My research is aimed at understanding the self-organization of soft materials, using theory and computational modeling. The goal is to understand the fundamental principles of the collective phenomena leading to controlled assembly and self-organization in soft matter, including biological materials. I am strongly attracted to interdisciplinary research, in particular, at areas where biology, chemistry and physics have common interests.</td>
		
		<td width="233"><img src="images/faculty18/restrepo.jpg" width="200" height="200" alt=""/>
		  <h3>David Restrepo</h3>
   <strong>Assistant Professor<br>
   Mechanical Engineering<br />
   College of Engineering <br>
			  Ph.D., Purdue University<br></strong><hr>
			  My main research interest to advance in the fundamental understanding of nonlinear behavior and failure mechanisms observed in materials and structures, with the aim of designing new materials that exhibit unique properties and functionalities. To achieve this goal, my research approach combines computational simulations, theoretical analysis, fabrication, and experimental testing. Applications of interest are in broad areas including healthcare, defense, robotics, civil infrastructure, and aerospace engineering.
		  </td>
		
		<td width="233"><img src="images/faculty18/richardson.jpg" width="200" height="200" alt=""/>
		  <h3>Nathan Richardson</h3>
			<strong>Department Chair and Professor<br>
         	Modern Languages and Literatures<br />
         	College of Liberal and Fine Arts       	  <br>
		    Ph.D., University of Kansas</strong><br><hr>
			  My principle expertise is in the literature (especially the novel) of 20th and 21st century Spain, having published two books on Spanish film and literature in the second half of the 20th century. Current long-term research interests in Spain's 20th Century as well as the culture and politics of soccer in the Spanish-speaking world.
				   </td>	
				  </tr>
				  <tr>
					  <td width="233"><img src="images/faculty18/rios.jpg" width="200" height="200" alt=""/>
					    <h3>Anthony Rios</strong></h3>
            <strong>Assistant Professor<br>
            Information Systems and Cyber Security<br />
            College of Business          <br>
			  ABD, University of Kentucky</strong><br><hr>
          My research interests are in data mining and machine learning with a specific emphasis on healthcare and biomedical applications. I am also interested in the use of natural language processing for computational social science. With the advent of social networks such as Twitter and Facebook, it is possible to collect large sums of unstructured textual data which can be used to answer traditional social science questions.
		  </td>
				  
		<td width="233"><img src="images/faculty18/romero.jpg" width="200" height="200" alt=""/>
		  <h3>Devon Romero</h3>
            <strong>Assistant Professor<br>
            Counseling<br />
            College of Education and Human Development          <br>
			  Ph.D., University of Alabama</strong><br><hr>
          My research focuses on child externalizing behaviors, early life trauma, wellness/practitioner self-care, and complementary and integrative interventions such as mindfulness and neurofeedback. Evidence based practices, applied interventions, and outcome research are at the core of my research agenda. Currently, I am examining the influence of parenting practices and autonomic functioning on child behavior.
          </td>  
				  <td width="233"><img src="images/faculty18/roy.jpg" width="200" height="200" alt=""/>
				    <h3>Arka Roy</h3>
			<strong>Assistant Professor<br>
         	Management Science and Statistics<br />
         	College of Business       	  <br>
			  Ph.D., Purdue University</strong><br><hr>
           My research is focused on decision-making under uncertainty, using robust optimization and data analytics. The major application area is in cancer radiotherapy. This area is important, because over two-thirds of the annual 1.67 million new cancer cases in the United States undergo radiotherapy.
		  </td>
				  <td width="233"><img src="images/faculty18/sharma.jpg" width="200" height="200" alt=""/>
				    <h3>Bonita Sharma</h3>
            <strong>Assistant Professor<br>
            Social Work<br />
            College of Public Policy          <br>
			  Ph.D., The University of Texas at Arlington</strong><br><hr>
          My research focuses on sustainability issues on gender equity, health, and wellbeing within the human-environment nexus. Specifically, (1) understanding the socio-economic and environmental factors that influence women's health, (2) the role of natural and the built environment and ways they can be leveraged through the local knowledge to enhance human health and empowerment, and (3) involuntary human mobility. 
           </td>
				  </tr>
		<tr>
			<td width="233"><img src="images/faculty18/sinanan.jpg" width="200" height="200" alt=""/>
			  <h3>Kerry Sinanan</h3>
			<strong>Assistant Professor<br>
         	English<br />
         	College of Liberal and Fine Arts       	  <br>
			  Ph.D., Trinity College, Dublin</strong><br><hr>
			  I specialise in the literature of slavery in the Black Atlantic during the 18th and 19th centuries. I bring to the field a critical analysis of the ways in which slave masters attempted to justify their actions and ultimately failed to do so. I emphasise the writings of slaves and black British people of the era as formative and not merely marginal paying particular attention to the ways in which slavery is foundational to the major genre of the novel.
			  </td>
					<td width="233"><img src="images/faculty18/smith.jpg" width="200" height="200" alt=""/>
					  <h3>Jennifer Smith</h3>
            <strong>Assistant Professor<br>
            Environmental Science and Ecology<br />
            College of Sciences
          <br>
			  Ph.D., University of Birmingham</strong><br><hr>
          I am a wildlife ecologist and my research aims to evaluate the effects of global change on the spatial ecology, demography, and behavior of birds and, on occasion, other taxa. Overall, my objective is to conduct research that informs policy and promotes sustainable land uses that consider the conservation of wildlife and human well-being.
            
            </td> 
					 <td width="233"><img src="images/faculty18/swan.jpg" width="200" height="200" alt=""/>
					   <h3>Alicia Swan</h3>
			<strong>Assistant Professor<br>
         	Psychology<br />
         	College of Liberal and Fine Arts       	  <br>
			  Ph.D., Southern Illinois University</strong><br><hr>
			  My research focuses on the relationship between the brain and the behavioral phenomena it influences. My current work focuses on health outcomes in Post-9/11 Veterans, particularly those associated with traumatic brain injury, sensory dysfunction, cognitive disorders, and complex comorbidity patterns.
					 </td>
					 
                     <td width="233"><img src="images/faculty18/tekin.jpg" width="200" height="200" alt=""/>
                       <h3>Serife Tekin</h3>
            <strong>Assistant Professor<br>
            Philosophy and Classics<br />
            College of Liberal and Fine Arts          <br>
			  Ph.D., York University, Canada</strong><br><hr>
			  My work is in philosophy of science and bioethics. I use philosophical tools to expand psychiatric knowledge which can facilitate effective and ethical treatments that will help individuals with mental disorders flourish. 
			  </td>
		</tr>
		
            <tr>
				<td width="233"><img src="images/faculty18/testa.jpg" width="200" height="200" alt=""/>
				  <h3>Alexander Testa</h3>
			<strong>Assistant Professor<br>
				Criminal Justice<br />
				College of Public Policy			  <br>
	   ABD, University of Maryland, College Park</strong><br><hr>
						 My primary area of research is understanding the consequences of prior incarceration on later life health outcomes. My secondary area of examines disparities in the criminal justice system and how criminal justice actors make decisions. My third area of research investigates the underlying causes of differences in homicide rates across countries and overtime. 
		    </td>
								 			  
			  <td width="233"><img src="images/faculty18/todic.jpg" width="200" height="200" alt=""/>
			    <h3>Jelena Todic</h3>
            <strong>Assistant Professor<br>
            Social Work<br />
            College of Public Policy          <br>
			  Ph.D., The University of Texas at Austin</strong><br><hr>
          My research broadly focuses on systems-level interventions which aim to eliminate health inequities by targeting social determinants of health and their fundamental drivers. More specifically, I am interested in large organizational structures, such as schools or healthcare organizations, as potential hosts of such solutions.
           </td>
		<td width="233"><img src="images/faculty18/umeda.jpg" width="200" height="200" alt=""/>
		  <h3>Masataka Umeda</h3>
            <strong>Assistant Professor<br>
            Kinesiology, Health and Nutrition<br />
            College of Education and Human Development          <br>
			  Ph.D., University of Wisconsin-Madison</strong><br><hr>
          My research examines the capacity to inhibit pain within the central nervous system using laboratory tests, and the influence of physical activity in modifying the risks. In the past, I studied the central pain inhibition in chronic pain patients, individuals of racial/ethnic minorities, as well as healthy individuals. I plan to develop this line of research further at UTSA via active collaborations.
          </td>
            
		<td width="225"><img src="images/faculty18/villarreal.jpg" width="200" height="200" alt=""/>
		  <h3>Mike Villarreal</h3>
            <strong>Assistant Professor<br>
				Educational Leadership and Policy Studies<br />
				College of Education and Human Development			  <br>
		    Ph.D., The University of Texas at Austin</strong><br><hr>
			  I perform quantitative evaluations of education policies and programs on education and workforce outcomes of students. My research agenda also includes the study of the education policymaking process at the state and local levels and focuses on the policy impacts affecting economically disadvantaged students.
		  </td>
          
    </tr>

                 <tr>
					 <td width="233"><img src="images/faculty18/wang.jpg" width="200" height="200" alt=""/>
					   <h3>Dian Wang</h3>
            <strong>Assistant Professor<br>
            Marketing<br />
            College of Business          <br>
			  ABD, Texas A&M University</strong><br><hr>
          My research interest lies in the area of behavioral decision theory (BDT). My dissertation research investigates the issue of how to enhance individual creativity. In addition, I am involved in research that examines cultural differences in price fairness perceptions and decision making for others.
           </td>
 
					 <td width="233"><img src="images/faculty18/webster.jpg" width="200" height="200" alt=""/>
					   <h3>Crystal Webster</h3>
			<strong>Assistant Professor<br>
         	History<br />
         	College of Liberal and Fine Arts       	  <br>
			  Ph.D., University of Massachusetts, Amherst</strong><br><hr>
          My research interests include African American history, gender studies, and childhood studies.
           </td>
            
					 <td width="233"><img src="images/faculty18/wikberg.jpg" width="200" height="200" alt=""/>
					   <h3>Eva C. Wikberg</h3>
            <strong>Assistant Professor<br>
            Anthropology<br />
            College of Liberal and Fine Arts          <br>
			  Ph.D., University of Calgary</strong><br><hr>
			  I use a combination of  molecular genetic and field based data collection methods to investigate the evolution of dispersal and mating patterns. I am particularly interested in how these patterns affect social and genetic structure, and ultimately fitness and population viability. My current research focuses on primate populations in Ghana and Costa Rica with the goal of improving conservation outlooks for these and other threatened primates.
					 </td>
					  
				<td width="225"><img src="images/faculty18/wright.jpg" width="200" height="200" alt=""/>
				  <h3>Jessica Wright</h3>
			<strong>Assistant Professor<br>
         	Philosophy and Classics<br />
         	College of Liberal and Fine Arts       	  <br>
			  Ph.D., Princeton University</strong><br><hr>
			  I am a historian of the body. I write about representations of the brain and mental experience in philosophical, medical, and theological texts. I am trained in the literature of Greek and Roman antiquity, although I conduct my research in conversation with contemporary philosophy and history of science and medicine. 
			  </td>
		</tr>
		<tr>
		<td width="233"><img src="images/faculty18/wu.jpg" width="200" height="200" alt=""/>
		  <h3>Wenbo Wu</h3>
			<strong>Assistant Professor<br>
         	Management Science and Statistics<br />
         	College of Business
       	  <br>
			  Ph.D., University of Georgia</strong><br><hr>
          I am a statistician who dedicates to develop new methodologies to analyze complex datasets (Big Data). My research directions in statistics include high dimensional data, sufficient dimension reduction, feature selection, and causal inference. I am also interested in applying statistical methods to various applications such as finance, marketing, biology, etc.
          </td>
			
			<td width="233"><img src="images/faculty18/zhang.jpg" width="200" height="200" alt=""/>
			  <h3>Tianou Zhang</h3>
            <strong>Assistant Professor<br>
				Kinesiology, Health and Nutrition<br />
				College of Education and Human Development
			  <br>
			  Ph.D., University of Minnesota-Twin Cities</strong><br><hr>
			  My research interest is sports and exercise nutrition. More specifically, I am very interested in exploring the antioxidant and anti-inflammatory effects of phytochemicals (plant extracted compounds) supplementation in sports-related inflammation and inflammatory conditions under chronic diseases, such as atherosclerosis and obesity.</td>

		</tr>
		</table></div>
	
	  <hr>
	
<div id="noborder"> <table width="950" border="1">
    <tr>
      <td><h2>Not pictured</h2></td></tr>
     <tr>
		 <td width="233"><h3>Claudia G. Interiano</h3> (begins Spring 2019)
            <p><strong>Assistant Professor<br>
            Counseling<br />
            College of Education and Human Development            <br>
			  ABD, University of North Carolina-Charlotte</strong><br><hr>
          My research focuses primarily on multicultural interventions to effectively treat trauma or cultural distress among minority groups. My research has also engaged in community-based research exploring cultural and social capital as systemic protective factors that aid refugees during their resettlement in the U.S. 
          </td>
		 
		 <td width="233"><h3>Kamal Al Nasr</h3>(begins Jan. 2019)<br>
            <strong>Associate Professor<br>
            Computer Science<br />
            College of Sciences
          <br>
			  Ph.D., Old Dominion University</strong><br></td>
           
		 <td width="233"><h3>Andrea L. Ruiz</h3>
            <strong>Assistant Professor<br>
            Sociology<br />
            College of Liberal and Fine Arts
          <br>
			  Ph.D., Penn State University</strong><br><hr>
          My work examines the long-term effects of childhood abuse on adult health over the life-span.
          </td>

           <td width="233"><h3>Hui Zhang</h3> (begins Jan. 2019)<br />
            <strong>Associate Professor<br>
            Computer Science<br />
            College of Sciences
          <br>
			  Ph.D., Indiana University</strong><br></td>
              
    </tr>
   
        </table></div>
        <p>
        <br>
	  <p>See something that needs to be changed? Please email us at <a href="mailto:vpafs@utsa.edu">vpafs@utsa.edu</a> with any corrections.</p>
        
       <!-- <p align="right">All the information here also is available in <a href="New_Faculty_Orientation_2013.pdf">PDF format</a>.</p>-->
            

         


    </div>

  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// --><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5734c656db3aa025"></script>

</body>
</html>
