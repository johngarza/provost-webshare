<ul>
  <li><a class="home" href="http://provost.utsa.edu" title="Provost Home Page">Academic Affairs</a></li>
  <li><a href="../../VPAFS/dm/index.asp" >Digital Measures Home</a></li>
  <li><a href="/VPAFS/dm/dates.asp" >Important Dates</a></li>
  <li><a href="../../VPAFS/dm/new-in-dm.asp" >What's New in DM</a></li>
  <li><a href="/VPAFS/dm/faculty-resources.asp" >Help for Faculty</a></li>
  <li><a href="/VPAFS/dm/workflow.asp">Workflow</a></li>
  <li><a href="/VPAFS/dm/vita.asp" >Vita</a></li>
  <!--
  <li><a href="../../VPAFS/dm/annual-report-faculty.asp" >Annual Report<br />
    (Faculty)</a></li>
  -->
  <li><a href="/VPAFS/dm/workload-reporting.asp" >Faculty Role in Workload Reporting</a></li>
  <li><a href="/VPAFS/dm/college-resources.asp" >Help for Departments and Colleges</a></li>
  <li><a href="../../VPAFS/dm/faculty-workload.asp" >Faculty Workload</a></li>
  <!--
    <li><a href="../../VPAFS/dm/annual-report-colleges.asp" >Annual Report<br />
      (Departments and Colleges)</a></li>
  -->
  <!--
    <li><a href="../../VPAFS/dm/faculty-agreements.asp">Faculty Workload Distribution Agreements</a></li>
    -->
  <li><a href="../../VPAFS/dm/credentialing.asp" >Faculty Credentialing</a></li>
  <li><a href="../../VPAFS/dm/archives.asp" >DM Archives</a></li>
  <!--
    <li><a href="/VPAFS/dm/manuals.asp" >All DM User's Manuals and Guides</a></li>
  -->
  <li><a href="/VPAFS/dm/contact.asp" >Contact Us</a></li>
</ul>
