<ul>
	<li><a href="http://provost.utsa.edu/VPAFS/PromotionTenure/">Promotion and Tenure
Application Guidelines Home</a></li>
	
	<li><A href="/VPAFS/PromotionTenure/overview.asp">Overview of Process</A></li>
	
	<li><A href="/VPAFS/PromotionTenure/packet.asp">Preparation of the Promotion Packet</A></li>
	
	<li><A href="/VPAFS/PromotionTenure/external.asp">Solicitation of External Reviews</A></li>
	
	<li><A href="/VPAFS/PromotionTenure/criteria.asp">Criteria for Promotion and   Tenure</A></li>
	
	<li><A href="/VPAFS/PromotionTenure/process.asp">Review Process</A></li>
	
	<li  style="margin:0 0 1.5em 0;"><A href="/VPAFS/PromotionTenure/P&amp;T&ndash;CoverSheet.doc">Cover Sheet and Checklist</A></li>
	</ul>
    <hr  style="height:0.2em;"/>
    <ul>
	
  <li><A href="/VPAFS/PromotionTenure/Help_Support/index.asp">Help/Support</A>
    <dd><span class="style1"><span class="style2"><a href="/VPAFS/PromotionTenure/Help_Support/pdf/PT_User_Guide.pdf"> - P&T User Guide (PDF)</a></span></span></dd>
    <dd><span class="style1"><span class="style2"><a href="/VPAFS/PromotionTenure/Help_Support/scanning_services.asp"> - Scanning Services</a></span></span></dd>
    <dd><span class="style1"><a href="/VPAFS/PromotionTenure/Help_Support/rowdyspace_support.asp"> - RowdySpace Support</a></span></dd>
    <dd><span class="style1"><a href="/VPAFS/PromotionTenure/Help_Support/library_doc_trans_form.asp"> - Library Document Transmittal Form</a></span></dd>
    <dd><span class="style1"><a href="/VPAFS/PromotionTenure/Help_Support/Acrobat9_support.asp"> - Acrobat Professional Support</a></span></dd>
  </li>
</ul>