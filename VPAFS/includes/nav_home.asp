

<ul>
	<li><a class="home" href="http://provost.utsa.edu" title="Provost Home Page"><strong>Provost Home</strong></a></li>
  <li><strong><a href="/VPAFS/index.asp" title="Provost Home Page">Academic and Faculty Support Home</a></strong></li>
   
<li><strong><a href="/VPAFS/ombudsperson.asp">Academic Affairs Ombudsperson</a>
    	
</strong></li>
<hr />
<li><strong>Faculty Support
    	</strong>
  <ul>
   	  <li><a href="http://provost.utsa.edu/vpafs/dm/index.asp" target="_blank">- Digital Measures</a></li>
   	  <li><a href="http://www.utsa.edu/facultyawards/" target="_blank">- Faculty Awards</a></li>
   	 <li><a href="http://faculty.utsa.edu/" target="_blank">- Faculty Center</a></li>
    <li><a href="http://provost.utsa.edu/home/Evaluation/" target="_blank">- Faculty Review</a></li>
    
    <li><a href="http://provost.utsa.edu/vpafs/newfaculty/index.asp" target="_blank">- New Faculty</a></li>
    <li><a href="http://provost.utsa.edu/vpafs/pt-forums.asp" target="_blank"> - Promotion &amp; Tenure Forums</a></li>
        
    <li><a href="http://provost.utsa.edu/rfa/" target="_blank">- Retired Faculty Association</a></li>
   	<li><a href="http://teaching.utsa.edu" target="_blank">- Teaching & Learning Services</a></li>
</ul>
</li>
<hr />

<li><strong>Administrative Support
    	</strong>
  <ul>
   	  <li><a href="http://provost.utsa.edu/departmentchairs/"> - Department Chairs Council</a></li>
      <li><a href="/VPAFS/compensation.asp"> - Faculty Contracts</a></li>
<li><a href="/VPAFS/facultygrievance.asp"> - Faculty Grievance Process</a></li>
<li><a href="/VPAFS/facultyrecruitment.asp"> - Faculty Recruitment</a></li>
   	</ul>
</li>
<li><strong><a href="/VPAFS/information.asp">Information Resources</a></strong></li>
<li><strong><a href="/VPAFS/personnel.asp">Executives and Staff</a></strong></li>
</ul>
<p><strong>Contact Us:</strong><br />
(210) 458-2700<br />
vpafs@utsa.edu</p>