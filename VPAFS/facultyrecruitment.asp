<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Recruitment and Diversity | VPAFS | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		  <h1>Faculty Recruitment and Diversity</h1>

<p>UTSA is committed to identifying, recruiting, and retaining highly qualified, talented, and diverse faculty for all academic areas. The faculty recruitment process is designed to be a collaborative process resulting in the best qualified candidate for a position. Failure to follow the processes and procedures outlined in the Faculty Recruitment Manuals (available for download below) may result in the closing of a search.</p>

<p>Academic Affairs coordinates Faculty Recruitment Training in collaboration with Equal Opportunity Services and Human Resources. <strong>Faculty members who are participating on a search committee for the first time or those who have not attended training within the previous two years must complete Faculty Recruitment Training</strong>.</p>

<p>Starting in Fall 2018, faculty recruitment training <strong>for search committee members</strong> includes the following two components:</p>
	<ul>
		<li>(1) a <strong>seven-part animated video series on implicit bias</strong> (roughly 30 minutes)</li>
		<li>(2) a <strong>self-guided online training module</strong> that primarily focuses on UTSA's faculty search process (roughly 30 minutes)</li>
		</ul>

<p>In addition to the two online training components, <strong>Search Committee Chairs and Diversity Access & Equity Advocates must also attend an in-person session</strong> that will focus more specifically on understanding implicit bias in the faculty search process. Search committee members, department chairs and their staff are welcome to attend.</p>

		<br>

			<h1>Online Training</h1>

<table style="background-color: aliceblue" width="700" border="0">
  <tbody>
    <tr>
      <th align="left" width="350"><h2>Part 1: Implicit Bias Video Series</h2>
			<p>Institutions increasingly understand that implicit bias impacts key university processes, including faculty search decisions. The following videos were created by UCLA to enhance employee awareness and decision-making; we are incorporating them here as part of our online search committee training. Be aware of these issues as you work to provide all applicants with full and fair consideration during the search process.</p>
			<ul>
				<li><a href="https://www.youtube.com/watch?v=R0hWjjfDyCo&feature=youtu.be" target="_blank">Preface: Biases and Heuristics</a> (5:14)</li>
				<li><a href="https://www.youtube.com/watch?v=OQGIgohunVw&feature=youtu.be" target="_blank">Lesson 1: Schemas</a> (3:12)</li>
				<li><a href="https://www.youtube.com/watch?v=7FgqGAXvLB8&feature=youtu.be" target="_blank">Lesson 2: Attitudes and Stereotypes</a> (4:13)</li>
				<li><a href="https://www.youtube.com/watch?v=8SIb97tZSpI&feature=youtu.be" target="_blank">Lesson 3: Real World Consequences</a> (3:45)</li>
				<li><a href="https://www.youtube.com/watch?v=5S7Je6kbGDY&feature=youtu.be" target="_blank">Lesson 4: Explicit v. Implicit Bias</a> (2:49)</li>
				<li><a href="https://www.youtube.com/watch?v=hr9xAcWv790&feature=youtu.be" target="_blank">Lesson 5: The IAT</a> (5:14)</li>
				<li><a href="https://www.youtube.com/watch?v=RIOGenWu_iA&feature=youtu.be" target="_blank">Lesson 6: Countermeasures</a> (5:23)</li>
				</ul>
			</th>
      <th align="left" width="350"><h2>Part 2: Recruitment Training Module</h2>
		  <p>This self-guided training provides a overview of UTSA's faculty recruitment process. It offers best practices and resources to assist with each step in the search process, from forming the committee to finalizing the offer. This module is designed to be an evergreen resource that can be accessed at any time.</p>
		<p align="center"><a href="recruitmenttraining/index.html" target="_blank">CLICK HERE TO OPEN THE TRAINING MODULE</a></p>
			  <p>All faculty members <strong>must</strong> view the full Implicit Bias Video Series and go through the online recruitment training module. After the quiz at the end of the online module, you will be prompted to self-certify that you have completed both components.</p>
		</th>
    </tr>
  </tbody>
</table>
		<br>

<!--
			<h1>In-Person Training</h1>
			<p><strong>Required for Search Committee Chairs and Diversity Access & Equity Advocates.</strong> Open to all search committee members, department chairs and their staff.</p>

			<p><strong>2018 Sessions:</strong></p>
			<p>September 5  |  9 - 11 a.m. | Faculty Center Assembly Room (JPL 4.04.22), Main Campus<br>
			September 6  |  9 - 11 a.m. | Faculty Center Small Conference Room (JPL 4.04.12D), Main Campus<br>
			September 11  |  2 - 4 p.m.  |  El Paseo Room A, Durango Building (Downtown Campus)<br>
			September 18  | 1 - 3 p.m. | Faculty Center Assembly Room (JPL 4.04.22), Main Campus<br>
			October 4  |  9 - 11 a.m. | Faculty Center Small Conference Room (JPL 4.04.12D), Main Campus<br>
			October 5  |  1 - 3 p.m. | Faculty Center Large Conference Room (JPL 4.04.12C), Main Campus<br>
			October 9  |  9 - 11 a.m. | Faculty Center Large Conference Room (JPL 4.04.12C), Main Campus<br>
			October 23  |  1 - 3 p.m. | Faculty Center Large Conference Room (JPL 4.04.12C), Main Campus<br>
				October 30  |  9 - 11 a.m. | Faculty Center Large Conference Room (JPL 4.04.12C), Main Campus<br>
				November 1  |  9 - 11 a.m. | Faculty Center Large Conference Room (JPL 4.04.12C), Main Campus<br>

			<p><strong><a href="https://utsafacultycenter.wufoo.com/forms/s1eiae07080w8xc/" target="_blank">** SIGN UP HERE **</a></strong></p>

			<p>In addition to this required training offered by Academic Affairs, Human Resources offers a separate training on using STARS to hire tenure-track faculty. Visit the <a href="https://mytraining.utsa.edu/classroom/" target="_blank">HR Training & Development website</a> to find and enroll in these classes.</p>
		-->
<br>
<h1>Additional Resources</h1>
<ul>
<li><a href="documents/Faculty-Recruitment-Manual.pdf">Tenured/Tenure-Track Faculty Recruitment Manual</a></li>
<li><a href="documents/NTT-Faculty-Recruitment-Manual.pdf">Non-Tenure-Track Faculty Recruitment Manual</a></li>
<li><a href="documents/Special-Opportunity-Hires.pdf">Special Opportunity Hires</a></li>
	<li><a href="documents/ntt-faculty-handbook.pdf" target="_blank">New Faculty Handbook</a> - onboarding resource geared toward new NTT faculty</li>
	<li><a href="forms/recruitment/index.asp">Faculty Recruitment Forms</a></li>
</ul>

<p><strong>Resources for Recruiting Diverse Faculty</strong></p>
   <ul>
	   <li><a href="documents/diversity-advocate-resources.pdf">Diversity Advocate responsibilities and resources</a></li>
   <li><a href="documents/diversity-tips.pdf">Tips on creating an inclusive recruitment process</a></li>
   <li><a href="documents/diversity-advertising.pdf">Resources for advertising to diverse candidates</a></li>
   <li><a href="documents/diversity-articles.pdf">Articles and other online resources on recruiting, hiring, and retaining diverse faculty</a></li>
   <li><a href="documents/diversity-utsa-faculty-stats.pdf">Trend data on UTSA T/TT faculty by ethnicity and gender (2011-2016)</a> (Additional information is available in the <a href="http://www.utsa.edu/ir/pub/factbook/2015/FacultyStaffAll.pdf" target="_blank">UTSA Fact Book</a>)</li>
	   <li><a href="documents/using-nsf-data.pdf">Look up recent graduates in a field by race, ethnicity and gender</a> (NSF Survey of Earned Doctorates)</li>
   </ul>



<h2>Contacts</h2>
<p><strong>Academic Affairs</strong><br />
<em>for questions about the faculty recruitment process, timeline, offer letter, required forms, and other general information</em><br />
Attn: Kelly Garza<br />
210.458.5207<br />
	<a href="mailto:vpafs@utsa.edu">kelly.garza@utsa.edu</a></p>

<p><strong>Equal Opportunity Services</strong><br />
<em>for questions about diversity</em><br />
	Attn: Suzanne Patrick, Director of EOS<br>
210.458.4120<br />
<a href="http://www.utsa.edu/eos/" target="_blank">utsa.edu/eos</a></p>

<p><strong>Human Resources</strong><br />
<em>for questions about STARS</em><br />
Attn: Ronald Fosmire, HR advisor<br />
210.458.4256<br />
	<a href="http://www.utsa.edu/hr/" target="_blank">utsa.edu/hr</a></p>




        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
