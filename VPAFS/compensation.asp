<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->

<head>
	<link rel="shortcut icon" href="/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
	<meta name="copyright" content="The University of Texas at San Antonio" />
	<!-- InstanceBeginEditable name="meta" -->
	<meta name="Description" content="The University of Texas at San Antonio" />
	<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
	<!-- InstanceEndEditable -->
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>Faculty Compensation | VPAFS | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
	<!-- InstanceEndEditable -->
	<link rel="stylesheet" type="text/css" href="/css/master.css" />
	<link rel="stylesheet" type="text/css" href="/css/template.css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
	<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
	<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />
	<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
	<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
	<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
	<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="branding-blue">
		<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
		<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
	</div><!-- end branding -->

	<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="content-white">
		<div id="content" class="clearfix">

			<!-- InstanceBeginEditable name="content-title" -->
			<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
			<!-- InstanceEndEditable -->

			<div id="col-navigation">
				<div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
					<!-- InstanceBeginEditable name="nav" -->
					<!--#include virtual="/inc/nav/home-sub-nav.html" -->
					<!-- InstanceEndEditable -->
				</div>
			</div>

			<div id="col-main"><a name="acces-content" class="screen-reader"></a>
				<!-- InstanceBeginEditable name="content" -->
				<h1>Faculty Contracts and Compensation</h1>

				<p>Academic Affairs provides oversight and training on a variety of processes concerning faculty contracts and compensation. Our staff review faculty contracts for the Fall, Spring, and Summer semesters, working with the college administrative
					staff to ensure that the Faculty Contract Forms are completed accurately and that the guidelines are followed. All Faculty Contract Forms must be approved by Jenny De Los Santos in Academic Affairs prior to submitting to HR.</p>

				<p>Supplemental payments to faculty in addition to their nine-month or twelve-month salary are also reviewed by our office in accordance with rules, regulations and policies of The University of Texas System. Faculty can receive compensation
					above their nine-month salaray during the academic year for performing additional duties. Supplemental payment for services must be justified through an approved Request for Salary Supplementation form and must not exceed the cap established by
					the Provost.

					<p>As stated in <a href="http://www.utsa.edu/hop/chapter2/2-23.html" target="_blank">HOP 2.23 Faculty Compensation for Additional Dutes and Salary Adjustments</a>, ajustments to faculty compensation can occur in three major ways:

						<p>1. Temporary compensation to a faculty member’s Nine-Month pay for work-related responsibilities over a specific time, which could include:<br />
							<ul>
								<li>Administrative Services</li>
								<li>Executive Education</li>
								<li>Extended Education</li>
								<li>Professional Services</li>
							</ul>

							<p>2. Summer compensation <br />

								<p>3. Permanent adjustments to a faculty member’s Nine-Month Salary, which could include:<br />
									<ul>
										<li>Merit Awards</li>
										<li>Equity Adjustments</li>
										<li>Counter Offers</li>
										<li>Performance Awards</li>
									</ul>

									<p>Please direct all questions and documents pertaining to faculty contracts and supplemental pay to <a href="mailto:vpafs@utsa.edu">vpafs@utsa.edu</a> or 458-4110.</p>

									<h2>Resources</h2>
									<ul>
										<li><a href="http://www.utsa.edu/hop/chapter2/2-23.html" target="_blank">HOP 2.23 Faculty Compensation for Additional Dutes and Salary Adjustments</a></li>
										<li>Faculty Contract Request Form is available via the Interim Workflow Solutions site (accessible via <a href="http://my.utsa.edu/" target="_blank">myUTSA</a>)</li>
										<li>Request for Salary Supplementation form is available to <a href="https://www.utsa.edu/hr/forms.html" target="_blank">download from the Human Resources website</a>.</li>
									</ul>

									<a id="summer"></a>

									<h2>Summer Faculty Contract Processing Resources (Updated for Summer 2018)</h2>
									<ul>
										<li><a href="documents/fac-summer-contracts-training-presentation.pdf">Summer Appointments and Contracts Training Presentation</a> (Presentation slides) - created by HR</li>
										<li><a href="https://utsacloud-my.sharepoint.com/:f:/g/personal/samantha_hernandez_utsa_edu/Eub2NSyVed5Kodym-s8jG2wBqwblB30ayD1yUq3d-mgliQ?e=nuuz6y">Summer Appointments and Contracts Training Presentation</a> (Presentation slides with
											voice-over) - created by HR</li>
										<ul>
											<!--<li><a href="documents/fac-summer-contracts-examples.pdf">Training Handout - Examples</a></li>-->
											<li><a href="documents/fac-summer-contracts-timeline.pdf">Training Handout - Timeline</a></li>
											<li><a href="documents/fac-summer-contracts-handout-yellow.pdf">Training Handout - Faculty Member with Research Assignments Only (Yellow)</a></li>
											<li><a href="documents/fac-summer-contracts-handout-green.pdf">Training Handout - Faculty Member with Teaching Assignments Only (Green)</a></li>
											<li><a href="documents/fac-summer-contracts-handout-blue.pdf">Training Handout - Faculty Member with Teaching and Research Assignments (Blue)</a></li>
											<li><a href="documents/fac-summer-contracts-handout-pink.pdf">Training Handout - SAMS Summer Assignments Master Spreadsheet (Pink)</a></li>
										</ul>

										<!--<li><a href="documents/fac-summer-contracts-checklist.pdf">Checklist for Summer Salary Contracts</a></li>-->
										<li><a href="documents/fac-summer-contracts-planning-form.docx">Faculty Planning Form Summer 2018</a> (DOCX)</li>
										<li><a href="documents/fac-summer-contracts-SAMS.xlsm">SAMS-Summer Assignment Master Spreadsheet</a> (XLSM)</li>
									</ul>
									<br/>
									<!--
									<h3>Summer Contracts Timeline</h3>
									<ul>
										<li>April 13: Training provided</li>
										<li>May 11: SAMS and Contracts due to VPAFS</li>
										<li>May 18: Faculty Contract Forms due to HR</li>
										<li>June 15: DBTs due to Budget Office</li>
									</ul>
								-->
									<table width="600" border="1" cellpadding="10">
										<caption>
											<h3>Summer Contract Dates</h3>
										</caption>
										<tbody>
											<tr>
												<th scope="col">Summer Term 2018</th>
												<th scope="col">Classes Begin</th>
												<th scope="col">Grades Due</th>
												<th scope="col">Pay Period</th>
											</tr>
											<tr>
												<th scope="row">F (First Five-Week Term)</th>
												<td>5/30</td>
												<td>7/10</td>
												<td>6/1 - 7/15</td>
											</tr>
											<tr>
												<th scope="row">J (First Four-Week Term)</th>
												<td>6/11</td>
												<td>7/13</td>
												<td>6/1 - 7/15</td>
											</tr>
											<tr>
												<th scope="row">L (Second Four-Week Term)</th>
												<td>7/11</td>
												<td>8/13</td>
												<td>7/16 - 8/31</td>
											</tr>
											<tr>
												<th scope="row">S (Second Five-Week Term)</th>
												<td>7/9</td>
												<td>8/17</td>
												<td>7/16 - 8/31</td>
											</tr>
											<tr>
												<th scope="row">8 (Eight-Week Term)</th>
												<td>6/11</td>
												<td>8/13</td>
												<td>6/1 - 8/31</td>
											</tr>
											<tr>
												<th scope="row">T (Ten-Week Term)</th>
												<td>5/30</td>
												<td>8/17</td>
												<td>6/1 - 8/31</td>
											</tr>
										</tbody>
									</table>




									<!-- InstanceEndEditable -->
			</div><!-- end col-main-->
		</div>
	</div>
	<!--end contentWhite-->


	<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<!-- InstanceBeginEditable name="userFooter" -->
	<!--#include virtual="/inc/footer/footer-about.html" -->
	<!-- InstanceEndEditable -->

	<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="footer-blue">
		<div id="footer">
			<!--#include virtual="/inc/master/footerWrap.html" -->
		</div>
	</div>
	<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd -->

</html>
