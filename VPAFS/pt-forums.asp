<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Promotion and Tenure Forums | VPAFS | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		  <h1>Promotion and Tenure Forums by College</h1>

<p>Academic and Faculty Support coordinates forums and other opportunities for UTSA's faculty members to achieve greater clarity about the promotion and tenure criteria and process. The forums are hosted by college and offer faculty an opportunity to hear from and ask questions of the Provost, Dean, and fellow colleagues in order to ensure that they understand the expectations for promotion and tenure at UTSA and within their disciplines.</p>

<p>For more information, please contact 210-458-2700.

<h2>2018-2019 Promotion and Tenure Forums by College</h2>
<p>
<p><b>P&T Forum: College of Sciences</b><br />
Oct. 25, 2018<br />
1:00 - 3:00 p.m.<br />
Assembly Room (JPL 4.04.22)</p>

<p><b>P&T Forum: College of Education and Human Development</b><br />
Nov. 8, 2018<br />
1:00 - 3:00 p.m.<br />
Bexar Room (HSU 1.102)</p>

<p><b>P&T Forum: College of Engineering</b><br />
Jan. 31, 2019<br />
1:00 - 3:00 p.m.<br />
BSE 2.102</p>

<p><b>P&T Forum: College of Public Policy</b><br />
Feb. 21, 2019<br />
1:00 - 3:00 p.m.<br />
BV 1.322</p>

<p><b>P&T Forum: College of Architecture, Construction and Planning</b><br />
Mar. 7, 2019<br />
1:00 - 3:00 p.m.<br />
BV 1.322</p>

<p><b>P&T Forum: College of Liberal and Fine Arts</b><br />
Mar. 28, 2018<br />
1:00 - 3:00 p.m.<br />
Location TBA</p>
			
		<p><b>P&T Forum: College of Business</b><br />
Date TBD


        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
