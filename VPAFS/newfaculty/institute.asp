<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA New Faculty Institute | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">    <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            	<!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		 
				  <img src="images/nfi-logo-web.png" width="700" height="191" alt="UTSA New Faculty Institute"/> <br>
                  <br />
                 
			
       <p>The UTSA New Faculty Institute is a six session workshop series offered to all new tenure-track and tenured faculty. It is required of all new faculty hired at the assistant professor level. The objective of the Institute is to provide an introduction to UTSA’s teaching and research culture; identify people and services to support success; enhance skills related to grant seeking and writing, as well as instruction and pedagogy; share information about institutional resources and best practices; and promote collaboration and community in the first year.</p>
       <hr>
			<h1>Research Outcomes</h1>
			<ul>
				<li>Identify funding opportunities utilizing various tools</li>
				<li>Understand the priorities of funding agencies and cultivate relationships with program officers</li>
				<li>Apply effective project management strategies to the proposal development process</li>
				<li>Write grant proposals that are responsive to a call and meet reviewer expectations</li>
				<li>Understand and experience the proposal evaluation process</li> 
				<li>Prepare a draft proposal based on concepts learned through the program</li>

			</ul>
       

<h1>Teaching and Learning Outcomes</h1>
<ul>
<li>Understand the diverse UTSA student body including military, minority, and first-generation student populations</li>
	<li>Explore teaching strategies to improve student learning</li>
	<li>Identify support methods to help students achieve academic success</li>
	<li>Support inclusion and engagement in the classroom</li>
	<li>Explore wide-ranging assessment strategies to improve learning outcomes</li>
	<li>Develop cross-disciplinary dialogue around teaching and learning</li>
 </ul>
			<hr>
			<h1>Workshops</h1>
			<p>Faculty will be required to attend a total of six workshops and a closing luncheon. <strong>Participants will choose one of three tracks based on their teaching schedules and campus location and be responsible for attending the sessions within their track.</strong> See workshop schedule below.</p>
       
        
			<h3>Before You Write: Positioning Yourself to be Competitive</h3> (RESEARCH)
				<p>This session will cover strategies for developing a research agenda, identifying and analyzing funding opportunities, understanding funders’ expectations, planning a project, and talking to program officers.</p>
				
				<ul><li>Track A: <strong>Wednesday, September 12, 2 - 4 p.m.</strong> | Travis Room, Student Union 2.202 | Main Campus</li>
			 <li>Track B: <strong>Thursday, September 13, 3 - 5 p.m.</strong> | Executive Conference Room (FS 4.450A) | Downtown Campus</li>
				 <li>Track C: <strong>Friday, September 14, 9 - 11 a.m.</strong> | Faculty Center Small Conference Room (JPL 4.04.12D) | Main Campus</ul>
			</p>
		
		         <h3>Roadrunner Roundup: Achieving Teaching Excellence</h3>(TEACHING)
		<p>UTSA is has a richly varied student population which provides both pedagogical opportunities and complexities.  This introductory session will provide an introduction to teaching resources at UTSA and explore the ways in which UTSA promotes teaching excellence. We will present data and descriptions of the students you will meet in your classes, examine ideas to create engaged and inclusive classrooms, and explore strategies that improve student learning. </p>
			<ul><li>Track A: <strong>Wednesday, October 10, 2 - 4 p.m.</strong> | Faculty Center Small Conference Room (JPL 4.04.12D) | Main Campus</li>
			 <li>Track B: <strong>Thursday, October 11, 3 - 5 p.m.</strong> | Executive Conference Room (FS 4.450A) | Downtown Campus</li>
				 <li>Track C: <strong>Friday, October 12, 9 - 11 a.m.</strong> | Faculty Center Large Conference Room (JPL 4.04.12C) | Main Campus</ul>
			</p>

				<h3>Grantsmanship 101: Keys to Writing Effective Proposals</h3> (RESEARCH)
					<p>This session will cover the key steps to writing a competitive research proposal. We will step through proposal narrative sections and discuss what each section needs to accomplish, common characteristics of winning proposals, and common mistakes to avoid.</p>
					
					<ul><li>Track A: <strong>Wednesday, November 7, 2 - 4 p.m.</strong> | Faculty Center Small Conference Room (JPL 4.04.12D) | Main Campus</li>
			 <li>Track B: <strong>Thursday, November 8, 3 - 5 p.m.</strong> | Executive Conference Room (FS 4.450A) | Downtown Campus</li>
				 <li>Track C: <strong>Friday, November 9, 9 - 11 a.m.</strong> | Faculty Center Small Conference Room (JPL 4.04.12D) | Main Campus</ul>
			</p>
					
						
	 <h3>Small Teaching = Big Payoffs</h3> (TEACHING)
				 <p>Faculty will read and discuss the book Small Teaching by James Lang with a focus on exploring how even small changes in teaching can have big impacts on student learning.</p>
				<ul><li>Track A: <strong>Wednesday, January 23, 2 - 4 p.m.</strong> | AET 0.202 | Main Campus</li>
			 <li>Track B: <strong>Thursday, January 24, 3 - 5 p.m.</strong> | FS 3.516 | Downtown Campus</li>
				 <li>Track C: <strong>Friday, January 25, 9 - 11 a.m.</strong> | AET 0.202 | Main Campus</ul></p>
	
	<h3>Understanding Your Reviewer and Interpreting Reviews</h3> (RESEARCH)
						<p>This session will focus on the review process, including describing proposals from the reviewer’s point of view and providing guidance on how PIs can best interpret and respond to reviews. Participants will engage in mock review panels. (Some of these discussions may be conducted in break-outs by funder or by type of project (i.e., basic research, applied research, or interventions/programmatic grants).</p>
			 <ul><li>Track A: <strong>Wednesday, February 20, 2 - 4 p.m.</strong> | Faculty Center Large Conference Room (JPL 4.04.12C) | Main Campus</li>
			 <li>Track B: <strong>Thursday, February 21, 3 - 5 p.m.</strong> | Executive Conference Room (FS 4.450A) | Downtown Campus</li>
				 <li>Track C: <strong>Friday, February 22, 9 - 11 a.m.</strong> | Faculty Center Small Conference Room (JPL 4.04.12D) | Main Campus</ul>
			</p>


			
				 
			 <h3>Using Active Learning Strategies (Even in Lecture Classes!)</h3> (TEACHING)
				 <p>Get students to stop dozing and start doing! Active learning engages students in the learning process with activities such as problem solving, experiential learning, analysis, and discussion.  This workshop will cover a continuum of active learning strategies for every classroom size, time, mode of delivery, and discipline.</p> 
				 <ul><li>Track A: <strong>Wednesday, March 20, 2 - 4 p.m.</strong> | AET 0.202 | Main Campus</li>
			 <li>Track B: <strong>Thursday, March 21, 3 - 5 p.m.</strong> | DB 2.228 | Downtown Campus</li>
				 <li>Track C: <strong>Friday, March 22, 9 - 11 a.m.</strong> | AET 0.202 | Main Campus</ul></p>
				 <hr>
				 <br>
				 			<h1>Closing Luncheon</h1>
			<p>The Closing Luncheon will celebrate the completion of the institute and the faculty members' first year at UTSA. The lunch will also provide an opportunity for participants to provide feedback on the program and their experience at UTSA.</p>

<p><strong>Save the Date: Monday, May 6, 12 - 1 p.m. | JPL 4.02.22 (Faculty Center Assembly Room) | Main Campus</strong></p>
			<hr>
			<br>
<h1>Key Contacts</h1>
<p>
<p><strong>Liana Ryan</strong> (for questions about the research sessions)<br />
Assistant Director of Faculty Development, Office of Research Support<br />
<a href="mailto:Liana.Ryan@utsa.edu">Liana.Ryan@utsa.edu</a> | 210-458-7339</p>

<p><strong>Mary Dixson</strong> (for questions about the teaching sessions)<br />
Associate Vice Provost, Teaching & Learning Services<br />
<a href="mailto:Mary.Dixson@utsa.edu">Mary.Dixson@utsa.edu</a> | 210-458-6676</p>

<p><strong>Academic Affairs</strong> (for all other questions)<br />
210-458-4110</p>



        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
