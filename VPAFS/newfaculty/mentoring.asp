<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Mentoring | Academic and Faculty Support | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		  <h1>Faculty Mentoring at UTSA</h1>

<p>The goal of faculty mentoring is multifaceted: support and retain assistant professors as they progress toward tenure; provide ongoing guidance in the development of associate professors on their path to achieving full professor rank; and create an academic culture of high expectations and support for talented faculty as the institution itself progresses toward Tier One status.  

<p>More broadly, peer mentoring helps new faculty members get to know the institution, excel in teaching and research, effectively address challenges inherent in new roles, understand tenure and evaluation, create work-life balance and develop professional networks.

<p>UTSA formalized its faculty mentoring initiative beginning in 2014 with a Provost-initiated request for all departments to establish and implement a faculty mentoring program unique to their traditions and values. Today, all incoming faculty members have the opportunity to participate in a peer mentoring program within their own department.
		<p>
			
			<img src="../images/handshake.png" align="left" style="padding-right:10px" width="75" height="75" alt=""/>
<p><strong>Looking for a mentor? Contact your department chair to discuss how you can participate in your department's mentoring program.</strong> For those interested in finding a mentor outside of his/her own department or discipline, contact Academic Affairs at 210-458-4110.</p>			
			
			<br>

			<p><strong>Updates:</strong><br>
	<ul>
	<li>April 2017: A committee was formed to review the existing faculty peer mentoring programs at UTSA and both identify and share best practices that are associated with excellence in faculty mentoring and support. Members of this review committee were nominated by the Faculty Senate, Department Chairs Council, and Academic and Faculty Support.
	
	<li>May 2017 - May 2018: The committee conducted its review of departmental mentoring guidelines and drafted its findings in a <a href="../documents/2018-mentoring-committee-report.pdf" target="_blank">report</a> for incoming Provost Kimberly Andrews Espy.</li>

	</li></ul>

<p>As the committee's work progresses, any reports and formal recommendations will be made available at this website as well as communicated directly to the college deans and department chairs.</p>

			<h2>Reports, Resources & Best Practices</h2>
<br>
		<p><a href="../documents/mentoring-checklist.docx" target="_blank">Mentoring Guidelines Checklist</a> (to aid departments in their 2018-2019 revisions)</p>	
		<p><a href="../documents/2018-mentoring-committee-report.pdf" target="_blank">Faculty Mentoring Guidelines Quality Committee Report</a> (May 2018)</p>
			
			<p><a href="http://provost.utsa.edu/home/docs/2014-09-15-FacultyMentoring.pdf" target="_blank">UTSA Provost's memo</a> regarding the implementation of a Faculty Mentoring Program</a> (9/15/2014)

<p><a href="../documents/Faculty-Mentoring-Resource-Manual.pdf" target="_blank">UTSA Faculty Mentoring Program Resource Manual</a> (for colleges and departments)<br>
<ul>
	<li><a href="../documents/Example-1-UTSA-Mentoring-Contract.doc" target="_blank">Example #1 Mentoring Contract</a></li>
	<li><a href="../documents/Example-2-UTSA-Mentoring-Contract.doc" target="_blank">Example #2 Mentoring Contract</a></li>
	</ul>

<p>Examples of faculty mentoring programs at other institutions:
			<ul>
				<li><a href="http://www.albany.edu/academics/mentoring.best.practices.chapter2.shtml" target="_blank">SUNY Albany</a></li>
				<li><a href="http://www.crlt.umich.edu/faculty/facment" target="_blank">University of Michigan</a></li> 
				<li><a href="http://www.advance.cornell.edu/documents/Exemplary-Junior-Faculty-Mentoring-Programs.pdf" target="_blank">Cornell University</a> (a compilation of 5 exemplary mentoring programs) </li>
				<li><a href="http://www.faculty.harvard.edu/development-and-mentoring/faculty-mentoring-resources" target="_blank">Harvard University</a></li>
				<li><a href="http://fod.msu.edu/resources-faculty-mentoring" target="_blank">Michigan State University</a></li>
				<li><a href="http://www.niu.edu/facdev/services/newfacmentoring.shtml" target="_blank">Northern Illinois University</a></li>
				<li><a href="http://www.umass.edu/ctfd/mentoring/index.shtml" target="_blank">The Center for Teaching and Faculty Development at the University of Massachusetts at Amherst</a></li>
				<li><a href="http://mentor.unm.edu/home/" target="_blank">University of New Mexico Mentoring Institute</a></li>
			</ul>

        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
