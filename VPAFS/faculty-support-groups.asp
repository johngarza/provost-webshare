<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Assocations and Support Groups | VPAFS | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/inc/nav/home-sub-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        		  <h1>Faculty Associations and Support Groups</h1>
<br>
<p><strong>Association of Chinese Professors</strong><br>
	Faculty Contact: <a href="mailto:Hai-Chao.Han@utsa.edu">Hai-Chao Han</a></p>

<p><strong>Black Faculty/Staff Association</strong><br>
	Faculty Contact: <a href="mailto:MICHELLE.SKIDMORE@UTSA.EDU">Michelle Skidmore</a></p>


<p><strong>Faculty Parents Network</strong><br>
	Faculty Contact: <a href="mailto:Melinda.Denton@utsa.edu">Melinda Denton</a></p>


<p><strong>First-Gen Faculty</strong><br>
	Faculty Contact: <a href="mailto:Patricia.Sanchez@utsa.edu">Patricia Sanchez</a><br>


<p><strong>International Faculty</strong><br>
	Faculty Contact: <a href="mailto:Matthias.Hofferberth@utsa.edu">Matthias Hofferberth</a></p>

    <p><strong>La Raza Faculty and Administrators Association</strong><br>
	Faculty Contact: <a href="mailto:Roger.Enriquez@utsa.edu">Roger Enriquez</a></p>


<p><strong>LGBTQ Faculty and Staff Association</strong><br>
	Faculty Contact: <a href="mailto:Sharon.Nichols@utsa.edu">Sharon Nichols </a></p>


<p><strong>Provost's Task Force on Women Faculty Issues</strong><br>
	Faculty Contact: <a href="mailto:Heather.Shipley@utsa.edu">Heather Shipley</a></p>

<p><strong>Women's Professional Development and Synergy Academy (WPASA)</strong><br>
	Faculty Contact: <a href="mailto:Rhonda.Gonzales@utsa.edu">Rhonda M. Gonzales</a></p>
                   <br>
			<p><em>updated Sept. 2018</em></p>

        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
