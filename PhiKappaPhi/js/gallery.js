$(document).ready(function ($) {
    $(document).on('click', '*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({
            left_arrow_class: '.fa .fa-chevron-left .fa-5x',
            right_arrow_class: '.fa .fa-chevron-right .fa-5x .text-xs-right',
            onShown: function() {
                if (window.console) {
                    return console.log('onShown event fired');
                }
            },
            onContentLoaded: function() {
                if (window.console) {
                    return console.log('onContentLoaded event fired');
                }
            },
            onNavigate: function(direction, itemIndex) {
                if (window.console) {
                    return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                }
            }
        });
    });
});
