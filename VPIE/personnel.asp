<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Personnel | VPIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
 <style type="text/css">
  	h2 {border-bottom: 1px solid #002a5c}	
	.office p span.title{color: #002a5c;}
	.office p span.name {font-weight: bold;}
  </style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!-- #include virtual="/vpie/includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        
           <h1>Executives and Staff</h1>
        <h2>Institutional Effectiveness</h2>
        <div class="office">
            <p><img align="left" src="images/Saygin-new340-100.jpg" width="100" height="100" alt="Can Saygin" style="padding-right:10px" />    
                <span class="name">Dr. Can Saygin</span><br />
              <span class="title">Interim Senior Vice Provost for Institutional Effectiveness and Strategic Initiatives</span><br />
              MB 4.122, 
              210-458-4101<br />
              <a href="mailto:Can.Saygin@utsa.edu">Can.Saygin@utsa.edu</a><br />
            </p>
       
                 
 
<br />

        
          
                    
       </div>  
      <h2>Continuous Improvement and Accreditation</h2>
        <div class="office">
            <p><img align="left" src="images/KaseyNeeceFielder5402-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
                <span class="name">Dr. Kasey Neece-Fielder</span><br />
              <span class="title">Associate Vice Provost for Strategic Planning and Assessment; SACSCOC Liaison</span><br />
              GSR 2.204, (210) 458-4819<br />
                <a href="mailto:Kasey.Neece-Fielder@utsa.edu">Kasey.Neece-Fielder@utsa.edu</a>
            </p>
        <br />
          <p>
     <img align="left" src="images/LorrieSmith5392-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /><span class="name">Lorrie Smith</span><br />
              <span class="title">Director of Accreditation and Process Improvement</span><br />
            GSR 2.204, (210) 458-5188<br />
          <a href="mailto:Lorrie.Smith@utsa.edu">Lorrie.Smith@utsa.edu</a></p>
          <br />
           <p><img align="left" src="images/ElizabethHoff5399-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
<span class="name">Dr. Elizabeth Hoff</span><br />
              <span class="title">Director of University Assessment</span><br />
              GSR 2.204, (210) 458-4704<br />
<a href="mailto:Elizabeth.Hoff@utsa.edu">Elizabeth.Hoff@utsa.edu</a></p>
              <br />
          <p><img align="left" src="images/TiaPalsole5393-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
           <span class="name">Tia Palsole</span><br />
             <span class="title"> Project Coordinator</span> - Accreditation<br />
              GSR 2.204, (210) 458-8038<br />
          <a href="mailto:tia.palsole@utsa.edu">Tia.Palsole@utsa.edu</a></p>
          <br />
          <p><img align="left" src="images/LisaJohnston5379-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
        <span class="name">Lisa Johnston</span><br />
              <span class="title">Senior Administrative Associate</span><br />
          GSR 2.204, (210) 458-4965<br />
          <a href="mailto:Lisa.Johnston@utsa.edu">Lisa.Johnston@utsa.edu</a></p>
                    
                 <br />   
       </div>
        <h2>Evaluations and Surveys</h2>
        <div class="office">
              <p><img align="left" src="images/DougAtkinson5394-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /> 
                <span class="name">Doug Atkinson</span><br />
    <span class="title">Director of Evaluations and Surveys</span><br />
                GSR 2.204, (210) 458-4709<br />
          <a href="mailto:Douglas.Atkinson@utsa.edu">Douglas.Atkinson@utsa.edu</a></p>
<br />
           <p><img align="left" src="images/charlton-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    <span class="name">Leanne Charlton</span><br />
   <span class="title">Project Coordinator</span><br />
                GSR 2.204, (210) 458-8010<br />
          <a href="mailto:Leanne.Charlton@utsa.edu">Leanne.Charlton@utsa.edu</a></p>
          <br />
                    
                    
       </div>
        <h2>Institutional Research</h2>
        <div class="office">
            <p><img align="left" src="images/Wilkerson-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
                <span class="name">Dr. Steve Wilkerson</span><br />
              <span class="title">Associate Vice Provost for Institutional Research</span><br />
              GSR 2.204, (210) 458-4939<br />
                <a href="mailto:Steve.Wilkerson@utsa.edu">Steve.Wilkerson@utsa.edu</a><br />
            </p>
        <br />
          <p><img align="left" src="images/starnes-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
<span class="name">Linda Starnes</span><br />
              <span class="title">Senior Administrative Associate</span><br />
                GSR 2.204, (210) 458-5242<br />
            <a href="mailto:Linda.Starnes@utsa.edu">Linda.Starnes@utsa.edu</a></p>

         <br />
          <p><img align="left" src="images/SalmaFerdous5407-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /> 
            <span class="name">Salma Ferdous</span><br />
      <span class="title">Director of Analysis and Certification</span><br />
                GSR 2.204, (210) 458-4794<br />
            <a href="mailto:Salma.Ferdous@utsa.edu">Salma.Ferdous@utsa.edu</a></p>
<br />
          <p><img align="left" src="images/cordeau-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /> 
            <span class="name">Brian Cordeau</span><br />
            <span class="title">Director of Reporting</span><br />
              GSR 2.204, (210) 458-4705<br />
          <a href="mailto:Brian.Cordeau@utsa.edu">Brian.Cordeau@utsa.edu</a></p>
            <br />
    <p><img align="left" src="images/JinnyCase5386-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /><span class="name">Dr. Jinny Case</span><br />
        <span class="title">Senior Institutional Research Analyst</span><br />
GSR 2.204,  (210) 458-8180<br />
<a href="mailto:Jinny.Case@utsa.edu">Jinny.Case@utsa.edu</span></a></span></p>
<br />
<p><img align="left" src="images/MahmoudAbunawas5401-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /> <span class="name">Dr. Mahmoud Abunawas</span><br />
              <span class="title">Institutional Research Analyst II</span><br />
              GSR 2.204, (210) 458-4792<br />
          <a href="mailto:MAHMOUD.ABUNAWAS@UTSA.EDU">Mahmoud.Abunawas@utsa.edu</a></p>
          <br />
    <p><img align="left" src="images/FikrewoldBitew5388-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /> <span class="name">Fikrewold Bitew</span><br />
      <span class="title">Institutional Research Analyst</span> II<br />
GSR 2.204,  (210) 458-8204<br />
    <a href="mailto:FIKREWOLD.BITEW@UTSA.EDU">Fikrewold.Bitew@utsa.edu</a></p>
            
        <br />
        <p><img align="left" src="images/jayagopal-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /> <span class="name">Ashwin Jayagopal</span><br />
      <span class="title">Institutional Research Analyst</span> I<br />
GSR 2.204,  (210) 458-8797<br />
    <a href="mailto:FIKREWOLD.BITEW@UTSA.EDU">Fikrewold.Bitew@utsa.edu</a></p>
            <br />
            <br />
       
      
         <p><img align="left" src="images/personnel-placeholder.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
<span class="name">Vacant</span><br />
          <span class="title">Institutional Research Analyst</span> I</p>
         <p><br />
           <!--GSR 2.204D.14, 
              (210) 458-8797<br />
              <a href="mailto:Jason.Purcell@utsa.edu">Jason.Purcell@utsa.edu</a>-->
         </p>
        <br />
            <p><img align="left" src="images/apgar-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
<span class="name">Lauren Apgar</span><br />
              <span class="title">Institutional Research Analyst</span> I<br />
              GSR 2.204, 
              (210) 458-4552<br />
              <a href="mailto:Lauren.Apgar@utsa.edu">Lauren.Apgar@utsa.edu</a></p>
          <br />
         <p><img align="left" src="images/ArtPagano5398-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /><span class="name">Art Pagano</span><br />
             <span class="title">Systems Analyst III</span><br />
           GSR 2.204, (210) 458-5814<br />
             <a href="mailto:Art.Pagano@utsa.edu">Art.Pagano@utsa.edu</a><br />
           </p>
            <br />
         <p><img align="left" src="images/personnel-placeholder.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
              <span class="name">Vacant</span><br />
            <span class="title">Systems Analyst III</span><br />
            <!--GSR 2.204D.5, (210) 458-4739<br />
            <a href="mailto:Brenda.Rankin@utsa.edu">Brenda.Rankin@utsa.edu</a></p>    -->
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p><img align="left" src="images/pham-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
            <span class="name">Hoa Pham</span><br />
            <span class="title">Undergraduate Research Assistant</span><br />
            GSR 2.204, (210) 458-4706<br />
            <a href="mailto:Hoa.Pham@utsa.edu">Hoa.Pham@utsa.edu</a></p>
          <p>&nbsp;</p> 
            <p><img align="left" src="images/diep-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
            <span class="name">Michael Diep</span><br />
            <span class="title">Web Specialist Intern</span><br />
            GSR 2.204<br />
          <a href="mailto:Michael.Diep@utsa.edu">Michael.Diep@utsa.edu</a></p> 
        </div>
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
