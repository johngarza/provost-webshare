<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>VPIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            	<!-- #include virtual="/VPIE/Includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         
		<img src="images/Logo_OfficeInstitutionalEffectiveness_2color_Formal_Single_Short copy.png" width="695" height="84" alt="vpie-logo" />
    <p>&nbsp;</p>
   
<h2>Mission Statement</h2>

        <p>The mission of the Office of  Institutional Effectiveness is to provide logistical support and relevant, reliable information for institutional planning, accreditation, assessment, and accountability as UTSA moves to premier public university status. We also support the Academic Affairs community in fulfilling the public trust through a system of training and compliance.The functions in our office are described in links to the left.</p>

        <p>The staff members who support our mission are listed under the Executives and Staff link on this page. Their contact information is provided for your convenience.</p>

        <p>Our main office telephone number is (210) 458-4706; FAX: (210) 458-4708</p>

        <table>
          <tr>
            <td style="background-color:#eaeaea; color:#666">
              <h2><strong>Consulting Services</strong></h2>
              <p><br />
              Our office offers advice and consulting services to our University community regarding any of the areas for which we are responsible. For questions regarding advice and consulting services please contact:<br />
              &nbsp;<br />
              Steve Wilkerson (<a href="mailto:steve.wikerson@utsa.edu">steve.wikerson@utsa.edu</a>); 210-458-4939: Institutional Research Analyses; Data Warehouse<br />
Kasey Neece-Fielder (<a href="mailto:kasey.neece-fielder@utsa.edu">kasey.neece-fielder@utsa.edu</a>); 210-458-4819: Program Review; Assessment; SACSCOC Accreditation </p>
             <!-- <p>Questions<br />
                Lorrie Smith (<a href="mailto:lorrie.smith@utsa.edu">lorrie.smith@utsa.edu</a>); 210-458-5188: Faculty Workload<br />
                Linda Starnes (<a href="mailto:linda.starnes@utsa.edu">linda.starnes@utsa.edu</a>); 210-458-4706: SACSCOC Faculty Credentials <br />
                Tia Palsole (<a href="mailto:tia.palsole@utsa.edu">tia.palsole@utsa.edu</a>); 210-458-8038: Digital Measures<br />
            </p>--></td>
          </tr>
        </table>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
