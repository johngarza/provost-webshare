<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Academic Program Changes | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
 <style type="text/css">
  	h2 {border-bottom: 1px solid #002a5c}	
	.office p span.title{color: #002a5c;}
	.office p span.name {font-weight: bold;}
  </style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!--#include virtual="/inc/nav/home-nav3.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        
           <h1><strong>Proposals for Academic Program Changes</strong></h1>
           <p>Changes regarding academic programs may require UT System, Texas Higher Education Coordinating Board (THECB), and/or Southern Association of Colleges and Schools Commission on Colleges (SACSCOC) approval before becoming effective. The purpose of this webpage is to help those proposing changes navigate the applicable processes and to ensure that approved changes are moved forward in an appropriate and timely way.
        <p> Prior to any external approvals, it is important that the appropriate entities at UTSA are included in the process of proposing changes. From a faculty governance perspective, the respective Department Chairs, Deans, and/or Curriculum Committees should be involved prior to seeking approval for the change. In addition, the Faculty Senate and other entities are included in the approval process as noted on Approval Flowchart below.   </p>                       
           <h2> <strong>New academic degree program, certificate, minor, concentration, or track</strong></h2>
           <p>               There are two stages to new degree program, certificate, minor, concentration, or track formation: the development stage and approval stage.              
           <h3> <em>Development Stage</em>             </h3>
           <p>               This template should be used for the development stage and go through the following process before moving to the approval stage.  Once the template has been completed, it should be discussed by the academic Dean with the Vice Provost of Academic Affairs (Undergraduate Studies) or Vice Provost of Graduate School (Graduate) and subsequently the Deans’ Council.  
<ul>
               <li> <a href="docs/New Degrees and other programs Proposal Template.docx" target="_blank">Template </a></li>
               <!--<li> <a href="docs/New Program Development and Approval process.pptx">New Program Development and Approval Process</a></li>-->
             </ul>
        <h3> <em>Approval Stage </em></h3>
<p>        If the proposed change is supported through the development stage, the department/College will be notified and the full proposal development will begin in the approval stage.
<ul>
  <li> <a href="docs/Acad Prog Change Approval Process.pdf" target="_blank">Approval Flowchart </a></li>
</ul>
<h2> <strong>All other changes </strong></h2>
           <p>               For all other proposed changes, including but not limited to:              
        <ul>
             <li> Degree program closure             </li>
             <li> Degree program name change             </li>
             <li> Change in the number of credit hours required for a degree or certificate             </li>
             <li> New off-site location to offer 25% or more of a degree program             </li>
             <li> New collaborative academic agreement, including <a href="../home/resources_dean.asp#SACS" target="_blank">joint and dual degrees</a>
</h1>
          </li>
           </ul>
           <p><a href="https://utsa.az1.qualtrics.com/jfe/form/SV_8vtnCprmVKHXGId" target="_blank"><img src="images/submit-proposal.jpg" width="250" height="50" alt="submit" /></a></p>
           <p> <a href="https://graduateschool.utsa.edu/faculty-staff/developing-degree-proposals/">Click here for information on Graduate Degree Program Proposals</a></p>
           <p> <a href="cia/substantive-change.asp" target="_blank">Click here for more information on the SACSCOC Substantive Change guidelines </a></p>
<h1>&nbsp;</h1>
        <div class="office"> </div>
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
