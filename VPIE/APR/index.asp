<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Academic Program Review | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/APR/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <h1>Academic Program Review</h1>

        <p>The University of Texas at San Antonio considers the periodic review of each of its academic programs essential to promote and maintain excellence in undergraduate and graduate programs.</p>

        <p>Academic program review (APR) is a comprehensive process, in which departments engage in a methodical evaluation of the strengths and weaknesses of degree programs; determine the degree to which departmental, college, and university goals and objectives are aligned; and summarize the assessment of educational outcomes.</p>

        <p>External reviewer site visits will be held during fall and spring semesters. Programs are assigned to one of two review timelines based on when the site visit is scheduled.</p>
        <p>As a result of a thorough review, academic units can realize several benefits, such as:</p>
        <ul><li>the clarification of program goals, strengths, and weaknesses;</li>
          <li>the evaluation of the quality of the unit’s academic programs;</li>
         <li>the review and possible revision of objectives for the teaching, research and service missions of the academic program for future attainment of the University, college and departmental strategic goals; and</li>
         <li>development of a source of information to guide decisions on future priorities and available resources.</li></ul>
          <p>
          The Academic Program Review process consists of three components: the Internal Program Review, External Review Report, and Response Report.  This review satisfies requirements for the Southern Association of Colleges and Schools Commission on Colleges (SACSCOC), Texas Higher Education Coordinating Board (THECB), and UT Systems which is completed every seven years.</p>

        
        <!--<h2 align="center">Key dates of Program Review</h2>
        <table width="650">
       		

          <tr>
            <th width="140" align="center" style="background-color:#156570;">
            <strong><font color="white">FALL SITE<br />
            VISIT</font></strong></th>

            <th width="140" align="center" style="background-color:#988F86;">
            <strong><font color="white">SPRING SITE VISIT</font></strong></th>

            <th width="315" style="background-color:#A4B7B8;">
            <strong><font color="white">EVENT</font></strong></th>
          </tr>
          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">Mid-January</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">Mid-September</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Program review orientation meeting</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">March 15</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">November 15</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Department nominates external reviewers</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">March 30</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">November 30</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Dean selects external reviewers</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">May 15</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">January 15</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Preliminary self-study submitted</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">30 days prior<br />
            to visit</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">30 days prior<br />
            to visit</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Final self-study submitted</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">Before April 30</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">Before<br />
            November 15</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Site visit</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">Within 30 days</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">Within 30 days</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Review team report received</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">Within 30 days</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">Within 30 days</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Responses by department and deans</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">Within 90 days</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">Within 90 days</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Provost forwards final response to review</font></strong></td>
          </tr>

          <tr>
            <td align="center" style="background-color:#156570;"><strong><font color="white">One year later</font></strong></td>

            <td align="center" style="background-color:#988F86;"><strong><font color="white">One year later</font></strong></td>

            <td style="background-color:#A4B7B8;"><strong><font color="white">Follow-up meeting to review progress</font></strong></td>
          </tr>
        </table>-->

        <h2>Programs under review 2018&ndash;2019 (NEW)</h2>

        <h3>Anthropology<br />
          Communication<br />
          Psychology<br />
        Social Work</h3>
        <h3><br />
        </h3><p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
