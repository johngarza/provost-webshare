<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Resources | APR | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPAIE/APR/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <h1>Academic Program Review - Resources and Templates</h1>

        <h2>Schedules</h2>

        <ul>
          <li><a href="docs/PR_Archive_Schedule_by_program.xls">Program Review Schedule (alphabetical)</a></li>

          <li><a href="docs/PR_Review_schedule_by_semester.xls">Program Review Schedule (semester)</a></li>
        </ul>

        <h2>Timelines</h2>

        <ul>
          <li><a href="docs/Fall timeline.pdf">Program Review Timeline &mdash; Fall Site Visit</a></li>

          <li><a href="docs/Spring timeline.pdf">Program Review Timeline &mdash; Spring Site Visit</a></li>
        </ul>

        <h2>Checklists</h2>

        <ul>
          <li><a href="docs/Fall Site Visit Program Review Timeline and Checklist (Department).doc">Departmental Program Review Checklist &mdash; Fall Site Visit</a></li>

          <li><a href="docs/Spring Site Visit Program Review Timeline and Checklist (Department).doc">Departmental Program Review Checklist &mdash; Spring Site Visit</a></li>
        </ul>

        <h2>Aspirant Program Selection</h2>

        <ul>
          <li><a href="docs/Aspirant Program Selection Form[3].docx">Aspirant Program Selection Form</a></li>
        </ul>

        <h2>External Reviewer Selection</h2>

        <ul>
          <li><a href="docs/External Reviewer Nomination Selection Form - 10-3-13.docx">External Reviewer Nomination and Selection Form (Word file)</a></li>

          <li><a href="docs/Non-Sponsored Services Agreement - 7_2015.doc">Sample Services Agreement Form</a></li>
          <li><a href="docs/Non-Sponsored Research Accounts Routing Sheet - 4_15.pdf">Non-Sponsored Research Accounts Routing Sheet</a></li>
        </ul>

        <h2>Review Packet</h2>

        <ul>
          <li><a href="docs/Aspirant_Program_Information_Template_Fall_2016.docx">Aspirant Program Information Template</a> (To be complete by the program under review)</li>
        </ul>

        <h2>Site Visit Information</h2>

        <ul>
          <!--<li><strong><a href="docs/Sample%20Schedule%20for%20One-Day%20Site%20Visit.docx">Sample Schedule for One-Day Site Visit</a></strong></li>-->
<li><a href="docs/External Program Reviewer template.docx">External Program Review template</a></li>
          <li><a href="docs/Appendix D  Sample On-site Review Schedule.doc">Sample Schedule for Two-Day Site Visit</a> </li>
        </ul>

       <!-- <h2>General</h2>

        <ul>
          <li><strong><a href="../APR/docs/RS_QuickStart.pdf">How to post files in Rowdy Space</a></strong></li>
        </ul>-->

        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
