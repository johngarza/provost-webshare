<ul>
    <li><a class="home" href="/home/" title="Provost Home Page">Provost Home</a></li>
    <li><a href="/vpie/">VPIE Home</a></li> 
    <!--<li><a href="http://www.utsa.edu/strategicplan/">Strategic Planning: UTSA 2016</a></li>-->
    <li><a href="http://provost.utsa.edu/vpie/strategicplan/index.asp">Strategic Planning: VPIE 2016</a></li>
    <li><a href="http://provost.utsa.edu/vpie/SAC_reaffirmation.asp">SACSCOC Reaffirmation of Accreditation</a></li>
    <li><a href="http://www.utsa.edu/ir">Institutional Research</a></li>
    <li><a href="/vpie/apr/">Academic Program Review</a></li>
    <li><a href="/vpie/assessment/">Office of Assessment </a></li>
</ul> 
<hr  style="height:0.2em;"/>
<h2>Academic Program Review</h2>   
<ul>
    <li><a href="/VPIE/APR/index.asp">Home</a></li>
   <!-- <li><a href="../APR/docs/Program_Review_Schedule_(Alphabetical).pdf">Program Review Schedule</a></li>-->
    <li><a href="../APR/docs/Program-Review-Manual.pdf">Academic Review Handbook</a></li>
 <!-- <li><a href="/VPAIE/APR/resources.asp">Resources and Templates</a></li>-->
    <li><a href="http://www.utsa.edu/hop/chapter2/2-39.html">HOP 2.39 - Academic Program Review</a></li>
    <li><a href="../APR/docs/SACS-8.2.a.pdf">SACSCOC Principle 8.2.a</a></li>
    <li><a href="http://www.utsa.edu/ir/">Office of Institutional Research</a></li>
    <li><a href="/vpie/assessment/">Office of Assessment</a></li>
    <li><a href="mailto:Kasey.Neece-Fielder@utsa.edu">Contact Us</a></li>
</ul>
<hr  style="height:0.2em;"/>
<ul style="margin:1.5em 0 0 0;">
    <li><a href="/VPIE/personnel.asp" style="border-top: 0px;">Executives and Staff</a></li>
    <li><a href="/VPIE/docs/VPIE-Organizational-Chart.pdf">Organizational Chart</a></li>  
</ul>

