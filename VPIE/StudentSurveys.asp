<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Provost Office</a></div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!-- #include virtual="/VPIE/Includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <!-- <h1>Student Course Evaluations</h1>-->

       <img alt="Course Evaluations - Summer 2019 - evaluate@utsa.edu - Log into ASAP for your links" src="images/Course_Evaluation_SUMMER_2019002.jpg" width="700" height="397" />

                  <!--  <table width="350
                    " border="1">
  <tr>
    <td><strong>Summer Term</strong></td>
    <td><strong>Evaulation Date</strong></td>
  </tr>
  <tr>
    <td>Second 4-week</td>
    <td>8/1/18 - 8/7/18</td>
  </tr>
  <tr>
    <td>Second 5-week</td>
    <td>8/1/18 - 8/7/18</td>
  </tr>
  <tr>
    <td>8-week</td>
    <td>8/1/18 - 8/7/18</td>
  </tr>
  <tr>
    <td>10-week</td>
    <td>8/1/18 - 8/7/18</td>
  </tr>
</table>-->
<table width="100%
	" border="1">
	<thead>
		<tr>
			<th>Term</th>
			<th>Semester
		(Part of Term)</th>
			<th>Date of Evaluation</th>
			<th>Class Start Date</th>
			<th>Class End Date</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Summer 2019	(M)</td>
			<td>3 week</td>
			<td>6/12/19 - 6/14/19</td>
			<td>5/28/19</td>
			<td>6/17/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(C2)</td>
			<td>2 week</td>
			<td>6/25/19 - 6/26/19</td>
			<td>6/17/19</td>
			<td>6/28/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(F)</td>
			<td>1st 5-week</td>
			<td>6/24/19 - 6/27/19</td>
			<td>5/28/19</td>
			<td>7/3/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(J)</td>
			<td>1st 4-week</td>
			<td>6/28/19 - 7/2/19</td>
			<td>6/10/19</td>
			<td>7/9/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(8, 1)</td>
			<td>8 week</td>
			<td>7/30/19 - 8/2/19</td>
			<td>6/10/19</td>
			<td>8/7/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(L)</td>
			<td>2nd 4-week</td>
			<td>7/30/19 - 8/2/19</td>
			<td>7/10/19</td>
			<td>8/7/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(T)</td>
			<td>10-week</td>
			<td>7/30/19 - 8/2/19</td>
			<td>5/28/19	</td>
			<td>8/10/19</td>
		</tr>

		<tr>
			<td>Summer 2019	(S)</td>
			<td>2nd 5-week</td>
			<td>7/30/19 - 8/2/19</td>
			<td>7/5/19	</td>
			<td>8/10/19</td>
		</tr>
	</tbody>
</table>


<h2>Background and Process</h2>
        <p>Since 2010, UTSA has conducted evaluation of classes and instructors online using Scantron Corporation&rsquo;s Class Climate &reg; system. The results of these evaluations are provided to instructors, department chairs, and deans in order to improve classes and instruction at UTSA. They are also used as part of the regular performance appraisal conducted for each instructor for merit. </p>
            <h2>Evaluation Content</h2>
            <p>Students enrolled in classes are provided links to the evaluation forms and given time to complete the evaluations prior to final exams (number of days varies depending on whether the semester is fall/spring or summer sessions).</p>
            <p>Every survey sent to students must have the following items:  five informational statements and two essential statements.  </p>
<dl> <dd>
  <p><u>Informational Statements</u> <br />
    (answered on a scale of 5 = Strongly Agree; 4 = Agree; 3 = Neutral; 2 = Disagree; 1 = Strongly Disagree): </p>
  <ul>
    <li>The instructor clearly defined and explained the course objectives and expectations.</li>
    <li>The instructor communicated information effectively.</li>
    <li>The instructor was prepared to teach for each instructional period.</li>
    <li>The instructor encouraged me to take an active role in my own learning.</li>
    <li>The instructor was available outside of class either electronically or in person.</li>
    </ul>
  <dd>
    <p><u>Essential Statements</u> <br />
      (answered on a scale of 5 = Excellent; 4 = Above Average; 3 = Average; 2 = Below Average; 1 = Poor): </p>
    <ul>
      <li>My overall rating of the course is: </li>
      <li>My overall rating of the teaching of this course is: </li>
    </ul>
  </dl>
                <h2>Related Information</h2>
                <p>The Texas Higher Education Coordinating Board has provided <a href="docs/HB_2504_THECB_rules.pdf">rules for implementation of HB 2504</a>, part of which requires that the results of student evaluations of instruction be posted online on each institution&rsquo;s website. UTSA has implemented those requirements using an institutionally-developed, web-based system, <a href="http://bluebook.utsa.edu/">Bluebook</a>. </p>
                <h2>Questions</h2>
                <p>If you have any questions about UTSA&rsquo;s class and instructor system, please call Doug Atkinson (458-4709) or Leanne Charlton (458-8010), or email them at <a href="mailto:evaluate@utsa.edu">evaluate@utsa.edu</a>. <a name="_GoBack"></a></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
