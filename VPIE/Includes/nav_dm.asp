<ul>
    <li><a class="home" href="http://provost.utsa.edu" title="Provost Home Page">Provost Home</a></li>
    <li><a href="/vpie/">Institutional Effectiveness Home</a></li>
    <li><a href="../dm/index.asp" target="_blank">Digital Measures Home</a></li>
    <li><a href="/VPIE/dm/dates.asp" target="_blank">Important Dates</a></li>
<li><a href="../dm/new-in-dm.asp" target="_blank">What's New in DM</a></li>
        <li><a href="/VPIE/dm/faculty-resources.asp" target="_blank">Help for Faculty</a></li>
   
    <li><a href="/VPIE/dm/vita.asp" target="_blank">Vita</a></li>
  <li><a href="../dm/annual-report-faculty.asp" target="_blank">Annual Report<br />
    (Faculty)
</a></li>
    <li><a href="/VPIE/dm/workload-reporting.asp" target="_blank">Faculty Role in Workload Reporting</a></li>
    <li><a href="/VPIE/dm/college-resources.asp" target="_blank">Help for Departments and Colleges</a></li>    
   <li><a href="../dm/faculty-workload.asp" target="_blank">Faculty Workload</a></li>
    <li><a href="../dm/annual-report-colleges.asp" target="_blank">Annual Report<br />
      (Departments and Colleges)
</a></li>
    <li><a href="../dm/faculty-agreements.asp">Faculty Workload Distribution Agreements</a></li>
    <li><a href="../dm/credentialing.asp" target="_blank">Faculty Credentialing</a></li>
    <li><a href="../dm/archives.asp" target="_blank">DM Archives</a></li>
<li><a href="/VPIE/dm/manuals.asp" target="_blank">All DM User's Manuals and Guides</a></li>
    <li><a href="/VPIE/dm/contact.asp" target="_blank">Contact Us</a></li>    
</ul>