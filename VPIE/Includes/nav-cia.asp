

<ul>
	<li><a class="home" href="http://provost.utsa.edu" title="Provost Home Page"><strong>Provost Home</strong></a></li>
  <li><strong><a href="/VPIE/index.asp" title="Provost Home Page">Institutional Effectiveness</a></strong></li>
  <li><a href="../cia/index.asp"><strong>Continuous Improvement &amp; Accreditation</strong></a></li>
   
<li><strong><a href="/VPIE/assessment/index.asp" target="_blank">Assessment</a>
</strong></li>
<li><a href="../APR/index.asp" target="_blank"><strong>Academic Program Review</strong></a></li>
<hr />
<li><strong>Accreditation</strong>
  <ul>
   	  <li><a href="../cia/SACSCOC-accreditation.asp" target="_blank">- SACSCOC Accreditation</a></li>
   	  <li><a href="../SAC_reaffirmation.asp" target="_blank">- Racing to Reaccreditation 2020</a></li>
   	 <li><a href="../cia/substantive-change.asp" target="_blank">- Substantive Change</a></li>
    <li><a href="../cia/credentials.asp" target="_blank">- Faculty Credentials</a></li>
    
    <li><a href="../cia/specialized-accreditation.asp" target="_blank">- Specialized Accreditations</a></li>

</ul>
</li>


<!--<li><strong>Administrative Support
    	</strong>
  <ul>
   	  <li><a href="http://provost.utsa.edu/departmentchairs/"> - Department Chairs Council</a></li>
      <li><a href="/VPAFS/compensation.asp"> - Faculty Contracts</a></li>
<li><a href="/VPAFS/facultygrievance.asp"> - Faculty Grievance Process</a></li>
<li><a href="/VPAFS/facultyrecruitment.asp"> - Faculty Recruitment</a></li>
   	</ul>
</li>
<li><strong><a href="/VPAFS/information.asp">Information Resources</a></strong></li>-->
<li><strong><a href="/VPIE/cia/personnel.asp" target="_blank">About Us</a></strong></li>
</ul>
<!--<p><strong>Contact Us:</strong><br />
(210) 458-2700<br />
vpafs@utsa.edu</p>-->