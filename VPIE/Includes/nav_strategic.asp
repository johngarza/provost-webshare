<ul>
	<li><a class="home" href="/home/" title="Provost Home Page">Provost Home</a></li>
	<li><a href="/vpie/">Institutional Effectiveness Home</a></li>
	<!--<li><a href="http://www.utsa.edu/strategicplan/">Strategic Planning - UTSA 2016</a></li>-->
	<li><a href="/vpie/strategicplan/index.asp">Mission, Vision and Core Values</a></li>
    <li><a href="/vpie/SAC_reaffirmation.asp">SACS/COC Reaffirmation of Accreditation</a></li>
    <li><a href="http://www.utsa.edu/ir">Institutional Research</a></li>
    <li><a href="/vpie/apr/">Academic Program Review</a></li>
    <li><a href="/vpie/assessment/">Assessment </a></li>
</ul>