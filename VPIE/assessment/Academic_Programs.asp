<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <h1 align="center">Academic Program Assessment</h1>

        <p>Assessment of student learning outcomes is an integral part of the Teaching-Learning process. The purpose is to enhance a student's opportunity to acquire and apply the knowledge, skills, and attitudes/values provided through all aspects of instruction. The assessment of student learning at the program level relies on faculty expertise and experience. Information derived from the assessment of student learning will be used to facilitate student learning and development, to promote faculty and staff growth, to improve the quality of academic programs, and to achieve the university's mission in accordance with the strategic plan.</p>
<h2>Forms and Examples</h2>
        <ul>
          <li><a href="docs/Academic Assessment Plan Template.docx">Academic assessment plan template</a></li>
          <li> <a href="docs/Academic Assessment Plan Example.pdf">Academic assessment plan example</a></li>
          <li><a href="docs/Curriculum Map.docx">Curriculum mapping template</a></li>
<li><a href="docs/Academic Assessment Report Template.docx">Academic assessment report template</a></li>
          <li> <a href="docs/Academic Assessment Report Example.pdf">Academic assessment report example</a></li>
          <li> <a href="docs/Use of Results for Improvement Report.doc">Use of results for improvement report</a></li>
          <li><a href="docs/IISR Form.docx">Implementation of improvements status report</a></li>
          <li><a href="docs/IISR Example.pdf">Implementation of improvements status report example</a></li>
        </ul>
        <h2>Steps of Academic Program Assessment</h2>

        <ol>
          <li><strong><a href="academic-mission.asp">Specify Program Mission and Goals</a></strong> – Broad descriptions of the purpose of the academic program how students will be different as a result of successfully completing it.</li>
          <li><strong><a href="academic-outcomes.asp">Define Student Learning Outcomes</a></strong><a href="academic-outcomes.asp"><strong></strong></a> - Specific statements of what the student&nbsp;<strong>will know, be able to do, or value</strong>&nbsp;upon successfully completing the academic program.</li>
          <li><strong><a href="academic-opportunities.asp">Identify Learning Opportunities (Curricular Map)</a></strong>&nbsp;- How well does the curriculum address the learning outcomes?</li>
          <li><strong><a href="academic-methods.asp">Determine</a></strong><a href="academic-methods.asp">&nbsp;<strong>Methods</strong></a><strong>&nbsp;</strong>- What methods will be used to collect data on the strengths and weaknesses of the academic program and to determine whether students are achieving the learning intended? </li>
          <li><strong><a href="academic-criteria.asp">Set Criteria for Success</a></strong>&nbsp;- Establish the level of expected student performance.</li>
          <li><strong><a href="academic-report-results.asp">Collect Data and Report on the Results</a></strong>&nbsp;- Examine the information collected and evaluate results against program expectations.</li>
          <li><strong><a href="academic-use-results.asp">Use the Results</a>&nbsp;</strong>- How is the information going to be used to make program improvements?</li>
        </ol>
        <h2>Questions to Ask During the Assessment Cycle</h2>
        <ul>
          <li>Do current assessment efforts measure what is intended?</li>
          <li>Are results being used effectively? If not, maybe it is time to retire some of the assessment tools.</li>
          <li>Are there better ways to collect useful, needed data?</li>
          <li>What options are available to assess things differently?</li>
        </ul>
        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
