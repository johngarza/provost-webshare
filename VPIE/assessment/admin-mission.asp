<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Administrative Program Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="AdministrativePrograms.asp">Back to Steps of Administrative <br />
        Program Assessment</a></p>
       	<h1 align="center">Administrative Program Assessment        </h1>
       	<h1 align="center"><strong>Specifying Unit Mission and Goals</strong></h1>
        <h2>How do you develop a mission statement?</h2>
        <p>Mission statements enable the administrative unit to define their purpose within the context of the greater institutional mission. The unit’s mission statement should effectively communicate to internal and external stakeholders the key functions of the unit and how it contributes to the institution. To develop a mission statement, encourage personnel to discuss the following questions:</p>
        <ul>
          <li>What is your primary purpose? Discuss why you do what you do.</li>
          <li>Who do you primarily serve? Discuss who your key stakeholders are (e.g., students, faculty, staff, etc.)</li>
          <li>What are the most important functions or services that you provide?</li>
          <li>How do you support the institution’s mission?</li>
        </ul>
        <p>Once the unit has drafted a statement addressing the preceding questions, ensure that the resulting statement is specific and unique enough that it differentiates your unit from others.</p>
        <h2>Example Mission Statements</h2>
        <ul>
          <li><strong><u>Office of Continuous Improvement and Accreditation:</u></strong> <em> “To lead institutional accreditation and assessment activities in support of the University’s mission through continuous improvement of processes, programs, and services.”</em></li>
          <li><strong><u>Office of Institutional Research:</u></strong><em> “To provide the highest quality of institutional research to advance the university’s mission through collection and reporting of institutional data, research, and data analysis, and the support of institutional effectiveness related to: access and excellence in higher education in south Texas; research and discovery, teaching and learning, and public service; embracing of multicultural traditions of South Texas; serving as a center of intellectual and creative resources; and being a catalyst for the economic development of Texas.”</em></li>
          <li><strong><u>Student Health Services:</u></strong> <em>“To provide quality medical care and innovative health education to empower and support student success.”</em></li>
          <li><strong><u>Career Center:</u></strong> <em>“To assist students and alumni in identifying and developing the global skills necessary to successfully pursue and achieve lifelong career goals.”</em></li>
        </ul>
        <h2>How do you develop unit goals?</h2>
        <p>Successful administrative assessment depends upon the determination of shared goals. </p>
        <p>‘Goals’ can be defined in several ways. This handbook focuses on the development of goals at the administrative unit level, which describe in broad terms what the unit hopes to accomplish over the next several years. Goals should align with the unit’s mission statement, as well as with relevant VP- and institutional-level goals. Unit goals do not have to be directly measurable (this will be achieved when goals are ‘broken down’ into specific related outcomes) and may be aspirational. </p>
        <p> Shared goals provide the foundation for the unit’s activities and guide the selection of questions to address via assessment. The following activities can facilitate the definition of shared goals.</p>
        <h3>
          Engage personnel in the following discussions:</h3>
        <ul>
          <li>What are the most important functions or services that your unit provides?</li>
          <li>How does the university operate more efficiently as a result of your service?</li>
          <li>How does the university support students more effectively as a result of your service?</li>
          <li>How does the university benefit from using your service?</li>
          <li>If your unit was operating at its highest possible level, what would happen?</li>
          <li>If your unit was operating at its highest possible level, what would <em>not</em> happen?</li>
        </ul>
        <h3>Collect and review current unit descriptions and institutional materials. These may come from:</h3>
        <ul>
          <li>Web or brochure descriptions.</li>
          <li>Mission and vision statements.</li>
          <li>External accreditation agencies.</li>
          <li>VP-level strategic plans.</li>
          <li>Institutional strategic plans.</li>
        </ul>
        <h3>Review other units’ goals:</h3>
        <ul>
          <li>What are the goals of related units at the university?</li>
          <li>What are the goals of similar units at other universities?</li>
        </ul>
        <h2>Example Goals</h2>
        <ul>
          <li>“To provide timely and accurate information via regular and ad hoc reporting of institutional data to internal and external audiences.”</li>
          <li>“To attract external funding from public and private sources to initiate and sustain large-scale research projects and support the initiation of students into the research community.”</li>
          <li>“To enhance community ties by hosting or supporting music performances, artistic exhibitions, and athletic events that build community support and alumni loyalty.”</li>
          <li>“To protect, promote, and advance the UTSA brand through strategic messaging, compelling storytelling, and creative integrated marketing solutions.”</li>
        </ul>
        <p>&nbsp;</p>
<p>&nbsp;</p>
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
