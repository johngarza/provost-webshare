<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Set Criteria for Success</strong></h1>
          <h2>Setting Benchmarks or Standards for Student Learning Outcomes</h2>
        <p>Assessment plans should specify a results-oriented standard or benchmark related to each method that indicates the minimum acceptable level of student performance. </p>
        <p> There are two general methods of setting performance standards or benchmarks:</p>
        <ol>
          <li>Student performance in the academic program can be compared to past levels of performance or to a different or broader group of students. Example benchmarks and questions to consider include the following:</li>
          <ul>
            <li>Internal Peer Benchmark: How do our students compare to others within UTSA?</li>
            <li>External Peer Benchmark: How do our students compare with those of other universities that are similar to UTSA?</li>
            <li>Best Practices Benchmark: How do our students compare to the best of their peers?</li>
            <li>Value-Added Benchmark: Are our students improving?</li>
            <li>Historical Trends Benchmark: Is our program improving?</li>
          </ul>
        
          <li>Students in the academic program can be compared to a specific level of performance. Examples levels and questions to consider include the following:</li>
          <ul>
            <li>Local Standards: Are students meeting our own standards?</li>
            <li>External Standards: Are students meeting standards set by someone else?</li>
          </ul>
        </ol>
        <h2>Guidelines to inform benchmark or standard selection:</h2>
        <ul>
          <li>Consider how the assessment results will be used: If the purpose of assessment is to improve the academic program, the standard for success for the SLO should be set relatively high.</li>
      <li>Consider the consequences of setting the bar too high or too low: If the bar is set too high, the program may not have the resources available to address all of the identified areas needing improvement. If the bar is set too low, students may graduate the program without acquiring key competencies.</li>
        </ul>
        <ul>
          <li>Consult external sources: Professional standards, potential employers, alumni, peer programs, etc. can all be used to set and justify program standards and benchmarks.</li>
       <li>Set performance levels that represent minimal competence for each dimension on a rubric: Program faculty may have higher expectations for some aspects of an assignment (e.g., grammar and word choice on a research paper) than for others (e.g., effectively integrating information from primary research sources).</li>
        <li>Consider previous assessment results: If student performance has historically been far below the program’s desired benchmark, adjust standards in the short-term to focus on continuous improvement towards the desired, more aggressive benchmark.</li>
        </ul>
        <h1 align="center">&nbsp;</h1>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
