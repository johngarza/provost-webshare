<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Using Assessment Results</strong></h1>
          
          <p>Using assessment results effectively is the most challenging, yet most critical, component of the assessment process. Assessment, by itself, does not result in improved student learning. Assessment results, along with professional judgment, must be reflected upon and <em>used</em> to make decisions that result in improved student learning.</p>
       <h2> Guidelines to ensure that the use of assessment results is fair, ethical, and responsible:</h2>
        <ol>
          <li>Make assessments planned and purposeful: There should be a clear understanding at the outset of why the program is engaging in assessment and the types of decisions that assessment will inform.</li>
          <li>Focus assessments on important learning goals.</li>
          <li>Assess teaching and learning processes, not just outcomes, in order to make sense of outcomes.</li>
          <li>Actively involve those with a stake in decisions stemming from the results in discussions about assessment and use of results.</li>
          <li>Communicate assessment information widely and transparently.</li>
          <li>Discourage others from making inappropriate interpretations of assessment results. For example, communicate the limitations of assessment techniques, sampling, and other factors that could affect the accuracy and replicability of the results.</li>
          <li>Don’t hold people accountable for things that they cannot control.</li>
          <li>Don’t penalize faculty and staff for disappointing assessment results.</li>
          <li>Don’t let assessment results alone dictate decisions. Decisions should be based on sound professional judgment. </li>
          <li>Promote the use of multiple sources of information when making decisions.</li>
          <li>Keep faculty, students, and staff informed on how assessment findings are being used to inform decisions.</li>
        </ol>
        
        <h2>What should be considered when assessment results indicate a ‘need for improvement’?</h2>
        <p>Assessment results may prompt consideration of changes to the following areas of the academic program:</p>
        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="312" rowspan="6" valign="top"><br />
              <strong>Changes to Curriculum</strong><strong> </strong></td>
            <td width="312" valign="top"><p>Changes   in teaching practices</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Revision/enforcement   of prerequisites</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Revision   of course sequence</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Revision   of course content</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Addition   of courses</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Deletion   of courses</p></td>
          </tr>
          <tr>
            <td width="312" rowspan="6" valign="top"><p><strong>Changes to Academic   Processes</strong><strong> </strong></p></td>
            <td width="312" valign="top"><p>Modification   of frequency or schedule of course offerings</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Improvements   to technology</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Changes   in personnel</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Additional   training or professional development</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Revision   of advising standards or processes</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Revision   of admission criteria</p></td>
          </tr>
          <tr>
            <td width="312" rowspan="3" valign="top"><p><strong>Changes to Assessment   Plan</strong><strong> </strong></p></td>
            <td width="312" valign="top"><p>Revision   of SLOs</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Revision   of measurement approaches</p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p>Collection   of additional data</p></td>
          </tr>
        </table>
        <h2>What should be considered when results indicate ‘acceptable’ levels of student performance?</h2>
         <p> First of all, CELEBRATE your program’s success and RECOGNIZE your faculty and staff!          </p>
         <p>Then, consider setting more challenging goals or targets. Remember, the purpose of assessment is to drive continuous improvement!</p>
  <h2>Documenting the Use of Assessment Results
  </h2>
        <p>Use the Use of Results for Improvement Report to summarize assessment results and to identify improvement strategies that the program <strong><em>plans to implement</em></strong>.</p>
        <p> A blank Use of Results for Improvement Report is accessible via the Academic Program Assessment main page: <a href="http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp">http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp</a>. </p>
        <p>Use the Implementation of Improvements Status Report to describe the <strong><em>progress made in</em></strong> <strong><em>implementing</em></strong> the identified improvement strategies.</p>
        <p> A blank Implementation of Improvements Status Report is accessible via the Academic Program Assessment main page: <a href="http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp">http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp</a>. </p>
<h1>&nbsp;</h1><p>&nbsp;</p><!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
