<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Identify Learning Opportunities <br />
        with Curriculum Mapping</strong></h1>
          <h2><strong>What is curriculum mapping?</strong>
            </p>
          </h2>
        <p>Curriculum mapping involves relating each program SLO to program courses, co-curricular programs, or other educational opportunities. Curriculum maps take the form of a matrix, with SLOs represented on one axis and program courses or other educational opportunities represented on the other axis. <br />
          For each SLO, indicate the course or educational opportunity where the relevant information is Introduced, Reinforced, and Assessed. Note: information related to an SLO may be Introduced, Reinforced, and/or Assessed within a single course or educational opportunity. Below is an example from the University of Hawaii depicting the mapping of an undergraduate program with three separate tracks:<br />
  <img src="/VPIE/assessment/images/curriculum-mapping.png" alt="other" width="607" height="623" /> <br />
        <h2>Curriculum Mapping in TracDat</h2>
        <p>In TracDat, curriculum mapping can be completed by navigating to ‘Curriculum Mapping’ underneath the ‘Mapping’ tab. The Office of Continuous Improvement and Accreditation will upload program course titles to TracDat and assign them to their related academic programs. Program courses and active SLOs will then automatically populate in the Curriculum Mapping area of TracDat. The application then allows users to click ‘I’, ‘A’, and/or ‘R’ to indicate whether a SLO is Introduced, Assessed, or Reinforced in each course, as shown below:</p>
        <p><img src="/VPIE/assessment/images/tracdat-map.png" alt="other" width="688" height="303" /></p>
        <p>A blank Curriculum Mapping Template word document is accessible via the Academic Program Assessment main page: <a href="http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp">http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp</a>   </p>
<p>&nbsp;</p>
        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
