<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Specify Program Mission and Goals</strong><br />
         </h1>
        <h2>How do you develop a mission statement?</h2>
        <p>Mission statements enable the academic program to define its purpose within the context of the greater institutional mission. The program mission statement should effectively communicate to internal and external stakeholders what the program is, what it does, and for whom. It should state in specific terms how the program contributes to the education and careers of its graduates. To develop a mission statement, encourage program faculty to discuss the following questions:</p>
        <ul>
          <li>What is the purpose of the program? For example, does it exist to prepare students for specific types of careers or graduate education?</li>
          <li>Who are your primary stakeholders? Indicate who benefits from the program and its graduates.</li>
          <li>What are the most important functions, activities, services, and offerings of the program?</li>
          <li>How does the program support the missions of the institution, college, and department?</li>
        </ul>
        <p>Once the unit has drafted a statement addressing the preceding questions, ensure that the resulting statement is specific and unique enough that it differentiates the academic program from others.</p>
        <h3>Example Mission Statements:</h3>
        
        <ul>
          <li><strong><u>BA Economics:</u></strong> “To provide students with the opportunity to gain the necessary theoretical and quantitative tools in economics such that they can understand and apply economics in their daily lives, seek advanced degrees in economics, pursue careers in the global marketplace, and engage in public policymaking.”</li>
       
          <li><strong><u>MS Civil Engineering:</u></strong> “To produce graduates who are capable of research and professional practice in a specialized area of Civil Engineering, namely environmental engineering, geotechnical engineering, structural engineering, transportation engineering, and water resources engineering.”</li>
      
          <li><strong><u>EdD Educational Leadership:</u></strong> “To prepare educators to become transformational leaders who can work effectively in diverse, ambiguous, and challenging contexts. The goals of this transformational leadership include equity, excellence, social justice, democracy, risk-taking, and responsiveness to community needs.”</li>
        </ul>
       <h2>What are program goals?</h2>
        <p>Successful academic program assessment depends upon the determination of shared goals specifying what the program intends to accomplish. </p>
        <p>‘Goals’ can be defined in several ways. This handbook focuses on the development of student learning goals at the academic program level, which describe in broad terms how we want students to be different as a result of completing the academic program (Suskie, 2009). More specifically, student learning goals specify the <em>knowledge, values, or skills</em> we want students to have acquired throughout the program. </p>
        <p> Of course, programs may have additional goals unrelated to student learning (e.g., attaining a specialized accreditation, minimizing time to degree, increasing enrollments, etc.). In any case, program goals should align with department, college, and institutional goals. Program goals do not have to be directly measurable (this will be achieved with related Student Learning Outcomes) and may be aspirational, representing what the program hopes to accomplish over the next several years. </p>
        <p>  Shared goals provide the foundation for the program’s curricula and guide the selection of questions to address via assessment.</p>
        <h2>How do you develop program goals?</h2>
        <p>Achieving consensus for program goals can be challenging, as program faculty may have varied perspectives on student learning, disciplinary subspecialties, and teaching techniques. The following activities can serve as a starting point to brainstorm potential program goals.</p>
          <h3>Engage program faculty in the following discussions:</h3>
        <ul>
          <li>Describe the ‘ideal student’ graduating from your program.</li>
          <ul>
            <li>What does this student know?</li>
            <li>What can this student do?</li>
            <li>What does this student care about?</li>
            <li>What program experiences contributed the most to the development of this ideal student?</li>
          </ul>
          <li>What achievements do you expect of all graduates from your program?</li>
          <li>Describe program alumni in terms of their achievements.</li>
        </ul>
        <h3>Collect and review current program descriptions and institutional materials. These may come from:</h3>
        <ul>
          <li>Catalog and brochure descriptions.</li>
          <li>Program review reports.</li>
          <li>Mission and vision statements.</li>
          <li>External accreditation agencies.</li>
          <li>Departmental or college strategic plans.</li>
          <li>Institutional strategic plans.</li>
        </ul>
        <h3>Collect and review course and instructional materials, such as:</h3>
        <ul>
          <li>Syllabi and course descriptions from core program courses or capstone courses.</li>
          <li>Assignments and tests in core program courses.</li>
          <li>Discipline-specific textbook introductory or summary sections.</li>
        </ul>
        <h3>Review other programs’ goals</h3>
        <ul>
          <li>What are the program goals of other program in your department or college?</li>
          <li>What are the programs goals of similar programs at other universities?</li>
        </ul>
        <h3>Use the Delphi method to Achieve Consensus</h3>
        <ul>
          <li>Create a list of all identified potential learning goals for a program</li>
          <li>Distribute the list to program faculty and ask them to check off the goals they believe are ‘key’ to the program.</li>
          <li>Tabulate and present the results to program faculty.</li>
          <li>A few goals may emerge as clear contenders for the program, but the process may be repeated until consensus is achieved.</li>
        </ul>
        <h2>How do you write program goals?</h2>
         <p> There is not necessarily one ‘right’ way to write program goal statements, but the following is a common structure:<br />
        &nbsp;</p>
        <p align="center">
        <strong>“To (ACTION VERB) + Students’+ (Program-specific knowledge, values, or skills)”</strong></p>
        
        <h3>Example Goal Statements: </h3>
<ul>
  <li>“To develop students’ managerial, interpersonal, organizational, communications, analytical, and diagnostic skills.”</li>
  <li>“To prepare students for careers in the Accounting industry.”</li>
  <li>“To teach students to work and lead effectively in diverse and international teams and organizations.”</li>
</ul>
<p>&nbsp;</p>
        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
