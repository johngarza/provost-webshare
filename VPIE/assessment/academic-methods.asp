<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Determine Methods</strong></h1>
         <p> Selection of assessment methods can only be accomplished after, first, clearly articulating program student learning outcomes (SLOs) and, second, ensuring that the academic program curriculum provides students with learning experiences relevant to the SLOs. Assessment methods should be directly tied to program SLOs and curriculum. </p>
           <h2>Types of Assessment Methods</h2>
        <p>Assessment methods are typically categorized as direct or indirect: <br />
          <strong><u>Direct methods</u></strong> of assessing student learning call for students to demonstrate their acquired knowledge, values, or skills. Assessment plans should incorporate at least one direct method of assessment for each specified SLO.<br />
          <strong><u>Indirect methods</u></strong><strong> </strong>of assessing student learning measure students’ <em>perceptions of</em> or <em>satisfaction with </em>their learning experience. Indirect methods can support and contextualize direct methods of assessment.</p>
        <p>The following assessment method inventory lists potential direct and indirect strategies to consider:</p>
        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="623" colspan="2" valign="top"><br />
              <strong>Direct Assessment Methods</strong><strong> </strong></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Course Data</strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Objective   Tests (e.g., multiple choice, true-false, fill-in-the-blank)</li>
              <li>Essay   Tests</li>
              <li>Embedded   Questions and/or Assignments</li>
              <li>Other   Classroom Assessment Techniques (e.g., 1-minute papers, free-writing, etc.)</li>
            </ul></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Individual   Projects/Performance</strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Written   Products (e.g., term papers, lab reports, critiques)</li>
              <li>Oral   Presentations (e.g., speeches, role plays)</li>
              <li>Poster   Presentations</li>
              <li>Structural/Situational   Assessments</li>
            </ul></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Summative (End of   Program) Performance</strong><strong> </strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Standardized   Tests</li>
              <li>Locally-Developed   Exams</li>
              <li>Capstone   Experiences</li>
              <li>Internships/Professional   Applications</li>
              <li>Portfolios</li>
            </ul></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Collaborative Activities</strong><strong> </strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Research   and Group Projects (written and oral)</li>
              <li>Online   Group Activities (e.g., records of interactions in discussion forums) </li>
            </ul></td>
          </tr>
          <tr>
            <td width="623" colspan="2" valign="top"><p align="center"><strong>Indirect Assessment Methods</strong><strong> </strong></p></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Self-Assessment/Reflection</strong><strong> </strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Student   Journals</li>
              <li>Self-Critiques</li>
            </ul></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Interviews and Surveys</strong><strong> </strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Satisfaction   Measures (e.g., seniors, alumni, employers, graduate school advisors,   parents)</li>
              <li>Performance   Reviews (e.g., alumni, employers, graduate school advisors)</li>
              <li>Exit   Interviews</li>
              <li>Focus   Groups</li>
              <li>Follow-up   Alumni Interviews</li>
              <li>External   Reviewer Interviews (conducted by objective, external expert)</li>
            </ul></td>
          </tr>
          <tr>
            <td width="312" valign="top"><p><strong>Archival Measures</strong><strong> </strong></p></td>
            <td width="312" valign="top"><ul>
              <li>Transcript   Analysis</li>
              <li>Syllabus   Audit</li>
              <li>Library   or Resource Use Statistics</li>
            </ul></td>
          </tr>
        </table>
        <p><strong>&nbsp;</strong></p>
        <h2>Tips for Selecting Assessment Methods</h2>
        <ul>
          <li>When selecting an assessment method, ask the following questions:</li><ol>
          <li>Will the assessment strategy answer questions that are important and meaningful to the program?</li>
          <li>Does the strategy align with the outcome being assessed?</li>
          <li>Is the strategy feasible given available financial resources and time?</li>
          <li>Will the strategy result in useful information about the strengths and weaknesses of the program?</li>
        </ol>
       <li>Use existing information whenever possible: Exams, assignments, or projects in key program courses can be used for program-level assessment if they are consistent across course sections and representative of program requirements.</li>
        <li>Use capstone experiences or senior course assignments: These are typically common to all students completing the program and demonstrate the breadth and depth of students’ acquired knowledge and skills.</li>
      <li>Strive to use multiple measures to assess each SLO: This increases confidence that the results through assessment are accurate, consistent, and replicable.</li>
      <li>Don’t reinvent the wheel: Take advantage of published assessment tools in your discipline, such as rubrics or surveys, as opposed to developing your own.</li>
        </ul>
        <h2>FAQ: Why can’t we use course grades as our program assessment method?</h2>
        <p>Course grades are useful to evaluate individual students’ performance in a course. Course grades do not demonstrate what, specifically, students have learned (or not learned) from a course and may incorporate additional criteria, such as attendance, participation, and effort, that do not directly reflect learning. Academic program assessment examines <em>patterns</em> of student learning across courses and requires the use of direct measures of learning to identify what students have learned (and not learned) and to drive improvements at the program level.</p>
<h1 align="center">&nbsp;</h1>
        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
