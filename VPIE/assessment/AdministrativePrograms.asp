<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Administrative Program Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       	<h1 align="center">Administrative Programs Assessment        </h1>
       	<p>Administrative units (i.e., all areas ‘outside of the classroom’) at UTSA provide integral administrative and student support services that enable the university to carry out its mission. All administrative units participate in assessment activities designed to drive continuous improvement of university functions and services and to further student success in accordance with the university’s mission and strategic initiatives.</p>
<h2>Forms</h2>
        <ul>
          <li><a href="docs/Admin (Non-Acad) Assessment Report Template.doc">Administrative Assessment Report Template</a></li>
       </ul>
       	<h2>Steps of Administrative Program Assessment</h2>
        <ol>
          <li><a href="admin-mission.asp">Specify Unit Mission and Goals</a>- Describe the purpose of your unit and what it hopes to achieve long-term.</li>
          <li><a href="admin-outcomes.asp">Define Outcomes</a>- Describe the desired end-results and quality of your unit’s primary services and functions.</li>
          <li><a href="admin-methods.asp">Determine Methods</a>- What methods will be used to identify the strengths and weaknesses of the unit and determine whether the unit is making progress towards its goals? </li>
          <li><a href="admin-criteria.asp">Set Criteria for Success</a>- Establish the level of expected unit performance.</li>
          <li><a href="admin-report-results.asp">Collect Data and Report on the Results</a>- Examine the information collected and evaluate results against unit expectations.</li>
          <li><a href="admin-use-results.asp">Use the Results</a>- How is the information going to be used to make improvements to the unit?<strong>&nbsp;</strong></li>
        </ol>
        <h2>Questions to Ask During the Assessment Cycle</h2>
        <ul>
          <li>Do current assessment efforts measure what is intended?</li>
          <li>Are results being used effectively? If not, maybe it is time to retire some of the assessment tools.</li>
          <li>Are there better ways to collect useful, needed data?</li>
          <li>What options are available to assess things differently?</li>
        </ul>
<p>&nbsp;</p>
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
