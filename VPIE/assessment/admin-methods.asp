<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Administrative Program Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <p align="right"><a href="AdministrativePrograms.asp">Back to Steps of Administrative <br />
        Program Assessment</a></p>
       	<h1 align="center">Administrative Programs Assessment        </h1>
       	<h1 align="center"><strong>Determining Methods</strong><br />
       	  </h1>
       	<p>Assessment methods indicate how the administrative unit plans to collect data related to each outcome. Methods should logically align with the outcome in question and should also be reliable (i.e., produce replicable results), useful, and efficient.</p>
        
        <h2>Types of Assessment Methods </h2>
        <p>Assessment methods are typically categorized as direct or indirect: <br />
          <strong><u>Direct methods</u></strong> of assessing operational or strategic outcomes include measurements of the demand, quality, efficiency, and effectiveness of key functions and services. <br />
          Direct administrative assessment methods include, but are not limited to:</p>
        <ul>
          <li>Number of complaints<strong></strong></li>
          <li>Number of errors; error rate<strong></strong></li>
          <li>Number/percentage change of applications<strong></strong></li>
          <li>Number/percentage change of users<strong></strong></li>
          <li>Number of training sessions<strong></strong></li>
          <li>Growth in attendance<strong></strong></li>
          <li>Number/amount/percentage increase of donations<strong></strong></li>
          <li>Number of new alumni donors<strong></strong></li>
          <li>Timeliness of response<strong></strong></li>
          <li>Level of compliance<strong></strong></li>
          <li>Average service time<strong></strong></li>
          <li>Average wait time<strong></strong></li>
          <li>Auditor’s findings<strong></strong></li>
          <li>Pre- and post-workshop tests<strong></strong></li>
        </ul>
        <p><strong><u>Indirect methods</u></strong><strong> </strong>of assessing operational or strategic outcomes include measurements of customers’ <em>perceptions of </em>or <em>satisfaction with</em> key functions and services. Indirect methods can support and contextualize direct methods of assessment.<br />
          Indirect administrative assessment methods include, but are not limited to:</p>
        <ul>
          <li>Satisfaction surveys<strong></strong></li>
          <li>Participant feedback<strong></strong></li>
          <li>Staff training hours<strong></strong></li>
          <li>Focus groups<strong></strong></li>
          <li>Opinion surveys<strong></strong></li>
          <li>Awareness surveys<strong></strong></li>
        </ul>
        <h2>Tips for Selecting Assessment Methods</h2>
        
          <p>When selecting an assessment method, ask the following questions:</p>
          <ul><li>Will the assessment strategy answer questions that are important and meaningful to the unit?</li>
          <li>Does the strategy align with the outcome being assessed?</li>
          <li>Is the strategy feasible given available financial resources and time?</li>
          <li>Will the strategy result in useful information about the strengths and weaknesses of the unit?</li>
        <li>Use existing information whenever possible.</li>
        <li>Strive to use multiple measures to assess each outcome: This increases confidence that the results through assessment are accurate, consistent, and replicable.</li>
     <li>Don’t reinvent the wheel: Take advantage of published assessment tools in your area, such as rubrics or surveys, as opposed to developing your own. </li>
        </ul>
        <p>&nbsp;</p>
<p>&nbsp;</p>
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
