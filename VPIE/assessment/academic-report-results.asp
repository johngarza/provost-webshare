<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Reporting Assessment Results</strong></h1>
        <p> For assessment results to be used, they first need to be summarized, compared against specified benchmarks and targets, and reported.</p>
        <h2>Summarize Assessment Results
        </h2>
        <ul>
          <li><strong><u>Tallies:</u></strong> Take a simple count of the number of students demonstrating a particular type or level of performance. For example, you could tally the number of students who earned a specific rubric rating or selected a specific response on an exam question.</li>
      <li><strong><u>Percentages:</u></strong> Percentages are typically more meaningful than presenting raw numbers (tallies) and facilitate peer and historical comparisons across different groups of students. For example, you could report the percentage of students who earned a specific rubric rating or selected a specific response on an exam question and examine the percentage change in these values over time.</li>
        <li><strong><u>Aggregates:</u></strong> In many cases, multiple items on a rubric, exam, or survey relate to a single SLO. In those cases, it is appropriate to report tallies or percentages of students who exhibited a particular type or level of performance across all of the relevant items. For example, perhaps 80% of students correctly responded to the four exam questions targeting understanding of Research Design, but 40% responded correctly to the two questions targeting understanding of Quantitative Analysis. </li>
       <li><strong><u>Averages:</u></strong> Averages, including the arithmetic mean, median (i.e., the middle score), and mode (i.e., the most frequent response) can be used to summarize the central tendency of assessment results and to compare results to national benchmarks. </li>
      <li><strong><u>Qualitative Summaries:</u></strong> Qualitative assessment methods (e.g., reflective writing, focus groups, open-ended survey questions) can be analyzed via read-throughs and grouped listings. Read-throughs simply involve quickly reading through qualitative results to get a general sense of common responses. Grouped listings involve separating or tallying qualitative results into common, discrete categorizes (e.g., perhaps 10% of alumni indicated that performing a group research project was most influential to their learning in the program on an open-ended survey item, whereas 60% mentioned that participating in a practicum experience was most influential). </li>
        </ul>
        <p>Use the Academic Assessment Report Template to report academic program assessment results.<br />
          A blank Academic Assessment Plan Template is accessible via Academic Program Assessment main page: <a href="http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp">http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp</a>. </p>
        <h2>Evaluate Assessment Results</h2>
        <p>Assessment reports should include a description of the sample of students participating in assessment and a summary of the assessment results attained from each method. After the assessment results have been appropriately summarized, they should be compared against the associated benchmark or target. <br />
          Then, for each result, make a determination of:</p>
        <ul>
          <li><strong><u>“Acceptable”:</u></strong> The finding meets the minimal acceptable level of student performance.</li>
        </ul>
        <p align="center">OR</p>
        <ul>
          <li><strong><u>“Needs Improvement”:</u></strong> The finding does not meet the minimal acceptable level of student performance. Student performance needs to be improved.</li>
        </ul>
        <strong><u>NOTE:</u></strong>The goal of academic program assessment is to drive continuous improvement. Identifying areas in need of improvement does not constitute ‘failure’ and, on the contrary, is essential to guide forward action and promote positive change. <strong><u>ACADEMIC PROGRAMS WILL NOT BE JUDGED ON THEIR RESULTS</u></strong>. However, programs will be evaluated on the extent to which they use assessment results to make meanin
        <p>&nbsp;</p><!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
