<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Administrative Program Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="AdministrativePrograms.asp">Back to Steps of Administrative <br />
        Program Assessment</a></p>
       	<h1 align="center">Administrative Programs Assessment        </h1>
       	<h1 align="center"><strong>Defining Outcomes</strong></h1>
        <h2>What are Administrative Outcomes?</h2>
        <p>Administrative outcomes are specific, measurable statements describing the desired end-result and quality (e.g., timeliness, accuracy, responsiveness, frequency, etc.) of the key functions and services of the administrative unit. Outcomes should be directly related to the mission and goals of the unit. Administrative units may specify operational outcomes, which focus on the impact of their functions and services to their customers, strategic outcomes, which focus on the future plans of the unit, and/or student learning outcomes, which focus on the development of students’ knowledge, values, or skills.</p>
        <p> For information regarding how to develop student learning outcomes, visit the Academic Program Assessment main page: <a href="http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp">http://provost.utsa.edu/vpie/assessment/Academic_Programs.asp</a></p>
        <h2>Outcomes should be SMART:</h2>
        <u><strong>S</strong></u><strong>pecific
        </h2>
        </strong>
        <ul>
          <li>Stated in definite language, outcomes should describe the specific functions and services provided to customers or stakeholders.  </li>
        </ul>
        <p><u><strong>M</strong></u><strong>easurable</strong></p>
        <ul>
          <li>Data related to the outcome should be readily available, and the data collection process should be feasible considering available time and resources.</li>
        </ul>
        <p><u><strong>A</strong></u><strong>ggressive but <u>A</u>ttainable</strong></p>
        <ul>
          <li>In the spirit of continuous improvement, units should determine an assessable criterion for success or benchmark for the outcome that will progressively move the unit closer to achieving its goals.</li>
        </ul>
        <p><u><strong>R</strong></u><strong>esults-oriented and <u>T</u>ime-bound</strong></p>
        <ul>
          <li>Outcomes should specify what the expected level of unit performance should be after a finite period of time (e.g., 5% improvement in customer satisfaction rates in the next year). These specifications may be based on experience, previous assessment results, external requirements, local, state, or national benchmarks, etc.</li>
        </ul>
        <h2>Example Outcomes</h2>
        <p>The following table, retrieved from the University of Texas Rio Grande Valley Office of Accreditation and Assessment, provides examples for translating key functions and services into measurable outcomes.</p>
<p><img src="images/example-outcomes.png" width="682" height="662" /></p>
<h2>FAQ: How many outcomes does my unit need to assess?</h2>
<p>This decision is entirely up to unit administrators and staff. Each goal should be assessed with at least one outcome and corresponding assessment method, and goals should be representative of the key functions and services of the unit. </p>
<p>&nbsp;</p>
<p>&nbsp;</p>
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
