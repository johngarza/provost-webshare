<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>TracDat | VPIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       	<h1 align="center">TracDat </h1>
       	<p>TracDat software is used for documenting planning and assessment activities
					undertaken by each administrative unit, college, department, and academic program as identified by each Vice Presidential area at UTSA. </p>
       	<p>&nbsp;</p>
       	<p><a href="https://tracdat.utsa.edu/tracdat/" target="_new">TracDat Login</a>       	</p>
       	<p><a href="docs/Academic-Program-TracDat-User-Guide.pdf">Academic Program TracDat User Guide</a></p>
       	<p><a href="docs/Administrative-Program-TracDat-User-Guide.pdf">Administrative Program TracDat User Guide</a></p>
       	<h2 align="center">TracDat Videos/Tutorials</h2>
        <ul>
					<li>Introduction to TracDat
						<br/>
						<div style="text-align:center; padding-bottom:0.75rem;">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/HP9ckTNoW7E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</li>
					<li>Updating Assessment Plan Information in TracDat
						<br/>
						<div style="text-align:center; padding-bottom:0.75rem;">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/NXeFISehwgo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</li>
					<li>
						Recording Assessment Results and Improvement Activities in TracDat
						<br/>
						<div style="text-align:center; padding-bottom:0.75rem;">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/zPeMTSWfnts" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</li>
					<li>Making Assignments in TracDat
						<br/>
						<div style="text-align:center; padding-bottom:0.75rem;">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/dNEEd9jclu8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</li>
					<li>Generating Reports of Assessment Activities in TracDat
						<br/>
						<div style="text-align:center; padding-bottom:0.75rem;">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/mx_ZU5btebg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</li>

        </ul>
        <p>For access requests or other questions, contact <a href="mailto:lisa.johnston@utsa.edu">Lisa Johnston</a>.</p>
<p>&nbsp;</p>

        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
