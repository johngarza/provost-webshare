<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>General Resources | Assessment | VPIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
   	    <h1 align="center">Resources        </h1>
   	     <div class="size1of2">
        <div class="gutter">
<h2 align="justify"><a href="#process">• Assessment Process</a></h2>
<h2><a href="#examples">• Simple Assessment Example</a></h2>
          <h2 align="justify"><a href="#strategic">• Strategic Planning</a></h2></div></div>
 <div class="size2of2">
        <div class="gutter">
        <h2 align="justify"><a href="#myths">• Common Assessment Myths</a></h2>
        <h2 align="justify"><a href="#commonterms">• Common Assessment Terms</a></h2><p>&nbsp;</p><p>&nbsp;</p></div></div>
 <hr />
 <h2>Assessment Handbooks   </h2>
 <ul><li><a href="docs/UTSA_AcademicHandbook.pdf">Academic Assessment Handbook </a></li>
 <li><a href="docs/UTSA_AdministrativeHandbook.pdf">Administrative Assessment Handbook </a></li></ul>
 <hr />
 <h2 align="justify">Assessment Process<a name="process" id="process"></a></h2>
        <blockquote>
          <p align="justify"><strong>Goals:</strong> What do you want to do or achieve overall? Goals should be tied to your mission, vision, and strategic plan. Example:</p>
          <ul>
            <li>To provide useful training and support for employees regarding strategic planning and assessment.</li>
          </ul>
          <p align="justify"><strong>Outcomes:</strong> What needs attention or what could be better? You must determine what the desired result/improvement is for your department. Examples: 
          </p>
          <ul>
            <li>Staff will indicate the trainings have increased their knowledge of assessment methods.</li>
            <li>Departments will submit quality assessment plans reflecting their desired elements.</li>
          </ul>
          <p align="justify"><strong>Methods:</strong> What will success look like, and how can it be measured? What will be different if the outcome is reached?</p>
          <p align="justify"><strong>Criteria of Excellence:</strong> What is your criteria for success regarding each outcome? You must have a threshold in order to  measure the success of your outcomes. Examples:</p>
          <ul>
            <li>85% of training participants will accurately identify direct versus indirect assessment methods.</li>
          </ul>
          <p><strong align="justify">Results:</strong> What was the output once the assessment plan was implemented? This data should then be used to enhance your assessment plan.</p>
          <p align="justify"><strong>Action Plan/Use of Results:</strong> What do our results mean? What are the next steps to be taken to improve these results? This information will allow you to create an action plan. Examples:</p>
          <ul>
            <li>Reformat the content of training based on knowledge gaps.</li>
            <li>Conduct survey for other training needs.</li>
          </ul>
        </blockquote>
        <p align="justify"><a href="#top">Back to Top</a></p>
        
<hr />
<h2 align="justify">Simple Assessment Example<a name="examples" id="examples"></a></h2>
<blockquote>
  <p align="justify"><strong>Goal: </strong>Improve my health.</p>
  <p align="justify"><strong>Outcome:</strong> I will lose five pounds in the next month.</p>
  <p align="justify"><strong>Method:</strong> Use scale to determine if weight was lost.</p>
  <p align="justify"><strong>Action Plan: </strong>Did not meet goal therefore I will now exercise three times a week.</p>
</blockquote>
<p align="justify"><a href="#top">Back to Top</a></p>
<hr />
        <h2 align="justify">Strategic Planning<a name="strategic" id="strategic"></a></h2>
        <p align="justify"><a href="file:///C|/Users/vfv912/Downloads/PrinciplesofGoodPracticeforAssessingStudentLearning.doc" target="_blank"></a>Institutions create a streamlined planning process which strategically connects academic departments and units to achieve overall goals set by the University. Strategic planning is necessary to set direction and to ensure goals are reached based on the University's mission and vision. The Office of Strategic Planning and Assessment provides the resources and tools needed to create and implement a strategic plan.</p>
        <p align="justify"><a href="#top">Back to Top</a></p>
        <hr />        <h2 align="justify">Common Assessment Myths<a name="myths" id="myths"></a></h2>
        <p align="justify">Assessment can often be viewed as an overwhelming process and the purpose can be misconstrued. The University of Central Florida (2008) compiled most common assessment misconceptions and detailed how assessment should be viewed by dispelling the myths.</p>
        <div title="Page 3">
          <div>
            <div>
<blockquote>
  <p><strong>Myth 1</strong>: The results of assessment will be used to evaluate faculty performance. </p>
  <blockquote>
    <p>Nothing could be further from the truth. Faculty awareness, participation, and ownership are essential for successful program assessment, but assessment results should never be used to evaluate or judge individual faculty performance. The results of program assessment are used to improve programs. </p>
  </blockquote>
  <p><strong>Myth 2</strong>: Our program is working well, our students are learning; we don’t need to bother with assessment. </p>
  <blockquote>
    <p>The primary purpose of program assessment is to improve the quality of educational programs by improving student learning. Even if you feel that the quality of your program is good, there is always room for improvement. In addition, various accrediting bodies mandate conducting student outcomes assessment. For example, the Southern Association of Colleges and Schools (SACS) requires that every program assess its student outcomes and uses the results to improve programs. Not to conduct assessment is not an option. </p>
  </blockquote>
  <p><strong>Myth 3</strong>: We will assign a single faculty member to conduct the assessment. Too many opinions would only delay and hinder the process. </p>
  <blockquote>
    <p>While it is a good idea to have one or two faculty members head the assessment process for the department, it is really important and beneficial to have all faculty members involved. Each person brings to the table different perspectives and ideas for improving the academic program. Also it is important that all faculty members understand and agree to the mission (i.e., purpose) and goals of the academic program. </p>
  </blockquote>
  <p><strong>Myth 4</strong>: The administration might use the results to eliminate some of the department’s programs. </p>
  <blockquote>
    <p>There are two types of evaluation processes: summative and formative. The purpose of summative program evaluation is to judge the quality and worth of a program. On the other hand, the purpose of formative program evaluation is to provide feedback to help improve and modify a program. Program assessment is intended as a formative evaluation and not a summative evaluation. The results of program assessment will not be used to eliminate programs. </p>
  </blockquote>
  <p><strong>Myth 5</strong>: Assessment is a waste of time and does not benefit the students. </p>
  <blockquote>
    <p>The primary purpose of assessment is to identify the important objectives and learning outcomes for your program with the purpose of improving student learning. Anything that enhances and improves the learning, knowledge and growth of your students cannot be considered a waste of time.            </p>
  </blockquote>
</blockquote>
<div title="Page 4">
              <div>
                <div>
                  <blockquote>
                      <p><strong>Myth 6</strong>: We will come up with an assessment plan for this year and use it every year thereafter. </p>
                    <blockquote>
                        <p>For program assessment to be successful, it must be an ongoing and continuous process. Just as your program should be improving, so should your assessment plan and measurement methods. Each academic department must look at its programs and its learning outcomes on a continual basis and determine if there are better ways to measure student learning and other program outcomes. Your assessment plan should be continuously reviewed and improved. </p>
                    </blockquote>
                    <p><strong>Myth 7</strong>: Program assessment sounds like a good idea, but it is time- consuming and complex. </p>
                      <blockquote>
                        <p>It is impossible to &ldquo;get something for nothing.&rdquo; Effective program assessment will take some of your time and effort, but there are steps that you can follow that can help you to develop an assessment plan that will lead to improving student learning. </p>
                      </blockquote>
                  </blockquote>
</div>
              </div>
              </div>
</div>
          </div>
        </div>
<p align="justify">Source: University of Central Florida. (2008). Academic Program Assessment Handbook. Retrieved from UEP Assessment Handbook<a href="#"> http://oeas.ucf.edu/doc/acad_assess_handbook.pd</a>f .</p>
        <p align="justify"><a href="#top">Back to Top</a></p>
        <hr />
        <h2 align="justify">Common Assessment Terms<a name="commonterms" id="commonterms2"></a></h2>
        <blockquote>
          <p><strong>Academic Program<br />
            </strong>An academic program is a program of study over a period of time that leads  to a degree. <br />
            <em>Examples:
          BBA-Marketing, BA–History, BS- Biology, MBA– Finance, MS-  Statistics, PhD- Environmental Science and Engineering</em></p>
          <p><strong>Academic Success Measures<br />
            </strong>Typically associated with measures of retention, grades, and transfer rates;  these success measures do not give us information about what students have learned and therefore should not be confused with student learning outcomes.<br />
          </p>
          <p><strong>Assessment  plan</strong><br />
            An  assessment plan describes the student learning outcomes to be achieved, a  description of the direct and indirect assessment methods used to evaluate the  attainment of each outcome, the intervals at which evidence is collected and  reviewed, and the individual(s) responsible for the collection/review of  evidence. </p>
          <p><strong>Assessment Instruments </strong><br />
            Assessment  instruments are used to gather data about student learning. These instruments can  be either quantitative or qualitative, and may be both traditional tests  (multiple choice, essay, or other formats), as well as to alternative forms of  assessment such as oral examinations, group problem solving, performances, and  demonstrations, portfolios, peer observations, etc. The most important factor  in choosing an instrument is ensuring that it (a) is gathering information  about the desired outcome, not something else and, (b) that the results  gathered from using it can be used to make improvements.</p>
          <p><strong>Benchmark<br />
          </strong>A description or example of  student or institutional performance that serves as a standard of comparison  for evaluation or judging quality. Benchmarks can be &ldquo;internal&rdquo; (i.e.  comparing performance against past performance) as well as &ldquo;external&rdquo; (i.e. comparing  performance against the performance of another institution/department/program). </p>
          <p><strong>Direct </strong><strong>Measures  of Learning</strong><strong></strong><br />
            Direct  measures require students to demonstrate their knowledge, skills and abilities. Examples: </p>
          <ul>
            <li>standardized exams</li>
            <li>locally developed exams</li>
            <li>essays scored through a common rubric or scoring matrix</li>
            <li>capstone experiences</li>
            <li>portfolios, performance on state and national licensure exams</li>
            <li>review of performances in the arts<br />
            </li>
          </ul>
          <p><strong>Indirect  Measures of Learning </strong><br />
            Indirect measures asks students to reflect on their learning rather than to demonstrate it. Examples:</p>
          <ul>
            <li>alumni, employer and student surveys</li>
            <li>exit interviews of graduates</li>
            <li>graduate follow-up studies</li>
            <li>focus groups</li>
          </ul>
          <p><strong>Formative  Assessment</strong> <br />
            Formative  Assessment methods involves  gathering  information about student-learning outcomes during the progression of a course or program to improve student-learning</p>
          <ul>
            <li>teacher observations</li>
            <li>analysis of student work</li>
            <li>feedback on assignments</li>
            <li>group discussions</li>
            <li>portfolios, oral presentations</li>
            <li>peer assessment</li>
            <li>student journals</li>
          </ul>
          <p><strong>Summative  Assessment</strong><br />
            Summative Assessment methods involves  gathering  information about student-learning at the conclusion of a course or program to improve student-learning<br />
          </p>
          <ul>
            <li>standardized senior exit exams</li>
            <li>juried review of essays </li>
            <li>senior exit interviews</li>
            <li>performance on state and national licensure exams.</li>
          </ul>
          <p><strong>Learning Goal</strong><br />
            Statements that describe broad learning concepts, for  example clear communication, problem solving, and ethical awareness. A  description of what our students <u>will be</u> or what our students <u>will  have</u> upon successfully completing the Program.<br />
            <em>Example: Know and apply basic research  methods in psychology, including research design, data analysis, and  interpretation.</em></p>
          <p><strong>Learning Outcome</strong><br />
            Examine what a <strong>student</strong> (or other stakeholders) is to do, think or feel as a result of the program, course, service.</p>
          <p><strong>Program Outcome (Operational)</strong><br />
            Examine what a<strong> program or process</strong> is to do, achieve or accomplish for its own improvement; generally needs/satisfaction driven. <br />
            <em></em></p>
          <p><strong>Methods  of Assessment</strong><br />
            Techniques  or instruments used in assessment. </p>
          <p><strong>Qualitative Assessment Methods</strong><br />
            Methods  which rely on descriptions rather than numerical analyses. Examples are ethnographic  field studies, logs, journals, participant observation, interviews and focus  groups, and open-ended questions on interviews and surveys. The analysis of  qualitative results requires non-statistical skills and tools. </p>
          <p><strong>Quantitative Assessment Methods</strong><br />
            Methods  which rely on numerical scores or ratings such as surveys, inventories,  institutional/departmental data, departmental/course-level exams (locally  constructed, standardized, etc.). In order to analyze quantitative results, either  descriptive or inferential statistics are needed.<br />
            <br />
            <strong>Portfolio</strong><br />
            An  accumulation of evidence about individual proficiencies, especially in relation  to learning outcomes. Examples include but are not limited to samples of  student work including projects, journals, exams, papers, presentations, videos  of speeches and performances. The evaluation of portfolios requires the  application of tools using the judgment of experts (faculty members). </p>
          <p><strong>Reliability</strong><br />
            Reliable  measures are measures that produce consistent responses over time.</p>
          <p><strong>Rubrics</strong> (Scoring Guidelines)<br />
            A set  of categories that define and describe the important components of the work  being evaluated. Each category contains a level of completion or competence  with a score assigned to each level and a clear description of what criteria  need to be met to attain the score at each level. Written and shared for  judging performance to differentiate levels of performance, and to anchor  judgments about the degree of achievement.</p>
          <p><strong>Student Learning Outcome Assessment Cycle<br />
            </strong>An institutional pattern of identifying outcomes,  assessment, and improvement plans based on the assessment. <br />
          </p>
          <p><strong>Teaching-Improvement  Loop</strong><br />
            Teaching,  learning, outcomes assessment, and improvement may be defined as elements of a  feedback loop in which teaching influences learning, and the assessment of  learning outcomes is used to improve teaching and learning.</p>
          <p><strong>Triangulation</strong><br />
            A method where multiple assessment methods are used to provide a  more complete description of student learning. An example of triangulation  would be an assessment plan for a learning outcome that incorporated surveys,  interviews, and observations of student behavior.</p>
          <p><strong>Validity</strong><br />
            As  applied to a test refers to a judgment concerning how well a test/instrument  does in fact measure what it purports to measure.</p>
        </blockquote>
        <p><a href="#top">Back to Top</a></p>
        <p align="justify">&nbsp;</p>

        <p align="justify">&nbsp;</p>

       

        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
