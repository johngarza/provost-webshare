<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Administrative Program Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="AdministrativePrograms.asp">Back to Steps of Administrative <br />
        Program Assessment</a></p>
       	<h1 align="center">Administrative Programs Assessment        </h1>
       	<h1 align="center"><strong>Using Assessment Results</strong><br />
       	  </h1>
       	<p>Using assessment results effectively is the most challenging, yet most critical, component of the assessment process. Assessment, by itself, does not result in improvements to functions and services. Assessment results, along with professional judgment, must be reflected upon and <em>used</em> to make decisions that result in improvements.<br />
  <strong>Guidelines to ensure that the use of assessment results is fair, ethical, and responsible:</strong></p>
        <ol>
          <li>Make assessments planned and purposeful: There should be a clear understanding at the outset of why the unit is engaging in assessment and the types of decisions that assessment will inform.</li>
          <li>Focus assessments on important goals.</li>
          <li>Assess unit processes, not just outcomes, in order to make sense of outcomes.</li>
          <li>Actively involve those with a stake in decisions stemming from the results in discussions about assessment and use of results.</li>
          <li>Communicate assessment information widely and transparently.</li>
          <li>Discourage others from making inappropriate interpretations of assessment results. For example, communicate the limitations of assessment techniques, sampling, and other factors that could affect the accuracy and replicability of the results.</li>
          <li>Don’t hold personnel accountable for things that they cannot control.</li>
          <li>Don’t penalize personnel for disappointing assessment results.</li>
          <li>Don’t let assessment results alone dictate decisions. Decisions should be based on sound professional judgment. </li>
          <li>Promote the use of multiple sources of information when making decisions.</li>
          <li>Keep personnel informed on how assessment findings are being used to inform decisions.</li>
        </ol>
        <h2>What to do when assessment results indicate a ‘need for improvement’</h2>
        <p>The unit should reflect on what they learned through the assessment process and report actions they plan to take to improve relevant functions and services. Actions should directly relate to the reported outcomes and be clear, logical, and feasible. Possible actions to report include planned modifications of service offerings, improvements to technology, changes in unit personnel or roles, additions to trainings or professional development resources, revisions to unit standards or processes, improvements to communications or marketing, etc. The unit may also report planned modifications to their assessment plan, including revisions to outcomes, assessment methods, or criteria for success.</p>
       <h2>What to do when results indicate ‘acceptable’ levels of performance</h2>
        <p>First of all, CELEBRATE your unit’s success and RECOGNIZE your personnel!</p>
        <p> Then, consider setting more challenging goals or targets, or changing the unit’s assessment plan to focus on new outcomes. Remember, the purpose of assessment is to drive continuous improvement!</p>
       <h2>FAQ: Do we need to report Actions for ‘Acceptable’ results?</h2>
        <p>Yes, there should be an action in TracDat associated with each reported result. Actions for ‘acceptable’ results might include revisions to goals or targets, discontinuation of assessment for an outcome with consistently acceptable results, or plans to continue monitoring the outcome to ensure that performance remains acceptable over time.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
