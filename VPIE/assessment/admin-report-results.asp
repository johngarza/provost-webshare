<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Administrative Program Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <p align="right"><a href="AdministrativePrograms.asp">Back to Steps of Administrative <br />
        Program Assessment</a></p>
       	<h1 align="center">Administrative Programs Assessment        </h1>
       	<h1 align="center"><strong>Reporting Assessment Results</strong><br />
   	    </h1>
       	<p>For assessment results to be used, they first need to be summarized, compared against specified benchmarks and targets, and reported.</p>
        <h2>Summarizing Assessment Results</h2>
        <p>Suskie (2009) outlines five basic ways to summarize assessment results:</p>
        <ul>
          <li><strong><u>Tallies:</u></strong> Take a simple count of results related to the unit’s outcome. For example, units could tally the number of service requests completed, the number of errors made, the number of trainings delivered, etc.</li>
        <li><strong><u>Percentages:</u></strong> Percentages are typically more meaningful than presenting raw numbers (tallies) and facilitate historical comparisons. For example, units could report the percentage of customers who indicated that a particular service met their needs this year compared to last year.</li>
        <li><strong><u>Aggregates:</u></strong> Often, multiple items on a survey or instrument relate to a single outcome. In those cases, it is appropriate to report tallies or percentages across all of the relevant items. For example, perhaps 80% of workshop participants indicated high levels of satisfaction on survey items related to the content of the workshop, but only 40% indicated high levels of satisfaction on items related to the setting of the workshop.</li>
         <li><strong><u>Averages:</u></strong> Averages, including the arithmetic mean, median (i.e., the middle score), and mode (i.e., the most frequent response) can be used to summarize the central tendency of assessment results and to compare results to national benchmarks. </li>
        <li><strong><u>Qualitative Summaries:</u></strong> Qualitative assessment methods (e.g., focus groups, open-ended survey questions) can be analyzed via read-throughs and grouped listings. Read-throughs simply involve quickly reading through qualitative results to get a general sense of common responses. Grouped listings involve separating or tallying qualitative results into common, discrete categorizes (for example, perhaps 60% of customers commented on the speed of service, whereas 20% commented on the quality of service). </li>
        </ul>
        <h2>Interpreting Assessment Results</h2>
        <p>After data has been collected, compare the results to the set criterion for success or target. Then, for each result, make a determination of:</p>
        <ul>
          <li><strong><u>“Acceptable”:</u></strong> The finding meets the minimal acceptable level of unit performance.</li>
        </ul>
        <p align="center">OR</p>
        <ul>
          <li><strong><u>“Needs Improvement”:</u></strong> The finding does not meet the minimal acceptable level of unit performance. Unit performance needs to be improved.</li>
        </ul>
        <p><strong><u>NOTE:</u></strong><strong> </strong>The goal of administrative assessment is to drive continuous improvement. Identifying areas in need of improvement does not constitute ‘failure’ and, on the contrary, is essential to guide forward action and promote positive change. <strong><u>AREAS WILL NOT BE JUDGED ON THEIR RESULTS.</u></strong> However, units will be evaluated on the extent to which they use assessment results to make meaningful improvements over time. </p>
<p>&nbsp;</p><!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
