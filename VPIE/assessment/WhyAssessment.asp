<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Why Assessment | VPAIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!-- #include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"> <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       	<h1 align="center"> Why Assessment? </h1>
       	<p>Assessment is more than statistics, evaluations, performance appraisals, research, and student grades.  Assessment involves gathering information and documenting results on a continual basis to identify successes and potential areas of improvement and can be used to:</p>
       	<ul>
       	  <li>Produce data which will inform your progress and help improve your outcomes</li>
       	  <li>Show your success</li>
       	  <li>Comply with accreditation</li>
   	    </ul>
       	<p>&quot;<em>Assessment is essential not only to guide the development of individual students but also to monitor and continuously improve the quality of programs, inform prospective students and their parents, and provide evidence of accountability to those who pay our way</em>.&quot; -Redesigning Higher Education: Producing Dramatic Gains in Student Learning by Lion F. Gardiner, ASHE-ERIC Higher Education Report Volume 23, No. 7, p. 109</p>
<p><strong>Overarching Assessment Principles</strong></p>
<ul>
  <li>Assessment will help us work more efficiently and effectively towards our main priority, student success.</li>
  <li>It is better to assess ourselves first. Knowing our own areas of improvement sooner rather than later will only make us stronger.</li>
  <li>Your role and purpose stays intact as you continuously assess. You play a critical role in student learning and development.</li>
</ul>
<p><strong>Accreditation</strong></p>
<p>The Southern Association of Colleges and Schools Commission on Colleges (SACSCOC) requires outcomes assessment of all educational programs, administrative support services, academic and student services, and general education competencies.</p>
        <blockquote>
          <p>As stated in Section 7: Institutional Planning and Effectveness of the <a href="http://sacscoc.org/pdf/2018PrinciplesOfAcreditation.pdf">2018 SACSCOC Principles of Accreditation</a>, &quot;An institutional planning and effectiveness process involves all programs, services and constituencies; is linked to decision-making processes at all levels; and provides a sound basis for budgeting decisions and resource allocations.&quot;</p>
        </blockquote>
        <p>&nbsp;</p>
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
