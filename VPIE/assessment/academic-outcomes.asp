<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/VPIE/assessment/includes/navbar.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <p align="right"><a href="Academic_Programs.asp">Back to Steps of Academic <br />
        Program Assessment</a></p>
        <h1 align="center">Academic Program Assessment</h1>

        <h1 align="center"><strong>Define Student Learning Outcomes</strong></h1>
        <h2>What are Student Learning Outcomes?</h2>
        <p>Student learning outcomes (SLOs) describe the specific, measurable <em>knowledge, values, or skills</em> that students will be able to demonstrate upon completing the academic program <em>using precise language focused on the student,</em> as opposed to the program. SLOs directly relate to the program’s identified goals and address the specific behaviors students must demonstrate to prove that the program is making progress towards their goals.<br />
          To develop SLOs, examine each of your program’s goals. For each goal, ask:</p>
        <ul>
          <li><em>“What specifically would students have to do to convince us that this goal was being achieved?”</em></li>
          <li><em>“How would we prove to others that students are achieving this goal?”</em></li>
        </ul>
        <h2>SLOs should be SMART </h2>
         <p> <u>S</u>pecific</p>
        <ul>
          <li>Stated in definite language, SLOs should describe the specific knowledge, values, or skills graduates from the program are expected to demonstrate. </li>
        </ul>
        <p><u>M</u>easurable<u></u></p>
        <ul>
          <li>Data related to the SLO should be readily available, and the data collection process should be feasible considering available time and resources.</li>
        </ul>
        <p><u>A</u>ggressive but <u>A</u>ttainable</p>
        <ul>
          <li>In the spirit of continuous improvement, program faculty and staff should determine an assessable criterion for success or benchmark for the SLO that will progressively move the program closer to achieving its goals.</li>
        </ul>
        <p><u>R</u>esults-oriented and <u>T</u>ime-bound</p>
        <ul>
          <li>SLOs should specify what students’ levels competence should be after a finite period of time (e.g., 5% improvement in pass rates on the state licensure exam in the next year). These specifications may be based on experience, previous assessment results, external requirements, local, state, or national benchmarks, etc.</li>
        </ul>
        <h2>How do you write Student Learning Outcomes?</h2>
        <p>As with program goals, there is no one ‘right’ way to write SLOs, but the following is a popular structure:</p>
          <p align="center"><strong>“Students will be able to (ACTION VERB) (Product of specific knowledge, value, or skill)</strong></p>
        <h2>Example SLOs:</h2>
        <ul>
          <li>“Students will be able to identify historical periods of English literature.”</li>
          <li>“Students will be able to apply differential calculus to model rates of change in time of physical and biological phenomena”</li>
          <li>“Students will be able to describe the function of key economic institutions”</li>
          <li>“Students will be able to construct effective messages for diverse audiences”</li>
          <li>Students will be able to locate, interpret, evaluate, and use professional dietetics literature to make evidence-based practice decisions.</li>
        </ul>
       <h2>Writing Knowledge-Based SLOs</h2>
        <p>When constructing <em><u>knowledge-based</u></em><u> SLOs</u> (i.e., describing what we want graduates of the academic program to know), it may be helpful to consider Bloom’s Taxonomy, presented below. The taxonomy presents hierarchical levels of knowledge ranging from simple (remembering) to complex (creating). Lower levels of knowledge serve as necessary preconditions for higher levels of expertise. Each cognitive ‘level’ is associated with specific demonstrable actions and abilities. Write SLOs with action verbs corresponding to the academic program’s desired level of student performance.<br />
          <img width="440" height="233" src="/VPIE/assessment/images/taxonomy.png" alt="Bloom's Taxonomy" />&nbsp;</p>
        <p><strong>&nbsp;</strong></p>
       
        <h2>Writing Affective or Value-Based SLOs</h2>
        <p>When constructing <em><u>affective</u></em><u> or <em>value-based</em> SLOs</u> (i.e., describing what we want graduates of the academic program to value, take interest in, appreciate, feel, etc.), the following classifications may be helpful to consider:</p>
        <table border="1" cellspacing="0" cellpadding="0" width="696">
          <tr>
            <td width="156" valign="top"><br />
              <strong>Category</strong></td>
            <td width="238" valign="top"><p align="center"><strong>Description</strong></p></td>
            <td width="294" valign="top"><p align="center"><strong>Key Verbs</strong></p></td>
          </tr>
          <tr>
            <td width="156" valign="top"><p>Accepting</p></td>
            <td width="238" valign="top"><p>Demonstrates   a willingness to participate in the activity.</p></td>
            <td width="294" valign="top"><p>Ask,   choose, describe, follow, give, hold, identify, locate, name, point to,   reply, select, use</p></td>
          </tr>
          <tr>
            <td width="156" valign="top"><p>Responding</p></td>
            <td width="238" valign="top"><p>Shows   interest in objectives, phenomena, or activities by seeking them out.</p></td>
            <td width="294" valign="top"><p>Answer,   Assist, compile, conform, discuss, greet, help, label, perform, practice,   present, read, recite, report, select, tell, write</p></td>
          </tr>
          <tr>
            <td width="156" valign="top"><p>Valuing</p></td>
            <td width="238" valign="top"><p>Internalizes   an appreciation for the objectives, phenomena, or activities. </p></td>
            <td width="294" valign="top"><p>Complete,   describe, differentiate, explain, follow, form, initiate, invite, join,   justify, propose, read report, select, share, study work</p></td>
          </tr>
          <tr>
            <td width="156" valign="top"><p>Organizing</p></td>
            <td width="238" valign="top"><p>Begins   to compare different values and resolves conflicts between them to form an   internally consistent system of values.</p></td>
            <td width="294" valign="top"><p>Adhere,   alter, arrange, combine, compare, complete, defend, explain, generalize,   identify, integrate, modify, order, organize, prepare, relate, synthesize</p></td>
          </tr>
          <tr>
            <td width="156" valign="top"><p>Characterizing <br />
              by   Value</p></td>
            <td width="238" valign="top"><p>Adopts   a long-term value system that is pervasive, consistent, and predictable.</p></td>
            <td width="294" valign="top"><p>Act,   discriminate, display, influence, listen, modify, perform, practice, propose,   qualify, question, revise, serve, solve, use, verify</p></td>
          </tr>
        </table>
        <h3>&nbsp;</h3>
        <h2>Writing Skill-Based SLOs</h2>
       <p>When constructing <em>skill-based</em> SLOs (i.e., describing what we want graduates of the academic program to be able to do), the following classifications (proposed by Simpson, 1972) may be helpful to consider:</p>
        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="147" valign="top"><br />
              Perception </td>
            <td width="249" valign="top"><p>Using   senses to obtain cues to guide action.</p></td>
            <td width="228" valign="top"><p>Choose,   describe, detect, differentiate, distinguish, identify, isolate, relate,   select, separate</p></td>
          </tr>
          <tr>
            <td width="147" valign="top"><p>Set</p></td>
            <td width="249" valign="top"><p>Readiness   to take action.</p></td>
            <td width="228" valign="top"><p>Begin,   display, explain, move, proceed, react, respond, show, start, volunteer</p></td>
          </tr>
          <tr>
            <td width="147" valign="top"><p>Guided   Response</p></td>
            <td width="249" valign="top"><p>Knowledge   of the steps required to perform a task.</p></td>
            <td width="228" valign="top"><p>Assemble,   build, calibrate, construct, dismantle, display, dissect, fasten, fix, grind,   manipulate, measure, mend, mix, organize, sketch, work</p></td>
          </tr>
          <tr>
            <td width="147" valign="top"><p>Mechanism</p></td>
            <td width="249" valign="top"><p>Performs   tasks in a habitual manner, with a degree of confidence and proficiency.</p></td>
            <td width="228" valign="top"><p>Assemble,   build, calibrate, construct, dismantle, display, dissect, fasten, fix, grind,   manipulate, measure, mend, mix, organize, sketch, work</p></td>
          </tr>
          <tr>
            <td width="147" valign="top"><p>Complex <br />
              Overt   Response</p></td>
            <td width="249" valign="top"><p>Skillful   performance of tasks involving complex movement patterns.</p></td>
            <td width="228" valign="top"><p>Assemble,   build, calibrate, construct, dismantle, display, dissect, fasten, fix, grind,   manipulate, measure, mend, mix, organize, sketch, work</p></td>
          </tr>
          <tr>
            <td width="147" valign="top"><p>Adaptation</p></td>
            <td width="249" valign="top"><p>Modifies   movement patterns to account for problematic of novel situations.</p></td>
            <td width="228" valign="top"><p>Adapt,   alter, change, rearrange, reorganize, revise, vary</p></td>
          </tr>
          <tr>
            <td width="147" valign="top"><p>Origination</p></td>
            <td width="249" valign="top"><p>Creates   new movement patterns to account for problematic or novel situations; Creates   new tasks that incorporate learned ones.</p></td>
            <td width="228" valign="top"><p>Arrange,   combine, compose, construct, design, originate</p></td>
          </tr>
        </table>
        <p>(Adapted from UCF Academic Assessment Handbook; Ball State Assessment Workbook)</p>
       <h2>Common verbs/verb phrases to avoid when writing SLOs</h2>
        <ul>
          <li>Appreciate</li>
          <li>Become familiar with</li>
          <li>Become aware of</li>
          <li>Learn</li>
          <li>Know</li>
          <li>Understand</li>
          <li>Demonstrate knowledge</li>
          <li>Demonstrate understanding</li>
        </ul>
        <p>These verbs are vague and not measurable, and thus, should not be used to specify SLOs. </p>
        <h2>FAQ: How many Student Learning Outcomes does my program need to assess?</h2>
        <p>Most programs assess around three to five SLOs, but this decision is entirely up to program faculty and staff. Each program goal should be assessed with at least one SLO, and SLOs should be representative of the knowledge, values, and skills students should have acquired throughout the course of the academic program. </p>
<h1 align="center">&nbsp;</h1><!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->


<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
