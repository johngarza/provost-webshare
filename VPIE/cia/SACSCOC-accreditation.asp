<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
  <!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/Includes/nav-cia.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h1>Accreditation by the Southern Association of Colleges and Schools Commission on Colleges (SACSCOC)</h1>
				 <p><em>
					 The University of Texas at San Antonio is accredited by the Southern Association of Colleges and Schools
					 Commission on Colleges to award baccalaureate, master's and doctoral degrees. Contact the Southern
					 Association of Colleges and Schools Commission on Colleges at 1866 Southern Lane, Decatur, Georgia
					 30033-4097 or call 404-679-4500 for questions about the accreditation of The University of Texas at San Antonio.
				 </em></p>
        <h2 >          Who is UTSA’s Accreditation Liaison?</h2>
        <p > The Accreditation Liaison at UTSA is Dr. Kasey Neece-Fielder. Among her many duties as the Liaison, she serves as the contact person between UTSA and SACSCOC. <strong><em>If you have questions about accreditation policies and processes, please route those queries through Dr. Neece-Fielder, rather than contacting SACSCOC directly</em></strong>. Please contact Dr. Neece-Fielder at <a href="mailto:kasey.neece-fielder@utsa.edu">kasey.neece-fielder@utsa.edu</a> or 210-458-4819; she will contact SACSCOC staff, if necessary, and relay their response.</p>
        <h2 > What is SACSCOC accreditation?</h2>
        <p > In order to award degrees/certificates and to be eligible to receive student financial aid and participate in other federal programs, institutions in the U.S. must be accredited by one of six regional accrediting bodies. Institutions in Texas are accredited by <a href="http://sacscoc.org/">SACSCOC</a>. This peer-review accreditation process involves a self-study, off-site review, as well as an on-site visit. As stated by SACSCOC, “The process provides an assessment of an institution’s effectiveness in the fulfillment of its mission, its compliance with the requirements of its accrediting association, and its continuing efforts to enhance the quality of student learning and its programs and services.”</p>
        <p > Reaffirmation of accreditation is on a ten-year schedule.  <strong>The University of Texas at San Antonio was last reaccredited in 2010 (Approval date January 7, 2011).</strong>  </p>
        <h2 > Why is it important to UTSA?</h2>
        <p > Accreditation is important to UTSA because it is important to the success of our students. Without recognized regional accreditation, our students cannot earn valid degrees/certificates and therefore cannot be successful. Additionally, SACSCOC contends that the purposes are to 1) improve quality throughout the institution, 2) assure the public that institutions meet established standards, and 3) enable institutions to provide financial aid. All of these items are needed for student success. We are constantly striving to provide excellent educational experiences that support and promote the value of higher education. Additionally, we must be able to support our students financially and accreditation is key to this.</p>

        <h2>What is the process for reaccreditation?</h2>
        <p> After institutions receive initial accreditation through SACSCOC, they are reevaluated every ten years for “Reaffirmation of Accreditation.” In this process, each institution must comply with the standards contained in the <em><a href="http://sacscoc.org/pdf/2012PrinciplesOfAcreditation.pdf">Principles of Accreditation</a>: Foundations for Quality Enhancement</em> covering topics such as governance and administration, finances, faculty, student services, educational programs, and policy compliance. Based on these principles, a self-study called the Compliance Certification Report is submitted for review. The report includes detailed narratives of our programs, services, and processes, as well as documentation and data to support our claims. An off-site team of fellow higher education professionals reviews that report and provides feedback. Then, the institution responds to that feedback and prepares for an on-site review. A separate team of peer reviewers visits the campus to make their final assessment. The last step in the process is the review by the SACSCOC Board of Trustees. </p>
        <h2> How long does it take and why?</h2>
        <p> UTSA will submit the Compliance Certification Report in September 2019. Our on-site review will take place March 16-19, 2020. Then, the Board of Trustees for SACSCOC will make decisions regarding our reaffirmation in December 2020.</p>
        <h2> What’s the benefit to UTSA when we complete reaccreditation?</h2>
        <p> Accreditation is all about continuous improvement of the University and its processes in order to support students in achieving excellence. Everyone at UTSA has a role in this process. During the course of accreditation, we have the chance to evaluate our programs and services. If we are successful in improving our processes and procedures, UTSA will continue to have degree-granting authority and access to federal programs including student aid. Additionally, we will have an ongoing system of improvement that insures fulfillment of our mission and vision for UTSA.</p>
        <div>
          <div> </div>
        </div>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
