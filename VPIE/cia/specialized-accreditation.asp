<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->

<head>
	<link rel="shortcut icon" href="/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
	<meta name="copyright" content="The University of Texas at San Antonio" />
	<!-- InstanceBeginEditable name="meta" -->
	<meta name="Description" content="The University of Texas at San Antonio" />
	<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
	<!-- InstanceEndEditable -->
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
	<!-- InstanceEndEditable -->
	<link rel="stylesheet" type="text/css" href="/css/master.css" />
	<link rel="stylesheet" type="text/css" href="/css/template.css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
	<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
	<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />
	<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
	<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
	<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
	<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="branding-blue">
		<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
		<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
	</div><!-- end branding -->

	<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="content-white">
		<div id="content" class="clearfix">

			<!-- InstanceBeginEditable name="content-title" -->
			<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
			<!-- InstanceEndEditable -->

			<div id="col-navigation">
				<div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
					<!-- InstanceBeginEditable name="nav" -->
					<!--#include virtual="/VPIE/Includes/nav-cia.asp" -->
					<!-- InstanceEndEditable -->
				</div>
			</div>

			<div id="col-main"><a name="acces-content" class="screen-reader"></a>
				<!-- InstanceBeginEditable name="content" -->
				<h1>Specialized Accreditation</h1>
				<p> <em>(The following information was adapted from the <a href="www.sacscoc.org/">SACSCOC website</a>.)</em></p>
				<p> In accordance with Southern Association of Colleges and Schools Commission on Colleges (SACSCOC) Policy regarding <a href="www.sacscoc.org/pdf/081705/AccredDecisionsOthers.pdf">Accrediting Decisions of Other Agencies</a>, UTSA keeps track of all of its
					discipline-specific programs that are accredited by agencies other than the SACSCOC.  It is important that UTSA tracks this information because the SACSCOC does not grant reaffirmation to an institution if the Commission knows, or has reason to
					know, that the institution is on probation or is subject of a pending or final action by a U.S. Department of Education (DOE) recognized agency to suspend, revoke or deny accreditation or candidacy.  </p>
				<p> UTSA’s <a href="docs/specialized-accreditation.pdf">Specialized Accreditation List</a> is broken out by DOE recognized and non-DOE recognized agencies and then by college and program.  It also lists when the program was last reviewed and
					whether the program has been sanctioned or is on probation by its accrediting agency.   </p>
				<p> Additionally, in accordance with the same SACSCOC policy listed above, it is UTSA’s responsibility to describe itself in identical terms to each accrediting agency with regard to purpose, governance, programs, degree, diplomas, certificates,
					personnel, finances, and constituents.  When asked by an accrediting agency to provide an overall description of UTSA in any report for an accrediting agency, use the following description to ensure uniformity in our description across
					agencies.</p>
				<blockquote>
					<h3> UTSA Description:</h3>
					<p> “The University of Texas at San Antonio (UTSA) is a public urban serving university specializing in health, cybersecurity, energy, sustainability, and human and social development. With nearly 31,000 students, it is the largest university
						in the San Antonio metropolitan region. UTSA advances knowledge through research and discovery, teaching and learning, community engagement and public service. The university embraces multicultural traditions and serves as a center for
						intellectual and creative resources as well as a catalyst for socioeconomic development and the commercialization of intellectual property—for Texas, the nation and the world.”</p>
				</blockquote>
				<h1>&nbsp;</h1>
				<div>
					<div> </div>
				</div>
				<!-- InstanceEndEditable -->
			</div><!-- end col-main-->
		</div>
	</div>
	<!--end contentWhite-->


	<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<!-- InstanceBeginEditable name="userFooter" -->
	<!-- InstanceEndEditable -->

	<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="footer-blue">
		<div id="footer">
			<!--#include virtual="/inc/master/footerWrap.html" -->
		</div>
	</div>
	<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd -->

</html>
