<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
  <!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/Includes/nav-cia.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <h1>Substantive Change</h1>
        <p>The Southern Association of Colleges and Schools Commission on Colleges (SACSCOC) <a href="http://www.sacscoc.org/pdf/081705/SubstantiveChange.pdf" >defines a substantive change</a> as “a significant modification or expansion in the nature and scope of an accredited institution.” There are two major types of substantive change: institutional-related substantive change and academic-related substantive change.</p>
         <p> The UTSA Handbook of Operating Procedures addresses both types of
					 substantive change in <a href="https://www.utsa.edu/hop/chapter2/2-35.html" >Section 2.35,
						 Substantive Change Procedures Required to Obtain Southern Association of
						 Colleges and Schools Commission on Colleges Approval</a>.</p>
         <p> All substantive changes must be submitted to SACSCOC, through the
					 Office of Continuous Improvement and Accreditation, either as a notification
					  or with a completed prospectus, depending on the nature and complexity of
						the change involved. Substantive changes requiring a prospectus must be
						reviewed and approved by SACSCOC before those changes can be implemented
						at UTSA.

					</p>
					<p>For changes to be effective in a spring term, all materials need to be
						received by the Office of Continuous Improvement and Accreditation by
						June 1 to meet the SACSCOC deadline of July 1. For changes to be effective
						in a fall term, all materials need to be received by December 1 to meet
						the SACSCOC deadline of January 1.
						<a href="http://provost.utsa.edu/vpie/academic-program-changes.asp">Click here to begin the process</a>
						 by indicating the type of change and implementation term desired. A
						<a href="/VPIE/cia/docs/Substantive Change Guide for Colleges.pdf">Substantive Change Guide for Colleges can also be found here</a>.
					</p>
					<p>
						For more information, please contact <a href="mailto:lorrie.smith@utsa.edu">Lorrie Smith</a> or <a href="mailto:tia.palsole@utsa.edu">Tia Palsole</a>.</p>
         <p> Below are the types of academic-related substantive change most frequently seen at UTSA and resources to aid you in obtaining SACSCOC approval:</p>
         <table border="1" cellspacing="0" cellpadding="0" width="700">
           <tr>
             <td width="207" valign="top"><br />
               <strong>Agreements Involving Joint and Dual   Academic Awards</strong></td>
             <td width="487" valign="top"><p><a href="http://www.sacscoc.org/pdf/joint-dual-academic-awards-list.pdf" >SACSCOC ‘Agreements   Involving Joint and Dual Academic Awards’ Policy</a><br />
               <a href="docs/List of Approved Agreements Involving UTSA.pdf" >List of approved   agreements involving UTSA</a><br />
               <a href="http://provost.utsa.edu/VPIE/docs/Agreements_Involving_Joint_and_Dual_Academic_Awards-Guidelines.pdf" >Guidelines for   SACSCOC Approval</a><br />
               <a href="http://provost.utsa.edu/VPIE/docs/Agreements_Involving_Joint_and_Dual_Academic_Awards-Information_Form.pdf" >Information Form</a><br />
               <a href="http://provost.utsa.edu/VPIE/docs/Agreements_Involving_Joint_and_Dual_Academic_Awards-Checklists_and_Approvals.pdf" >Checklists and   Approvals</a><br />
               <a href="http://provost.utsa.edu/VPIE/docs/Agreements_Involving_Joint_and_Dual_Academic_Awards-Annual_Assessment_and_Periodic_Review_Report.pdf" >Annual Assessment   and Periodic Review Report</a></p></td>
           </tr>
           <tr>
             <td width="207" valign="top"><p><strong>Altering Significantly the Length or Scope of a Program</strong></p></td>
             <td width="487" valign="top"><p><a href="docs/altering-length-scope-of-program.pdf" >UTSA Instructions   and Process for SACSCOC Approval</a></p></td>
           </tr>
           <tr>
             <td width="207" valign="top"><p><strong>New Degree Programs</strong></p></td>
             <td width="487" valign="top"><p><a href="docs/new-degree-program-instructions-prospectus.docx" >UTSA Instructions,   Process, and Prospectus Template for SACSCOC Approval </a></p>
               <p><strong>Helpful information for steps prior to SACSCOC   approval:</strong><u> </u><br />
                 <a href="/vpie/academic-program-changes.asp">Curriculum Changes</a></p></td>
           </tr>
        <tr>
             <td width="207" valign="top"><p><strong>Closing an Educational Program or   Instructional Site</strong></p></td>
             <td width="487" valign="top"><p><a href="http://www.sacscoc.org/pdf/081705/CloseProgramSite.pdf" >SACSCOC “Closing a Program, Site, Branch or Institution” – Good Practices</a><br />
               <a href="docs/closing-program-or-site.pdf" >UTSA Instructions   and Process for SACSCOC Approval</a><br />
              <a href="docs/teach-out-template.docx" >Teach out Agreement   Template </a>(if program that is closing involves another institution)</p></td>
           </tr>
           <tr>
             <td width="207" valign="top"><p><strong>Initiating Off-Campus Instructional Sites   (including dual-enrollment high school sites) </strong><br />
               Sites   where students may earn at least 25% but no more than 49% of credits toward a   program require SACSCOC notification only.<br />
               Sites   where students may earn 50% or more of credits toward a program require a   prospectus be submitted to SACSCOC for approval.<strong></strong></p></td>
             <td width="487" valign="top"><p><a href="docs/off-campus-instructional-sites.pdf" >List of   SACSCOC-approved off-campus sites</a><br />
               <a href="http://www.sacscoc.org/pdf/081705/Dual Enrollment.pdf" >SACSCOC “Dual   Enrollment” Policy</a><br />
               <a href="docs/off-campus-dual-enrollment-sites-list.pdf" >List of dual   enrollment sites</a> (currently no dual enrollment sites meet the 25% mark for   notification to SACSCOC)<br />
               <a href="docs/off-campus-sites-instructions-prospectus.docx" >UTSA Instructions,   Process, and Prospectus Template for SACSCOC Approval</a></p></td>
           </tr>
           <tr>
             <td width="207" valign="top"><p><strong>Other substantive changes not listed here</strong></p></td>
             <td width="487" valign="top"><p>Please contact us:</p>
               <h3> Office of Continuous   Improvement and Accreditation</h3>
               <h3> 210-458-4706</h3>
               <p><strong>Kasey   Neece-Fielder</strong>, Associate Vice   Provost for Strategic Planning and Assessment and SACSCOC Liaison<br />
               <a href="mailto:kasey.neece-fielder@utsa.edu">kasey.neece-fielder@utsa.edu</a></p>
               <p>                 <strong>Lorrie   Smith</strong>, Director, Accreditation and Process Improvement <br />
               <a href="mailto:lorrie.smith@utsa.edu">lorrie.smith@utsa.edu</a></p>
               <p>                 <strong>Tia Palsole</strong>,   Project Coordinator, Accreditation<br />
             <a href="mailto:tia.palsole@utsa.edu">tia.palsole@utsa.edu</a></p></td>
           </tr>
         </table>
         <p>&nbsp;</p>
<blockquote>&nbsp;
        </blockquote>
<h1>&nbsp;</h1>
        <div>
          <div> </div>
        </div>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
