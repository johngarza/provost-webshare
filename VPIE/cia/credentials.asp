<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->

<head>
	<link rel="shortcut icon" href="/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
	<meta name="copyright" content="The University of Texas at San Antonio" />
	<!-- InstanceBeginEditable name="meta" -->
	<meta name="Description" content="The University of Texas at San Antonio" />
	<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
	<!-- InstanceEndEditable -->
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
	<!-- InstanceEndEditable -->
	<link rel="stylesheet" type="text/css" href="/css/master.css" />
	<link rel="stylesheet" type="text/css" href="/css/template.css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
	<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
	<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />
	<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
	<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
	<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
	<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="branding-blue">
		<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
		<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
	</div><!-- end branding -->

	<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="content-white">
		<div id="content" class="clearfix">

			<!-- InstanceBeginEditable name="content-title" -->
			<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
			<!-- InstanceEndEditable -->

			<div id="col-navigation">
				<div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
					<!-- InstanceBeginEditable name="nav" -->
					<!--#include virtual="/VPIE/Includes/nav-cia.asp" -->
					<!-- InstanceEndEditable -->
				</div>
			</div>

			<div id="col-main"><a name="acces-content" class="screen-reader"></a>
				<!-- InstanceBeginEditable name="content" -->
				<h1>Faculty Credentials</h1>
				<p>The Southern Association of Colleges and Schools Commission on Colleges (SACSCOC) requires that institutions justify and document the qualifications of their faculty members in order to be accredited.  <strong>Faculty credentialing
						is the responsibility of department chairs and deans, with oversight from the Office of the Provost.</strong> SACSCOC provides
						<a href="docs/SACSCOC Faculty Credentials Guidelines.pdf">Faculty Credentials guidelines</a> to help
					institutions manage the process of documenting faculty qualifications.</p>
				<p>Links for further assistance with faculty credentialing:</p>
					<ul style="font-size:1.5em;font-weight:700;">
						<li>
							<a href="docs/Job-Aid-for-Faculty-Credentialing-062719.pdf">Job Aid for Faculty Credentialing</a>
						</li>
						<li>
							<a href="docs/Faculty Official Transcript Requirements.pdf">Transcript Requirements for Faculty</a>
						</li>
						<li>
							<a href="docs/What-Displays-on-the-UTSA-SACSCOC-Faculty-Roster-Report.pdf">What Displays on the UTSA SACSCOC Faculty Roster Report</a>
						</li>
					</ul>
				<h2> Providing Information that Establishes Credentials</h2>
				<p><em>(The following information was adapted from the SACSCOC website.)</em></p>
				<p> SACSCOC usually accepts common collegiate practice in recognizing an academic discipline, concentration, and/or field of study.  Examples include history, mathematics, chemistry, English, sociology, finance, accounting, marketing, and
					management.  For faculty teaching in these areas, it is expected that we provide information that justifies and documents each faculty member’s qualifications <strong>relevant to the specific courses they are assigned to teach</strong>.   For
					faculty teaching interdisciplinary courses, it is expected that we provide information that justifies and documents the faculty member’s qualifications relevant to the disciplines that are components of the course.  </p>
				<p> This process is different than credentialing faculty for hire at UTSA.  When credentialing faculty members for the SACSCOC faculty roster, it may be obvious that only one of the faculty member’s degrees needs to be cited in order to justify
					his/her qualifications to teach a specific course.  In other cases, it will be necessary to list two or more degrees and to list the specific course titles and number of semester hours in those degrees relevant to the courses assigned.  </p>
				<p> It may also be necessary to indicate additional qualifications such as diplomas or certificates earned (with discipline indicated); related work or professional experience; licensure and certifications; continuous documented excellence in
					teaching; honors and awards; scholarly publications and presented papers; and other demonstrated competencies and achievements that contribute to effective teaching and student learning outcomes.  Indicate the dates for these additional
					qualifications and clearly describe the relationship between these qualifications and the course content and/or expected outcomes of the courses assigned to the faculty member. </p>
				<h2> Digital Measures</h2>
				<p>
					At UTSA, faculty educational and credentialing information must be entered into
					<a href="http://provost.utsa.edu/VPAFS/dm/index.asp">Digital Measures</a>
					(DM) by the appropriate departments/colleges.
					DM is administered by Academic Affairs (
					<a href="tel:210-458-6137">210-458-6137</a> or <a href="mailto:digitalmeasures@utsa.edu">digitalmeasures@utsa.edu</a>).

					Faculty educational and credentialing information in DM is used to generate a SACSCOC faculty roster for UTSA’s reaffirmation of accreditation review, which includes all faculty who were assigned as the primary instructor on a course or courses
					during the review period. Substantive change faculty rosters, which include all faculty members appropriate to the scope of that review, are also generated using DM.</p>
				<h2> Questions</h2>
				<p>For questions regarding faculty qualification requirements, please contact <a href="mailto:tia.palsole@utsa.edu">Tia Palsole</a> or <a href="mailto:lorrie.smith@utsa.edu">Lorrie Smith</a>. </p>

				<!-- InstanceEndEditable -->
			</div><!-- end col-main-->
		</div>
	</div>
	<!--end contentWhite-->


	<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<!-- InstanceBeginEditable name="userFooter" -->
	<!-- InstanceEndEditable -->

	<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="footer-blue">
		<div id="footer">
			<!--#include virtual="/inc/master/footerWrap.html" -->
		</div>
	</div>
	<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd -->

</html>
