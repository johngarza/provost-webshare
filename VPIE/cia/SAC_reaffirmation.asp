<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
  <!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/VPIE/Includes/nav-cia.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h1><img src="../images/Header 700x200.png" width="699" height="143" /></h1>
        <p >Our office serves as the coordinator of activities the University undertakes to complete requirements for reaffirmation of accreditation with the Southern Association of Colleges and Schools Commission on Colleges (SACSCOC), including Compliance Report preparation and coordination of the development of the Quality Enhancement Plan (QEP). In this capacity we work with team members from every area of the University to coordinate our reaffirmation and quality enhancement efforts.</p>
        <p >&nbsp;</p>

        <div align="right"></div>
         <a href="https://utsa.compliance-assist.com/" target="_blank"><img src="../images/Button 220x65.png" width="221" height="66" align="left" style="margin: 0 0 0 0px;"/></a>
         <img src="images/18_169889_Timeline-Fall_18_350x335.png" width="350" height="335" align="right" style="margin: 0 0 0 20px;"/>
<p>&nbsp;<br />
        </p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <h2><a href="../SAC-faq.asp">Frequently Asked Questions <br />
          about Reaccreditation</a></h2>
        <p>&nbsp;</p>
      <p>For other questions regarding SACSCOC Reaffirmation <br />
    of Accreditation please contact:<br />
            <strong>Kasey Neece-Fielder </strong><br />
      <em>SACSCOC Liaison </em><br />
          <a href="mailto:kasey.neece-fielder@utsa.edu)">kasey.neece-fielder@utsa.edu</a> | 210-458-4819<br />
          <strong>Lorrie Smith<br />
          </strong><em>Director, Accreditation and Process Improvement          </em><br />
        <a href="mailto:lorrie.smith@utsa.edu">lorrie.smith@utsa.edu</a> | 210-458-5188 <br />
        <strong>Tia Palsole</strong><br />
        <em>Project Coordinator, Accreditation</em><br />
        <a href="mailto:tia.palsole@utsa.edu">tia.palsole@utsa.edu</a> | 210-458-8038</p>
          <p align="center">&nbsp;</p>
        <p align="center"><em>The University of Texas at San Antonio is accredited by the Southern Association of
            Colleges and Schools Commission on Colleges to award  baccalaureate, master's and doctoral degrees. Contact
            the Commission on Colleges at 1866 Southern Lane, Decatur, Georgia
            30033-4097 or call 404-679-4500 for questions about the accreditation of
            UTSA.</em><br />
          </p>
<p>&nbsp;</p>
              
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
