<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Personnel | VPIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
 <style type="text/css">
  	h2 {border-bottom: 1px solid #002a5c}	
	.office p span.title{color: #002a5c;}
	.office p span.name {font-weight: bold;}
  </style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!--#include virtual="/VPIE/Includes/nav-cia.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
<!-- InstanceBeginEditable name="content" -->
        
           <h1>About Us</h1>
       
  <div class="office">
            <p><img align="left" src="../images/KaseyNeeceFielder5402-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
                <span class="name">Kasey Neece-Fielder, Ed.D.</span><br />
              <span class="title">Associate Vice Provost for Strategic Planning and Assessment; SACSCOC Liaison</span><br />
              GSR 2.204, (210) 458-4819<br />
                <a href="mailto:Kasey.Neece-Fielder@utsa.edu">Kasey.Neece-Fielder@utsa.edu</a>
            </p>
        <br />
          <p>
     <img align="left" src="../images/LorrieSmith5392-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" /><span class="name">Lorrie Smith, M.A.</span><br />
              <span class="title">Director of Accreditation and Process Improvement</span><br />
            GSR 2.204, (210) 458-5188<br />
          <a href="mailto:Lorrie.Smith@utsa.edu">Lorrie.Smith@utsa.edu</a></p>
          <br />
           <p><img align="left" src="../images/ElizabethHoff5399-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />
<span class="name">Elizabeth Hoff, Ph.D.</span><br />
              <span class="title">Director of University Assessment</span><br />
              GSR 2.204, (210) 458-4704<br />
<a href="mailto:Elizabeth.Hoff@utsa.edu">Elizabeth.Hoff@utsa.edu</a></p>
              <br />
          <p><img align="left" src="../images/TiaPalsole5393-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
           <span class="name">Tia Palsole, M.F.A.</span><br />
             <span class="title"> Project Coordinator</span> - Accreditation<br />
              GSR 2.204, (210) 458-8038<br />
          <a href="mailto:tia.palsole@utsa.edu">Tia.Palsole@utsa.edu</a></p>
          <br />
          <p><img align="left" src="../images/LisaJohnston5379-100.jpg" width="100" height="100" alt="Maricela Oliva" style="padding-right:10px" />    
        <span class="name">Lisa Johnston</span><br />
              <span class="title">Senior Administrative Associate</span><br />
          GSR 2.204, (210) 458-4965<br />
          <a href="mailto:Lisa.Johnston@utsa.edu">Lisa.Johnston@utsa.edu</a></p>
                    
                 <br />   
       </div>
      <h2><br />
         <!--GSR 2.204D.5, (210) 458-4739<br />
            <a href="mailto:Brenda.Rankin@utsa.edu">Brenda.Rankin@utsa.edu</a></p>    -->
      </h2>
      
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
