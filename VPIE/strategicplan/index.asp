<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Strategic Plan | VPIE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">  <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!-- #include virtual="/VPIE/includes/nav_strategic.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <a name="acces-content" class="screen-reader" id="acces-content"></a><br />

        <h1>Mission, Vision and Core Values</h1>

        <p><strong><em>Introduction</em></strong></p>

        <p>The Office of the Vice Provost for Institutional Effectiveness 
        (VPIE) is responsible for coordination of institutional strategic planning and SACS 
        Commission on Colleges reaffirmation of accreditation; assessment of academic 
        programs; academic accountability; academic affairs training; and a variety of activities 
        related to the functions of institutional research. We report directly to the Provost and 
        Vice President for Academic Affairs. By university policy, our office provides the 
        official information about the University. In this capacity, we provide reports to the U.S. 
        Department of Education, the Legislative Budget Board, the Texas Higher Education 
        Coordinating Board, and the University of Texas System. In addition, we fulfill other 
        internal and external requests for information about the university. We partner with other 
        vice presidential areas to address issues beyond strict academic affairs concerns, such as 
        student success, facilities, compliance, and service quality issues.</p>

        <p><strong><em>Mission</em></strong> <strong><em>Statement</em></strong></p>

        <p>The mission of the Office of the Vice Provost for Institutional 
        Effectiveness is to provide logistical support and relevant, reliable information for 
        institutional planning, accreditation, assessment, and accountability as UTSA moves to 
        premier public university status. We also support the Academic Affairs community in 
        fulfilling the public trust through a system of training and compliance.</p>

        <p><strong><em>Vision Statement</em></strong></p>

        <p>To be recognized within and outside the University for providing outstanding expertise 
        and information for planning, assessment, accreditation and accountability, assisting 
        UTSA in its goal to become a premier public university.</p>

        <p><strong><em>Core Values</em></strong></p>

        <p>The core values of our office reflect those of the University: integrity; excellence, 
        inclusiveness, respect, collaboration, and innovation.</p>

       
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
