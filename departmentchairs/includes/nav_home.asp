<ul>
    <li><a class="home" a href="/departmentchairs/index.asp" title="Department Chairs Home Page">Department Chairs Home</a></li>
    <li><a href="/departmentchairs/council.asp">Department Chairs Council</a></li>
   <li><a href="/departmentchairs/roster.asp">List of Department Chairs</a></li>
   <li><a href="/departmentchairs/training.asp">Department Chair Training</a></li>
   <!-- <li><a href="/home/docs/academic-administrative-calendar-2015-2016.pdf">Academic Administrative Calendar 2015-2016</a></li>-->
     <li><a href="../../home/resources_dean.asp">Resources for Department Chairs</a></li>
</ul>
<p>&nbsp;</p>
<h4>UTSA Links </h4>
<ul>
<li><a href="http://provost.utsa.edu/home/">Office of the Provost</a></li>
<li><a href="http://utsa.edu/hr/">Human Resources</a></li>
<li><a href="https://utdirect.utexas.edu/">UT Direct</a></li>
<li><a href="http://lib.utsa.edu/">Libraries</a></li>
<li><a href="http://faculty.utsa.edu/">Faculty Center</a></li>
<li><a href="http://one.utsa.edu/sites/oit/OITConnect/Pages/OITConnect.aspx">OIT Connect</a></li></ul>
