<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Academic Administrative Calendar | Department Chairs | UTSA | The University of Texas at San Antonio</title>

<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style>
#content table {border:none;}
#content table th {border:none;}
#content table tr {border:none;}
#content table td {border:none;}
</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">UTSA Department Chairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/departmentchairs/includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <h1>Academic Administrative Calendar</h1>
       <p><a href="../home/docs/academic-administrative-calendar-2015-2016.pdf">PDF file of 2015-2016 Academic Administrative Calendar</a></p>
     <!-- <table width="680" border="0" cellpadding="1" cellspacing="1">
          <tr>
    <td><h4><a href="#september">September</a></h4></td>
    <td><h4><a href="#october">October</a></h4></td>
    <td><h4><a href="#november">November</a></h4></td>
    <td><h4><a href="#december">December</a></h4></td>
    <td><h4><a href="#january">January</a></h4></td>
         </tr>
  <tr>
    <td><h4></a><a href="#february">February</a></h4></td>
    <td><h4><a href="#March">March</a></h4></td>
    <td><h4><a href="#april">April</a></h4></td>
    <td><h4><a href="#may">May</a></h4></td>
    <td><h4><a href="#summer">Summer</a></h4></td>
  </tr>
 
</table>
       <table width="680" border="0">
         
         <tr>
           <td><h3><a name="september" id="september"></a>September</h3></td>
         </tr>
         <tr>
           <td width="559"><a href="http://provost.utsa.edu/VPAFS/forms/recruitment/index.asp">Faculty Recruitment (T/TT)</a> - Training by VPAFS/EOS; place advertisement; send letters soliciting nominations</td>
           <td width="111">7/1 - 8/31</td>
         </tr>
         <tr>
           <td>Deadline for Faculty to upload <a href="http://provost.utsa.edu/home/Evaluation/PromotionTenure/index.asp">Promotion and Tenure</a> documents to Rowdy Space</td>
           <td>9/1**</td>
         </tr>
         <tr>
           <td>Review of Promotion and Tenure Cases via Rowdyspace (internal deadlines set by each college)</td>
           <td>9/1 - 10/31</td>
         </tr>
         <tr>
           <td>Review of <a href="http://provost.utsa.edu/home/evaluation/cpe/">CPE</a> Cases via Rowdyspace (internal deadlines set by each college)</td>
           <td>9/1 - 10/31</td>
         </tr>
         <tr>
           <td>Deadline for Departments to provide <a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/process.asp">P&amp;T DFRAC</a> names to Provost's Office</td>
           <td>9/7</td>
         </tr>
         <tr>
           <td>Deadline for Departments to elect <a href="http://provost.utsa.edu/home/evaluation/cpe/review.asp">CPE FRAC</a></td>
           <td>9/7</td>
         </tr>
         <tr>
           <td>Deadline for Colleges to provide CFRAC names to Provost's Office</td>
           <td>9/10</td>
         </tr>
         <tr>
           <td>Deadline for Dept. Chairs to provide CPE FRAC committee names to Dean's offices</td>
           <td>9/10</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>9/10</td>
         </tr>
         <tr>
           <td>Deadline for Faculty Credentials to be completed in <a href="http://provost.utsa.edu/vpaie/dm/">Digital Measures</a> for the Fall 2014 semester SACS Roster</td>
           <td>9/12</td>
         </tr>
         <tr>
           <td>Deadline for all Faculty Assignments for the Fall 2013 semester</td>
           <td>9/18</td>
         </tr>
         <tr>
           <td>Deadline for Faculty to upload CPE documents to Rowdy Space</td>
           <td>9/15*</td>
         </tr>
         <tr>
           <td>Colleges begin nomination process for the <a href="http://utsa.edu/facultyawards/awards/pres.html">President's Distinguished Achievement Awards</a></td>
           <td>9/15*</td>
         </tr>
         <tr>
           <td>Notify Program Faculty who are expected to provide <a href="http://provost.utsa.edu/VPAIE/assessment/index.asp">Assessment</a> information this Fall</td>
           <td>9/19
           </p></td>
         </tr>
         
         <tr>
           <td><h3><a name="october" id="october"></a>October</h3></td>
        </tr>
         <tr>
           <td>Fall 2013 Faculty Workload Verification</td>
           <td>10/1-‐10/31</td>
         </tr>
         <tr>
           <td><a href="http://provost.utsa.edu/VPAFS/documents/FRT_Manual_2013-2014.pdf">Faculty Recruitment (T/TT)</a> - Soft closing date; review applicants; develop short list of candidates</td>
           <td>10/1 - 10/15</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>10/8</td>
         </tr>
         <tr>
           <td>Use of results for Improvement Report due</td>
           <td>10/12</td>
         </tr>
         <tr>
           <td>Class Schedule for Summer 2014 is available online to departments (Depts enter in Banner/SSASECT)</td>
           <td>mid - October</td>
         </tr>
         <tr>
           <td><a href="http://provost.utsa.edu/VPAFS/documents/FRT_Manual_2013-2014.pdf">Faculty Recruitment (T/TT)</a> - Phone/video interviews (optional); (extend soft closing date, if needed)</td>
           <td>10/15 - 10/31</td>
         </tr>
         <tr>
           <td>Compose Chairs' CPE evaluation report</td>
           <td>late Oct - first Nov.</td>
         </tr>
         <tr>
           <td><h3><a name="november" id="november"></a>November</h3></td>
        </tr>
         <tr>
           <td>Class Schedule for Summer 2014 is due to Registrar's Office 1st deadline (due date to Deans will be earlier and set by colleges)</td>
           <td>11/1</td>
         </tr>
         <tr>
           <td>Faculty Recruitment (T/TT) - On-campus interviews with candidates on short list</td>
           <td>11/1 - 12/31</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>11/12</td>
         </tr>
         
         <tr>
           <td>Promotion and Tenure packets due in Provost's Office</td>
           <td>11/15</td>
         </tr>
         <tr>
           <td>Submit CPER and Chair's memos to respective deans; copy to faculty member</td>
           <td>11/15</td>
         </tr>
        
         <tr>
           <td><h3><a name="december" id="december"></a>December</h3></td>
          
         </tr>
         <tr>
           <td>Remind faculty to submit Assessment data</td>
           <td>12/8</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>12/10</td>
         </tr>
         <tr>
           <td>Notification to faculty in 2nd academic year of employment - non-reappointment</td>
           <td>12/15*</td>
         </tr>
         
         <tr>
           <td>Promotion and Tenure letters distributed to faculty</td>
           <td>December</td>
         </tr>
         <tr>
           <td><h3><a name="january" id="january"></a>January</h3></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Class Schedules for Fall 2014 available online to departments (Depts enter in Banner/SSASECT)</td>
           <td>1/1</td>
         </tr>
         <tr>
           <td><a href="http://provost.utsa.edu/VPAFS/documents/FRT_Manual_2013-2014.pdf">Faculty Recruitment (T/TT)</a> - Select best candidate; submit CBC for approval; work with Dean to develop startup package; negotiate offer</td>
           <td>January</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>No meeting</td>
         </tr>
         <tr>
           <td>2013-2014 <a href="http://provost.utsa.edu/home/docs/2013-2014-Faculty_Development_Leave_Guidelines.pdf">Faculty Development Leave</a> Proposals due for leaves taken during FY14-15</td>
           <td>1/16</td>
         </tr>
         <tr>
           <td>Deadline for Faculty to upload Third Year Review documents to RowdySpace</td>
           <td>1/20**</td>
         </tr>
         <tr>
           <td>Submit completed Fall Assessment Report (results) to college</td>
           <td>1/23</td>
         </tr>
         <tr>
           <td>Deadline for Faculty Credentials to be completed in Digital Measures for the Spring 2014 semester SACS Roster</td>
           <td>1/28</td>
         </tr>
         <tr>
           <td>Deadline for all Faculty Assignments for the Spring 2014 semester</td>
           <td>1/30</td>
         </tr>
         <tr>
           <td>Deadline for Annual Reports to be uploaded into Digital Measures</td>
           <td>1/31</td>
         </tr>
         <tr>
           <td><h3><a name="february" id="february"></a>February</h3></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Deadline to notify Provost's office of any <a href="http://provost.utsa.edu/home/Evaluation/Emeritus/index.asp">emeritus applications</a></td>
           <td>2/1</td>
         </tr>
         <tr>
           <td>Class Schedules for Fall 2014 due to Registrar's Office-1st deadline (due date to Deans will be earlier and set by college)</td>
           <td>2/1</td>
         </tr>
         <tr>
           <td>Spring 2014 Workload Verification</td>
           <td>2/1 - 2/28</td>
         </tr>
         <tr>
           <td>Conduct Performance Evaluations (2/1/13 1/31/14)</td>
           <td>2/1-2/28</td>
         </tr>
         
         <tr>
           <td>TYR FRAC report due to Department Chair</td>
           <td>2/15</td>
         </tr>
         <tr>
           <td>Deadline to upload <a href="http://provost.utsa.edu/home/Evaluation/Emeritus/index.asp">emeritus</a> documents to Rowdyspace</td>
           <td>2/20</td>
         </tr>
         
         <tr>
           <td>Deadline for Department Review Committee to review annual report and recommend evaluation ratings</td>
           <td>2/22*</td>
         </tr>
         
         <tr>
           <td>BUDs for next fiscal year - deadlines will be announced</td>
           <td>February</td>
         </tr>
         <tr>
           <td><h3><a name="March" id="March"></a>March</h3></td>
           <td>&nbsp;</td>
         </tr>
        
         <tr>
           <td><a href="http://provost.utsa.edu/VPAFS/documents/FRT_Manual_2013-2014.pdf">Faculty Recruitment (T/TT)</a> - Finalize appointment and begin assembling Recruitment Package (transcripts, reference letters, etc.)</td>
           <td>3/1</td>
         </tr>
         <tr>
           <td><a href="http://provost.utsa.edu/VPAFS/documents/FRT_Manual_2013-2014.pdf">Faculty Recruitment (T/TT) </a>- Prepare office and/or research space for new faculty member</td>
           <td>3/1 - 6/30</td>
         </tr>
         <tr>
           <td>Deadline for notification to faculty scheduled for faculty reviews in 2014-2015</td>
           <td>3/1</td>
         </tr>
         <tr>
           <td>Deadline for faculty to request early promotion and tenure review or promotion to full professor</td>
           <td>3/1</td>
         </tr>
         <tr>
           <td>Notification to faculty in first academic year of employment - nonreappointment</td>
           <td>3/1</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (second Wednesday of each month)</td>
           <td>3/11</td>
         </tr>
         <tr>
           <td><h3><a name="april" id="april"></a>April</h3></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Class Schedule for Spring 2015 is available online to departments (Depts enter in Banner/SSASECT)</td>
           <td>4/1</td>
         </tr>
        
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>4/8</td>
         </tr>
          <tr>
           <td>University Excellence Awards (combined <a href="http://utsa.edu/facultyawards/">faculty</a> and <a href="http://utsa.edu/hr/employeerelations/sesa.html">staff award</a> ceremony)</td>
           <td>4/14</td>
         </tr><tr>
           <td>TYR - FRAC and Department Chair's memos due to Deans</td>
           <td>4/10</td>
         </tr>
         <tr>
           <td>Deadline for Department Chairs to hold evaluation interviews with faculty</td>
           <td>4/15</td>
         </tr>
         <tr>
           <td>Deadline for <a href="http://provost.utsa.edu/home/Evaluation/Emeritus/index.asp">Emeritus cases</a> due into Provost's Office</td>
           <td>4/15</td>
         </tr>
         <tr>
           <td>Deadline for Department Chair to finalize performance evaluations for Dean's review</td>
           <td>4/22</td>
         </tr>
         <tr>
           <td>Deadline for faculty to provide optional faculty statement regarding evaluation in <a href="http://provost.utsa.edu/vpaie/dm/">Digital Measures</a></td>
           <td>4/22</td>
         </tr>
         <tr>
           <td>Remind faculty to submit Assessment data</td>
           <td>4/27</td>
         </tr>
         <tr>
           <td>FY 15 budget request from direct reports – deadline will be announced</td>
           <td>June</td>
         </tr>
         <tr>
           <td><h3><a name="may" id="may"></a>May</h3></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Notification to faculty regarding nonreappointment</td>
           <td>5/1</td>
         </tr>
         <tr>
           <td>Class Schedule for Spring 2015 due to Registrar's Office-1st deadline (due date to Deans will be earlier and set by college)</td>
           <td>5/1</td>
         </tr>
         <tr>
           <td>Deadline for Deans to review evaluations prepared by Department Chairs for consistency with college standards and consistency within and across departments</td>
           <td>5/8</td>
         </tr>
         <tr>
           <td>Department Chair Council <a href="http://provost.utsa.edu/departmentchairs/council.asp">meeting</a> (2nd Wed. of each month)</td>
           <td>5/13</td>
         </tr>
         <tr>
           <td>Deadline for Deans to amend faculty member's evaluation if response was submitted by faculty member</td>
           <td>5/15</td>
         </tr>
         <tr>
           <td>Submit completed 2013-14 Assessment Cycle Report to college</td>
           <td>5/30</td>
         </tr>
         <tr>
           <td>Final draft of 2014-15 Assessment Plan due (New)</td>
           <td>5/30</td>
         </tr>
         <tr>
           <td><h3><a name="summer" id="summer"></a>Summer </h3></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Department Chair Council does not meet in June, July or August</td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Faculty scheduled for promotion and tenure reviews compile materials and submit to external reviewers</td>
           <td>6/1 - 6/30</td>
         </tr>
         <tr>
           <td>Faculty Recruitment (T/TT) - Assist faculty member with transition to UTSA</td>
           <td>6/1 - 8/31</td>
         </tr>
         <tr>
           <td>Faculty Recruitment (T/TT) - <a href="http://provost.utsa.edu/home/orientation/index.asp">Provost's New Faculty Orientation</a> (Tentative date)</td>
           <td>8/18-8/19</td>
         </tr>
         <tr>
           <td>Deadline to enter returning benefits-eligible NTT faculty and new TT faculty assignments for the Fall 2014</td>
           <td>8/7</td>
         </tr>
         <tr>
           <td>Department Chair or delegate reviews promotion and tenure files on Rowdyspace for accuracy and completeness and uploads external review letters</td>
           <td>8/1 - 8/31</td>
         </tr>
         <tr>
           <td>&nbsp;</td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>*When a deadline falls on a weekend, the deadline is extended to the following Monday<br />
           **When a deadline falls on a holiday, the deadline is extended to the next scheduled workday</td>
           <td>&nbsp;</td>
         </tr>
       </table>-->
      
       <h1>
         <!--<h2>Honors College</h2><table width="680" border="0">
          <tr>
            <td colspan="2"><h2>Honors College</h2></td>
          </tr>
          <tr>
            <td><p><img src="images/Papagiannakis150.jpg" alt="" width="127" height="152" /><br />
              <strong>Ann Eisenberg</strong></p></td>
          </tr>
        </table>
        <h2>University College</h2><table width="680" border="0">
          <tr>
            <td colspan="3"><h2>University College</h2></td>
          </tr>
          <tr>
            <td><p><img src="images/Papagiannakis150.jpg" alt="" width="127" height="152" /><br />
              <strong>Gail Pizzola</strong><br />
            Writing Core Program</p></td>
            <td><p><img src="images/Papagiannakis150.jpg" alt="" width="127" height="152" /><br />
              <strong>Tammy Wyatt</strong><br />
            University College</p></td>
            <td><p><img src="images/Papagiannakis150.jpg" alt="" width="127" height="152" /><br />
              <strong>Susan Colorado</strong><br />
            University College</p></td>
          </tr>
          
        </table>-->
        </h1>
       <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->

<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
