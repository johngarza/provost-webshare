<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>Personnel | EVP | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" />
  <!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <style type="text/css">
  	h2 {border-bottom: 1px solid #002a5c}	
	.office p span.title{color: #002a5c;}
	.office p span.name {font-weight: bold;}
  </style>
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
  <script type="text/javascript" src="/js/custom.js"></script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
	<!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle">
       <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a>
      </div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a> 
		  <!-- #include virtual="/evp/includes/nav_home.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a>
        <h1>Executives and Staff (by Office/Name/Title)</h1>
        <h2>Executive Vice Provost</h2>
        <div class="office">           
            <p>
            	<span class="name">Julius Gribou</span><br />
            	<span class="title">Executive Vice Provost and Senior International Officer</span><br />
            	MB 4.120S, (210) 458-4110<br />
           	<a href="mailto:Julius.Gribou@utsa.edu">Julius.Gribou@utsa.edu </a></p>
            
            <p>
            	<span class="name">Maria Espericueta</span><br />
              	<span class="title">Administrative Service Officer I</span><br />
              	MB 4.120R, (210) 458-4110<br />
           	<a href="mailto:Maria.Espericueta@utsa.edu">Maria.Espericueta@utsa.edu </a></p>    
        </div>
    
        <h2>Office of International Programs</h2>
        <div class="office">
            <p>
                <span class="name">Charles Crane</span><br />
                <span class="title">Executive Director of International Programs</span><br />
                MB 1.210, (210) 458-7202<br />
                <a href="mailto:Charles.Crane@utsa.edu">Charles.Crane@utsa.edu</a><br />
            </p>
       </div>  
       
       <h2>Office of Space Management</h2>
         <div class="office">        
             <p>
                <span class="name">Daniel Sibley</span><br />
                <span class="title">Director for Space Utilization</span><br />
                SB 1.01.06, (210) 458-6074<br />
                <a href="mailto:Daniel.Sibley@utsa.edu">Daniel.Sibley@utsa.edu</a><br />
             </p>
        </div>
 
		 <h2>East Asia Institute</h2>
         <div class="office">        
             <p>
                <span class="name">Mimi Yu</span><br />
                <span class="title">Associate Director of East Asia Institute</span><br />
                MB 1.209, (210) 458-8558<br />
                <a href="mailto:Mimi.Yu@utsa.edu">Mimi.Yu@utsa.edu</a><br />
             </p>
        </div>
        

      
      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
</body>
</html>
