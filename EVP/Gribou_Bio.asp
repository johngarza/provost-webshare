<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>Julius Gribou Bio | EVP | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
	<!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle">
       <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a>
      </div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
		  <!-- #include virtual="/evp/includes/nav_home.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a>

        <h1>Biography</h1>

        <h2>Julius M. Gribou, AIA, IIDA</h2>

        <p>After earning a Bachelor&rsquo;s degree in Design from the University of Florida (1971) and a Master&rsquo;s degree in Architecture from the University of Illinois, Champaign-Urbana (1977), Julius Gribou initiated his academic career at the University of Southwestern Louisiana (USL). His professional career as an architect was on a parallel track, as he became a registered architect in 1977. In 1985 Gribou joined the faculty at Texas A&amp;M University as an Associate Professor, and subsequently was appointed as the Head of the Department of Architecture and a Professor.</p>

        <p>Since 2000, at UTSA, Gribou has held the positions of the Director and Dean of the School of Architecture, the founding Dean of the College of Architecture and the Ronald K. Blumberg Endowed Professor of Architecture, The Interim Provost and Vice President for Academic Affairs, and since February 18, 2008 the Executive Vice Provost. In July 2008 he assumed the role of the Interim Chief Information Officer (CIO) and in January 2009 he was appointed as the Senior International Officer (SIO).</p>

        <p>Gribou was awarded a Fulbright Scholarship to Poland in 1991, where he researched the country&rsquo;s historic preservation systems. Professionally, Gribou has been active with the American Institute of Architects and the Texas Society of Architects (TSA), where he has held a number of officer positions and committee assignments.</p>

        <p>Among his awards and accomplishments are numerous design and photography awards, the Edward J. Romieniec, FAIA Award from the Texas Society of Architects (TSA), and a Presidential Citation from the TSA. Over the past several years, Gribou has served as a juror for regional, national and international design award competitions. In 2007 he co-edited a guidebook titled: &ldquo;Traditions and Visions: San Antonio Architecture&rdquo;.</p>

        <p>Gribou&rsquo;s community involvement includes service on a number of local non-profit boards and organizations, and city boards and committees.</p>

        <p>An educator and architect with more than 20 years of international networking experience, Gribou directs the development of UTSA&rsquo;s international programs, is responsible for space utilization and allocation, provides leadership in the master planning endeavors, oversees the academic compliance activities and represents the Provost in his absence.</p>

        <p>Gribou is commited to working with the Provost to help the university in its movement toward becoming a premier research university.&nbsp; He feels confident that his academic and professional experience, plus the involvement on the strategic and master plan teams, will prove beneficial as UTSA is being elevated to a higher level of excellence.</p>
      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
</body>
</html>
