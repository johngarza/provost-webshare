<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>UTSA Provost Home | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"--><!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle">
       <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a>
      </div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
           <!-- #include virtual="/evp/includes/nav_home.asp"-->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a>
		<h1>Department Chair Training</h1>
        
		<p>
		The documents below have been provided at Department Chair training sessions:</p>
		<h2>Department Chair Training Presentations</h2>
			<ul>
			    <li><a href="Curriculum_Management.pdf">Curriculum Management and Class Scheduling</a> (Sept. 2010)</li>
                <li><a href="FacultyEvaluation-2010.pdf">Faculty Evaluation Processes - Comprehensive</a> (Sept. 2010)</li>
				<li><a href="Evaluation Processes 2011-2012.pdf">Faculty Evaluation Processes - Additional</a> (Sept. 2011)</li>
		        <li><a href="FAIR_and_Sustaining_SACS_Preparedness.pdf">FAIR and Sustaining SACS Preparedness</a> (Sept. 2010)</li>
		        <li><a href="FY 2011 Budget Initiatives-final.pdf">FY 2011 Budget Initiatives</a> (Sept. 2010)</li>
		        <li><a href="Student_Learning_Outcomes_Assessment.pdf">Student Learning Outcomes Assessment</a> (Sept. 2010)</li>
			</ul>
       

          
          <!--          <p><strong>OIT Chief Information Officer Search</strong></p>            <ul>                <li><a href="CIO position ad_r1909.pdf">Position Advertisement</a><br>            For full consideration, applications must be received by February 27, 2009.</li>              <li><a href="CIO position description_r1909.pdf">Position Description</a></li>              </ul>        --><br /></dt>
        </dl>
      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
  
 <!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
</body>
</html>
