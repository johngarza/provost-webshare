<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <link rel="shortcut icon" href="http://www.utsa.edu/favicon.ico" />
  <meta http-equiv="Content-Type" content="text/html; charset=us-ascii" />
  <meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2011" />
  <meta name="copyright" content="The University of Texas at San Antonio" />
  <meta name="description" content="The University of Texas at San Antonio" />
  <meta name="keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />

  <title>EVP | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
  <link rel="stylesheet" type="text/css" href="/css/master.css" />
  <link rel="stylesheet" type="text/css" href="/css/template.css" /><!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]--><!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]--><!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
  <link rel="stylesheet" type="text/css" href="/css/print.css" />
  <script type="text/javascript" src="/js/jquery-optimized-utsa.js">
</script>
  <script type="text/javascript" src="/js/custom.js">
</script>
</head>

<body>
  <!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="branding-blue">
    <!-- #include virtual="/inc-share/accessibility/screen-reader-skip-local.html"-->
	<!-- #include virtual="/inc/master/brandWrap-globalNav.html"-->
  </div><!-- end branding --><!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="content-white">
    <div id="content" class="clearfix">
      <div class="pageTitle">
       <a href="/home/">Office of the Provost and Vice President for Academic Affairs</a>
      </div>

      <div id="col-navigation">
        <div class="nav-page">
          <a name="acces-localnav" class="screen-reader" id="acces-localnav"></a>
          <!-- #include virtual="/evp/includes/nav_home.asp" -->
        </div>
      </div><!-- end of col-navigation -->

      <div id="col-main">
        <a name="acces-content" class="screen-reader" id="acces-content"></a>
        <h1>Executive Vice Provost</h1>
        <h2>Mr. Julius Gribou <br /> Executive Vice Provost <img src="../images/JuliusGribou2011.jpg" width="190" height="253" hspace="5" vspace="5" border="0" align="right" class="image" title="Mr. Julius Gribou" alt="Julius Gribou"/>
        and Senior International Officer</h2>
        <hr noshade="noshade" width="280" />

        <p>The executive vice provost directs the development of UTSA's international programs, is responsible for space utilization and allocation, provides leadership in the master planning endeavors, oversees the academic compliance activities and represents the Provost in his absence.</p>        
        <hr noshade="noshade" width="280" />

        <p align="center"></p>

        <ul>
          <li><a href="Gribou_Bio.asp" title="Mr. Gribou's Vita">Mr. Gribou's Bio</a></li>

          <li><a href="http://www.googlesyndicatedsearch.com/u/UTSAEDU?hq=inurl%3Awww.utsa.edu/today&amp;hl=en&amp;domains=utsa.edu&amp;ie=ISO-8859-1&amp;q=" title="Search for Mr. Gribou in other UTSA Today articles" target="_blank">Search for Mr. Gribou in UTSA Today articles</a></li>

          <li style="list-style: none; display: inline">
            <p>For any calendar/scheduling information,<br />
            please contact <a href="mailto:Maria.Espericueta@utsa.edu">Maria Espericueta</a> at (210) 458-4994.</p>
          </li>
        </ul>
      </div><!-- end col-main-->
    </div>
  </div><!--end contentWhite--><!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->

  <div id="footer-blue">
    <div id="footer">
      <!-- #include virtual="/inc/master/footerWrap.html"-->
    </div>
  </div>
</body>
</html>
