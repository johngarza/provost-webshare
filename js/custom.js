$(document).ready(function(){
	
	///////////////////////////////////////////////////////////////////////////////////
	//ANCHOR - class="scroll" or class="top"
	///////////////////////////////////////////////////////////////////////////////////
	$("a.scroll").each(function(){
	$(this).click(function() {
		var targetName = $(this).attr("href");	
		//find out offset and subtrace the size of our header (150)
		var offset = $(targetName).offset().top - 150;
		
		$('html, body').animate({scrollTop: offset}, 800);
		return false;
		});
	});
	
	$("a.top").click(function(){
		$('html, body').animate({scrollTop: 0}, 800);
	});


	///////////////////////////////////////////////////////////////////////////////////
	//TEMPLATE PAGE NAVIGATION
	///////////////////////////////////////////////////////////////////////////////////
	$(".toggleWrap").hide();
	

});



