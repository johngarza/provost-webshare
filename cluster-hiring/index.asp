<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<title>Faculty Cluster Hiring | UTSA | The University of Texas at San Antonio</title>

<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<style>
#content table {border:none;}
#content table th {border:none;}
#content table tr {border:none;}
#content table td {border:none;}
</style>
<style> #col-main {
    float: none;
</style>
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
<div class="pageTitle"><a href="http://provost.utsa.edu">Office of the Provost and Vice President for Academic Affairs</a></div>
<h2>FACULTY CLUSTER HIRING</h2>
          <h4>Strengthening academic and research areas is critical to Tier One designation. Under the leadership of President Romo, UTSA launched its GoldStar Initiative in 2014, a $40 million commitment to hire 60 new research-intensive faculty members. UTSA is now recruiting experts in advanced materials, open-cloud computing and cybersecurity, big data, biomedicine with an emphasis on neuroscience, brain health and infectious diseases, social and educational transformation as well as sustainable communities.</h4>
      <h4>&nbsp;</h4>
          <h4>Through the <a href="https://www.utsystem.edu/offices/chancellor/chancellors-vision-university-texas-system">UT System Chancellor's 2015 Vision</a>, Chancellor McRaven has identified a series of initiatives called ‘Quantum Leaps’. Leveraging the collective research power and facilitating collaboration across all 14 universities and healthcare institutions, UT System will address complex societal challenges, including leadership, national security, elementary education and brain health.</h4>
          <h4>&nbsp;</h4>
      <h4>Given these priorities, UTSA will use funds from the GoldStar program, the generosity of our donors and other sources to “cluster hire” new faculty with well-established research expertise. The first clusters to be have been identified are the following. </h4>
      <p>&nbsp;</p>
<h2>HIRING CLUSTERS</h2>
<table width="700" border="1">
  <tr>
    <td><img src="images/brainhealth.jpg" width="450" height="300" /></td>
    <td><img src="images/cyber[1].jpg" width="450" height="300" /></td>
  </tr>
  <tr>
    <td><h2 align="right"><a href="brain-health.asp">BRAIN HEALTH</a></h2></td>
    <td><div align="right">
      <h2><a href="cyber.asp">CYBER</a></h2>
    </div></td>
    
  </tr>
</table>
<p>&nbsp;</p>

        <p>&nbsp;</p>
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body></html>
