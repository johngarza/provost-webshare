<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Cluster Hiring | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" --><style>
#noborder table {border:none;}
#noborder table th {border:none;}
#noborder table tr {border:none;}
#noborder table td {border:none;}
</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            	<!--#include virtual="/cluster-hiring/includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
		<h2><img src="images/cyber_banner.jpg" width="700" height="200" />Faculty Hiring Cluster</h2>
		<h1>CYBER</h1>

        <p>Through the <a href="https://www.utsystem.edu/offices/chancellor/chancellors-vision-university-texas-system">UT System Chancellor's 2015 Vision</a>, Chancellor William McRaven has identified a series of eight initiatives called ‘Quantum Leaps’. Leveraging the collective research power and facilitating collaboration across all 14 universities and healthcare institutions, UT System will address complex societal challenges, UTSA is particularly well-positioned to contribute to National Security with our cybersecurity expertise and capabilities. </p>
        <p>Recent surveys of cybersecurity professionals ranked the UTSA program as the number one education program in cybersecurity in the United States in 2014 (Ponemon Institute for Hewlett Packard) and the number two graduate program in cybersecurity in 2016 (Universities.com). Under the leadership of President Ricardo Romo, UTSA has been a champion in the cybersecurity realm since 2000, and current expansion of UTSA’s cyber efforts demonstrates the institution’s commitment to the Chancellor’s Quantum Leaps. President Romo continues the momentum with a plan to recruit leading cybersecurity faculty under the GoldStar Initiative, with six new hires slated for 2017. In addition, UTSA, with resources and guidance from UT System, has adopted a forward thinking, novel approach to cybersecurity education that is scalable, cost-efficient, and highly effective.<br />
          <br />
          UTSA has two key designations from the National Security Agency and Department of Homeland Security.<br />
National Center of Academic Excellence in: <br />
          • INFORMATION ASSURANCE RESEARCH (CAE-R)<br />
          • INFORMATION ASSURANCE (IA)/CYBER DEFENSE (CD)</p>
        
        <h2>OPENING:  <a href="http://jobs.sciencecareers.org/job/421618/faculty-cluster-hire-in-cybersecurity/">Faculty Cluster Hire in Cybersecurity </a></h2>
        <hr />
        <h2>Research Centers and Institutes</h2>
        <p>Spanning three colleges – including the Colleges of Business, Engineering and Sciences -  the cybersecurity program includes five nationally recognized research centers, <u>t</u><a href="http://cias.utsa.edu/">he Center for Infrastructure Assurance and Security (CIAS)</a>, <a href="HTTP://ICS.UTSA.EDU">the Institute for Cyber Security (ICS)</a>, <a href="http://business.utsa.edu/ceris/">the Center for Education and Research in Information and Infrastructure Security (CERI2S)</a>, <a href="http://opencloud.utsa.edu/">the Open Cloud Institute (OCI)</a>, and <a href="http://texasenergy.utsa.edu/">the Texas Sustainable Energy Research Institute (TSERI)</a>.  </p>
        <p><strong><a href="http://ics.utsa.edu/">Institute for Cyber Security (ICS)</a></strong><br />
          Ravi Sandhu, Ph.D., Executive Director<br />
        ESTABLISHED 2007</p>
        <p>The ICS focuses on doctoral level sponsored research with special emphasis on security and privacy policies and models applied to Cloud Computing, Internet of Things, Social Computing, Big Data and Cyber-Physical Systems. It operates two world-class academic research labs for Cloud Computing and Malware.<br />
          <br />
          <strong><a href="http://cias.utsa.edu/">Center for Infrastructure Assurance and Security (CIAS)</a></strong><br />
          Greg White, Ph.D., Director<br />
          ESTABLISHED 2001<br />
  <br />
          CIAS is the foremost center for multidisciplinary education and development of operational capabilities in the areas of infrastructure assurance and security. They were selected to lead the response to Presidential Executive Order 13691 to create the Information Sharing and Analysis Organization Standards Organization in 2015.<br />
  <br />
  <strong><a href="http://opencloud.utsa.edu/">Open Cloud Institute (OCI)</a></strong><br />
          Bernard Arulanandam, Ph.D., M.B.A., Interim Director<br />
          ESTABLISHED 2015<br />
  <br />
          The OCI fosters collaboration with industry, positioning UTSA and San Antonio as world leaders in cloud computing technology. The OCI focuses on Cloud Security, Multi Clouds, and Machine Learning.<br />
          Most recently, the OCI Bootcamp and Certification Pipeline was launched with local industry partners,<br />
          employing UTSA students with specific cyber and cloud skill sets.<br />
  <br />
  <strong><a href="http://texasenergy.utsa.edu/">Texas Sustainable Energy Research Institute (TSERI)</a></strong> <br />
          Juan Gomez, Ph.D., P.E., Interim Director<br />
          ESTABLISHED 2010</p>
        <p>The Energy Institute serves as a catalyst for developing innovative research programs in collaboration with local utilities, industry partners, and key stakeholders. The Institute is helping the San Antonio area position itself as the epicenter of the new energy economy.</p>
<h2>Key Cyber Milestones (2015-2016) </h2>
       <ul>
         <li>UTSA&nbsp;Vice President for Research testified for US Rep.&nbsp;Will&nbsp;Hurd’s&nbsp;Congressional hearing &nbsp;<a href="https://hurd.house.gov/media-center/press-releases/rep-hurd-leads-utsa-s-first-congressional-field-hearing">“State of the Cloud”</a>&nbsp;(Sept. 2015)</li>
          <li>UTSA Research hosted a reception celebrating the university’s achievements in cyber and cloud in Washington, D.C., for high-level government and industry officials. Given its success, UTSA plans to host another DC reception in Fall 2016 (Oct. 2015)</li>
          <li>UTSA sponsored and hosted the <a href="http://www.sachamber.org/cwt/External/WCPages/WCEvents/EventDetail.aspx?EventID=3874">San Antonio Converge Summit</a>, organized by the San Antonio Chamber of Commerce, UT System and UTSA (Nov. 2015)</li>
          <li>In partnership with the San Antonio Chamber of Commerce and the Canadian Consulate, UTSA hosted the first-ever <a href="http://www.sachamber.org/cwt/External/WCPages/WCEvents/EventDetail.aspx?EventID=3987">North American Cybersecurity Summit</a><strong> </strong>(Jan.  2016)</li>
          <li>UTSA hosted the <a href="http://www.texasfreshair.org/texas-freshair-data-analytics-/">Texas FreshAIR Big Data and Data Analytics</a> Conference (March 2016)</li>
          <li>UTSA was chosen as the location for the international <a href="http://sanantonio2016.honeynet.org/">Honeynet Project</a> (May 2016)</li>
          <li>Invited keynote speaker and exhibitor at the <a href="http://www.sahcc.org/event/save-the-date-cybersecurity-conference/">2016 Cybersecurity Conference</a>, co-hosted by the U.S. Chamber of Commerce and the Texas Association of Business (June 2016)</li>
          <li>Invited keynote speaker, exhibitor and sponsor of the Cyber Education and Workforce Development Track at <a href="https://www.fbcinc.com/e/CyberTexas/">Cyber Texas</a> (August 2016)</li>
          <li><strong>Gregory White</strong>, professor of computer science and director of the UTSA Center for Infrastructure Assurance and Security (CIAS), and <strong>Glenn Dietrich</strong>, professor of Information Systems and Cyber Security, were  inducted into the Inaugural <a href="http://www.utsa.edu/today/2016/08/hallofhonor.html">San Antonio Cyber Hall of Honor</a>, part of the first class of leaders and pioneers in the local cybersecurity industry (August 2016)</li>
         
        </ul>
<h2>RESEARCH AREAS</h2>

<div style="float: left;">
	<ul>
	  <li>Cyber Security</li>
	  <li>Digital Forensics</li>
	  <li>Cloud and High Performance Computing</li>
	  <li>Software Engineering and Systems</li>
	  <li>Computer Architecture and Embedded Systems</li>
	  <li>Unmanned Systems</li>
	  <li>Bioinformatics</li>
	  <li>Data Analytics</li>
	  <li>Distributed Sensors and Secure Network</li>
	  <li>Electronic Sensors and Actuators</li>
	  <li>Power Electronics and Energy Research</li>
	  <li>Wireless and Fiber Optic Communication</li>
	  <li>Renewable Energy and Smart Grids</li>
	  </ul>
	<p>&nbsp;</p>
</div>

<div style="float: left;">
	<ul>
	  <li>Information Security Management and Strategy</li>
	  <li>Applied Network and Information Systems Security</li>
	  <li>Economic and Psychology of Information Security</li>
	  <li>Internet of Things</li>
	  <li>Distributed Energy Resources</li>
	  <li>Cyber-Physical Systems</li>
	  <li>Honeypot/Honeynet</li>
	  <li>Security of Industrial Control Systems</li>
	  <li>Data Compression</li>
	  <li>Encryption</li>
	  <li>Steganography</li>
	  <li>Intrusion Detection</li>
	  </ul>
	<p>&nbsp; </p>
</div>
<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
<p>&nbsp;</p>


<!--<div id="noborder"><table width="700
" border="1">
  <tr>
    <td><h4>• Security of Cyber Physical Systems</h4></td>
    <td><h4>• Digital Signal Processing</h4></td>
    <td><h4>• Wireless Security and Authentication </h4></td>
  </tr>
  <tr>
    <td><h4>• Digital Forensics</h4></td>
    <td><h4>• Insider Threat</h4></td>
    <td><h4>&nbsp;</h4></td>
  </tr>
</table></div>-->
       
        
       <hr />
        
        <h2>New UTSA faculty for 2016–2017</h2>
        <h3><strong>H. Raghav Rao, Ph.D.</strong></h3>
        <p><img src="images/RaghavRao-210.png" alt="rao" width="210" height="231" align="left" style="padding-right:10px" />Rao’s research interests include information assurance, management information systems, emergency response management systems, decision support systems and e-business. A renowned academic, he was ranked No. 3 in publication productivity internationally in a Communications of the Association for Information Systems study. </p>
      <p>Rao has authored or co-authored more than 175 technical publications and co-edited four books including Information Assurance, Security and Privacy Services. He is co-editor in chief of Information Systems Frontiers, advisory editor of Decision Support Systems and senior editor at MIS Quarterly. He also chairs a United Nations affiliated working group for Information Systems Security Research. Rao has recently been invited to join the National Academy of Medicine Standing Committee on Medical and Public Health Research during large-scale emergency events.</p>
      <p> In addition to being a prolific scholar, he is also renowned for his ability to secure external grant funding. He currently has three on-going National Science Foundation grants covering topics such as insider threat, phishing and the recent security breach at the U.S. Office of Personnel Management. He has also received funding for his research from the Department of Defense and the National Security Agency.        </p>
      <p>Prior to joining UTSA’s faculty, he served as the SUNY Distinguished Service Professor at the University of Buffalo, where he had worked since 1987. He received his Ph.D. from the Krannert Graduate School at Purdue University. Rao is a graduate of the FBI Citizens Academy and a member of Infragard.</p>
        <div id="noborder"></div>
        <h2 align="center"><a href="cyber-faculty.asp">Cyber Faculty at UTSA </a></h2>
        <div id="noborder"><table width="700" border="1">
  <tr>
    <td><h2 align="center"><a href="cyber-faculty.asp#computer"><strong>Computer Science</strong></a></h2></td>
    <td><h2 align="center"><a href="cyber-faculty.asp#electrical"><strong>Electrical &amp; Computer<br />
Engineering</strong></a></h2></td>
    <td><h2 align="center"><a href="cyber-faculty.asp#information"><strong>Information Systems <br />
      &amp; Cyber Security</strong></a></h2></td>
    <td><h2 align="center"><a href="cyber-faculty.asp#management"><strong>Management Science <br />
      &amp; Statistics</strong></a></h2></td>
   
  </tr>
</table></div>
<hr />
<h2>Cyber Hiring Committee</h2>
<p>Chair: Bernard Arulanandam<br />
  College of Liberal Arts:	Dan Gelo | Raquel Marquez <br />
  College of Business:	Gerry Sanders | Hamid Beladi <br />
  College of Engineering: JoAnn Browning | Harry Millwater <br />
  College of Sciences:	George Perry | Floyd Wormley</p>

<h2>Cyber Updates</h2>
        <p> <a href="http://utsa.edu/cybersecurity">http://utsa.edu/cybersecurity</a><a href="http://www.utsa.edu/spotlights/brainresearch/"></a></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
