<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<title>Faculty Clusters | UTSA | The University of Texas at San Antonio</title>

<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<style>
#content table {border:none;}
#content table th {border:none;}
#content table tr {border:none;}
#content table td {border:none;}
</style>
<style> #col-main {
    float: none;
</style>
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
  &lt;
  <div id="content" class="clearfix">
<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
<a name="top" id="top"></a>
<p><img src="images/cyber_banner.jpg" width="950" height="271" /></p></style>
<h2>Faculty Hiring Cluster</h2><h1><a href="cyber.asp">CYBER</a></h1>
<p>The University of Texas at San Antonio (UTSA) embarked on a focused cluster hiring plan under the Gold Star Initiative, to recruit top-tier researchers over a four year period. The plan focuses on strategic areas of research excellence, and includes cybersecurity, cloud computing and data analytics.  The recruitment of GoldStar Initiative researchers is in addition to the hiring of top-tier faculty that takes place on a regular basis at UTSA to support academic excellence. </p>
<p>With more than a thousand undergraduate students enrolled in our cyber and related programs, UTSA has over fifty faculty members dedicated to cyber research and teaching. Of those, six are endowed faculty positions including chairs endowed by AT&amp;T and Rackspace through the 80/20 Foundation, and includes seven NSF CAREER award winners. Since 2010, UTSA has received over $28 million in cyber-related research awards.</p>
<p>Given our growth and momentum, UTSA is increasing the number of cyber faculty, to find candidates to foster collaborative research, education and outreach and to create interdisciplinary areas of knowledge that will advance the field of cybersecurity. This includes enterprise security, cyber analytics, cyber decision support cloud computing security, embedded systems security, privacy and data protection.</p>
<h2>OPENING:  <a href="http://jobs.sciencecareers.org/job/421618/faculty-cluster-hire-in-cybersecurity/">Faculty Cluster Hire in Cybersecurity </a></h2>
<hr size="10" class="blue" />
<h1>UTSA Faculty</h1>
<table width="950" border="1">
  <tr>
    <td><h2 align="center"><a href="#computer"><strong>Computer Science</strong></a></h2></td>
    <td><h2 align="center"><a href="#electrical"><strong>Electrical &amp; Computer<br />
Engineering</strong></a></h2></td>
    <td><h2 align="center"><a href="#information"><strong>Information Systems <br />
      &amp; Cyber Security</strong></a></h2></td>
    <td><h2 align="center"><a href="#management"><strong>Management Science <br />
      &amp; Statistics</strong></a></h2></td>
   
  </tr>
</table>
<hr size="10" class="blue" />
<h2><strong><a name="computer" id="computer"></a>Department of Computer Science | College of Sciences</strong></h2>
<h3><strong>Rajendra V. Boppana, Ph.D</strong></h3
    Cortical microcircuits process sensory information to drive behavior. >
</h3>
<p><img src="images/rajboppana-113.png" width="113" height="103" align="left" style="padding-right:10px" /><strong>Professor and Interim Chair </strong></p>
<p>• Computer Networks<br />
• Computer and Information Security</p>
<p>&nbsp;</p>
<hr />
<h3><strong>Jianwei Niu, Ph.D</strong></h3>
<p><img src="images/JianweiNiu-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" /><strong>Associate Professor of Computer Science</strong></p>
<p>• Software Engineering<br />
• Computer and Information Security</p>
<p>&nbsp;</p>
<hr />
<h3><strong>Ravi Sandhu, Ph.D</strong></h3>
<p><img src="images/RaviSandhu-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />
  <strong>Executive Director of Institute for Cyber Security (ICS)<br />
  Lutcher Brown Endowed Chair in Cyber Security<br />
  Professor of Computer Science</strong></p>
<div style="float: left;"><p>• Secure Information Sharing<br />
• Secure Social Computing<br />
• Infrastructure Assurance</p></div>
<div style="float: left;"><p>&nbsp; • Trustworthy Cloud Computing<br />
&nbsp; • Secure SOA<br />
&nbsp; • Botnet Analysis and Detection</p></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<h3><strong>Gregory White, Ph.D</strong></h3>
  <p><img src="images/GregWhite-113.png" alt="white" width="113" height="103" align="left" style="padding-right:10px"/><strong>Director of CIAS<br />
  Professor</strong></p>
<p>• Computer and Information Security</p>
<p>&nbsp;</p>
<hr />
<h3>
  <strong>Shouhuai Xu, Ph.D</strong></h3>
  <p><img src="images/ShouhuaiXu-113.png" alt="xu" width="113" height="103" align="left" style="padding-right:10px"/><strong>Professor </strong></p>
<p>• Computer and Information Security<br />
• Cryptography; Intrusion Detection</p>
<p>&nbsp;</p>

<hr />
<h3><strong>Meng Yu, Ph.D</strong></h3>
  <p><img src="images/MengYu-113.png" alt="xu" width="113" height="103" align="left" style="padding-right:10px"/><strong>Associate Professor </strong></p>
<p>• Computer and system security<br />
  • Cloud computing<br />
  • Virtualization
    </p>
 <hr />
  <h3><strong>Tongping Liu, Ph.D</strong></h3>
  <p><img src="images/TongpingLiu-113ot.png" alt="liu" width="113" height="103" align="left" style="padding-right:10px"/><strong>Assistant Professor</strong></p>
  <p>• Parallel computing<br />
  • Big data<br />
  • Operating systems</p>
<hr />
<h3><strong>Mikhail Gubanov, Ph.D</strong></h3>
<p><img src="images/MichaelGubanov-113.png" alt="gubanov" width="113" height="103" align="left" style="padding-right:10px"/><strong>Cloud Technology Endowed Assistant Professor</strong></p>
<p>• Big data<br />
  • Machine learning<br />
  • Natural language processing<br />
  • Biomedical informatics. </p>
<h4 align="right"><a href="#top">BACK TO TOP </a></h4>
<hr size="10" class="blue" />
<h2><strong><a name="electrical" id="electrical"></a>Electrical and Computer Engineering | College of Engineering</strong></h2>
<h3><strong>Ram Krishnan, Ph.D.</strong></h3>
  <p><img src="images/RamKrishnan-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Assistant Professor</strong></p>
  <p>• Computer security<br />
    • Authorization and authentication aspects of computing systems.<br />
    • Addressing such issues in specific application domains such as cloud computing.</p>
 
  <p>&nbsp;</p>
  <hr />
  <h3><strong>Wei-Ming Lin, Ph.D.</strong></h3>
  <p><img src="images/Wei-MingLin-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Professor of Electrical and Computer Engineering</strong></p>
 <div style="float: left;"><p> • Computer Architecture<br />
  • Parallel and Distributed Computing<br />
  • Computer Network Routing</p>
  </div>
  <div style="float: left;"><p>&nbsp; • Real-Time Performance Optimization Methodologies<br />
  &nbsp; • Network Intrusion Detection</p></div>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <hr />
  <h3><strong>Chunjiang Qian, Ph.D.</strong><br />
  </h3>
  <p><img src="images/ChunjiangQian-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Professor and Assistant Chair</strong></p>
  <p>• Non linear control, robust and adaptive control, output feedback control, homogeneous system theory, optimal control, and fault detection<br />
  • Flight control, modeling and aerospace systems, and control of UAVs<br />
  • Digital control and its implementation, hybrid control systems, real-time optimal control and model predictive control<br />
  • Cyber physical systems, network control, cyber security in control systems<br />
  • System modeling and controller design in power plants, control and sensing in smart grid<br />
  • Biodynamic modeling, control of biomedical systems, and human gait identification</p>
<h3>&nbsp;</h3>
<hr size="10" class="blue" />
<h2><strong><a name="information" id="information"></a>Department of Information Systems and Cyber Security | College of Business</strong></h2>

<h3><strong>Yoris A. Au, Ph.D.</strong></h3>

<p><img src="images/YorisAu-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />   <strong>Department Chair of Information Systems and Cyber Security & Associate Professor</strong></p>
<p>• Economics of cloud computing and cyber security, technology adoption<br />
• Web and mobile application security<br /> 
• Big data technology
</p>
<hr />

<h3><strong>Nicole Beebe, Ph.D, CISSP </strong></h3>
 <p><img src="images/Beebe-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />   <strong>Associate Professor &amp; Melvin Lachman Distinguished Professor in Entrepreneurship<br />
 Director, Center for Education and Research in Information and Infrastructure Security (CERI²S)</strong></p>
 <p>•  Digital forensics (media and network intrusion forensics; intelligent forensic algorithms)<br />
  • Cyber security (behavioral, managerial and technical issues)<br />
  • Data Analytics (cyber analytics; computational forensics)
  </p>
<hr />

<h3><strong>Kim-Kwang Raymond Choo, Ph.D..</strong></h3>
 <p><img src="images/Kim-KwangRaymondChoo-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />   <strong>Associate Professor and Cloud Technology Endowed Professorship</strong></p>
 <p>•  Digital forensics<br />
• Cryptographic Protocols
  </p>
 <p>&nbsp;</p>
<hr />

<h3><strong>Glenn Dietrich, Ph.D. CISSP</strong></h3>
  <p> <img src="images/GlennDietrich-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />    <strong>Professor </strong></p>
  <p>• Information assurance<br />
  • Cyber physical systems security and privacy<br />
  • Technology management and innovation </p>
<hr />
<h3><strong>Max Kilger, Ph.D.</strong></h3>
  <p><img src="images/MaxKilger-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Director of Data Analytics Program &amp; Senior Lecturer</strong></p>
  <p>• Research the motivations of malicious online actors and entities as well as changes in the hacking community social structure. <br />
  • Develop a better theoretical understanding of how social dynamics affect the nature of the current and emerging cyberthreat matrix.</p>
  <p>&nbsp;</p>
<hr />
<h3><strong>Myung Ko, Ph.D.</strong></h3>
  <p><img src="images/MyungKo-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Associate Professor</strong></p>
  <div style="float: left;"><p>• Impact of IT on organizations<br />
  • Environmental performance<br />
  • Data mining</p></div>
 <div style="float: left;"><p>&nbsp; • Economics of security breaches<br />
  &nbsp; • Online banking<br />
  &nbsp; • Word of mouth</p></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<hr />
<h3><strong>Zhechao Charles Liu, Ph.D.</strong></h3>
  <p><img src="images/charles_liu-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Associate Professor</strong></p>
  <div style="float: left;"><p>• Economics of IS<br />
• Network externalities<br />
• Standards/platform competition</p></div>
 <div style="float: left;"><p>&nbsp; • Mobile apps<br />
&nbsp; • Game theory modeling</p></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<hr />
<h3><strong>Chino Rao, Ph.D.</strong></h3>
 <p> <img src="images/ChinoRao-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Professor</strong></p>
  <p>• Software piracy <br />
  • Information security culture <br />
  • Electronic commerce <br />
  • Technological support of groups</p>
<hr />
<h3><strong>H. Raghav Rao, Ph.D.</strong></h3>
  <p><img src="images/RaghavRao-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>AT&amp;T Distinguished Chair in Infrastructure Assurance and Security and Professor</strong></p>
 <div style="float: left;"><p>•  Management information systems<br />
  • Decision support systems<br />
  • E-business<br /></div>
 <div style="float: left;"><p>&nbsp;  • Emergency response management systems<br />
  &nbsp; • Information assurance </p></div>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
<hr />
<h3><strong>Rohit Valecha, Ph.D.</strong></h3>
  <p><img src="images/RohitValecha-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Assistant Professor</strong></p>
  <p>• Healthcare security and privacy </p>
  <p>&nbsp;</p>
  <p>&nbsp;</p>
  <hr />
<h3><strong>Diane Walz, Ph.D.</strong></h3>
  <p><img src="images/DianeWalz-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Professor</strong></p>
  <p>• Software development and knowledge sharing <br />
  • Knowledge sharing between and among application developers<br />
  • Agency theory application to password complexity <br />
  • Characteristics of IS professionals </p>
  <hr />
<h3><strong>John Warren, Ph.D.</strong></h3>
  <p><img src="images/JohnWarren-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Associate Professor</strong></p>
  <p>• Virtual team interaction and performance<br />
• Virtual Communities<br />
• Information Technology Adoption and Implementation</p>
  <h3>&nbsp;</h3>
   <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
<hr size="10" class="blue" />
<h2><strong><a name="management" id="management"></a>Department of Management Science and Statistics | College of Business</strong></h2>
<h3><strong>Ram Tripathi, Ph.D.</strong></h3>
  <p><img src="images/RamTripathi-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" />  <strong>Professor </strong></p>
 <div style="float: left;"><p>• Models for count data<br />
  • Biostatistics<br />
  • Inflated models</div>
 <div style="float: left;"><p>&nbsp; • Statistical inference<br />
   &nbsp; • Survival analysis</p></div>
     <p>&nbsp;</p>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <hr />
   <h3><strong>Kefeng Xu, Ph.D.</strong></h3>
   <p><img src="images/KefengXu-113.png" alt="" width="113" height="103" align="left" style="padding-right:10px" /><strong>Associate Professor</strong></p>
   
   
   
   
   <div style="float: left;">
     <p>• Operations, logistics and information management strategy<br />
       • Global and domestic supply chain<br />
       • Inventory and supply management</p>
     </div>
   
   <div style="float: left;">
     <p>&nbsp; • Transportation policy<br />
       &nbsp; • Cost and demand studies </p>
     </div>
   
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <p>&nbsp;</p>
   <hr />
   <h3> <strong>Keying Ye, Ph.D.</strong></h3>
   <p><img src="images/KeyingYe-113.png" alt="" width="111" height="101" align="left" style="padding-right:10px" />  <strong>Professor </strong></p>
   <p>• Inference and methods <br />
     • Mathematical Statistics <br />
     • Statistical applications in biostatistics, cyber security, environmental, ecological and geological sciences, experimental design and industrial statistics</p>
   <h3>&nbsp;</h3>
   <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
   
</div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body></html>
