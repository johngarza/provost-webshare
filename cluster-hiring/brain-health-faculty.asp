<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<title>Faculty Clusters | UTSA | The University of Texas at San Antonio</title>

<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<style>
#content table {border:none;}
#content table th {border:none;}
#content table tr {border:none;}
#content table td {border:none;}
</style>
<style> #col-main {
    float: none;
</style>
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
  <div id="content" class="clearfix">
<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>

<img src="images/brainhealth_banner2.jpg" width="950" height="271" />
<h2><a name="top" id="top"></a>Faculty Hiring Cluster</h2>
<h1><a href="brain-health.asp">BRAIN HEALTH</a></h1>
<p>The UTSA Brain Health Initiative is an interdisciplinary, research-dedicated program that encompasses a broad spectrum of research interests and expertise including neuroscience, regenerative medicine, medicinal chemistry, biomedical engineering and data analytics. State-of-the-art laboratory facilities encompassing an entire floor of a new research building will be dedicated to Brain Health interdisciplinary research. UTSA has long pursued excellence in the study of the brain through its existing Neurosciences Institute, which has active participation from approximately 25 primary investigators and their research teams. To this strength, expertise has been added in brain-related neurodegenerative diseases including Alzheimer’s and Parkinson’s. Additional groups at UTSA are focused on regenerative medicine, stem cell therapies, medicinal chemistry, drug design, neuroinflammation and the use of data analytics to study the pathophysiology, epidemiology, diagnosis and potential treatment of diseases or debilitations impacting brain health.</p>
<p>UTSA is well situated to foster studies of health disparities associated with brain health, and enjoys partnerships with multiple academic, research and medical institutions within San Antonio and the South Texas region. </p>
<h2>OPENING: <a href="http://jobs.sciencecareers.org/job/419681/professor-endowed-chair-in-brain-health-/">UTSA Professor/Endowed Chair in Brain Health</a></h2>
<p>&nbsp;</p>
<hr size="10" class="blue" />
<h1>UTSA Faculty</h1>
  <table width="950" border="1">
  <tr>
    <td><h2 align="center"><a href="#biology"><strong>Biology</strong></a></h2></td>
    <td><h2 align="center"><a href="#chemistry"><strong>Chemistry</strong></a></h2></td>
    <td><h2 align="center"><a href="#ece"><strong>Electrical & Computer <br />
      Engineering</strong></a></h2></td>
	<td><h2 align="center"><a href="#health"><strong>Kinesiology, Health <br />
	  & Nutrition</strong></a></h2></td>
    <td colspan="2"><h2 align="center"><a href="#psychology"><strong>Psychology</strong></a></h2>      </h2></td>
  </tr>
  
</table>
<hr size="10" class="blue" />
<h2><strong><a name="biology" id="biology"></a>Department of Biology | College of Sciences</strong></h2>

<h3><strong>Apicella Lab | </strong><strong><em>Sensory Microcircuits</em></strong></h3
    Cortical microcircuits process sensory information to drive behavior. ></h3>
  <p><img src="images/Apicella-113.jpg" width="113" height="103" align="left" style="padding-right:10px" />Deciphering how populations of neurons encode information, generate perceptions, and execute behavioral decisions require working at both cellular and system levels. </p>
    <p><strong>Dr. Alfonso Apicella</strong> manipulates specific neurons in behaving mice; he turns neurons 'ON' and 'OFF' using optogenetic and pharmacogenetic approaches in order to understand how specific subsets of neurons contribute to sensory processing and behavior.
    </p>
   <hr />
    <h3><strong>Arulanandam Lab |  <em>Host Pathogen Interaction and modulation of Inflammatory Responses at Mucosal Surfaces</em></strong></h3>
    <p><img src="images/Arulanandam-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Host-pathogen interaction at mucosal surfaces lead to inflammatory responses that influence the protective/ pathogenic environment in vivo. Altered inflammatory milieu at the body’s mucosal niches may significantly affect brain function and health of the infected host. </p>
    <p> Using model organisms including Chlamydia trachomatis, Francisella tularensis and Acinetobacter baumannii, <strong>Dr. Bernard Arulanandam</strong> investigates the molecular mechanisms involved in generation of these inflammatory cascades at the genital, respiratory and gastric mucosae respectively, in animal models and human cohorts. He uses omics-level approaches to study the role of host factors including microRNAs and metabolites and the contribution to generation of protective or pathogenic inflammatory responses in infected hosts. </p>
	<hr />
	<h3><strong>Barea Lab | <em>The Aging Brain</em></strong></h3>
    <p><img src="images/Barea-113.jpg" alt="" width="111" height="101" align="left" style="padding-right:10px" />Cognitive decline in aging may result from changes in cellular and molecular mechanisms. Aging is associated with oxidative stress, which may be the underlying cause of impairment in learning and memory.</p>
    <p> <strong>Dr. Edwin Barea-Rodriguez</strong> seeks to understand how age-related oxidative changes affect the brain, and how resulting changes lead to impairment of physiological processes underlying learning and memory.</p>
<hr />
	<h3><strong>Cardona Lab |  <em>Neuroimmunology</em></strong>
     
    </h3>
    <p><img src="images/Cardona-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Inflammatory processes exert their effect on the central nervous system via proteins called cytokines.  Cytokines can alter neuronal and microglial pathology, change how cellular precursors are trafficked, and can mediate the progression of chronic immune responses.</p>
    <p> <strong>Dr. Astrid Cardona</strong>’s research focuses on the immune-pathology of chronic inflammatory diseases that affect the central nervous system, like multiple sclerosis.  Specifically, she is looking at the interaction of the cytokine fractalkine with microglial cells and neurons, and how they contribute to the pathogenosis of disease. </p>
    </p>
    <hr /><h3><strong>Derrick Lab | <em>Memory Modes in the Brain</em></strong>
    </h3>
    <p><img src="images/Derrick-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />The brain is essential for memory, suggesting there must be a physical change in the brain in order for a new memory to be encoded and persist over time. 
    </p>
  <p><strong>Dr. Brian Derrick</strong> investigates these changes, and how they are initiated in the hippocampus.&nbsp; His lab is using combined pharmacological, physiological and behavioral techniques to understand hippocampal transitions from the “recall” mode to the “encoding” mode, and how this is mediated by novelty. </p>
 <hr /> 
 	<h3><strong>Forsthuber Lab |</strong> <strong><em>Immune mechanisms driving autoimmune</em></strong><em> <strong>encephalomyelitis</strong></em> </h3>
   <p><img src="images/Forsthuber-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Erroneous activation of the immune system can lead to autoimmune diseases such as multiple sclerosis (MS). <strong>Dr. Thomas Forsthuber</strong>’s lab pursues several lines of investigation to understand how the immune system, in particular T cells, contribute to autoimmune diseases and how to modulate T cell immunity for therapeutic purposes in humans.  </p>
   <p>Specifically, he studies immune mechanisms in the central nervous system in experimental autoimmune encephalomyelitis (EAE), the animal model for MS. The lab has generated transgenic and knockout mouse models to investigate the role of important autoimmune disease-related genes such as HLA-DR.  His research is aimed towards direct applicability to human diseases, for example by developing novel drugs for autoimmune diseases and biomarkers to monitor the efficacy of treatments for autoimmune diseases. </p>
   <hr /><h3><strong>Gaufo Lab | <em>Early Brain Patterning</em></strong></h3>
    <p><img src="images/Gaufo-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />During development, neurons are assigned identities, migrate into the correct positions and make specific contacts along dorsal and ventral axes. &nbsp;Cell fate markers can be used to both identify neurons by origin, and follow them through migration and maturation. </p>
    <p> <strong>Dr. Gary Gaufo</strong> looks at how patterning programs generate complexity in vertebrates by deciphering the converging mechanisms that control stem cell fate.  His work is informing how we understand and  develop interventions for birth defects and disease.</p>
  <hr />
    <h3><strong>Jaffe Lab | </strong><strong><em>Neurons as Processors</em></strong></h3>
    <p><img src="images/Jaffe-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Neurons transmit and process information in the brain. Their function is determined to a large extent by how they convert a spectrum of spatial and temporal patterns of stimulation into electro-chemical responses.&nbsp;&nbsp;</p>
    <p>
    <strong>Dr. David Jaffe</strong> uses a combination of computer modeling and experimentation to explore how neurons, singly and in networks, filter and process information in normal and diseased states, such as epilepsy, Alzheimer’s disease, and pain processing. </p>
    <hr />
    
    
    <h3><strong>Ko Lab | <em>Neurostatistics  *also with College of Business</em></strong></h3>
    <p><img src="images/Ko-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Neurons process and transmit information in seemingly random electrical spiking patterns. How can one extract the signal from the noise?  </p>
    <p> <strong>Dr. Daijin Ko</strong>’s research focuses on the use of stochastic modeling and machine learning methods to build statistical tools and know-how for application to neuroscience.  Recent work includes characterization and modeling of groups of neural spike trains and multiple group comparison of point processes. </p>
    <hr />
   <h3><strong>Lee Lab | <em>Cell Cycle and Neurodegeneration</em></strong></h3>
   <p><img src="images/Lee-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Neurodegenerative diseases cause neuronal death, but how?  Neurons are non-proliferative, meaning their cell-cycle is arrested; perhaps accidental activation of the cell-cycle sets them on a course to die. </p>
   <p> <strong>Dr. Hyoung-gon Lee</strong>’s research hypothesizes that cell cycle re-entry in the CNS is a key pathogenic mechanism in neurodegeneration.  He is using transgenic mouse models to dissect and understand what might trigger cell cycle activation and whether this event bears any causal relationship with neurodegeneration like that observed in Alzheimer’s disease. </p>
<hr />
   <h3><strong>Lin Lab |  <em>From Stem Cell to Neuron</em></strong></h3>
   <p><img src="images/Lin-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Neurons differentiate from stem cells in the embryonic nervous system.&nbsp; Errors in stem cell differentiation and migration can cause developmental abnormalities and tumors. </p>
   <p> <strong>Dr. Annie Lin</strong> is identifying factors that regulate stem cells as they become neurons. She uses animal models, molecular techniques, and high-throughput genome-wide analyses to understand cellular differentiation in the nervous system.  Her work is aimed at developing therapies for degenerative conditions like Parkinson's Disease and certain cancers.</p>
   <hr />
 <h3><strong>Maroof Lab | <em>Cortical Interneuron Fate and Function in Disease</em></strong></h3>
   <p><img src="images/fpo-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Projection neurons transmit information between brain regions, but it’s the local circuit interneurons that shape the signals being transmitted.  The diversity of interneurons confers the powerful computational capacity of the CNS, and their dysfunction results in pathological states.  </p>
  <p> <strong>Dr. Asif Maroof</strong> is using cutting-edge transgenic technology and stem cells to study the differentiation of cortical interneurons.  He is determining their diversity, how they connect, and serve information flow in the brain.  His research is fundamental to building the next generation of cell-based therapies for a whole array of neurological disorders and diseases.</p>
 <hr />
 <h3><strong>McCarrey Lab |  <em>Nonhuman Primate Models of Stem Cell Therapies</em></strong></h3>
   <p><img src="images/McCarrey-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Pluripotent stem cells promise a medical revolution in the form of novel cell-based therapies. However the efficacy and safety of these therapies must first be optimized in clinically relevant animal models.</p>
   <p> <strong>Dr. John McCarrey</strong> studies stem cells derived from baboons which can be manipulated <em>in vitro </em>and then transplanted into animals to test their function <em>in vivo</em>. He uses genomic and epigenomic techniques to analyze these cells. </p>
 <hr />
    <h3><strong>Muzzio Lab | <em>Memory Integration</em></strong></h3>
    <p><img src="images/Muzzio-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />The hippocampus plays a crucial role in encoding and retrieval of episodic memories.  These representations are used to generate a cognitive map that is necessary for navigation behaviors and providing the spatial context for events. </p>
    <p> <strong>Dr. Isabel Muzzio</strong>’s lab studies how the brain forms and uses representations of the external world during navigation by combining long-term single cell recordings in freely moving mice with pharmacological, genetic, behavioral, and computational tools.</p>
	<hr />
   <h3><strong>Navara Lab | <em>Pluripotent Stem Cell Biology</em></strong></h3>
   <p><img src="images/Navara-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Pluripotent stem cells have the ability to differentiate into all cells of the body and therefore provide a novel opportunity for cell therapies of neurodegenerative diseases including Parkinson’s Disease (PD) and Alzheimer’s Disease (AD). Additionally they also provide a unique opportunity to study human disease in vitro in the exact genetic background of the patients themselves.  </p>
   <p><strong>Dr. Navara</strong> has produced induced pluripotent stem cells (iPSCs) from the baboon as a model for the optimization of cell therapy for PD to identify the proper cell type, cell number and means of transplant.  He also generates iPSCs from patients with AD and differentiates these into neurons to be used to understand the pathogenesis of the disease. Dr. Navara is particularly interested in the role of the mitochondria in the progression of the disease and uses a combination of live cell microscopy and molecular and cellular techniques.</p>
    <hr /><h3><strong>Paladini Lab |  <em>The Neurophysiology of Delight and Disappointment</em></strong></h3>
    <p><img src="images/Paladini-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Midbrain dopaminergic neurons integrate widespread information from memory,&nbsp;sensory inputs, and cognitive state to produce signals that direct motivated behavior like eating, engaging in a task, and even sex. </p>
    <p> <strong>Dr. Carlos Paladini</strong> uses a powerful stable of viral and pharmacological tools to study the electrical activity of dopamine neurons.  He looks at how they integrate information from various brain pathways to generate choices, and considers how drugs of abuse hijack this system to alter reward-seeking behavior. </p>
    <hr />
    <h3><strong>Perry Lab | <em>The Cytopathology of Alzheimer’s Disease</em></strong></h3>
   <p><img src="images/Perry-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Oxidative damage is the initial cytopathology in Alzheimer’s disease. The sequence of events leading to neuronal oxidative damage and the source of oxygen radicals in this devastating degenerative disease process are as yet unknown. </p>
  <p> <strong>Dean George Perry</strong>’s research focuses on the molecular pathology of Alzheimer’s disease, with special emphasis on the metabolic basis for the mitochondrial damage in vulnerable neurons and the consequences of RNA oxidation on protein fidelity.</p>
  <hr /><h3><strong>Robbins Lab |  <em>Data Visualization and Modeling</em></strong></h3>
    <p><img src="images/Robbins-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Technological advances allow researchers to collect vast amounts of data on brain function simultaneously.  Large array-based data sets, such as multi-channel EEG, contain huge amounts of information.  Data analysis and visualization tools must keep apace with technological advances in order to extract meaning from large volumes of complex data.  </p>
    <p> <strong>Dr. Kay Robbins</strong> builds new tools that integrate machine learning, visualization, statistical analysis and advanced data-handling techniques to mine large scale data sets.  Her work is connecting the dots between EEG data and human performance.  **Also with the Dept. of Computer Science </p>
   <hr /> <h3><strong>Santamaria Lab |<em>  The Geometry of Communication</em></strong></h3>
   <p><img src="images/Santamaria-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />Of the hundreds of types of neurons in the brain, each has its own unique shape and complexity. &nbsp;The specialization of neuron shape suggests that neuronal geometry is critical to the function of each cell circuit. </p>
   <p> <strong>Dr. Fidel Santamaria</strong> combines theory, computation and experiments to study how structure affects integration of electrical and biochemical intracellular signals. His work spans studies from nanoscopic volumes within a single dendritic spine to entire neurons.</p>
   <hr /><h3><strong>Suter Lab |  <em>The Neurobiology of Reproduction</em></strong></h3>
   <p><img src="images/Suter-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />
    GnRH neurons in the hypothalamus control the onset of puberty and regulate fertility.  They simplify into a linear dendritic structure during puberty, but remain able to filter and integrate information from brain inputs to regulate sexual reproduction.</p>
   <p> 
     <strong>Dr. Kelly Suter</strong> examines the electrical signals of GnRH neurons to determine how synaptic inputs along the linear dendrite control the decision to fire and release hormone in an oscillatory manner.&nbsp;</p>
 <hr />
   <h3><strong>Troyer Lab | <em>Neuronal Clocks and Coding</em></strong></h3>
   <p><img src="images/Troyer-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />
    The brain generates behavior. &nbsp;Since neural and behavioral events unfold over the same units of time, temporal analysis can be a powerful way to understand how the brain builds behavior. </p>
   <p> <strong>Dr. Todd Troyer</strong> uses learned birdsongs, which are sequences of precisely timed utterances driven by neuronal activity, as a “Rosetta Stone” for decoding the neural signals that command learned behavior. His lab uses computational models to bridge the neural and behavioral levels of analysis. </p>
 <hr />
   <h3><strong>Wanat Lab | <em>The Neurobiology of Motivated Behavior</em></strong></h3>
   <p><img src="images/Wanat-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />
    Motivation drives our decisions and actions and involves the neurotransmitter dopamine. Motivation can be altered in psychiatric disorders and drug addiction.</p>
   <p> <strong>Dr. Matt Wanat</strong> studies the function of the dopamine system in rodents, both under normal conditions and in models of drug addiction and depression.&nbsp; His research employs both in vitro and in vivo experiments, including using voltammetry to record dopamine release with a subsecond temporal resolution in behaving rodents. </p>
  <hr /> <h3><strong>Wicha Lab | </strong><strong><em>The Bilingual Brain</em></strong> </h3>
   <p><img src="images/Wicha-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />
    In order to keep up with the fast pace of speech, the brain might predict words based on context. &nbsp;Language processing would be quick when predictions are correct, but slowed when predictions are wrong. </p>
   <p> <strong>Dr. Nicole Wicha</strong> studies prediction and language processing in the bilingual brain. The moment at which meaning is extracted from words can be seen in ERP brain waves. &nbsp;She uses these as a tool to examine how the bilingual brain processes meaning in two languages simultaneously, and whether the languages interfere or interact. </p>
   <hr /><h3><strong>Wilson Lab | <em>Cellular Computation in the Basal Ganglia</em></strong> </h3>
   <p><img src="images/Wilson-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />
    The brain’s electrical signals control our muscles and movements.&nbsp; Parkinson’s disease results from loss of midbrain dopamine neurons, but its symptoms result from pathological electrical signals created and communicated among the cells that remain.&nbsp;</p>
   <p><strong>Dr. Charles Wilson</strong> uses mathematical models and cell-specific electrophysiology to understand the computations embedded in the electrical signals of the basal ganglia, and their dysfunction in Parkinson’s Disease.&nbsp; His work is informing the next generation of Deep Brain Stimulation therapy for Parkinson's patients. </p>
	
  <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
  <hr size="10" class="blue" />
  <h2><strong><a name="chemistry" id="chemistry"></a>Department of Chemistry | College of Sciences</strong></h2>
  <h3><strong>Frantz Lab</strong><strong> </strong></h3>
    <p><img src="images/Frantz-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /><strong>Dr. Douglas Frantz</strong>’s research in medicinal chemistry focuses on identifying new potential small molecule therapies in various CNS indications including Alzheimer’s, brain cancer and chronic pain.  This involves expertise not only in the synthesis of compounds with biological activity within the Frantz lab but also close collaborations with biologist, biochemists and pharmacologists to understand the underlying biological mechanisms associated with each disease.  </p>
    <p>Recently, Dr. Frantz has built a strong collaborative program with the Open Innovation Drug Discovery (OIDD) program at Eli Lilly where students in the laboratory work closely with Lilly scientists to optimize lead compounds into pre-clinical candidates in two CNS indications.  This innovative collaboration between academia and industry provides fruitful avenues for discovery at the bench that may ultimately translate to treatments at the bedside.</p>
 <hr /> 
 <h3><strong>McHardy Lab &amp; Center For Innovative Drug Discovery (CIDD) | <em>CNS Drug Discovery</em></strong></h3>
   <p><img src="images/McHardy-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Dr. McHardy</strong>’s research has focused on numerous small molecule medicinal chemistry/drug discovery approaches to psychotherapeutic and neurodegenerative diseases, including the discovery of successful clinical candidates for addiction, Schizophrenia and ADHD during his time at Pfizer Neuroscience Drug Discovery.  </p>
   <p>Currently, Dr. McHardy’s lab and the CIDD focus on developing collaborative research programs, integrating medicinal chemistry, pharmacology, structural biology, biochemistry, and molecular biology across a number of therapeutic areas.  The CIDD medicinal chemistry core facility (UTSA) and high-throughput screening core facility (UTHSCSA) provide a diverse array of core facilities and expertise to facilitate the translation of basic scientific discoveries into tangible pre-clinical candidate drugs that can be further developed into clinical therapies for human disease.     </p>
    <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
  <hr size="10" class="blue" /> 
  <h2><strong><a name="ece" id="ece"></a>Department of Electrical and Computer Engineering | College of Engineering</strong></h2>
  <h3><strong>Agaian Lab</strong><strong> </strong></h3>
 <p><img src="images/Agaian-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Dr. Sos Agaian</strong>’s area of expertise is on the modeling of neural networks for biological vision and brain information processing.  In an attempt to introduce brain-like artificial systems that perform robust classification and information processing in spite of varied, changing data input conditions. Dr. Agaian’s group are working on developing several brain&nbsp;inspired&nbsp;information processing tools including an&nbsp;extending&nbsp;deep neural networks&nbsp;structure (a network with many hidden layers),&nbsp;deep learning method (machine learning of complex representations in a deep neural network),&nbsp;and human visual system based image processing and recognition systems (security application, cancer screening and prediction application, human behavior and stress classification systems).</p>
 <hr />
 <h3><strong>Dong Lab</strong><strong> </strong></h3>
   <p><img src="images/dong-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> 
    <strong>Dr. Bing Dong</strong></strong>’s expertise are in building performance simulation, building controls and diagnostics, indoor environmental quality, energy informatics, probabilistic graphical models, and numerical optimization. In 2014 he was invited to lead an International Energy Agency (IEA) study with researchers from many of the world’s top research universities to set an international standard for measuring energy-related occupant behavior in buildings. He is now expanding his research to explore new physiological factors for improving the prediction of thermal-driven performance. In collaboration with Dr. Yufei Huang, he is studying how indoor environmental factors, such as temperature, impact human performance. Because much of the work performed indoors today has become increasingly more mental, a significant aspect of one’s thermal-driven performance is related to their cognitive performance; yet little research has been done on the correlations between what and how specific neurological factors contribute to thermal-driven performance, and the discriminant brain activities that can be used to predict thermal-driven performance are completely unknown. </p>
	<hr />
	<h3><strong>Huang Lab</strong><strong> </strong></h3>
   <p><img src="images/Huang-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Dr. Yufei Huang</strong> has extensive multidisciplinary expertise in computational neuroergonomics, brain computer interface (BCI), and computational biology. Dr. Huang is a member of the Army Research Lab Cognition and Neuroergonomics Collaborative Alliance (CaNCTA), where he is developing deep learning models to predict human cognitive behaviors and building BCI systems to control drones to optimize human performance. He also leads a team of scientists from UTSA and UTHSCSA who are currently working on a National Institutes of Health funded project that uses high throughput sequencing technology, machine learning, and bench experiments to study the functions of transcriptome methylation and its role in cancer and viruses.  **Also with Neurosciences Institute </p>
   <hr />
   <h3><strong>Jamshidi Lab</strong><strong> </strong></h3>
  <p><img src="images/Jamshidi-113.jpg" alt="" width="111" height="101" align="left" style="padding-right:10px" /> <strong>Dr. Mo Jamshidi </strong>has extensive research experience in a variety of areas including machine learning, classification of brain cancer tumors, progress identification for Alzheimer’s disease, and deep learning. He previously served as an advisor, IPA or special government employee for NASA (10 years), ASFOSR (9 years) and DOE (9 years), and holds a number of fellowships and honors from the more than 10 professional organizations that he is involved with, including IEEE, ASME, AAAS, NYAS, TWAS, AIAA and US-SoSEN. Over the past 3 years he and his research team have worked with UTHSCSA to conduct brain studies using bioinformatics. This research has been focused on the classification and advancement of brain cancer based on model identification of tumor growth. The team is also utilizing machine learning tools such as auto-encoders and deep architecture, including deep learning, to forecast stages of Alzheimer’s disease based on the history of patient brain MRI images. </p>
  <hr />
  <h3><strong>Jin Lab</strong><strong> </strong></h3>
 <p><img src="images/yufang-jin-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Dr. Yufang Jin</strong>’s expertise are in computational biology, mathematical modeling, and control theory. She currently focuses on multi-scale modeling for cardiovascular remodeling post-MI, cardiac aging, and proteomics as well as for studies in protective engineering, including developing molecular neuroprotective engineering strategies for alleviating ischemic brain injury. She was a recipient of Best Paper Award of 2006 and 2005 Artificial Neural Networks in Engineering Conference. Her research has been supported by the NSF, National Institutes of Health, Texas Higher Educational Board, and AT&amp;T foundation. She has been an organizer of several special sessions and workshops including the IEEE International Conference on Mechatronics and Automation, International Conference on BioComp, and IEEE international Conference on Machine Learning and Cybernetics. </p>
 <hr />
 <h3><strong>Rad Lab</strong><strong> </strong></h3>
  <p><img src="images/paulrad-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Dr. Paul Rad</strong>’s expertise are in a number of areas related to cloud computing as well as machine learning and telemedicine. One current area that he is exploring is the development of computational infrastructure for understanding the brain. Funded by Nvidia and in partnership with Nvidia, Intel and Google, one of his current projects is focused on developing new mass parallel computational infrastructure and algorithms that can mimic human neural networks to better understand and advance fundamental neural computational principles, opening the door to new machine learning algorithms. </p>
  <p>Another area that he is currently working in, along with Dr. Mo Jamshidi and UTHSCSA, is applying machine learning to brain tumor detection and diagnosis. The team is developing and validating machine learning and deep learning algorithms to better predict the stage, tumor growth rate and probable treatment outcomes for Glioblastoma, the most common and most aggressive of the primary malignant brain tumors in adults. </p>
  <hr />
 <h3><strong>Ye Lab</strong><strong> </strong></h3>
<p><img src="images/Ye-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> <strong>Dr. Jing Yong Ye</strong> (BME) has focused his research on the development of cutting-edge ultrasensitive and ultrafast laser-based technologies to address critical issues at the frontiers of biomedical research and applications. Especially, he is interested in utilizing his patented double-clad fiber based technique for ultrasensitive biosensing at an unprecedented deep brain level. This technique can be applied for a wide range of applications, including for deep-brain optical imaging of TBI-linked neurodegeneration in freely behaving mice. Dr. Ye is also interested in developing a photostimulation technique for optimized effects on enhancing cognitive brain functions. In addition, Dr. Ye and his group have also been working on developing photonic crystal based label-free technique for cardiotoxicity drug screening with an organ-on-a-chip model and developing photoacoustic imaging technique for cancer research. Those innovative technologies may also be potentially applied for brain research.</p>
 <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
  <hr size="10" class="blue" />
 
  <h2><strong><a name="psychology" id="psychology"></a>Department of Psychology | College of Liberal and Fine Arts</strong></h2>
   <h3> <strong>Coyle Lab</strong></h3>
    <p><img src="images/Coyle-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Dr. Thomas Coyle focuses on intelligence and specific cognitive abilities (e.g., processing speed and executive function), which have been linked to brain health and cognitive aging. He also examines non-IQ factors such as social and emotional intelligence, which are related to a network of regions that comprise the “social brain” (e.g., amygdala and temporal cortex). Finally, Dr. Coyle has collaborated with scientists at UTHSCSA and elsewhere on projects mapping cognitive abilities and brain morphology over the lifespan.</p>
    <p>&nbsp;</p>
	<hr />
   
  <h3><strong>Golob Lab</strong></h3><p><img src="images/golob-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /><strong>Dr. Edward Golob</strong>’s lab studies how auditory processing is affected by attention, memory, and the relations between perception and action.  He seeks to understand the cognitive and neurobiological differences that accompany normal aging, age-related neurological disorders such as Alzheimer's disease, and speech fluency disorders. In many studies, they monitor the brain's electrical activity using event-related potentials and EEG; in others, they use transcranial magnetic and electrical stimulation to transiently influence brain activity. The lab is expanding its work to include traumatic brain injury and risk of future cognitive impairments, as well as patient rehabilitation using advanced computing and brain-computer interface methods.
  <hr />
   
  <h3><strong>Morissette Lab</strong></h3><p><img src="images/Morissette-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> 
    Mental health and functional recovery among service members and veterans returning from warzones is a major public health priority. Dr. Morissette’s lab conducts longitudinal research to understand predictors of longterm functional trajectories among veterans. Her research emphasizes identifying modifiable predictors of, and understanding the complex relationships between, post-traumatic stress disorder, addictive behaviors, traumatic brain injury, and other comorbidities, with the aim of empirically informing clinical interventions. Dr. Morissette’s lab also conducts clinical trials of novel PTSD treatments.</h3>
   <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
  <hr size="10" class="blue" />
  <h2><strong>
    <a name="health" id="health"></a>Department of Kinesiology, Health, and Nutrition | College of Education and Human Development</strong></h2>
    <h3>
      <strong>Laboratory for Applied Autonomic Neurophysiology</strong><strong> </strong></h3>
   <p><img src="images/Cooke-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />  Cerebral autoregulatory mechanisms maintain brain blood flow constant over a wide range of arterial pressures. Malfunction, or maladaptation of cerebral autoregulation puts humans at risk for hypoperfusion, leading to dizziness and ultimately frank syncope.</p>
   <p>
    <strong>Dr. William Cooke</strong> studies how the autonomic nervous system regulates both cardiovascular and cerebrovascular control under a wide range of conditions. </p>
	<hr />
   <h3><strong>Development Motor Cognition Laboratory</strong></h3>
   <p><img src="images/Cordova-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Mental imagery strategies must be learned, and impact both motor skill function and motor planning.</p>
   <p>
  <strong>Dr. Alberto Cordova</strong> studies visual system planning as it pertains to imagery, constraints, and initial stages of motor planning. These efforts combine to enhance action planning and movements that may reduce the risk of injury.  **Also with Neurosciences Institute </p>
   <p>&nbsp;</p>
  <hr />
   <h3>
     <strong>Laboratory for Exercise Biochemistry and Metabolism</strong></h3>
   <p><img src="images/Fogt-113.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" /> Blood is diverted from relative inactive organs and organ systems to the brain during exercise. Associations among brain blood flow and neurocognition are dynamic, and can malfunction.</p>
   <p>
  <strong>Dr. Donovan Fogt</strong> studies mechanisms by which the body adapts to short-term chronic exposure to exercise and dietary manipulation. </p>
   <p>&nbsp;</p>
 <hr />
    <h3><strong><a href="http://education.utsa.edu/health_and_kinesiology/human_performance_laboratory/">Human Performance Laboratory</a></strong></h3>
   <p><img src="images/Land-113-2.jpg" alt="" width="113" height="103" align="left" style="padding-right:10px" />  A principle function of the brain is to support and control movement. It is through movement that humans interact with the environment, communicate, and reproduce.</p>
 <p> <strong><a href="http://education.utsa.edu/faculty/profile/william.land@utsa.edu">Dr. William Land</a></strong> studies cognitive factors that influence the learning and control of voluntary actions with the purpose of facilitating motor learning and rehabilitating motor impairments.  </p>
 <h4 align="right"><a href="#top">BACK TO TOP </a></h4>
  </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body></html>
