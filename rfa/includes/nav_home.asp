<ul>
    <li><a class="home" a href="/rfa/" title="Retired Faculty Association Home Page">Retired Faculty Association Home</a></li>
    <!--<li><a href="/rfa/">Retired Faculty Association Home</a></li>-->
    <li><a href="/rfa/members.asp">Officers and Founding Members</a></li>
    <li><a href="/rfa/bylaws.asp">Bylaws</a></li>
    <li><a href="/RFA/docs/RFA-signedMOU(color).pdf">Memorandum of Understanding</a></li>
 <!-- <li><a href="/RFA/meetings.asp">Meetings</a></li>
   <li><a href="/RFA/history.asp">UTSA History Project</a></li>
   <li><a href="/RFA/scholarships.asp">Faculty-Sponsored Scholarships and Awards</a></li>-->
<li><a href="/RFA/benefits.asp">Benefits for Retired Faculty</a></li>
<li><a href="../RFA/volunteers.asp">RFA Volunteers in Action</a></li>
    <li><a href="/RFA/docs/RFA-Membership-Application.doc">Join the RFA</a></li>
    <li><a href="mailto:retiredfacultyassociation@utsa.edu">Contact the RFA</a></li>
     
</ul>
<p>&nbsp;</p>
<h4>UTSA Links </h4>
<ul>
<li><a href="http://provost.utsa.edu/home/">Office of the Provost</a></li>
<li><a href="http://faculty.utsa.edu">Faculty Center</a></li>
<li><a href="http://provost.utsa.edu/home/Evaluation/Emeritus/index.asp">Emeritus Application Guidelines</a></li>
<li><a href="http://utsa.edu/hr/">UTSA Human Resources</a></li>
<li><a href="https://utdirect.utexas.edu/">UT Direct</a></li>
<li><a href="http://www.utsa.edu/today/">UTSA Today</a></li>
<li><a href="http://utsa.edu/directory/">UTSA Directory</a></li>
<li><a href="http://utsa.edu/auxiliary/parking.html">Parking</a></li>
<li><a href="http://lib.utsa.edu/">Libraries</a></li>
<li><a href="http://one.utsa.edu/sites/oit/OITConnect/Pages/OITConnect.aspx">OIT Support</a></li></ul>
