<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Retired Faculty Association | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/rfa/">UTSA Retired Faculty Association</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!-- #include virtual="/rfa/includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
       <h1> Faculty-Supported Student Scholarships and Awards</h1>
        <p>One of the goals of the Retired Faculty Association is to support student success through scholarships and other awards. The following awards are either established in honor/memory of a UTSA faculty member, or funded by current or former faculy.</p>
        <h2>College of Architecture</h2>
        <p> <a href="http://utsa.edu/scholarships/coa/arch.html#george">Gene George Endowed Architecture Scholarship in HIstoric Preservation</a><br />
          <a href="http://utsa.edu/scholarships/coa/arch.html#gribou">Julius M. Gribou and Kathleen A. Gribou Endowed Scholarship in Architecture</a><br />
        <a href="http://utsa.edu/scholarships/coa/arch.html#bill">Bill and Diane Hayes Endowed Scholarship in Architecture and Interior Design</a></p>
         <h2>College of Business</h2>
        <p> text</p>
         <h2>College of Education and Human Development</h2>
        <p> text</p>
         <h2>College of Engineering</h2>
        <p> text</p>
        <h2>College of Liberal and Fine Arts</h2>
        <h3>Department of Anthropology</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/ant.html#adams">Richard E.W. Adams Endowed Scholarship</a><br />
          <a href="http://utsa.edu/scholarships/colfa/ant.html#friends">Friends of Anthropology Endowed Scholarship in Honor of Anne Fox and Don Lewis</a><br />
        <a href="http://utsa.edu/scholarships/colfa/ant.html#dona">Dona Agripina de Urdaneta Endowed Scholarship in Anthropology</a></p>
        <h3>Department of Art and Art History</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/arthis.html#james">James A. and Cindy G. Broderick Endowed Scholarship in Art & Art History</a><br />
          <a href="http://utsa.edu/scholarships/colfa/arthis.html#field">Charles and Germaine Field Endowed Painting Scholarship</a><br />
          <a href="http://utsa.edu/scholarships/colfa/arthis.html#quirarte">Jacinto Quirarte Endowed Scholarship in Art History</a><br />
          <a href="http://utsa.edu/scholarships/colfa/arthis.html#reynolds">Steve Reynolds Endowed Scholarship</a>
        <h3>Department of Classics and Philosophy</h3>
          <p><a href="http://utsa.edu/scholarships/colfa/clasphil.html#miller">A. Ron Miller Scholarship</a></p>
        <h3>Department of English</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/eng.html#craven">Alan E. Craven Endowed Scholarship</a><br />
          <a href="http://utsa.edu/scholarships/colfa/eng.html#hovey">Kenneth Alan Hovey Endowed Memorial Scholarship</a><br />
          <a href="http://utsa.edu/scholarships/colfa/eng.html#lundy">Eileen Lundy Scholarship for Excellence In English Education</a><br />
        <a href="http://utsa.edu/scholarships/colfa/eng.html#thomas">Keith Thomas Memorial Fund</a></p>
        <h3>Department of History</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/his.html#henderson">Dwight F. Henderson Endowed Scholarship in History</a></p>
        <h3>Department of Music</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/music.html#chris">Joe and Chris Stuessy Endowed Scholarship in Music</a><br />
          <a href="http://utsa.edu/scholarships/colfa/music.html#bess">Bess Hieronymus Endowed Scholarship</a><br />
          Bess Hieronymus Endowed Faculty Fellowship in Organ Studies<br />
          <a href="http://utsa.edu/scholarships/colfa/music.html#jan">Janice K. Hodges Memorial Scholarship Endowment</a><br />
          <a href="http://utsa.edu/scholarships/colfa/music.html#rho">Rhoderick E. Key Memorial Fund</a><br />
          <a href="http://utsa.edu/scholarships/colfa/music.html#sam">Samuel A. and Pamela R. Kirkpatrick Endowed Presidential Scholarship</a><br />
          <a href="http://utsa.edu/scholarships/colfa/music.html#lin">Linda Poetschke Endowed Scholarship in Voice</a><br />
        <a href="http://utsa.edu/scholarships/colfa/music.html#fac">Music Faculty and Alumni Endowed Scholarship Fund</a></p>
        <h3>Department of Political Science and Geography</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/pol.html#calder">Dr. James D. Calder Annual Scholarship</a><br />
        <a href="http://utsa.edu/scholarships/colfa/pol.html#baxter">Jim Baxter Endowed Scholarship in Political Science</a></p>
        <h3>Department of Psychology</h3>
        <p><a href="http://utsa.edu/scholarships/colfa/psy.html">Richard Wenzlaff Scholarship Endowment</a><br />                
        </p>
         <h2>College of Public Policy</h2>
        <p> text</p>
         <h2>College of Sciences</h2>
        <p> text</p>
        <h2>Honors College</h2>
        <p> text</p>
        <p><br />
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
