<!-- Google Analytics code includes -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-2438226-1', 'provost.utsa.edu');
//THIS IS THE WAMS ACCOUNT: UA-2438226-1

try {
  ga('require', 'eventTracker');
  ga('require', 'outboundLinkTracker');
  ga('require', 'urlChangeTracker');
} catch (e) {
  //console.log("unable to load autotrack modules");
}
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>
<script async src='/_files/components/autotrack/autotrack.js'></script>
<!-- end Google Analytics code includes -->
