<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA Provost Home | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Academic Affairs at UTSA</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            	<!--#include virtual="/inc/nav/home-nav3.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
            
       
           <h1>
           <!-- <hr noshade="noshade" />-->

           <!-- <table width="596">
              <tr>

                <td height="24" colspan="2" style="background-color:#C60;">
                  <h1 style="color:#fff;">MEET THE FACULTY: Rhonda Gonzales</h1>
                </td>
              </tr>

              <tr>
                <td style="background-color:#FFC; color: #000;">
                  <p><img src="Faculty_Profile/Gonzales_thumbnail.jpg" width="110" height="165" align="left" alt="Rhonda Gonzales" style="padding-right:5px;"/></p>
                  <p>&nbsp;</p>

                  The file cabinets in Rhonda Gonzales' fourth floor office in the HSS are filled with copies of thousands of pages of 16th and 17th century documents all related to the Spanish Inquisition and collected during trips to Spain and Mexico. She once attended a three-week paleographic institute to get practical training in how to read the unfamiliar script. But even with that training, her reading and therefore, her research  is slow going.

                  <p>&nbsp;</p>
                  <p>Maybe, she jokes, she should take the advice of friends who suggest she research 19th century topics instead. <a href="Faculty_Profile/Gonzales.asp" style="background-color:#FFC;">Read the full story</a>.</p>
                </td>
              </tr>
            </table>-->
          </dd>

          <br />
          <!--          <p><strong>OIT Chief Information Officer Search</strong></p>            <ul>                <li><a href="CIO position ad_r1909.pdf">Position Advertisement</a><br>            For full consideration, applications must be received by February 27, 2009.</li>              <li><a href="CIO position description_r1909.pdf">Position Description</a></li>              </ul>        --></dt>Ensuring healthy and inclusive learning environments <br />
          at UTSA</h1>
        <h3><em>November 20, 2018</em></h3>
        <h2>Dear UTSA faculty,</h2>
        <p>Following President Taylor Eighmy’s message to the Roadrunner community&nbsp;<a href="http://www.utsa.edu/today/2018/11/story/EighmyUpdate3.html" target="_blank">detailing the outcomes of the investigations</a>&nbsp;into last week’s classroom incident at UTSA, my gratitude is extended to all those who worked diligently and cooperatively toward resolution. Thank you, too, to all the students, staff and faculty who have shared their thoughts with me, the President, Interim College of Sciences Dean Howard Grimes, and others. Most notably, our students, faculty and staff, alumni, and community conveyed that <strong>they</strong> <strong>care deeply about</strong> <strong>this university and our collective work to advance a healthy and inclusive campus learning environment</strong>.</p>
           <p>To this end, I’d like to tell you about a few of our ongoing efforts within Academic Affairs to improve our campus climate and support academic excellence and success.</p>
           <ul>
             <li>First, at UTSA we define excellence by inclusion – which is the fundamental underpinning of all of our efforts. In accordance with this core value, we have more work ahead of us to recruit, attract and retain more UTSA faculty and staff that are more reflective of the diversity of our student body. For the 2018-2019 academic year, Academic Affairs introduced a work plan process for the deans—focused on the university’s key performance indicators—for the colleges to align their efforts toward institutional objectives,&nbsp;<strong>including increasing by 50% the number of faculty from underrepresented groups over the next decade</strong>. In their work plans, it is clear that the deans share President Eighmy’s and my commitment to this goal. Accordingly, they have set annual targets for increasing faculty diversity and outlined specific steps they will take, with the understanding that their annual, third- and sixth-year reviews will now be based, in part, on their leadership in diversity and inclusion. I am also engaging the Provost’s Diversity and Inclusion Advisory Council to help me develop a strategy to support diversity hiring of faculty at the institutional scale, including through our upcoming university-wide strategic hiring initiatives. </li>
           
             <li>Last month, Academic Affairs partnered with the Collaborative in Academic Affairs in Higher Education (COACHE) at the Harvard Graduate School of Education to conduct an independent survey of faculty who have left UTSA in the last several years. Our goal is to better understand the experiences of our faculty, to systematically uncover the factors that contributed to their decision to leave the university, and to benchmark against other universities. These results should be available in the next few months.</li>
     </ul>
           <p>If we are to reach our destination as a model university, we must do more to build a culture of faculty development and peer support on campus. We do have several resources currently available, and we are examining Academic Affairs staffing to augment our capacity for training and development. </p>
           <ul>
             <li><a href="http://teaching.utsa.edu/" target="_blank">Teaching &amp; Learning Services</a> is an invaluable resource for faculty regarding classroom instruction, offering a range of programs, assistance and consultation. Some of our faculty have asked questions about&nbsp;<a href="http://teaching.utsa.edu/development-on-demand/classroom-management-resources/" target="_blank">classroom management</a> practices following this incident, and several UTSA faculty with particular interest in this issue have reached out in the last few days to offer their assistance as colleagues, however they can. Associate Vice Provost Mary Dixson has begun working with those faculty to put together&nbsp;<strong>a series of programming and online resources related to best practices in classroom management</strong>. Expect to hear more in the coming days; as with all faculty-related programming, keep an eye on the&nbsp;<a href="https://faculty.utsa.edu/" target="_blank">Faculty Center</a>&nbsp;and the Teaching &amp; Learning Services&nbsp;websites and social media channels for more information on upcoming programming.</li>
         <li>Beyond classroom management, faculty who have serious concerns about student behavior have the support of the Behavioral&nbsp;Intervention&nbsp;Specialist&nbsp;<strong>Rosanne McSweeney, who began her role at UTSA last spring and</strong>&nbsp;is actively working with the Behavior Intervention Team&nbsp;to confidentially respond to behavior concerns brought forward by faculty and staff. To learn more about the <a href="https://www.utsa.edu/bit/">Behavioral Intervention Team</a> and to schedule a presentation for your department, please contact <a href="mailto:Rosanne.McSweeney@utsa.edu">Rosanne.McSweeney@utsa.edu</a>.</li>
         <li>Our students benefit immensely from you—our talented and dedicated instructors—and we are grateful that you have chosen UTSA as your academic home. Investing in your professional development is important to us, given the strong connection between your success and that of your students. This year, in addition to the traditional travel support provided by Academic Affairs to the colleges, I am setting aside $100,000 in new matching funds to support travel for professional development to improve or learn new teaching skills, or alternately to share our pedagogic or curricular innovations at high profile national meetings. This program specifically funds such professional development both for full-time, non-tenure-track faculty and for tenure-track/tenured faculty. Details on these new funding opportunities will be forthcoming from the colleges.</li>
           </ul>
           <p>I’m looking forward to working with newly named Vice President for Inclusive Excellence Myron Anderson, staff, faculty and college and university leadership, in support of these and other initiatives to improve the campus climate, particularly for those from underrepresented groups. In particular, Dean of Libraries Dean Hendrix will work with Dr. Anderson on previously announced efforts to promote a university-wide dialogue about <a href="http://www.utsa.edu/president/campusandcommunity/cde/">civil discourse and respect</a>, to include faculty training, scalable curricular integrations and co-curricular programming.</p>
           <p>These are only initial steps, and our conversations are just beginning. We will continue to work with leadership across Academic Affairs and the entire university to develop programming and processes to help us meet our goals, and&nbsp;<strong>I welcome your input along the way.</strong>&nbsp;Please feel free&nbsp;<a href="mailto:provost@utsa.edu?subject=Re:%20Ensuring%20healthy%20and%20inclusive%20learning%20environments%20at%20UTSA">to share your thoughts and concerns</a>&nbsp;with me.</p>
           <p>Last week was a difficult one for those of us who study and work at UTSA. However, as the university prepares to break for the Thanksgiving holiday, I am thankful to have been given the opportunity to join UTSA and appreciate all that you do for the university to advance and support our academic community.</p>
           <p>Kimberly&nbsp; </p>
           <p><strong>Kimberly Andrews Espy, Ph.D.</strong> <br />
             <em>Provost and Vice President for Academic Affairs</em> <br />
             <em>Peter T. Flawn Distinguished Professor</em></p>
           <p><br />
           </p>
        </dl>

        
        
        
        
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
