<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Common Syllabus Information | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Common Syllabus Information</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            	<!--#include virtual="/inc/nav/syllabus-nav.html" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
            
       
           <!-- <hr noshade="noshade" />-->

           <!-- <table width="596">
              <tr>

                <td height="24" colspan="2" style="background-color:#C60;">
                  <h1 style="color:#fff;">MEET THE FACULTY: Rhonda Gonzales</h1>
                </td>
              </tr>

              <tr>
                <td style="background-color:#FFC; color: #000;">
                  <p><img src="Faculty_Profile/Gonzales_thumbnail.jpg" width="110" height="165" align="left" alt="Rhonda Gonzales" style="padding-right:5px;"/></p>
                  <p>&nbsp;</p>

                  The file cabinets in Rhonda Gonzales' fourth floor office in the HSS are filled with copies of thousands of pages of 16th and 17th century documents all related to the Spanish Inquisition and collected during trips to Spain and Mexico. She once attended a three-week paleographic institute to get practical training in how to read the unfamiliar script. But even with that training, her reading and therefore, her research  is slow going.

                  <p>&nbsp;</p>
                  <p>Maybe, she jokes, she should take the advice of friends who suggest she research 19th century topics instead. <a href="Faculty_Profile/Gonzales.asp" style="background-color:#FFC;">Read the full story</a>.</p>
                </td>
              </tr>
            </table>-->
          </dd>
          <h1>Common Syllabus Information</h1>
 <h2>This page represents links and information
   that is explicitly included in all syllabi.</h2>
         <p><strong>Counseling Services:</strong> Counseling Services provides confidential, professional services by staff psychologists, social workers, counselors and psychiatrists to help meet the personal and developmental needs of currently enrolled students. Services include individual brief therapy for personal and educational concerns, couples/relationship counseling, and group therapy on topics such as college adaptation, relationship concerns, sexual orientation, depression and anxiety. Counseling Services also screens for possible learning disabilities and has limited psychiatric services. Visit Counseling Services at <a href="http://utsa.edu/counsel/">http://utsa.edu/counsel/</a> or call (210) 458-4140 (Main Campus) or (210) 458-2930 (Downtown Campus).</p>
         <p><strong>Student Code of Conduct and Scholastic Dishonesty:</strong> The Student Code of Conduct is Section B of the Appendices in the Student Information Bulletin. Scholastic Dishonesty is listed in the Student Code of Conduct (Sec. B of the Appendices) under Sec. 203 <a href="http://catalog.utsa.edu/informationbulletin/appendices/studentcodeofconduct/  ">http://catalog.utsa.edu/informationbulletin/appendices/studentcodeofconduct/ </a></p>
        <p><strong>Students with Disabilities: </strong>The University of Texas at San Antonio in compliance with the Americans with Disabilities Act and Section 504 of the Rehabilitation Act provides “reasonable accommodations” to students with disabilities. Only those students who have officially registered with Student Disability Services and requested accommodations for this course will be eligible for disability accommodations. Instructors at UTSA must be provided an official notification of accommodation through Student Disability Services. Information regarding diagnostic criteria and policies for obtaining disability-based academic accommodations can be found at <a href="https://www.utsa.edu/disability">www.utsa.edu/disability</a> or by calling Student Disability Services at (210) 458-4157. Accommodations are not retroactive.</p>
          <p>
            <strong>Transitory/Minor Medical Issues:</strong> In situations where a student experiences a transitory/minor medical condition (e.g. broken limb, acute illness, minor surgery) that impacts their ability to attend classes, access classes or perform tasks within the classroom over a limited period of time, the student should refer to the class attendance policy in their syllabus.</p>
          <p>
            <strong>Supplemental Instruction:</strong>  Supplemental Instruction offers student-led study groups using collaborative learning for historically difficult classes. Supported courses and schedules can be found on the <a href="http://utsa.edu/trcss/si/">TRC website</a>. You can call the SI office if you have questions or for more information at (210) 458-7251. </p>
          <p><strong>Tutoring Services:</strong> Tomás Rivera Center (TRC) may assist in building study skills and tutoring in course content. The TRC has several locations at the Main Campus and is also located at the Downtown Campus. For more information, visit the <a href="https://www.utsa.edu/trcss/tutoring/">Tutoring Services web page</a> or call (210) 458-4694 on the Main Campus and (210) 458-2838 on the Downtown Campus.</p>
          <p><strong>Learning Assistance:</strong>   The Tomas Rivera Center (TRC) Learning Assistance Program offers one-on-one study skills assistance through Academic Coaching. Students meet by appointment with a professional to develop more effective study strategies and techniques that can be used across courses. Group workshops are also offered each semester to help students defeat common academic challenges. Find out more information on the TRC Learning Assistance <a href="http://www.utsa.edu/trcss/la/">website</a> or call (210) 458-4694. </p>          
          <p>&nbsp;</p>
        <p><strong><a href="https://utsa.edu/about/creed/">The Roadrunner Creed</a></strong></p>
          <p><img src="/images/creed200.jpg" alt="creed" width="200" height="171" hspace="9" align="right" style="margin: 0 0 0 6px;" />The University of Texas at San Antonio is a community of scholars, where integrity, excellence, inclusiveness, respect, collaboration, and innovation are fostered.</p>
        <p>As a Roadrunner, I will:
        <ul>
            <li>Uphold the highest standards of academic and personal integrity by practicing and expecting fair and ethical conduct;</li>
          
            <li>Respect and accept individual differences, recognizing the inherent dignity of each person;</li>
          
            <li>Contribute to campus life and the larger community through my active engagement; and</li>
          
            <li>Support   the   fearless   exploration   of   dreams   and   ideas   in   the advancement of ingenuity, creativity, and discovery.</li>
        </ul>
          
          <p><em>Guided by these principles now and forever, I am a Roadrunner!</em></p>
<dt><br />
        </dt>
        </dl>

        
        
        
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
