<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Review | Third-Year Review Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!-- #include virtual="/home/includes/nav_TYRGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h2>Third Year Review</h2>
         <h1>Review Process</h1>

        <p>The third-year review should not differ significantly from the review that takes place for promotion from assistant to associate professor with tenure; and, therefore departments are encouraged to have the TYR-DFRAC committee consist of DFRAC as a whole. Whether the entire DFRAC or a subcommittee prepares the initial report, the TYR-DFRAC report should be signed by all members of DFRAC. The process for third-year reviews involves three levels, including the TYR-DFRAC, the chair, and the dean. At each level of this process, the reviewer(s) should pay particular attention to the question whether or not the candidate is making acceptable progress toward promotion and tenure.  Deficiencies should be assessed to determine whether or not a candidate’s performance in any category places him or her in a situation in which success is unlikely.  In those cases, the review should explicitly state the problems and the reviewers should make a recommendation to the Provost as to the continuing appointment of the candidate.</p>
				<h2>Membership of the TYR-DFRAC Committee</h2>
				<p>
        	The membership of the TYR-DRAC committee is determined by the following process:</p>

        <ul>
          <li>In years that a faculty member is undergoing a third-year review, the DFRAC of each department, as defined by department and/or college bylaws, shall serve as the TYR-DFRAC or the DFRAC may elect a subcommittee to serve as the TYR-DFRAC committee. The formation of the TYR-DFRAC should occur no later than December each year.</li>

          <li>If the TYR-DFRAC committee does not consist of the entire DFRAC, the TYR-DFRAC should include at least three (3) tenured faculty members. Faculty who are appointed as part-time administrators are eligible to serve on the TYR-DFRAC, provided that they do not also participate in another level of review.</li>

          <li>If there are fewer than three (3) eligible faculty members to serve on the committee, the chair shall nominate a slate of potential members from outside the department to be appointed by the dean. The slate should include at least two nominees for each vacancy that needs to be filled by dean&rsquo;s appointment.</li>

          <li>The TYR-DFRAC shall elect its own chair.</li>

          <li>The committee chair is responsible for constructing and submitting the TYR-DFRAC&rsquo;s report to the chair.</li>

          <li>Each DFRAC member shall sign-off on the committee chair&rsquo;s report. If the TYR-DFRAC is a subcommittee of the whole DFRAC, then the committee&rsquo;s report shall be made available to the entire DFRAC for final review and approval.</li>
        </ul>

        <h2>Roles of Various Entities</h2>
				<p>
					The third-year review process is governed by the college dean, with the primary evaluation occurring at the department level. The role of each entity may be summarized as follows:</p>

        <p><strong>Tenure-Track Faculty Member</strong>&mdash; compiles the review packet for evaluation by the department and college. Upon completion of the department&rsquo;s review, the faculty member will meet with the department chair and the chair of the TYR-DFRAC to discuss the review, no later than February 28. The faculty member should receive a copy of the reports from the TYR-DFRAC and department chair at least three (3) work days in advance of the meeting. After discussing the review, the faculty member is invited to optionally comment on the reports in terms of clarification, the likelihood of accomplishing the necessary steps to be awarded tenure, neglected or additional information that came in after the review process was initiated, and explain any extenuating circumstances that may warrant further consideration before a decision will be rendered about contract renewal.&nbsp; The candidate may also elect <strong><em>not</em></strong> to respond to the report without penalty.</p>

        <p><strong>TYR-DFRAC</strong>&mdash; a full and detailed analysis of the faculty member&rsquo;s performance during the evaluation (probationary) period. The TYR-DFRAC provides a peer review by those members of the university community best qualified to judge the quality of the faculty member&rsquo;s activities. Accordingly, the TYR-DFRAC should provide a detailed written analysis of the faculty member&rsquo;s instructional, research/scholarly/creative, and service activities and make a recommendation for the performance evaluation in each of these three areas of activity. The analysis of the TYR-DFRAC should ideally represent a consensus of the committee, but where there are differences, the report should clarify all perspectives.</p>

        <p><strong>Department Chair</strong>&mdash; a full and detailed review of the faculty member&rsquo;s performance during the evaluation period, from the perspective of how that performance meets the expectations of the discipline and the department and addresses its mission, subject to the protections of academic freedom. The chair should independently evaluate the review packet, but consider the analysis of the TYR-DFRAC in arriving at a recommended evaluation in each area of performance and an overall evaluation. The chair&rsquo;s report should succinctly amplify points in the TYR-DFRAC report where there is agreement, and fully explain the reasons for any differences of opinion with the TYR-DFRAC report. When the chair&rsquo;s evaluation report is completed, the chair shall send a copy of that report, along with the TYR-DFRAC report, to the candidate, then schedule a meeting during the last week of February with the faculty member and the TYR-DFRAC chair to discuss the evaluation. If the candidate opts to provide a response to the chair’s and/or the TYR-DFRAC’s reports, the chair will then need to provide a summarizing analysis of the review report that takes the faculty member&rsquo;s response into account.</p>

        <p><strong>Dean</strong>&mdash; an independent, comprehensive review of the review packet, taking the TYR-DFRAC and department chair&rsquo;s analysis and recommendations, as well as the faculty member&rsquo;s optional response, into consideration. The dean is responsible for maintaining high standards for performance within the college and for analyzing cases in the context of how faculty performance achieves the mission and objectives of the college, again subject to the protections of academic freedom.&nbsp; The dean should provide a written analysis of each case under review, but may liberally cite opinions and analysis provided by earlier levels of review. In cases where the dean is in agreement with all previous analyses and recommendations, it is sufficient to provide a simple statement of agreement. When final evaluations are forwarded to the provost&rsquo;s office, the dean shall notify all faculty members undergoing third-year review of the final evaluation.
				</p>

        <p>The dean is also responsible for evaluating and recommending the need for corrective and/or remedial actions needed to enhance the possibility for successful promotion or, conversely, for determining whether a terminal contract should be offered.</p>

        <h2>General Guidelines for the Third-Year Review DFRAC</h2>
				<p>
        In addition to the policies expressed under HOP 2.24, the TYR-DFRAC should adhere to the following guidelines. Careful observance of these policies and guidelines is necessary to ensure a fair, objective, and consistent process throughout the analysis of each third-year review.</p>

        <ul type="disc">
          <li>The TYR-DFRAC conducts internal peer evaluations for the purpose of making recommendations on the evaluation of faculty performance.</li>

          <li>Faculty members serving on the TYR-DFRAC have the responsibility to read <strong>all</strong> review packet materials, reviewing the applicant&rsquo;s performance in each of the performance criteria thoroughly and participating in committee discussions and formulating committee recommendations. Participation by proxy is not appropriate.</li>

          <li>The analysis of a TYR-DFRAC should indicate the factors that contributed to the committee's recommendations <strong>and</strong> illuminate any factors that were prominently cited during the deliberations that would have been supportive of a contrary or alternative recommendation.</li>
          <li>Recommendations should be based on consistently applied criteria appropriate for the faculty candidate’s academic discipline.</li>

          <li>Faculty serving on a TYR-DFRAC should focus on factual information and guard against inaccuracies caused by either emphasis or omission of information.</li>
					<li>Minority opinions and analyses from members of FRACs may optionally be included as part of the FRAC report along with other committee analyses and recommendations. These “minority reports,” if utilized, may be segregated in a separate section of the report, but should not be submitted separately, and should be made available for review by the entire FRAC to ensure consistency. No other information or correspondence may be placed in the applicant’s file for transmittal to a department chair or onward to the provost.
					</li>
          <li>Only tenured faculty members may serve on a TYR-DFRAC.  As described above, if the committee, when constituted in accordance with the <em>Handbook</em> and college bylaws, has fewer than three tenured professors, the chair shall nominate and the dean shall appoint additional tenured professors from outside of the department until there are three on the committee.</li>

        <li>The TYR-DFRAC report should be signed by all DFRAC members. On the signature page, the report should include a header that reads: <strong>&ldquo;We, the undersigned members of the DFRAC have reviewed this report for completeness and accuracy, and attest that we have reached our recommendations through a thorough review and discussion of the available documentary evidence.&rdquo;</strong></li>
        </ul>

        <p>Each dean is responsible for reviewing policies and procedures and these guidelines with department chairs and for assuring that these policies, procedures and instructions are followed.</p>

        <h2>Guidelines for Department Chair&rsquo;s Evaluation</h2>
				<p>
        A critical part of the third-year review process is the recommendation of the department chair because the chair has the greatest competence to review the quality of a faculty member&rsquo;s performance within the academic discipline, balanced by the context of the department&rsquo;s long-term needs and aspirations. The chair&rsquo;s report should contain the following essential elements:</p>

        <ul type="disc">
          <li>a copy of the TYR-DFRAC&rsquo;s report;</li>

          <li>an analysis of the candidate&rsquo;s contributions in each of the areas of teaching, research/ scholarly/ creative activity, and service within the context of the department&rsquo;s needs;</li>

          <li>an explanation of aspects of the case within the academic discipline that may be unfamiliar to reviewers at the college-level&mdash; for example,</li>

          <li style="list-style: none; display: inline">
            <ul type="circle">
              <li>accepted standards for publishing scholarly work, including multiple author protocols,</li>

              <li>the importance of the faculty member&rsquo;s contribution in any collaborative activities,</li>

              <li>the quality and impact of the publication outlets, performance venues, or exhibition galleries (as appropriate) utilized by the applicant,</li>

              <li>clarification of usual citation numbers for researchers in the faculty member&rsquo;s field,</li>

              <li>the significance of the teaching activities undertaken by the faculty member relative to disciplinary norms,</li>

              <li>departmental expectations for student research mentoring at both undergraduate and graduate levels,</li>

              <li>the significance of any professional service contributions made by the faculty member,</li>

              <li>the willingness of the faculty member to serve on departmental committees, <em>etc</em>.</li>
            </ul>
          </li>

          <li>a succinct statement of the chair&rsquo;s recommendation, with explanation of the factors leading to this recommendation.</li>
        </ul>

        <p>The chair should strive to compose the report using factual data to support conclusions, and expressing the evaluation in terms of departmental expectations and aspirations. Most importantly, the chair should endeavor to connect the performance with the department&rsquo;s expectations for a tenured faculty member.</p>

        <h2>Guidelines for Dean&rsquo;s Evaluation</h2>
				<p>
        The role of the dean&rsquo;s evaluation is to uphold high standards across the college and ensure that each faculty member&rsquo;s performance supports the long-term quality and productivity of the college. In all cases, the dean&rsquo;s evaluation should provide the following essential information:</p>

        <ul type="disc">
          <li>the nature of the department&rsquo;s recommendations through the TYR-DFRAC and chair,</li>

          <li>a succinct statement of the dean’s evaluation and any      recommendations— possible outcomes include:</li>
        <ul> <li>If performance evaluation is “satisfactory,” send written notification of that fact to the Provost and recommend reappointment of faculty member to Provost by April 10. Also, send written notification to the faculty member, and include a concise analysis of the faculty member’s performance strengths and weaknesses, with specific advice about areas in which the faculty member should focus attention during the remaining probationary period.</li>
        <li>If performance evaluation is “unsatisfactory,” submit all written reports to the provost with dean’s recommendations for reappointment and/or corrective actions or issuance of a terminal contract by April 10.</li></ul></ul>


        <p>If the TYR-DFRAC and chair have provided analyses and recommendations with which the dean is in agreement, then the dean may provide a succinct statement of concurrence with the earlier analyses.&nbsp; If earlier recommendations express diverse outcomes, or if the dean disagrees with their conclusions, then a more comprehensive report should be provided.&nbsp; That report should contain an explanation of the reasons supporting the dean&rsquo;s recommendation, citing decisive arguments included in the TYR-DFRAC and/or chair&rsquo;s reports.&nbsp; The dean should take care to express conclusions within the context of the college&rsquo;s expectations and aspirations for long-term quality among its faculty.</p>
        <h2>Retention of Third Year Review Documents</h2>
				<p>

        Third Year Review documents are retained in accordance with the university’s official retention schedule, as follows:
        <ul>
					<li>Record series 3.1.143 Faculty Promotion and Tenure Review Records (Promotion File) – Evaluations,
						recommendations memos, etc. are retained for 2 years following the date granting/denying reappointment.</li>
					<li>Record series 3.1.137 Employee Recognition Records – A copy of the letter granting/denying
						reappointment is retained for 5 years after employment ends.</li>
        </ul>
				</p>
				<p>
        <p>The university’s full records retention schedule can be accessed through the following link: <a href="https://www.utsa.edu/openrecords/retention.html" target="_blank">https://www.utsa.edu/openrecords/retention.html
       </a></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
