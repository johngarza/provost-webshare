<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Third-Year Review Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!-- #include virtual="/home/includes/nav_TYRGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
           <h1>Third-Year Review Guidelines </h1>

        <p>This set of guidelines provides information for faculty undergoing a Third-Year Review (TYR) as prescribed by the <em>Handbook of Operating Procedures</em> (<em>HOP</em>), <a href="http://utsa.edu/hop/chapter2/2-24.html">chapter 2.24 &ldquo;Third-Year Review.&rdquo;</a> These guidelines also provide information for those faculty serving on Department Faculty Review Advisory Committees (DFRAC), department chairs, and deans involved in the review process. These guidelines are reviewed annually and updated by the provost&rsquo;s office.</p>

        <p>The process of promotion and tenure is one of the most important activities undertaken by the university each year as it is one means by which the university upholds high standards and expectations for its faculty. It is the incumbent responsibility of all who are involved in the review process to read all applicable materials, deliberate the strengths and weaknesses of each case in good faith, independence and with objectivity, and to observe confidentiality concerning the views of others, as revealed during review discussions. A respectful, thorough, and objective review of faculty accomplishments depends upon the conscientious efforts of all participants in the review process.</p>

				<p>
					The TYR process is a pre-tenure comprehensive review process employed by the university (i) “to assess overall performance and provide the basis for a fair evaluation which may be used in the decision regarding reappointment;” and (ii) “to afford faculty an opportunity to practice preparing their files for review, document their achievements, and understand how they will be judged for tenure and promotion at UTSA” (HOP 2.24).

				</p>

        <p>These guidelines are divided into several sections with the following contents:<br />
        <strong><a href="overview.asp">Overview of Process</a></strong>&mdash; a brief description of the timeline for review and the responsibilities of each party at each stage of the process.<br />
        <strong><a href="preparation.asp">Preparation of the Review Packet</a></strong>&mdash; a listing of essential and optional elements to include in the review packet prepared by tenure-track faculty.<br />
        <strong><a href="review.asp">Review Process</a></strong>&mdash; an outline of the responsibilities of the Third-Year Review DFRAC (or TYR-DFRAC), department chair, and dean in carrying out the review.<br />
        <strong><a href="docs/TYR-Cover-Sheet-and-Checklist.pdf">Cover Sheet and Checklist</a></strong>&mdash; a summary of required and optional materials to be submitted for the purposes of third-year review.</p>

        <p><strong><em>Schedule for Third-Year Review</em></strong><br />
        Each tenure-track faculty member will undergo a third-year review in the spring of the third full academic year of service. The review will consider all activities and achievements that are eligible for consideration in a promotion and tenure review. This includes activities undertaken by tenure-track faculty members in a comparable position at another institution.&nbsp; If the tenure-track faculty member began service at UTSA in a non-tenure-track position, the review should be held in the third year of full service since appointment to the tenure track, and should include only those activities and accomplishments since entering the tenure track.</p>

        <p align="center"><a href="docs/TYR-Guidelines.pdf">This information also is available in PDF format.</a></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
