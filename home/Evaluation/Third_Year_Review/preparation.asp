<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Preparation | Third-Year Review Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" --><style>

#noborder table {border:none;}

#noborder table th {border:none;}

#noborder table tr {border:none;}

#noborder table td {border:none;}

</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!-- #include virtual="/home/includes/nav_TYRGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h2>Third Year Review</h2>
         <h1>Preparation of the Review Packet</h1>

        <p>The review packet contains the materials that form the basis for the evaluation at all levels of review. It is the responsibility of the tenure-track faculty members undergoing a third-year review to ensure that the material contained in the packet is complete, accurate, and professionally presented.</p>

        <p>The contents of the review packet should include the following elements (described in more detail below):</p>

        <ol start="1" type="1">
          <li><a href="docs/TYR-Cover-Sheet-and-Checklist.pdf">Third-Year Review cover sheet and checklist</a></li>

          <li>a professional curriculum vitae</li>

          <li>a statement of professional goals, objectives, and accomplishments during the evaluation period</li>

          <li>evaluations and analyses reported by the TYR-DFRAC, department chair, and dean, as well as any optional external review letters solicited by the department and the optional response of the faculty member to the departmental review reports</li>

          <li>documentation of teaching effectiveness</li>
          <ul><li>summary of course evaluations (complete template)</li>
<li>peer observer’s report(s)</li>
<li>faculty member’s report (s)</li></ul>


          <li>documentation of research accomplishments and plans</li>

          <li>documentation of service contributions</li>

          <li>optional supplementary materials</li>
        </ol>

        <p>The professional curriculum vitae should serve as a simple listing of professional activities, while the optional supplementary materials may provide more in-depth information about those activities. The suggested contents of each of these elements should include but are not limited to those suggested below. The faculty member undergoing third-year review is responsible for preparing all items, except #4; the TYR-DFRAC, department chair, and college dean are responsible for appending materials contained in #4.</p>

        <p>Items #1&nbsp;&ndash;&nbsp;3 and 5 &ndash; 7 constitute the review materials utilized by the TYR-DFRAC and the chair in evaluating the faculty member&rsquo;s performance. In general, the dean will only review items #1 &ndash; 3 along with the department&rsquo;s evaluation reports in #4, placing the primary responsibility for the review with the department. Items included in #5 &ndash; 8 may be made available to the dean, but are chiefly used at the department level. This information is summarized in the table below.</p>

        <table border="1" cellspacing="0" cellpadding="0">
          <!--<tr>
            <td width="96" valign="top">
              <p>&nbsp;</p>
            </td>

            <th width="364" colspan="8" valign="top">
              <p align="center"><em>Item number</em></p>
            </th>
          </tr>-->

          <tr>
            <th width="96" valign="top" scope="row">
              <p><em>Review Activity</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #1</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #2</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #3</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #4</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #5</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #6</em></p>
            </th>

            <th width="45" valign="top">
              <p align="center"><em>Item #7</em></p>
            </th>

            <th width="47" valign="top">
              <p align="center"><em>Item #8</em></p>
            </th>
          </tr>

          <tr>
            <th width="96" valign="top" scope="row">
              <p><em>Dept. review</em></p>
            </th>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">&nbsp;</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="47" valign="top">
              <p align="center">X</p>
            </td>
          </tr>

          <tr>
            <th width="96" valign="top" scope="row">
              <p><em>College review</em></p>
            </th>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">X</p>
            </td>

            <td width="45" valign="top">
              <p align="center">(X)</p>
            </td>

            <td width="45" valign="top">
              <p align="center">(X)</p>
            </td>

            <td width="45" valign="top">
              <p align="center">(X)</p>
            </td>

            <td width="47" valign="top">
              <p align="center">(X)</p>
            </td>
          </tr>
        </table>

        <p>One hard copy of the required materials (#1-3) should be made available in a secured location in the department office and an electronic copy of items 1-3 and 5-8 should be posted in SharePoint for review by TYR-FRAC members.  Item #4 should be added to the hard copy folder and uploaded to SharePoint as reviews are completed.  Each of these items is described more fully in the following sections. </p>
        <p>&nbsp;</p>

        <p><strong><em>1. Third-Year Review Cover Sheet and Checklist</em></strong><br />
        The <a href="docs/TYR-Cover-Sheet-and-Checklist.pdf">TYR cover sheet form</a> is available on this website as a PDF document, and should be filled in by the faculty member and provided with the other materials. The second and later pages of the cover sheet are a checklist of the essential contents of the review package, as well as a checklist of possible optional supplementary materials that may be submitted. The Checklist needs to be signed by the department chair and dean once they have reviewed the files. All should be actual signatures, not electronic or digital signatures.</p>

        <p><strong><em>2. Professional Curriculum Vitae</em></strong><br />
        </p>
        <div id="noborder"><table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="184"><div align="right"><em>Name and Contact Information</em></div></td>
            <td width="452">This should include UTSA address, phone number, and email address, as well as current academic rank (for example, &ldquo;Assistant Professor&rdquo; or “Associate Professor without Tenure”).</td>
          </tr>
          <tr>
            <td><div align="right"><em>Educational Background</em></div></td>
            <td>Please list all institutions from which a degree was earned, including the degree received and the major field of study.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Professional Employment History</em></div></td>
            <td>List all positions held in sequential order, with applicable dates, since earning the baccalaureate degree, including the present position at UTSA.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Awards and Honors</em></div></td>
            <td>List any awards, honors, prizes, competitions, or other recognition received related to professional activities.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Research/Scholarly/Creative Activities Summary</em></div></td>
            <td>Summarize all products of research/scholarly/creative activities, including publications, exhibitions, performances, architectural projects, reviews, or other documentation of scholarly contributions. List separately the different types of publications (e.g. journal articles, books, reviews, etc.), scholarly products, or creative activity outcomes, providing respective listings of invited contributions, refereed contributions, and non-refereed contributions. All contributions should include the date and title of publication/exhibition/performance, the venue, and where applicable, the inclusive page numbers or size of the scholarly contribution.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Scholarly Presentations</em></div></td>
            <td>List all external oral or poster presentations at conferences, meetings, or other institutions/universities related to scholarly work, and provide the dates and locations of presentations. Provide separate listings for invited presentations, refereed, and non-refereed contributions.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Granting Activities</em></div></td>
            <td>Provide a list of grant proposals submitted, whether for research, instructional, or public service activities (please indicate one of these for each grant), giving the name of the granting agency, the project dates, the project title, and the total amount requested and awarded, if appropriate, for each.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Intellectual Property</em></div></td>
            <td>Where applicable, provide a summary of any intellectual property generated from scholarly activities and indicate any patent applications, copyright privileges, licensing, or other commercialization that has resulted. The summary should include dates, titles, and identifying information.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Teaching Activities</em></div></td>
            <td>List all formal courses taught at UTSA, indicating the level of the course (undergraduate or graduate) and its title. Provide a list of students mentored in research/scholarly/creative activities and any theses or dissertations directed. Summarize any service on graduate committees and for student advising.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Service Activities</em></div></td>
            <td>Provide separate listings of all committee assignments, assigned administrative activities (for example, department chairmanship, center directorship, etc.), and professional service activities (including leadership in disciplinary organizations, service as a journal editor, manuscript or grant proposal reviewer, meetings or symposia organized, etc.). Each activity should include the dates of participation, the organizational level of the activity (for example, department, college, etc.), and any leadership roles played.</td>
          </tr>
        </table></div>
       <p><strong><em>3. Statement of Professional Goals, Objectives, and Accomplishments</em></strong><br />
        The statement of self-evaluation should be organized in three sections, outlining the applicant’s activities, experiences, and plans in the areas of teaching, research/scholarly/creative activities, and service, respectively.</p>

				<ul type="disc">
					<li>For the teaching section, the applicant may wish to include a teaching statement outlining her or his philosophy/approach to teaching, and describe any innovative approaches used in delivering instruction.</li>
					<li>In the research/scholarly/creative activity section, the applicant can provide a context for her or his scholarly work, indicating the relationship between different projects and plans for future scholarship and how those plans build on past accomplishments (if applicable).</li>
					<li>The service section should provide an overview of service activities and explain the applicant’s participation in key service roles, including her or his philosophy of service and how it complements teaching and scholarly activities.</li>
				</ul>
				<p>
					The statement of self-evaluation should be no more than 8 – 10 pages long.
				</p>


        <p><strong><em>4. Evaluation and Review Materials</em></strong><br />
        As the TYR packet goes through the evaluation process, each level of review should append its analysis and recommendation to the packet for consideration by the next level of review. Guidelines for these various levels of review are provided in the &ldquo;Review Process&rdquo; section of these guidelines below. The materials should be arranged in the following order, with the responsibility for appending each set of materials indicated below:</p>

        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="167" valign="top">
              <p><em>Item</em></p>
            </td>

            <td width="152" valign="top">
              <p><em>Responsible Individual</em></p>
            </td>

            <td width="160" valign="top">
              <p><em>When</em></p>
            </td>
          </tr>

          <tr>
            <td width="167" valign="top">
              <p>Peer Observer’s Report<br />
Faculty Member’s Report
              <br />
              TYR-DFRAC evaluation report<br />
              Chair&rsquo;s evaluation report<br />
              Response to departmental review by faculty member (optional)<br />
              Chair’s summary memo, if faculty member provided a written response to departmental review
            </td>

            <td width="152" valign="top">
              <p>Department Chair</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion of department-level evaluation</p>
            </td>
          </tr>

          <tr>
            <td width="167" valign="top">
              <p>Dean&rsquo;s final evaluation</p>
            </td>

            <td width="152" valign="top">
              <p>Dean</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion of college-level review</p>
            </td>
          </tr>
        </table>

        <p>The department may opt to solicit a &ldquo;friendly&rdquo; external review letter as part of its total evaluation process. <u>This is <strong><em>not</em></strong> a required step</u>, but may provide helpful third-party constructive advice to the faculty member. For example, one might ask a former mentor of the faculty member to examine the review packet and provide feedback to the faculty member about appropriate activities to undertake that would optimize chances for a successful application for promotion and tenure. The use of such a letter should be agreed upon in advance by the faculty member and the department chair as it may necessitate earlier preparation of the review packet. If the optional external review is used by the TYR-DFRAC and the Chair, the letter should be included along with the department&rsquo;s evaluation reports in this section of the packet before sending forward to the dean.</p>

        <p><strong><em>5. Documentation of Teaching Effectiveness</em></strong><br />
        </p>
        <div id="noborder"><table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="138"><div align="right"><em>Listing of Courses with Teaching Evaluation Summaries</em></div></td>
            <td width="498">Provide a table of courses taught during the evaluation period (the probationary period), using the template provided below (<a href="../PromotionTenure/Summary-of-Student-Course-Evaluation-Template.docx">this template</a> may be downloaded from the Provost's website. Do not include copies of student evaluation surveys or comments among these materials.</td>
          </tr>
        </table></div>

<table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><p align="center"><em>Semester</em></p></td>
            <td width="100"><p align="center"><em>Course No.</em></p></td>
            <td width="105"><p align="center"><em>Course Type</em></p></td>
            <td width="82"><p align="center"><em>New prep?</em></p></td>
            <td width="71"><p align="center"><em>Course Enrollmt.</em></p></td>
            <td width="82"><p align="center"><em>No. of Responses</em></p></td>
            <td width="64"><p align="center"><em>Course rating</em></p></td>
            <td width="78"><p align="center"><em>Instructor rating</em></p></td>
          </tr>
          <tr>
            <td width="100"><p align="center">SP2011</p></td>
            <td width="100"><p align="center">ABC   nnn3</p></td>
            <td width="105"><p align="center">LD,   UD, or GR</p></td>
            <td width="82"><p align="center">NEW</p></td>
            <td width="71"><p align="center">xxx</p></td>
            <td width="82"><p align="center">yyy</p></td>
            <td width="64"><p align="center">X.X</p></td>
            <td width="78"><p align="center">Y.Y</p></td>
          </tr>
          <!--<tr>
            <td width="428" valign="top"><p align="center"><em>Note:</em> LD = lower division,<br />
            UD = upper division,<br />GR = graduate-level</p></td>
           </tr>-->
        </table>
<p><em>Note:</em> LD = lower division, UD = upper division, GR = graduate-level</p>
<p>&nbsp;</p>
        <div id="noborder"><table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="138"><div align="right"><em>Teaching Portfolios</em></div></td>
            <td width="498">For each course taught, provide a portfolio containing the course syllabus, exams, handouts, problem sets and other written assignments, and other course materials developed by the faculty candidate.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Peer Observation Report(s)</em></div></td>
            <td>Please refer to the Peer Observation Guidelines and the HOP, Chapter 2.20 “Peer Observation of Teaching” for more information. Provide the report to the department chair according to the Peer Observation Guidelines, so that the department chair may upload it to the official Faculty Review folder in SharePoint.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Instructional Development</em></div></td>
            <td>List any workshops, seminars, or other related meetings attended (or organized) to increase pedagogical effectiveness. This information should include the dates, formats, locations, and names of organizers.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Instructional Grants</em></div></td>
            <td>List all grants related to instructional activities. This may be taken directly from relevant grants listed on the professional curriculum vitae and should contain the information indicated above for &ldquo;Grant Activities.&rdquo; Provide copies of all funded grants and, optionally, referee comments for those grant proposals.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Teaching Awards</em></div></td>
            <td>List any awards received for excellence in university-level teaching. This may include both awards received at UTSA and at other institutions of higher education, and should indicate the date, award name, awarding unit (for example, college, university, <em>etc</em>.), and institution.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Students Mentored</em></div></td>
            <td>Provide a list of all students mentored in scholarly and research activities under your mentorship. For undergraduate course advisement, a summary of the number of students served is sufficient.</td>
          </tr>
        </table></div>
        <p><strong><em>6. Documentation of Research Accomplishments and Plans</em></strong><br />
        </p>
        <div id="noborder"><table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="138"><div align="right"><em>Scholarly Products</em></div></td>
            <td width="498">
Provide an electronic copy of all research/scholarly/ creative works produced during the evaluation period. This includes full copies of any journal articles, book chapters, papers in conference proceedings, architectural projects, digital images of artwork, recordings of musical performances or compositions, and other short-format works. These may include manuscripts under review or in preparation. Portions of books may also be scanned to create a digital image for use in the internal review. In cases where the amount of scholarly products is extensive, a representative sample of scholarly products may be submitted, after consultation with the department chair.
Note that citation indices of published work may be included among the optional supplementary materials.
</td>
          </tr>
          <tr>
            <td><div align="right"><em>Reviews</em></div></td>
            <td>Where appropriate, provide copies of any reviews of scholarly and creative activity, including reviews of books published, exhibitions, performances, compositions, architectural projects, and other creative endeavors.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Grant Proposals</em></div></td>
            <td>An electronic copy of all funded grant proposals, as well as any proposals under review, or in preparation, should be provided with an indication of the present status of the proposal. Referee comments from funded proposals may be submitted along with the proposals themselves. If the amount of funded proposals is extensive, a representative sample of proposals may be submitted, after consultation with the department chair.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Intellectual Property</em></div></td>
            <td>Provide documentation of any intellectual property produced, including patents, copyrights, licensing agreements or other commercialization activities. Faculty are not required to divulge sensitive information concerning the intellectual property, but may document its development and potential commercialization through letters and other communications.</td>
          </tr>
        </table></div>


        <p><strong><em>7. Documentation of Service Activities</em></strong><br />
        </p>
        <div id="noborder"><table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="138"><div align="right"><em>Committee Assignments</em></div></td>
            <td width="498">Separately list committee assignments at the department, college, and university levels, indicating dates of service and the name of the committee chair. Applicants should also indicate the extent of their contributions to the work of each committee listed.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Professional Service Activities</em></div></td>
            <td>List any activities related to professional service, both internal and external. These may include committee assignments, manuscript and proposal review, journal editorship, organization of meet&shy;ings, and other assistive activities. In all cases, provide dates of service, organizations served, and time committed.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Leadership Positions</em></div></td>
            <td>Provide a summary of any leadership positions held at the university or within a professional/disciplinary organization or society. List the dates for each applicable position, the responsibilities of the position, and the time commitment involved in executing the responsibilities of the position. Also, indicate any special accomplishments achieved while in the leadership position.</td>
          </tr>
        </table></div>
       <p><strong><em>8. Optional Supplementary Materials</em></strong><br />
        Faculty members undergoing third-year review may submit optional supplementary materials to highlight or document achievement in the areas of teaching, research/scholarly/creative activities, and service activities. A checklist of possible items that might be included among the supplementary materials is included with the Third-Year Review Cover Sheet form available from the Provost's website. </p>
        <p>Faculty members are asked to provide supplementary materials in a non-printable (optional) electronic/digital format uploaded to SharePoint or other secure university-supported website as may be directed. In addition, each department may determine whether a hard copy of selected supplementary materials should be made available in the department office along with the required materials.</p>

        <p>Faculty members may also make available other supplementary materials in support of the application, including full sets of student teaching surveys with comments, works in progress, a statement of future research goals and directions, and other items as allowed by the department and college. The supplementary materials are primarily used at the departmental level of review. Once the department-level evaluations are completed, the optional supplementary materials will remain available (either in the department office or online) during the college review of the packet. In extraordinary cases, where the supplementary materials might clarify a point in dispute from earlier reviews, the dean may request access to these materials for a given case.</p>

        <p>The intent of supplementary materials is to facilitate the separate evaluations of the TYR-DFRAC and the department chair in this process.</p>

        <p><strong><em>Special Note to Faculty</em></strong><br />
        These guidelines are intended to help you prepare a compelling, well-documented packet for your third-year review. Much of the content of this packet is identical to the material that you will collect in preparation for consideration for tenure and promotion. As you prepare your packet, please consider how readily a reader may access and absorb the material it contains. Repetition and verbosity will only serve to fatigue reviewers without adding substance to your packet&mdash; be as succinct as possible in each section of the packet.</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
