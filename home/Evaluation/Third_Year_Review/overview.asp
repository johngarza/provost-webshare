<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Overview | Third-Year Review Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!-- #include virtual="/home/includes/nav_TYRGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        
        <h2>Third Year Review</h2>
        <h1>Overview of Process</h1>

        <p>The purpose of the third-year review is to provide mid-stream feedback to tenure-track faculty to help them assess their progress toward achieving tenure at UTSA. It is also an opportunity for the college and university to carry out a comprehensive assessment of performance and make recommendations concerning the reappointment of the faculty member. Each review involves evaluation by three different entities&mdash; the TYR-DFRAC, the department chair, and the dean. The dean concludes the review and makes a final recommendation concerning its results to the Provost and President. The review process is summarized in the table below:</p>

        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="68" valign="top"> <p><em>When</em></p></th>
            <th width="113" valign="top"><p><em>Who</em></p></th>
            <th width="425" valign="top"><p><em>Responsibility</em></p></th>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>Spring</strong></p></td>
            <td width="113" valign="top"><p><em>Provost</em></p></td>
            <td width="425" valign="top"><ul><li>Notify deans which tenure-track faculty members are due to undergo a third-year review in the coming year.</li>
            <li>Notify tenure-track faculty that a third-year review will take place in the coming academic year.</li></ul></td>
          </tr>
          
          <tr>
            <td width="68" valign="top"><p><strong>Fall</strong></p></td>
            <td width="113" valign="top"><p><em>Department Chair, Tenure-track faculty</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Meet to review TYR process and provide/receive advice on assembling review packet.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>December</strong></p></td>
            <td width="113" valign="top"><p><em>Department</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Departments are encouraged to have the entire DFRAC serve as the TYR committee, but departments may elect a TYR-DFRAC subcommittee to carry out the review.</li>
              <li>
                Report membership of TYR-DFRAC to the Dean no later than December 15th.  The Dean’s Office will provide the committee names to the Office of the Vice President for Academic Affairs.</li>
            </ul>
            </td>
          </tr>
          <tr>
            <td width="68" valign="top"><p>&nbsp;</p></td>
            <td width="113" valign="top"><p><em>TYR-DFRAC</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Convene to elect a committee chair by December 20.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>January</strong></p></td>
            <td width="113" valign="top"><p><em>Tenure-track Faculty under review</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Assemble review packet and upload documents to official Faculty Review folder in SharePoint. Also submit one manila folder with hard copies of the coversheet and checklist, current vita, and statement of professional goals to the department chair no later than January 15th or the first workday thereafter.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>January-February</strong></p></td>
            <td width="113" valign="top"><p><em>TYR-DFRAC</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Evaluate the review packet and prepare a written summary of evaluation analysis.</li>
              <li> Full DFRAC review and approval of evaluation.</li>
              <li>Submit evaluation to department chair by February 5th. </li>
            </ul>              </td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>February</strong></p></td>
            <td width="113" valign="top"><p><em>Department Chair</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Review the packet and the TYR-DFRAC report.</li>
              <li> Write the chair&rsquo;s evaluation.</li>
              <li> Send copies of TYR-DFRAC and chair&rsquo;s reports to the faculty member under review.</li>
              <li> Upload DFRAC memo and department chair memo to the official Faculty Review folder in SharePoint.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>February-March</strong></p></td>
            <td width="113" valign="top"><p><em>Department Chair,<br />
            TYR-DFRAC Chair</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Meet with tenure-track faculty member under review to discuss evaluation no later than February 28th.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p>&nbsp;</p></td>
            <td width="113" valign="top"><p><em>Tenure-track Faculty Member</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Optional opportunity to respond to departmental evaluation before it is forwarded to the Dean. Response due by March 10th.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>March-April</strong></p></td>
            <td width="113" valign="top"><p><em>Department Chair</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Write summarizing report.</li>
              <li>Upload faculty member’s response and department chair summarizing report to the official Faculty Review folder in SharePoint.</li>
              <li>Include original signed copies of the DFRAC memo, department chair memo, faculty member’s response (if one is provided), and department chair’s summarizing report in the faculty folder and forward to the Dean by March 25th.</li>
            </ul>            </td>
          </tr>
          <tr>
            <td width="68" valign="top"><p>&nbsp;</p></td>
            <td width="113" valign="top"><p><em>Dean</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Review TYR report and determine final evaluation. <br />
                Provide a written recommendation for forwarding to provost.</li>
              <li>Provide a written notification to applicant of the college’s recommendation by April 15th.</li>
              <li> Upload dean’s memo to the official Faculty Review folder in SharePoint.</li>
              <li> Upload signed checklist(s) to the official Faculty Review folder in SharePoint.</li>
              <li> Include original signed copy of the coversheet and checklist and dean’s memo to the faculty folder and forward to the Provost’s Office by April 10th.</li>
            </ul>            </td>
          </tr>
          <tr>
            <td width="68" valign="top"><p>&nbsp;</p></td>
            <td width="113" valign="top"><p><em>Provost</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Discuss unsatisfactory evaluations with deans and make recommendation to president concerning the reappointment of all faculty members undergoing third-year review.</li>
            </ul></td>
          </tr>
          <tr>
            <td width="68" valign="top"><p><strong>April-May</strong></p></td>
            <td width="113" valign="top"><p><em>President</em></p></td>
            <td width="425" valign="top"><ul>
              <li>Review and decide all recommendations concerning reappointment of faculty members by April 20.</li>
              <li> Report non-reappointment decisions to provost, deans, faculty members by May 1.</li>
            </ul></td>
          </tr>
       
           
        </table>

        <p>The deadlines indicated in this chart are intended to be general guidelines so that the process may be concluded by the end of the spring semester. Any significant deviation from these deadlines (for example, by two weeks or more) must be approved by the provost upon the written request of the dean.</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
