<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Overview | CPE | Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/home/includes/nav_PPEGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        
        
         <h2>Comprehensive Periodic Evaluation</h2>
         <h1>Overview of Process</h1>
        

         <p>The CPE review process is summarized in the table below:</p>

        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <th width="82" valign="top">
              <p><em>When</em></p>
            </th>

            <th width="104" valign="top">
              <p><em>Who</em></p>
            </th>

            <th width="425" valign="top">
              <p><em>Responsibility</em></p>
            </th>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>February- March</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Provost and Vice President for Academic Affairs</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Notify deans which tenured faculty members are due to undergo a CPE review in the coming fall semester.</li>
              </ul>
           <p>&nbsp;</p> </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>March</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Provost and Vice President for Academic Affairs</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Notify tenured faculty that a CPE review will take place in the coming fall semester.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>Summer</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Tenured Faculty under review</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Assemble review packet and upload documents to the official Faculty Review folder in SharePoint.  Also submit a manila folder with hard copies of your coversheet and checklist, vita, and summary statement of professional accomplishments to Department Chair no later than September 15th or the first workday thereafter..</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>August-September</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Department</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Elects the D-CPER committee.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>Department Chair</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Report membership of the D-CPER committee to dean no later than Sept. 10th. The Dean’s office will provide the committee membership to the Provost’s Office.</li>
              </ul>
            </td>
          </tr>
          
          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>D-CPER committee</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Convene to elect a committee chair by September 10.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>Tenured Faculty under review</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Optional: candidate may request to meet with the D-CPER committee— request should be made no later than September 25.</li>
              </ul>
            </td>
          </tr>
          
          

          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>D-CPER committee</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Evaluate the review packet and provide evaluation recommendations for each individual category, as well as the overall rating.</li>
                <li> Submit report to department chair (deadline set by each college).</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>September-<br />
              November</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Department Chair</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Review all documentary materials and the report from the D-CPER committee.</li>
                <li> Compose Chair’s CPE evaluation report.</li>
                <li> Upload D-CPER memo and department chair memo to the official Faculty Review folder in SharePoint;</li>
                <li> Include original signed copies of D-CPER memo and department chair memo in the faculty folder and forward to the dean’s office by November 15th, with a copy to faculty member.</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>D-CPER committee, Chair, Dean</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>May request additional documentation from faculty member, if needed.</li>
              </ul>
            </td>
          </tr>

          

          <tr>
            <td width="82" valign="top">
              <p><strong>November-December</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Dean</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Review all documentary materials and reports from D-CPER committee and from the department chair;</li>
                <li> Determine final evaluation of faculty performance and notify faculty member by December 20th; </li>
                <li> Upload signed coversheet and checklist to SharePoint;<br />
                  Upload dean’s memo to the official Faculty Review folder in SharePoint;</li>
                <li> Include original signed copy of the coversheet and checklist and dean’s memo in the faculty folder.  Forward files to the Provost Office. </li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>January</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Dean</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Unless evaluation in any area is “unsatisfactory”, send the evaluation to the Provost and return submitted materials to faculty member.</li>
                <li> If performance evaluation is “unsatisfactory” in any area of evaluation, may appoint a C-CPER committee review committee.*</li>
              </ul>
            </td>
          </tr>
<tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>Provost</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Prepare final notification letter regarding outcome of CPE to faculty with a copy to the dean and department chair.</li>
              </ul>
          </td>
          </tr>
            <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>Faculty member</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Provides additional information to college level C-CPER committee.*</li>
              </ul>
            </td>
          </tr>
          
          <tr>
            <td width="82" valign="top">
              <p><strong>January- February</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>C-CPER committee</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Evaluate review materials and provide a written report of analysis to dean by February 15.*</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>Dean</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Submit all evaluation reports to provost with recommendations for follow-up actions by February 28.*</li>
              </ul>
            </td>

          </tr>

          <tr>
            <td width="82" valign="top">
              <p><strong>March</strong></p>
            </td>

            <td width="104" valign="top">
              <p><em>Provost</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Review and approve any faculty development plans recommended by dean as the result of an unsatisfactory CPE review.*</li>
              </ul>
            </td>
          </tr>

          <tr>
            <td width="82" valign="top">
              <p>&nbsp;</p>
            </td>

            <td width="104" valign="top">
              <p><em>Dean</em></p>
            </td>

            <td width="425" valign="top">
              <ul>
                <li>Communicate faculty development plans to faculty member.*</li>
              </ul>
            </td>
          </tr>
        </table>

        <p><em>*These steps are necessary only in the event of an unsatisfactory evaluation. The process for satisfactory evaluations ends in December with notification sent to the provost.</em></p>

        <p>The deadlines indicated in this chart are intended to be general guidelines so that the process may be concluded before the winter break holidays. Any significant deviation from these deadlines (for example, by two weeks or more) must be approved by the dean upon the written request of the department chair, with notification to the Provost's Office.</p>
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
