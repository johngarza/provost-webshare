<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Preparation | CPE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style>

#noborder table {border:none;}

#noborder table th {border:none;}

#noborder table tr {border:none;}

#noborder table td {border:none;}

</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/home/includes/nav_PPEGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
          <h2>Comprehensive Periodic Evaluation</h2>
          <h1>Preparation of the Review Packet<br />
          </h1>

        <p>The review packet contains the materials that form the basis for the evaluation at all levels of review. It is important that tenured faculty members undergoing a CPE make every effort to ensure that the material contained in the packet is complete, accurate, and professionally presented.</p>
         <p> The contents of the review packet should include the following elements:<br />
        <ol><li><a href="docs/CPE-Cover-Sheet-and-Checklist.pdf">CPE cover sheet and checklist</a></li>
          <li>a professional curriculum vitae</li>
          <li>a summary statement of professional accomplishments during the evaluation period (normally, the previous six (6) years). In addition to the summary statement, faculty may include a brief summary of future professional plans during the subsequent six (6) years</li>
          <li>a summary of student course evaluation survey results (use template)</li>
          <li>copy(s) of peer observer report(s) and faculty member report(s).  Please refer to the Peer Observation Guidelines for more information.</li>
          <li>copies      of the annual reports, including chair and dean written annual evaluations      during the evaluation period</li>

          <li>optional supplementary materials</li>
          <li>evaluations and analyses reported by the D-CPER committee, department chair, and dean
        </ol>
        </p>
				<p>
					The professional vitae should serve as a simple listing of professional activities,
					while each of the other components provides more in-depth information about those
					activities. The suggested contents of each of these elements should be consistent
					with those outlined in the Promotion and Tenure guidelines. The faculty member
					undergoing CPE review is responsible for preparing items #1 – 7, with assistance
					from the department chair in obtaining materials for items #4 - 6; the D-CPER
					committee, department chair, and college dean are responsible for appending materials
					contained in #8.
				</p>

				<p>Items #1&nbsp;–&nbsp;6 constitute the review materials utilized by the D-CPER committee, the chair, and the dean in evaluating the faculty member’s performance. Items included in #7 are optionally made available to evaluators at the department level only, but are not transmitted to the college level unless the dean and/or the C-CPER committee (should one be required) specifically request them. This information is summarized in the table below.</p>
<table border="1" cellspacing="0" cellpadding="0">
        <!--<tr>
            <td width="96" valign="top">
              <p>&nbsp;</p>
            </td>

            <th width="369" colspan="7" valign="top">
              <p align="center"><em>Item Number</em></p>
            </th>
          </tr>-->

        <tr>
          <th width="96" valign="top">
            <p><em>Review Activity</em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #1</strong></em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #2</strong></em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #3</strong></em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #4</strong></em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #5</strong></em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #6</strong></em></p>
          </th>

          <th width="53" valign="top">
            <p align="center"><em><strong>Item #7</strong></em></p>
          </th>
          <th width="53" valign="top"><em>Item #8</em></th>
        </tr>

        <tr>
          <td width="96" valign="top">
            <p><em><strong>Dept. review</strong></em></p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>
          <td width="53" valign="top">&nbsp;</td>
        </tr>

        <tr>
          <td width="96" valign="top">
            <p><em><strong>College review</strong></em></p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">X</p>
          </td>

          <td width="53" valign="top"><p align="center">X</p>
          </td>

          <td width="53" valign="top">
            <p align="center">(X)</p>
          </td>
          <td width="53" valign="top"><p align="center">(X)</p></td>
        </tr>
        </table>

        <p>One hard copy of the required materials (#1-3) should be made available in a secured location in the department office and an electronic copy of items #1-7 should be posted in SharePoint for review by D-CPER committee members, chair, dean, and C-CPER committee (when such a committee is formed).  Item #8 should be added to the hard copy folder and uploaded to SharePoint when completed. </p>
        <br clear="all" />

        <p><strong><em>1. CPE Cover Sheet and Checklist</em></strong><br />
        The <a href="docs/CPE-Cover-Sheet-and-Checklist.pdf">CPE cover sheet</a> and checklist form is available on the Provost’s website as a PDF document, and should be filled in by the faculty member and provided with the other materials. The coversheet should be signed by the faculty member and the department chair. The last page of the checklist should be signed by the department chair and dean. The department chair and dean should sign after they have reviewed the files. All should be actual signatures, not electronic or digital signatures.</p>

        <p><strong><em>2. Professional Curriculum Vitae</em></strong><br />
          </p>
        <div id="noborder"><table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="182"><div align="right"><em>Name and Contact Information</em></div></td>
            <td width="454">
							This should include UTSA address, phone number, and email address, as well as current academic rank (for example, “Assistant Professor” or “Associate Professor without Tenure”).</td>
          </tr>
          <tr>
            <td><div align="right"><em>Educational Background</em></div></td>
            <td>Please list all institutions from which a degree was earned, including the degree received and the major field of study.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Professional Employment History</em></div></td>
            <td>List all positions held in sequential order, with applicable dates, since earning the highest degree, including the present position at UTSA.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Awards and Honors</em></div></td>
            <td>List any awards, honors, prizes, competitions, or other recognition received related to professional activities.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Research/Scholarly/Creative Activities Summary</em></div></td>
            <td>
							Summarize all products of research/scholarly/creative activities, including publications, exhibitions, performances, architectural projects, reviews, or other documentation of scholarly contributions. List separately the different types of publications (e.g. journal articles, books, reviews, etc.), scholarly products, or creative activity outcomes, providing respective listings of invited contributions, refereed contributions, and non-refereed contributions. All contributions should include the date and title of publication/exhibition/performance, the venue, and where applicable, the inclusive page numbers or size of the scholarly contribution.
						</td>

          </tr>
          <tr>
            <td><div align="right"><em>Scholarly Presentations</em></div></td>
            <td>List all external oral or poster presentations at conferences, meetings, or other institutions/universities related to scholarly work, and provide the dates and locations of presentations. Provide separate listings for invited presentations, refereed, and non-refereed contributions.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Granting Activities</em></div></td>
            <td>Provide a list of grant proposals submitted, whether for research, instructional, or public service activities (please indicate one of these for each grant), giving the name of the granting agency, the project dates, the project title, and the total amount requested and awarded, if appropriate, for each.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Intellectual Property</em></div></td>
            <td>Where applicable, provide a summary of any intellectual property generated from scholarly activities and indicate any patent applications, copyright privileges, licensing, or other commercialization that has resulted. The summary should include dates, titles, and identifying information.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Teaching Activities</em></div></td>
            <td>List all formal courses taught at UTSA, indicating the level of the course (undergraduate or graduate) and its title.
Provide a list of students mentored in research/scholarly/creative activities and any theses or dissertations directed. Summarize any service on graduate committees and for student advising.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Service Activities</em></div></td>
            <td>Provide separate listings of all committee assignments, assigned administrative activities (for example, department chairmanship, center directorship, etc.), and professional service activities (including leadership in disciplinary organizations, service as a journal editor, manuscript or grant proposal reviewer, meetings or symposia organized, etc.). Each activity should include the dates of participation, the organizational level of the activity (for example, department, college, etc.), and any leadership roles played. </td>
          </tr>
        </table></div>
      <p><strong><em>3. Summary of Professional Accomplishments</em></strong><br />
        The summary of professional accomplishments should be succinct and provide only the highlights of the faculty member’s noteworthy accomplishments during the evaluation period. This is not intended to be a restatement of the curriculum vitae. In addition to the summary, one-to-two paragraphs may be devoted to outlining plans for professional activities over the coming evaluation period. The summary should ideally be no longer than two pages in total.</p>

        <p><strong><em>4. Summary of Student Course Evaluation Survey Results</em></strong><br />
        Provide a table of courses taught during the evaluation period using the template provided below (<a href="../PromotionTenure/Summary-of-Student-Course-Evaluation-Template.docx">this template</a> may be downloaded from the Provost’s website).  Do not include copies of student evaluation surveys or comments among these materials.  If you have IDEA surveys, please use the adjusted figure for course ratings and instructor ratings.  The online course surveys provide average ratings under the “Essential Statements” portion of the survey.</p>
<table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><p align="center"><em>Semester</em></p></td>
            <td width="100"><p align="center"><em>Course No.</em></p></td>
            <td width="105"><p align="center"><em>Course Type</em></p></td>
            <td width="82"><p align="center"><em>New Prep?</em></p></td>
            <td width="71"><p align="center"><em>Course Enrollmt.</em></p></td>
            <td width="82"><p align="center"><em>No. of Responses</em></p></td>
            <td width="64"><p align="center"><em>Course Rating</em></p></td>
            <td width="78"><p align="center"><em>Instructor Rating</em></p></td>
          </tr>
          <tr>
            <td width="100"><p align="center">SP2011</p></td>
            <td width="100"><p align="center">ABC.nnn3</p></td>
            <td width="105"><p align="center">LD,   UD, or GR</p></td>
            <td width="82"><p align="center">NEW</p></td>
            <td width="71"><p align="center">xxx</p></td>
            <td width="82"><p align="center">yyy</p></td>
            <td width="64"><p align="center">X.X</p></td>
            <td width="78"><p align="center">Y.Y</p></td>
          </tr>
          <!--<tr>
            <td width="428" valign="top"><p align="center"><em>Note:</em> LD = lower division,<br />
            UD = upper division,<br />GR = graduate-level</p></td>
           </tr>-->
        </table>
<p><em>Note:</em> LD = lower division, UD = upper division, GR = graduate-level</p>
<p><strong><em>5.  Peer Observation Report(s)</em></strong><br />
  Provide peer observer’s report(s) and faculty member’s report(s) for each instance in which the department has performed a peer observation of teaching since previous comprehensive review.  Please refer to the <a href="../PromotionTenure/docs/Peer-Observation-Guidelines.pdf">Peer Observation Guidelines</a> for assistance in completing this task.</p>
<p><strong><em>6. Annual Reports</em></strong><br />
        Annual reports should be submitted by the faculty member for each of the years comprising the evaluation period. The department chair may assist in providing these materials.</p>
<p><strong><em>7. Optional Supplementary Material</em></strong><br />
  Faculty members may submit optional supplementary materials as electronic documents posted to SharePoint to highlight or document achievement in the areas of teaching, research/ scholarly/creative activities, and service activities, particularly those highlighted in the summary of professional accomplishments (#3). A checklist of possible items that might be included among the supplementary materials is included with the <a href="docs/CPE-Cover-Sheet-and-Checklist.pdf">CPE Cover Sheet form</a> available from the Provost’s website.</p>
 <p> Once the department-level evaluations are completed, the optional supplementary materials may remain available during the dean and/or C-CPER committee’s evaluation of the review packet. In extraordinary cases, where additional supplementary materials might clarify a point made in the departmental review, the dean and/or C-CPER committee may request these materials.</p>
  <p>Please note that collecting supplementary materials in digital format is the responsibility of the faculty member. Assistance in creating digital archives is available through the Office of Information Technology and the University Library. By storing these materials online, the intent is that they are made accessible for the convenience of the D-CPER and C-CPER committees, Department Chair, and Dean to assist in their separate deliberations.</p>
<p><strong><em>8. Evaluation and Review Materials</em></strong><br />
        As the review packet goes through the evaluation process, each level of review should append its report to the packet for consideration by the next level of review. Guidelines for these various levels of review are provided in the “Review Process” section of these guidelines below. The materials should be arranged in the following order, with the responsibility and timing for appending each set of materials indicated below:</p>

        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <th width="160" valign="top">
              <p><em>Item</em></p>
            </th>

            <th width="160" valign="top">
              <p><em>Responsible Individual</em></p>
            </th>

            <th width="160" valign="top">
              <p><em>When</em></p>
            </th>
          </tr>

          <tr>
            <td width="160" valign="top">
              <p>D-CPER committee report<br />
              Chair’s evaluation report</p>
            </td>

            <td width="160" valign="top">
              <p>Department Chair</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion of department-level evaluation</p>
            </td>
          </tr>

           <tr>
            <td width="160" valign="top">
              <p>C-CPER committee report<br />
              (if convened)</p>
            </td>

            <td width="160" valign="top">
              <p>Dean</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion of departmental review, if needed.</p>
            </td>
          </tr>

<tr>
            <td width="160" valign="top">
              <p>Dean&rsquo;s evaluation(s)*</p>
            </td>

            <td width="160" valign="top">
              <p>Dean</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion of all prior reviews.</p>
            </td>
          </tr>
        </table>
        <p>*If a C-CPER committee is convened the Dean will write a final evaluation memo summarizing the review.</p>

        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
