<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>CPE | Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/home/includes/nav_PPEGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        
        <h1>Comprehensive Periodic Evaluation of Tenured Faculty<br />
        Guidelines </h1>

        <p>This set of guidelines provides information for faculty undergoing a Comprehensive Periodic Evaluation (CPE) as prescribed by the Handbook of Operating Procedures (HOP), chapter 2.22 “Comprehensive Periodic Evaluation of Tenured Faculty.” These guidelines also provide information for those faculty serving on the Department and College CPE Review Committees (D-CPER and C-CPER, respectively), department chairs, and deans involved in the review process. These guidelines are reviewed annually and updated as needed by the provost’s office.</p>
       <p>The CPE process is a post-tenure review process employed by the university to uphold high standards and expectations for its faculty. It is incumbent upon all who are involved in the review process to read all applicable materials, deliberate the strengths and weaknesses of each faculty member’s performance in good faith with objectivity, and to observe confidentiality concerning the views of others, as revealed during review discussions. A respectful, thorough, and objective review of faculty accomplishments depends upon the conscientious efforts of all participants in the review process. All written materials generated through the review process are available for the inspection of faculty candidates. Questions concerning the university’s procedures for CPE review may be directed to the Office of the Provost and Vice President for Academic Affairs.</p>

        <blockquote>
          <blockquote>
            <blockquote>
              <p>These guidelines are divided into several sections with the following contents:<br />
              <strong><a href="overview.asp">Overview of Process</a></strong>&mdash; a brief description of the timeline for review and the responsibilities of each party at each stage of the process.<br />
              <strong><a href="preparation.asp">Preparation of the Review Packet</a></strong>&mdash; a listing of essential and optional elements to include in the review packet prepared by tenured faculty.<br />
              <strong><a href="review.asp">Review Process</a></strong>&mdash; an outline of the responsibilities of the CPE review committees, department chair, and dean in carrying out the review.<br />
              <strong><a href="docs/CPE-Cover-Sheet-and-Checklist.pdf">Cover Sheet and Checklist</a></strong>&mdash; a summary of required and optional materials to be submitted for the purposes of CPE review.</p>
            </blockquote>
          </blockquote>
        </blockquote>

        <p><strong><em>Schedule for CPE Review</em></strong></p>

        <p>Each tenured faculty member, including those appointed to part-time administrative positions (e.g. chair or associate dean) will undergo a CPE review in the sixth year following the previous comprehensive review, whether that review be the awarding of tenure, the promotion to full professor, or the previous CPE review.</p>
        <p>In addition, if a faculty member is scheduled for a CPE review and requests to be reviewed for promotion to full professor, the CPE review is placed on hold until the outcome of the promotion to full professor has been decided. If the promotion to full professor is successful, the CPE will be rescheduled to the next sixth-year period. If the promotion to full professor is not awarded, the department chair and dean will then evaluate the promotion and tenure DFRAC recommendation and provide a succinct evaluation that will serve as the CPE review.</p>
        <p>The Provost and Vice President for Academic Affairs keeps the official calendar of CPE reviews and notifies the deans each spring of those faculty who are due to undergo review in the following academic year.</p>

        <p><strong><a href="docs/CPE-Guidelines.pdf">This information also is available in PDF format.</a></strong></p>
        
        
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->

<!--#include virtual="/inc/footer/footer-about.html" -->

<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
