<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Review | CPE | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/home/includes/nav_PPEGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
          <h2>Comprehensive Periodic Evaluation</h2>
          <h1>Review Process</h1>


        <p>The process for CPE review normally involves three levels, including the D-CPER committee, the chair, and the dean. If the final evaluation indicates unsatisfactory performance in one or more areas of activity, the dean may appoint, with the consent of the faculty member, a C-CPER committee to evaluate the review packet.</p>

        <h2>1. Membership of the Department CPE Review committee</h2>
          The membership of the D-CPER committee is determined by the following process:</p>
         <ul>
           <li>The voting faculty of each department shall elect members to serve on the D-CPER committee in years that a faculty member is undergoing CPE review.  Only tenured faculty are eligible to serve on a CPE Review committee.</li>
           <li>The D-CPER committee should have at least three (3) tenured faculty members of equal or greater rank to the faculty member(s) under review. Faculty who are appointed as part-time administrators are eligible to serve on the D-CPER committee; however, this should not include any individuals who otherwise participate in later stages of the CPE review.</li>
           <li>If there are fewer than three (3) eligible faculty members available to serve on the committee, the department chair, in consultation with the dean, may invite full professors from other departments to participate as members of the department CPE Review committee.  </li>
          <li>When the faculty member undergoing a CPE is the department chair, the dean shall appoint another faculty member at or above the academic rank of the department chair to serve in the role of department chair for this process.</li>
          <li>The D-CPER committee shall elect its own chair.</li>
        <li>The department chair will certify the election of members and of the committee chair and provide the dean with a list of the membership by September 10.</li></ul>

        <h2>2. Membership of the College-level CPE Review committee</h2>
          A C-CPER committee may be appointed by the dean, with the consent of the faculty member, when one or more areas of her/his performance are deemed to be unsatisfactory. This committee shall be constituted by the following procedure:</p>
          <ul><li>Members may come from any department of the university, but must be tenured and hold rank equal to or greater than the faculty member under review.</li>
          <li>The dean shall appoint no fewer than three (3) members to the college-level CPE Review committee. More than three members may be appointed in the event that multiple faculty members have received unsatisfactory reviews.</li>
          <li>The membership of the C-CPER committee should be determined by January 15.</li>
          <li>The C-CPER committee shall elect its own chair.</li>
        <li>The C-CPER committee may be used to consider more than one CPE review in a given year, if needed.</li></ul>

        <h2>3. Roles of Review Entities</h2>
        All reports are ultimately advisory to the dean, who governs the CPE review process for faculty. The role of each entity may be summarized as follows:</p>

        <p><strong>D-CPER committee</strong>— conducts a full review of the faculty member’s performance during the evaluation period. The Department CPE Review should afford the faculty member an opportunity to meet with the committee, if requested by either the D-CPER committee or the faculty member being reviewed, to clarify aspects of her/his performance. If the analysis of the record raises performance concerns, the committee may request additional documentation from the faculty member. </p>
        <p>The final report from the department committee will provide an evaluation for each area and an overall evaluation, using the ratings listed in HOP 2.22 (exceeds expectations, meets expectations, fails to meet expectations, and unsatisfactory), but will not include a narrative or summary unless one or more areas or the overall evaluation is one of “fails to meet expectations” or “unsatisfactory.” The report must be signed by the members of the committee.</p>
        <p><strong>Department Chair</strong>— conducts a full review of the faculty member’s performance during the evaluation period, from the perspective of how that performance addresses the mission and responsibilities of the department, subject to the protections of academic freedom. The chair should independently evaluate the review packet, but consider the report of the D-CPER committee in arriving at a recommended evaluation in each area of performance and an overall evaluation.  The department chair’s report should contain an evaluation for each area and an overall evaluation, using the ratings in HOP 2.22</p>
        <p>The chair’s report should fully explain the reasons for any differences of opinion with the D-CPER committee report. When the review packet is forwarded to the college, the chair shall notify the candidate(s) in writing about the conclusion of the departmental evaluation, indicating if there are any areas judged to be unsatisfactory.  Ideally, the Chair should provide verbal feedback to the D-CPER committee at the end of the departmental review and discuss areas of concurrence and disagreement.</p>
				<p><strong>Dean</strong> — an independent, comprehensive review of the review packet, taking the D-CPER committee, and department chair’s analysis and recommendations into consideration. The dean’s report should contain an evaluation for each area and an overall evaluation, using the ratings in HOP 2.22, but will not ordinarily include a narrative or summary unless he/she disagrees with the D-CPER or department chair analysis.  In addition, if any area of performance is evaluated as “unsatisfactory” the dean should provide a written analysis. The dean may optionally append narrative to commend faculty for noteworthy performance over the review period.</p>
        <p>When all cases from the college are transmitted to the provost’s office, the dean shall notify all faculty members undergoing CPE review of their evaluation.    Ideally, the Dean should provide verbal feedback to the department chair at the end of his or her review and discuss areas of concurrence and disagreement.</p>
        </p>
        <p><strong>C-CPER committee</strong>— a general evaluation of the review packet, but focusing on those areas in which performance was deemed unsatisfactory by prior reviews (D-CPER, department chair and/or dean).  The C-CPER committee should provide a report analyzing the earlier evaluations and providing its independent analysis of the problematic areas in the CPE review. In cases where the C-CPER committee differs with the prior analysis, it should detail its reasons for the different conclusions. As with the departmental CPE review committee, the report should ideally
        characterize the consensus view of the committee; however, if no consensus is reached, then all perspectives should be represented in the report.</p>

        <p><strong>Provost</strong>&mdash;review the reports of the D-CPER committee, department chair, dean, and C-CPER committee (if utilized) and approve or amend any recommended follow-up actions.</p>
       <p> The Provost notifies all faculty members undergoing CPE review of the final evaluation result and provide the date for the next CPE review, typically, six years from the previous review date.</p>

        <h2>4. General Guidelines for the D-CPER and C-CPER committees</h2>
        In addition to the policies expressed under HOP 2.22, the CPER committees should adhere to the following guidelines. Careful adherence to these policies and guidelines is necessary to ensure a fair, objective, and consistent process throughout the analysis of each CPE review.</p>

        <ul type="disc">
          <li>The CPER committees function exclusively to conduct internal peer evaluations for the purpose of making recommendations on the evaluation of faculty performance.</li>
            <li>Faculty members serving on the D-CPER committee have the responsibility to read all review packet materials, to review the applicant’s performance in each of the performance criteria thoroughly and to participate in committee discussions and formulation of committee recommendations. Participation by proxy is not appropriate.</li>
            <li>Faculty members serving on the C-CPER committee have the responsibility to read all review packet materials, with special focus on the materials related to the initial evaluation of unsatisfactory performance, and to participate in committee discussions and formulation of committee recommendations. Participation by proxy is not appropriate.</li>
            <li>When a recommendation of “unsatisfactory” is made, the D-CPER committee’s report will include an analysis that should indicate the factors that contributed to the committee’s recommendations and illuminate any factors that were prominently cited during the deliberations that would have been supportive of a contrary or alternative recommendation. This would also apply to the report from the C-CPER committee, should this committee be convened.</li>
            <li>Recommendations should be based on consistently applied criteria appropriate for the faculty candidate’s academic discipline and that reflect the expected workload distribution as reported by the chair.</li>
            <li>Faculty serving on a CPER committee should focus on factual information and guard against inaccuracies caused by either emphasis or omission of information.</li>
            <li>Faculty may serve on only one level of CPE review for a given faculty member, unless exception is granted by the dean.</li>
            <li>Only tenured faculty members may serve on a CPER committee and only full professors may serve when evaluating the performance of a full professor. If those committees, when constituted in accordance with the Handbook of Operating Procedures and college bylaws, have fewer than three tenured full professors, the chair shall nominate and the dean shall appoint additional full professors until there are three on the committee.</li>
           <li>The CPER committee report should be signed by all participating members of the review committee. On the signature page, the report should include a header that read: “We, the undersigned members of the CPE Review committee have reviewed this report for completeness and accuracy, and attest that we have reached our recommendations through a thorough review and discussion of the available documentary evidence.”</li>
        </ul>

        <p>Each dean is responsible for reviewing policies and procedures and these guidelines with department chairs and for assuring that these policies, procedures and instructions are followed.</p>

        <h2>5. Guidelines for Department Chair&rsquo;s Evaluation</h2>
          The chair’s report should contain the following essential elements:</p>
          <ul><li>a copy of the D-CPER committee’s report;</li>
          <li>an analysis of the candidate’s contributions in each of the areas of teaching, research/ scholarly/ creative activity, and service within the context of the department’s needs;</li>
          <li>a succinct statement of the chair’s recommendation, with explanation of the factors leading to this recommendation, especially in those areas where the chair may disagree with the D-CPER committee’s conclusions.</li></ul>
        The chair should strive to compose the report using factual data to support conclusions, and draw clear connections for how the faculty member’s performance contributes to the department’s fundamental missions of teaching, research, and service, and helps the department improve its overall performance.</p>

        <h2>6. Guidelines for Dean&rsquo;s Evaluation</h2>
          In all cases, the dean’s evaluation should provide the following essential information:</p>
         <ul><li>the nature of the department’s recommendations through the D-CPER committee and chair,</li>
         <li>a succinct statement of the dean’s evaluation and any recommendations for developmental activities to improve performance.</li></ul>
         <p> If the CPE Review committee and chair have provided analyses and recommendations with which the dean is in agreement, then the dean may provide a succinct statement of concurrence with the earlier analyses.</p>
        <p>If earlier recommendations express diverse outcomes, or if the dean disagrees with their conclusions, then a more comprehensive report explaining the rationale for the dean’s conclusion should be provided. The dean should take care to express conclusions within the context of the college’s expectations and aspirations for long-term quality among its faculty.</p>
        <h2>Retention of CPE Documents</h2>
				<p>
					CPE documents are retained in accordance with the university’s official retention schedule, as follows:
					<br />
						<ul>
							<li>Record series 3.1.119 Performance Appraisals – All supporting documentation
								and review materials are retained for 2 years following the CPE review conclusion date.
							</li>
						</ul>
				</p>
				<p>
					The university’s full records retention schedule can be accessed through the following link: <a href="https://www.utsa.edu/openrecords/retention.html" target="_blank">https://www.utsa.edu/openrecords/retention.html
				</p>
        </a><!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
