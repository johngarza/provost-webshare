<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Preparation | Emeritus Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style>
#noborder table {border:none;}
#noborder table th {border:none;}
#noborder table tr {border:none;}
#noborder table td {border:none;}
.style1 {color: #0033CC; text-indent:25px}
</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!--#include virtual="/home/includes/nav_emeritus.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h1>Preparation of the Emeritus Review Packet</h1>

        <p>The application packet contains the materials that form the basis for the review at all levels of evaluation. It is important that faculty members under consideration for emeritus status make every effort to ensure that the material contained in the packet is complete, accurate, and professionally presented.</p>
         <p> The contents of an application packet should include the following elements:<br />
          1. <a href="docs/Emeritus-Cover-Sheet-and-checklist.pdf">Cover Sheet and Checklist</a><br />
          2. a letter to the DFRAC/department chair<br />
          3. a current professional vitae<br />
          4. memos outlining evaluations and recommendations at various levels of review</p>
         <p> The letter to the DFRAC/Department Chair should serve as the basis for outlining the candidate’s qualifications for emeritus status. If the candidate plans to continue pursuing scholarly activities as an emeritus faculty member, a brief outline of those plans should be included in the letter. The professional vitae should serve as a simple listing of professional activities. The candidate is responsible for preparing items #1 – 3; and the departmental faculty review advisory committee, chair, and dean are responsible for appending materials contained in #4. Items #1 – 3 constitute the review materials utilized by the DFRAC, the chair, the dean, the provost, and the president in reviewing the application.</p>
       <p> One hard copy of the required materials (#1 – 3,) should be made available in a secured location in the department office and an electronic copy of 1 -3 should be posted in the official Faculty Review folder in SharePoint for review by DFRAC members. Item #4 should be added to the hard copy folder and uploaded to SharePoint when they are completed. Each of these items is described more fully in the following sections.</p>
        <p><strong><em>1. Cover Sheet</em></strong><br />
        The <a href="docs/Emeritus-Cover-Sheet-and-checklist.pdf">cover sheet form</a> is available on this web site as a PDF document, and should be filled in by the faculty candidate and provided with the other materials. The second page of the cover sheet is a checklist of the essential contents of the application package.</p>
        <p><strong><em>2. Letter to DFRAC/Department Chair</em></strong><br />
          The letter to the DFRAC/Department Chair should be organized in three sections, outlining the applicant’s professional activities and experiences. In cases in which the applicant has plans to continue professional activities, the letter should describe any future plans in the areas of teaching, research/scholarly/creative activities, and/or service as applicable The letter should be no more than 3 - 5 pages long.</p>
        <p><strong><em>3. Professional Vitae</em></strong></p>

<div id="noborder">
<table width="625" border="1">
  <tr>
    <td><em>Name and Contact Information</em></td>
    <td>This should include UTSA address, phone number, and email address, as well as current academic rank (for example, “Assistant Professor” or “Associate Professor without Tenure”).</td>
  </tr>
  <tr>
    <td><em>Educational Background</em></td>
    <td>Please list all institutions from which a degree was earned, including the degree received and the major field of study.</td>
  </tr>
  <tr>
    <td><em>Professional Employment History</em></td>
    <td>List all positions held in sequential order, with applicable dates, since earning the baccalaureate degree, including the present position at UTSA.</td>
  </tr>
  <tr>
    <td><em>Awards and Honors</em></td>
    <td>List any awards, honors, prizes, competitions, or other recognition received related to professional activities.</td>
  </tr>
  <tr>
    <td><em>Research/Scholarly/Creative Activities Summary</em></td>
    <td>Summarize all products of research/scholarly/creative activities, including publications, exhibitions, performances, architectural projects, reviews, or other documentation of scholarly contributions. List separately the different types of publications (e.g. journal articles, books, reviews, etc.), scholarly products, or creative activity outcomes, providing respective listings of invited contributions, refereed contributions, and non-refereed contributions. All contributions should include the date and title of publication/exhibition/performance, the venue, and where applicable, the inclusive page numbers or size of the scholarly contribution.</td>
  </tr>
  <tr>
    <td><em>Scholarly Presentations</em></td>
    <td>List all external oral or poster presentations at conferences, meetings, or other institutions/universities related to scholarly work, and provide the dates and locations of presentations. Provide separate listings for invited presentations, refereed, and non-refereed contributions.</td>
  </tr>
  <tr>
    <td><em>Granting Activities</em></td>
    <td>Provide a list of grant proposals submitted, whether for research, instructional, or public service activities (please indicate one of these for each grant), giving the name of the granting agency, the project dates, the project title, and the total amount requested and awarded, if appropriate, for each.</td>
  </tr>
  <tr>
    <td><em>Intellectual Property</em></td>
  <td>Where applicable, provide a summary of any intellectual property generated from scholarly activities and indicate any patent applications, copyright privileges, licensing, or other commercialization that has resulted. The summary should include dates, titles, and identifying information.</td></tr>
  <tr>
    <td><em>Teaching Activities</em></td>
    <td>List all formal courses taught, indicating the level of the course (undergraduate or graduate) and its title. Provide a list of students mentored in research/scholarly/creative activities and any theses or dissertations directed. Summarize any service on graduate committees and for student advising.</td>
  </tr>
  <tr>
    <td><em>Service Activities</em></td>
    <td>Provide separate listings of all committee assignments, assigned administrative activities (for example, department chairmanship, center directorship, etc.), and professional service activities (including leadership in disciplinary organizations, service as a journal editor, manuscript or grant proposal reviewer, meetings or symposia organized, etc.). Each activity should include the dates of participation, the organizational level of the activity (for example, department, college, etc.), and any leadership roles played.</td>
  </tr>
</table></div>

        <p><strong><em>4. Evaluation and Recommendation Materials</em></strong><br />
        As the application goes through the review process, each level of review should append its analysis and recommendation to the packet for consideration by the next level of review. Guidelines for these various levels of review are provided in the “Review Process” section of these guidelines. The materials should be arranged in the following order, with the responsibility and timing for appending each set of materials indicated below:</p>

        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="167" valign="top">
              <p><em>Item to be added to file</em></p>
            </td>

            <td width="152" valign="top">
              <p><em>Responsible Individual</em></p>
            </td>

            <td width="160" valign="top">
              <p><em>When</em></p>
            </td>
          </tr>

          <tr>
            <td width="167" valign="top">
              <p>DFRAC analysis</p>
              <p>Chair’s recommendation</p>
            </td>

            <td width="152" valign="top">
              <p>Department Chair</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion</p>
            </td>
          </tr>

          <tr>
            <td width="167" valign="top">
              <p>Dean’s recommendation</p>
            </td>

            <td width="152" valign="top">
              <p>Dean</p>
            </td>

            <td width="160" valign="top">
              <p>Upon completion of college-level review</p>
            </td>
          </tr>
        </table>


        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
