<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Criteria | Emeritus Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/home/includes/nav_emeritus.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h1>Qualifications and Criteria for Emeritus Review</h1>

        <p>For general guidelines to the criteria expected for successful emeritus, please see the UTSA Handbook of Operating Procedures, 2.03 “Emeritus Academic Titles.” Specific criteria will vary by discipline and will be left to individual Departments and Colleges to enunciate. </p>
        <p>Only candidates that receive consistent support at the departmental and college levels of the review process have a strong likelihood of a successful outcome in an emeritus application. Emeritus status is earned through the applicant’s demonstrated contributions that have impacted the university in a positive manner.</p>
        <p><strong><em>Emeritus Qualifiations</em></strong><br />
					Faculty may be granted an emeritus title if they have:<br/>
					<ul>
						<li>Retired or upon retirement, if they are or were a full professor or associate professor by the end of the academic year, </li>
						<li>Given distinguished and honorable full-time academic and professional service to UTSA over a period of at least 10 years and made a significant contribution to their professional area of expertise,</li>
						<li>Adhered to the highest academic, civic, and ethical standards, and</li>
						<li>Rated as “Exceeds Expectations” in at least 3 of the last 5 annual overall faculty ratings and the remaining overall faculty ratings at “Meets Expectations” or above.</li>
					</ul>
				</p>

				<p><strong><em>Emeritus Criteria</em></strong><br />
        A simple enunciation of the criteria to be used for emeritus applications is difficult. As UTSA strives for national research university status, it will be increasingly important that faculty demonstrate academic leadership in their disciplines through their research/scholarly/creative activities. However, sole adherence to this criterion would oversimplify our consideration of the variety of cases that come forward.</p>
         <p> For successful emeritus applications, faculty should demonstrate the following qualities:<br />
         <ul>
         <li>They are/have been active scholars whose scholarship manifests an inherent desire to learn about the world and the human condition within it.</li>
         <li>They are/have been devoted and effective teachers who promote student success, both inside and outside the classroom and laboratory.</li>
         <li>They are/have been committed citizens of the university and of their respective disciplines, and manifest this through significant service activities, including leadership positions.</li>
          <li>They have made extraordinary contributions to the university and/or their academic discipline.</li>
          <li> They have been a full-time faculty member in good standing at UTSA for an extended period (for clarification, “an extended period” will generally be interpreted as at least 10 calendar years, but the DFRAC, chair, and/or dean may recommend exceptions to this rule, if requested).</li></ul>
          There is significant latitude for departments and colleges to interpret these criteria for emeritus review liberally and to subject them to their own disciplinary lens and filter. It is the intention of the university to utilize emeritus titles to recognize faculty who have made, and continue to make, major contributions to the mission of the university, and by extension, to the national community of faculty in their respective disciplines.</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
