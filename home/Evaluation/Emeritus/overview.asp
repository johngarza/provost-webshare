<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Overview | Emeritus Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/home/includes/nav_emeritus.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->

        <h1>Overview of Emeritus Review Process</h1>

        <p>The purpose of the emeritus review process is to perform an objective evaluation of
					each case at several levels of review. Each application goes through at least five
					levels of independent review before a final recommendation is achieved: DFRAC, chair,
					dean, provost, and president. The president’s recommendation in all cases is final.</p>
       <p> The emeritus review process is summarized in the table below which outlines the rough timeline and actions of the procedure.</p>

        <table width="675
                " border="0" cellpadding="0" cellspacing="0">
          <tr>
            <th width="100" valign="top"> <p>WHEN</p></th>
            <th width="117" valign="top"><p>WHO</p></th>
            <th width="383" valign="top"><p>RESPONSIBILITY</p></th>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>Fall/Winter</strong></p></td>
            <td width="117" valign="top"><p>Applicant*</p></td>
            <td width="383" valign="top"><ul>
              <li>Indicate the date, or anticipated date, of retirement from university in writing to chair and dean. Complete <a href="docs/Form-for-Review-for-Emeritus.pdf">Request for Review for Emeritus</a> form and return to chair;</li>
              <li> Begin assembling materials for application packet</li>
            </ul></td>
          </tr>

          <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>Dean’s Office</p></td>
            <td width="383" valign="top"><ul>
              <li>Notify Provost’s Office of Faculty who wish to be considered for emeritus (send completed form “Request for Review for Emeritus” to the Provost’s Office)</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>January 20</strong></p></td>
            <td width="117" valign="top"><p>Department</p></td>
            <td width="383" valign="top"><ul>
              <li>Provide DFRAC member names to the college and the Provost’s Office</li>
            </ul>
            </td>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>February 1</strong></p></td>
            <td width="117" valign="top"><p>Applicant*</p></td>
            <td width="383" valign="top"><ul>
              <li>Deadline to submit letter to DFRAC via department chair requesting to be reviewed for emeritus status; Include current vita and completed coversheet and checklist</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>February 10</strong></p></td>
            <td width="117" valign="top"><p>Department</p></td>
            <td width="383" valign="top"><ul>
              <li>Deadline to upload applicant’s documents to SharePoint</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>March</strong></p></td>
            <td width="117" valign="top"><p>DFRAC (full professors only)</p></td>
            <td width="383" valign="top"><ul>
              <li>Review application packet;</li>
              <li> Deliberate in closed meeting and vote on case;</li>
              <li> Prepare a written summary of evaluation analysis</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>Department Chair</p></td>
            <td width="383" valign="top"><ul>
              <li>Review all application materials, including DFRAC recommendation;</li>
              <li> Prepare a written recommendation for forwarding to college;</li>
              <li> Upload DFRAC memo and Dept. Chair memo to the official Faculty Review folder in SharePoint and forward manila folder with hard copies of applicant’s materials, DFRAC memo and Dept. Chair memo to the Dean’s Office</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>College Dean</p></td>
            <td width="383" valign="top"><ul>
              <li>Review application packet and department recommendations;</li>
              <li> Prepare a written recommendation for forwarding to provost;</li>
              <li> Upload DFRAC, Dept. Chair and Dean’s memos to the official Faculty Review folder in SharePoint and forward manila folder with hard copies of the applicant’s materials, DFRAC, Dept. Chair and Dean’s memos to the Provost’s Office</li>
              <li>
                <strong>Application materials due in Provost Office by <br />
                April 1st, or the first work day thereafter</strong></li>
            </ul>            </td>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>April</strong></p></td>
            <td width="117" valign="top"><p>Provost</p></td>
            <td width="383" valign="top"><ul>
              <li>Review all application materials, including recommendations;</li>
              <li> Consult with Chairs and Deans, as needed, for clarification of application materials;</li>
              <li> Prepare recommendations for the president</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>President</p></td>
            <td width="383" valign="top"><ul>
              <li>Review all application materials, including recommendations at each level;</li>
              <li> Render final decisions concerning emeritus recommendations</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p><strong>May</strong></p></td>
            <td width="117" valign="top"><p>Provost</p></td>
            <td width="383" valign="top"><ul>
              <li>Prepare written notification to all applicants concerning outcome of emeritus review</li>
            </ul></td>
          </tr>
          <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>Department</p></td>
            <td width="383" valign="top"><ul>
              <li>Create a zero percent Emeritus faculty contract and forward through normal approval process</li>
            </ul></td>
          </tr>
					<!--
          <tr>
            <td width="100" valign="top"><p><strong>August</strong></p></td>
            <td width="117" valign="top"><p>UT Board of Regents</p></td>
            <td width="383" valign="top"><ul>
              <li>Approve emeritus recommendations from all campuses in UT System</li>
            </ul></td>
          </tr>

           <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>Provost</p></td>
            <td width="383" valign="top"><ul>
              <li>Prepare written notification to applicants concerning outcome of Board of Regents review</li>
            </ul></td>
          </tr>
         <tr>
            <td width="100" valign="top"><p>&nbsp;</p></td>
            <td width="117" valign="top"><p>Department</p></td>
            <td width="383" valign="top"><ul>
              <li>Create a zero percent Emeritus faculty contract and forward through normal approval process</li>
            </ul></td>
          </tr>
				-->
        </table>

        <p>* Any tenured faculty member, or group in the department or college, may nominate a qualified tenured UTSA faculty who is retired or anticipates retiring by the end of the academic year. In this case the nominator is responsible for writing the letter to the Department Chair.</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
