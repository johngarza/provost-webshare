<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Emeritus Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!--#include virtual="/home/includes/nav_emeritus.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
           <h1>Emeritus Application Guidelines </h1>

        <p>This set of guidelines provides information for faculty applicants for emeritus, and for faculty review advisory committees (FRACs), department chairs, and deans involved in the review process. These guidelines are reviewed annually and updated as needed by the Provost’s Office.</p>
          <p>The process of emeritus faculty review is undertaken by the university each year. Emeritus status represents an honorary designation, a mechanism by which a faculty member can maintain an official status with the university after separation of employment, and a means for continuing scholarly activities post-retirement. The review of candidates for faculty emeritus status will focus on the career achievements of the applicant. For those candidates intending to remain active scholars as emeritus faculty, it is also appropriate to share any plans for continued scholarly work as part of the submission letter in the review packet.</p>
          <p>It is incumbent upon all who are involved in the review process to read all applicable materials, deliberate the strengths and weaknesses of each case in good faith with objectivity, and to observe confidentiality concerning the views of others, as revealed during review discussions. A respectful, thorough, and objective review of faculty accomplishments depends upon the conscientious efforts of all participants in the review process.</p>
         <p> UTSA’s process is intended to be as transparent as possible, and all written materials generated through the review process are available for the inspection of faculty candidates. Questions concerning the university’s procedures for emeritus applications may be directed to the Provost and Vice President for Academic Affairs.</p>
         <p> These guidelines are divided into several sections with the following contents:<br />
           <a href="overview.asp">Overview of Process</a> — a brief description of the timeline for review and the responsibilities of each party at each stage of the process.<br />
           <a href="preparation.asp">Preparation of the Review Packet</a> — a listing of essential elements to include in the emeritus review packet prepared by faculty applicants.<br />
           <a href="criteria.asp">Criteria for Emeritus</a> — information about the criteria to be followed in reviewing emeritus applications.<br />
           <a href="review.asp">Review Process</a> — an outline of the responsibilities of the FRAC, department chair, and dean in carrying out the review.<br />
        <a href="docs/Emeritus-Cover-Sheet-and-checklist.pdf">Cover Sheet and Checklist</a> — a summary of required materials to be submitted for emeritus consideration.</p>
        <p>&nbsp;</p>
         <p align="center"><a href="docs/Emeritus-Application-Guidelines.pdf">This information also is available in a PDF format.</a></p>
        <p align="center">&nbsp;</p>
         <p align="center"><a href="docs/Form-for-Review-for-Emeritus.pdf"><img src="../../images/emeritus-review.png" width="400" height="75" /></a></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
