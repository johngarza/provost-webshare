<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Review | Emeritus Guidelines | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/home/includes/nav_emeritus.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h1>Emeritus Review Process</h1>

        <p>The review process for emeritus applications involves five levels of review, including the Departmental Faculty Review Advisory Committee (DFRAC), the Chair, the Dean, the Provost, and the President. This structure promotes a thorough, objective review of each case, and provides for input from all relevant perspectives, from the departmental through university-wide viewpoints.</p>

        <h2>Roles of Review Entities</h2>
          All reports from the various levels are ultimately advisory to the President, who makes final decisions concerning the university’s recommendations for emeritus status. The role of each entity in this review hierarchy can be summarized as follows:</p>
          <p><strong>DFRAC</strong> — a full and detailed review of the candidate’s application for emeritus status. The members of the DFRAC provide a peer review by those members of the university community best qualified to judge the quality of the candidate’s activities. All votes should be by secret ballot so that the votes of individuals are not divulged. Only full professors on the DFRAC may review and vote on the candidate’s application. If the DFRAC has fewer than three tenured full professors, the Dean shall appoint additional full professors until there are three on the committee. <strong>Note: Only DFRAC members physically present for the discussion should participate in the vote of the committee. Voting by proxy, absentee, or email is not permitted.</strong></p>
         <p> 	The DFRAC report should be signed by all participating members of the review committee.  On the signature page, the report should include a header that reads:  “We, the undersigned members of the DFRAC, have reviewed this report for completeness and accuracy and attest that we have reached our recommendations through a thorough review and discussion of the available documentary evidence.”</p>
          <p><strong>Department Chair</strong> — a full and detailed review of the candidate’s application for emeritus status. The chair should independently evaluate the candidate’s application packet, but consider the recommendations of the DFRAC in arriving at a recommendation.</p>
          <p><strong>Dean</strong> — an independent, comprehensive review of the candidate’s packet, taking the DFRAC and Chair’s, recommendations into consideration.</p>
          <p><strong>Provost</strong> — an independent review of the candidate’s packet, and general analysis of the earlier reviews (DFRAC, Chair, Dean). The Provost is responsible for maintaining equivalent, and high, standards across the university. The Provost shall then convey her/his recommendations to the President.</p>
        <p><strong>President</strong> — an independent review of the candidate’s packet, and general analysis of the earlier reviews (DFRAC, Chair, Dean, Provost). The President may consult with the Provost and other entities prior to reaching a final decision as to the institutional recommendation. Upon reaching a final decision in all cases, the President shall instruct the Provost to prepare appropriate written notification to all candidates for emeritus status concerning the outcome of the university’s review process.</p>
        <h2>Retention of Emeritus Documents</h2>
				<p>
					Emeritus documents are retained in accordance with the university’s official retention schedule, as follows:</p>
				<ul>
					<li>Record series 3.1.143 Faculty Promotion and Tenure Review Records (Promotion File) – Evaluations, recommendations memos, etc. are retained for 2 years following the date granting/denying promotion to emeritus.</li>
					<li>Record series 3.1.137 Employee Recognition Records – A copy of the letter granting/denying promotion to emeritus is retained for 5 years after employment ends.</li>
				</ul>
				</p>
				<p>
					The university’s full records retention schedule can be accessed through the following link:: <a href="https://www.utsa.edu/openrecords/retention.html" target="_blank">https://www.utsa.edu/openrecords/retention.html</a></p>
				</p>        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
