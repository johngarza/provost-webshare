<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
             <!--#include virtual="/home/includes/nav_evaluation.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
           <h1>Faculty Evaluation</h1>

        <p>UTSA has evaluation and promotion processes for tenure-track (Assistant Professor), tenured (Professor and Associate Professor), and non-tenured track faculty (who hold the titles of Lecturer, Professor of Research, and Professors in Practice) that are consistent with UTSA’s dedication to developing and maintaining excellent faculty.</p>
        <p>Evaluation is intended to enhance and protect academic freedom while ensuring that faculty members are contributing to their field and meeting their responsibilities to UTSA and the State of Texas. </p>
        <p>The information and guidelines for each process are made available here to assist faculty along the way.</p>
        <h3>Annual Performance Evaluation</h3>
          <p>All full-time faculty members and continuing part-time faculty members at UTSA must be evaluated annually for the purpose of merit review. Visit <a href="http://www.utsa.edu/hop/chapter2/2-11.html">HOP 2.11 Annual Faculty Performance Appraisal for Merit Consideration</a> for details.</p>
<h3><a href="Third_Year_Review/index.asp">Third-Year Review </a></h3>
<p>All non-tenured faculty members with tenure track appointments receive a comprehensive review during the fall semester of their third year of full-time service to the university. </p>
<h3><a href="PromotionTenure/index.asp">Promotion and Tenure</a></h3>
<p>Applicable to all UTSA tenured and tenure-track faculty, the purpose of promotion at UTSA is to recognize and reward faculty with records of sustained professional accomplishment.</p>
<h3><a href="CPE/index.asp">Comprehensive Periodic Evaluation</a></h3>
<p>CPE of tenured faculty is a significant part of the faculty member’s long-term career development.</p>
<h3>Promotion of Non-Tenure-Track Faculty</h3>
  <p>UTSA offers a promotion process for NTT faculty with the titles of Lecturers, Professors of Research, and Professors in Practice. Visit <a href="http://www.utsa.edu/hop/chapter2/2-50.html">HOP 2.50 Nontenure-Track Faculty Recruitment, Evaluation, and Promotion Processes</a> for details.</p>
<h3><a href="Emeritus/index.asp">Emeritus Application</a></h3>
<p>Emeritus faculty status is an honorary designation by which a faculty member can maintain an official status with the university after separation of employment, and means for continuing scholarly activities post-retirement. The process of emeritus faculty review is undertaken by the university each year.</p>
<h3><a href="http://provost.utsa.edu/home/Evaluation/PromotionTenure/docs/Peer-Observation-Guidelines.pdf">Peer Observation Guidelines</a></h3>
<p>Peer evaluation is an effective tool for faculty development in the area of teaching. These guidelines provide recommendations and minimum requirements for the peer observation process.</p>
<!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
