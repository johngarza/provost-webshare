<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Scanning Services | Help Support | Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
          <!--#include virtual="/home/includes/nav_EvalSupport.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <h2>Help and Support  </h2>
        <h1>Scanning Services (Library)</h1>

        <p><strong>Scanning Services</strong><br />
        If you would like JPL staff to scan your materials, follow the steps below. <em>Note: Faculty materials will only be scanned by full-time library staff. No students will be allowed to handle the materials. Faculty materials will be secured with lock and key while at the JPL.</em></p>
        <ol>
          <li>Access and complete the appropriate library form on the <a href="pdf/Library-Doc-Trans-Form.pdf">Library Document Transmittal Form</a>.</li>
          <li>In the “Items to be scanned” section, <strong>clearly define what you will be leaving to scan.</strong> This itemized list should be in the correct order and must have the proper electronic name. (Use letters, numbers and underscores ONLY.)</li>
          <li><strong>Print this form</strong> and take it with you to the JPL Front Desk along with your materials. The Front Desk staff will be trained to assist faculty with scanning requests. <em>Note: Items with any sensitive content should not be brought by UTSA faculty to the library.</em></li>
          <li>Once you and the Library staff have verified that all documents on the list have been submitted, both parties will <strong>sign the form</strong> to approve the transaction.</li>
          <li>Your name, date and list of items to be scanned will be entered into the Library scanning log.</li>
          <li>Items will be scanned and the PDF files will be emailed to you. A flash drive or CD of your files may also be created upon request. Each scanned PDF file will consist of multiple pages if the item is longer than one page.</li>
          <li>The library may require up to <strong>5 business days</strong> to fulfill the scanning request. Upon completion of the scan, the scanned files will be sent to you via email, or you will be notified to pick up the flash drive or CD.  Please review the scanned files.  .&nbsp; <em>(Note: If there are any problems with the scanned images such as missing pages, poor quality images, etc., the faculty should notify the library and the items will be rescanned.)</em></li>
          <li>Upon approval of the scanning services, <strong>pick up your materials and the flask drive or CD (if requested)</strong> from the JPL Library Front Desk.</li>
          <li>You and a library staff member will be required to <strong>sign the form</strong> to verify all items have been returned to you. You will also receive a copy of the form to keep for your records.</li>
        </ol>

        <p><img src="/images/pdficon_small.gif" align="absmiddle" width="17" height="17" alt="Adobe Acrobat pic"/><a href="pdf/Library-Scanning-Services-Instructions.pdf">Print version of Scanning Services instructions</a> (PDF).</p>

        <p><strong>Library Staff Will Scan:</strong></p>

        <ol>
          <li>articles</li>

          <li>artwork (please notify Library staff of dimensions)</li>

          <li>3-dimensional models</li>

          <li>reports</li>

          <li>book covers and first chapters</li>
        </ol>

        <p><strong>Library Staff Will NOT Scan:</strong></p>

        <ol>
          <li>entire books</li>

          <li>sensitive documents</li>
        </ol>

        <p><strong><a name="OwnYourOwn">Scanning Materials on Your Own</a></strong><br />
        If you – or a trusted representative – would like to scan your materials using library equipment, follow the steps below. Faculty members have access to Library scanning equipment throughout the year.</p>
        <ol>
          <li>Call Amy Chang (458-7503, amy.chang@utsa.edu) of Library Access Services to make an appointment to use library scanning equipment.</li>
          <li>At your scheduled time, bring your files and a portable storage device (such as a USB flash drive or CD). Storage devices will not be provided for you.</li>
          <li>Library staff will help you set up the equipment and provide you with proper instructions.</li>
        </ol>

        <p><br /></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
