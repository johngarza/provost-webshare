<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>SharePoint Support | Help Support | Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/home/includes/nav_EvalSupport.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
           <h2>Help/Support </h2>
           <h1>SharePoint Support</h1>
        <h2>SharePoint Instructions (PDF) – print versions</h2>
<ul>
  <li><a href="pdf/2019-2020-SharePoint-Instructions-Accessing-Folders.pdf">How to access your official SharePoint folder</a></li>
  <li><a href="pdf/2019-2020-SharePoint-Instructions-Managing-Files.pdf">How to manage your files and folders in SharePoint</a> (upload, delete, rename, navigate within SharePoint, etc.)</li>
  <li><a href="pdf/SharePoint-Guide-for-Reviewers_updated-2019.pdf">SharePoint guide for reviewers</a></li>
</ul>
<h2>Other SharePoint Support</h2>
  <p>Contact OIT Connect via email at <a href="mailto:oitconnect@utsa.edu">oitconnect@utsa.edu</a> or via phone by dialing (210) 458-5555.</p>
<h2>Preferred File Type for Electronic “Box” Submission</h2>
<p>Although SharePoint has the capability of uploading any file type, reviewers will need to have the specific application on their own computer to open the file.</p>
<p>After speaking with some faculty and UTSA technology experts, members of the Office of Information Technology (OIT) and the Office of the Provost have come up with a list of the most common and accessible file formats at UTSA.</p>
<p>The recommended file formats are as follows:</p>
<ul>
  <li>Documents:  PDF</li>
  <li>Graphics/Images:  JPEG, GIF</li>
  <li>Audio:  mp3</li>
  <li>Video:  WMV (OIT will offer support to faculty who need assistance with converting video files to WMV format.  Please email <a href="mailto:oitconnect@utsa.edu">oitconnect@utsa.edu</a> for assistance.)</li>
</ul>
<p>If you would like to submit a file format not listed above, please work with your department and college to ensure your reviewers will have the proper software to access this file.</p>
<!--  <h2>RowdySpace Instructions (PDF) &ndash; print versions</h2>

        <ul type="circle">
          <li><a href="pdf/Access_PT_Folder_Bkmk.pdf">How to access your official RowdySpace folder</a></li>

          <li><a href="pdf/Managing_FilesFolders.pdf">How to manage your files and folders in RowdySpace (upload, delete, rename, navigate within RowdySpace, etc.)</a></li>

          <li><a href="pdf/RSFolder_for_LibraryStaff.pdf" target="_blank">How to create a folder for Library staff to upload your scanned files</a></li>

          <li><a href="pdf/Copy_Files_From_Personal_to_Offical.pdf">How to copy files from your personal RowdySpace account to your official faculty evaluation folder</a></li>
        <li><a href="pdf/RowdySpace_Guide_for_Reviewers.pdf">RowdySpace guide for reviewers</a></li></ul>


            <h2>Other RowdySpace Support</h2>
            <p>Contact OIT Connect via email at <a href="Mailto: oitconnect@utsa.edu">oitconnect@utsa.edu</a> or via phone by dialing (210) 458-5555.</p>
            <h2>Preferred File Type for Electronic "Box" Submission</h2>
            <p>Although RowdySpace has the capability of uploading any file type, reviewers will need to have the specific application on their own computer to open the file.</p>
            <p>After speaking with some faculty and UTSA technology experts, members of the Office of Information Technology (OIT) and the Office of the Provost have come up with a list of the most common and accessible file formats at UTSA.</p>
            <p>The recommended file formats are as follows:</p>
            <ul>
              <li>Documents: PDF</li>
              <li>Graphics/Images: JPEG, GIF</li>
              <li>Audio: mp3</li>
              <li>Video: WMV (OIT will offer support to faculty who need assistance with converting video files to WMV format. Please e-mail <a href="Mailto: oitconnect@utsa.edu">oitconnect@utsa.edu</a> for assistance.)</li>
            </ul>
            <p>If you would like to submit a file format not listed above, please work with your department and college to ensure your reviewers will have the proper software to access this file.</p>
          </dd>
        </dl>-->
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
