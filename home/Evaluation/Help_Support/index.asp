<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Help Support | Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!--#include virtual="/home/includes/nav_EvalSupport.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
          <h1>Help/Support</h1>

        <p>The UTSA Office of the Provost, the Office of Information Technology and Library Services are committed to assisting faculty that will be going through an electronic evaluation process.<br /></p>

        <p>The Help and Support pages will provide information on areas that include SharePoint support and scanning services.
					For an overview of the electronic evaluation process, please review either the
					<a href="../CPE/docs/CPE-SharePoint-User-Guide.pdf">Comprehensive Periodic Evalution User Guide (PDF)</a>,
					the <a href="../PromotionTenure/docs/PT-SharePoint-User-Guide.pdf">Promotion and Tenure User Guide (PDF)</a> or
					the <a href="../Third_Year_Review/docs/TYR-SharePoint-User-Guide.pdf">Third-Year Review User Guide (PDF)</a>.<br /></p>

        <p><a href="scanning_services.asp">Scanning Services (Library)</a><br /></p>

        <p>The John Peace Library will provide a service to scan documents for UTSA faculty who are going through an electronic evaluation process. Upon formal request from UTSA faculty, library staff in the Access Services Department will scan textual materials creating PDF format files and load the files to a CD, flashdrive or send via email.</p>

        <p>Faculty members will also have the option of scanning materials on their own. Instructions are provided in the <a href="scanning_services.asp#OwnYourOwn">Scanning Materials on Your Own</a> section of the <a href="scanning_services.asp">Scanning Services</a> page.<br /></p>

        <p><a href="sharepoint_support.asp">SharePoint Support</a><br /></p>

        <p>SharePoint support is available via the following: <!--online tutorials,--> downloadable instructions, or by contacting OIT Support Services.</p>

        <p>Please visit the <a href="sharepoint_support.asp">SharePoint Support</a> page for more information.</p>

        <p><a href="Acrobat9_support.asp">Acrobat Professional Support</a><br /></p>

        <p>Adobe Acrobat 9 Professional will provide you with several PDF features, such as text editing, added security, combining files into one PDF, creating PDF portfolios and more.</p>

        <p>Adobe offers a wide range of support and help materials on their website, including video tutorials. Visit the <a href="http://help.adobe.com/en_US/Acrobat/9.0/Professional/index.html">Acrobat Professional Support page</a> for links to helpful tutorials.</p>


        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
