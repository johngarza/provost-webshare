<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Acrobat 9 | Help Support | Evaluation | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!--#include virtual="/home/includes/nav_EvalSupport.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h2><span>Help/Support </span></h2>
         <h1><span>Acrobat Professional Support</span></h1>

        <h2>VIDEOS: Acrobat 9 Professional - Adobe TV</h2>

        <ul>
          <li><a href="http://tv.adobe.com/watch/learn-acrobat-9/combining-documents-into-a-single-pdf/">Combining Documents Into a Single PDF</a></li>

          <li><a href="http://tv.adobe.com/watch/learn-acrobat-9/adding-watermarks-to-your-pdf-documents/">Adding Watermarks to your PDF Documents</a></li>

          <li><a href="http://tv.adobe.com/watch/learn-acrobat-9/exporting-pdf-files-to-office-formats/">Exporting PDF Files to Office Formats</a></li>

          <li><a href="http://tv.adobe.com/watch/learn-acrobat-9/protecting-your-pdf-using-a-password/">Disable Printing and/or Editing of PDF Document by Password Protection</a> (*Note: Do not require a password for others to view the document. Follow this video exactly.)<br /></li>
        </ul>

        <p>View the full list of Adobe &quot;Learn Acrobat 9&quot; episodes at: <a href="http://tv.adobe.com/show/learn-acrobat-9/">http://tv.adobe.com/show/learn-acrobat-9/</a></p>

        <h1>ONLINE SUPPORT: Acrobat Professional - Web Help</h1>

        <p>To view Adobe's entire online support for Acrobat 9 Pro, go to: <a href="http://help.adobe.com/en_US/Acrobat/9.0/Professional/index.html">http://help.adobe.com/en_US/Acrobat/9.0/Professional/index.html</a>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
