<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Criteria | Evaluation | Promotion/Tenure | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           <!-- #include virtual="/home/includes/nav_PTGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
          <h2>Promotion and Tenure</h2>
          <h1>Principles Guiding Promotion and Tenure</h1>

        <p>For general guidelines to the criteria expected for successful promotion and tenure, please see the UTSA&nbsp;<em>Handbook of Operating Procedures</em>, 2.10 “Faculty Reappointment, Promotion, and Tenure.”&nbsp; Although specific expectations vary by discipline and by other guidance put forth by the Academic Colleges and individual Departments, the over-arching university standards for excellence that comport with the university’s goals are described below.</p>
        <h2><em><strong>Principles guiding Tenure</strong></em></h2>
        <p>Central to UTSA’s future is to join the community of premier, national Carnegie “Research 1” and AAU-like research universities. Consequently, our standards for scholarly achievement and academic excellence and the attendant decisions concerning the awarding of tenure to faculty are commensurate with this aspiration.&nbsp;What defines these institutions at the core is scholarly excellence in all its varieties - from fundamental, interdisciplinary, applied, community engaged and translational - particularly relevant to UTSA as an urban-serving, multicultural, engaged university. Furthermore, we value and recognize the pivotal role of faculty in our interconnected, mutually supportive institutional goals of student success and excellence through innovation.  Accordingly — and fundamental to our university’s mission — successful applicants for tenure and promotion will have the following attributes:</p>
        <ul>
          <li><strong>Significant scholarly contribution</strong> as active researcher, scholar, and creative artist.  Successful applicants will be engaged in discovery — exploring the nature of the world and the diverse human condition in new ways — which advances new knowledge, perspectives and understanding.</li>
          <li><strong>Independence</strong>. Work put forward as a part of this evaluation shall include ideas and concepts developed by the applicant, and is beyond those explored in the applicant’s graduate dissertation and thesis, as well as distinct and distinguishable from those of mentors, advisors, collaborators or colleagues.</li>
          <li><strong>Progressive record of achievement</strong>. Academic efforts and outcomes should build and unfold, showing evidence of ongoing and evolutionary development of expertise, skills and accomplishments.</li>
          <li><strong>Innovation</strong>. Works put forward as a part of this evaluation should break new ground, advance new understanding, or improve learning or inclusion (e.g., utilizing or developing new pedagogical approaches or evaluation methods in novel instructional activities; disrupting current thinking by integrating threads; connecting or integrating methods from multiple disciplines that may reveal new lines of inquiry or pedagogy; or translating scholarly outcomes into new products, technologies or programs; to name a few).</li>
          <li><strong>National / International Dissemination</strong>. Scholarship by the faculty member should have broad dissemination in top-rate venues of scholarly discoveries, works and creations, manifested by, for example, publications, exhibitions, reviews, performances, products, technologies or presentations, or proceedings, extending to scholarly communities nationally and internationally, or authoring widely adopted textbooks, national society presentations, federal grants, or national awards for pedagogical innovation or systematic evaluation of instructional methodology.</li>
          <li><strong>Impact</strong>. Discoveries, works and creations that “matter,” addressing important and unresolved questions or matters in a field or area of inquiry, including interdisciplinary, and as such, are cited and recognized by other researchers, scholars or creative artists.  Impact can be evinced by, for example, citations, references, honorifics and similar recognition, patents or commercialization, programs, policies, grants or other similar contractual awards. Examples of impact might include applying pedagogies that measurably improve student learning and success or that expand access or inclusion; translating a new idea into a community service program to promote positive change; serving on a national advisory panel that propels new legislation or public discourse; or translating findings into technologies, products and other entrepreneurial activities. </li>
          <li><strong>Benefit</strong>.  Academic efforts and outcomes provide recognizable value. Examples might include scholarship that makes available opportunities for undergraduate and/or graduate students, particularly those from underrepresented groups, to be involved personally in the act of discovery, scholarship and creation; activities that model the excitement of intellectual engagement to promote lifelong learning or uses inquiry to systematically demonstrate the application of new pedagogy on classroom outcomes; or service to the discipline or area by serving as an evaluative reviewer.</li>
          <li><strong>Engagement</strong>. Examples of engagement might include fostering the success and development of students into transformative leaders of a diverse, inclusive society within the local to global context; serving with distinction in roles to support a federal agency or institution, academic society, community agency, or private and not-for-profit industry; advancing UTSA as an exemplar of innovative excellence through service that substantively positively impacts the university’s trajectory and its effects towards our goals.</li>
          <li><strong>Bearer of the standard of the university’s future</strong>. Contributions by the faculty member should measurably improve the department, college, and university by raising the standard for the next generation of tenure-track faculty at UTSA. Each department and college and leader must consciously ask the question, “Does this faculty member raise the level of our department, college and university by their presence, activity and impact, or not?” If the answer is not affirmative, then tenure should not be recommended.</li>
        </ul>
        <p>For tenure-track faculty candidates with years in faculty rank (or equivalent; note, NOT including graduate or post-doctoral work) gained at other institutions, the review process including the departments, colleges and the university levels shall include consideration of any scholarly productivity and impacts, teaching excellence and service participation completed prior to joining UTSA as faculty. </p>
        <p><em>Early Tenure.</em> Tenure-track faculty may be considered early for award of tenure and promotion, that is, before the beginning of the sixth year of the probationary period. However, the expectation is that early tenure is reserved for faculty with demonstrated<em>, </em>exceptional achievements relative to years in rank in research, scholarship and creative activity, instruction and service. As such, although the overall record for scholarly achievement, teaching excellence, and participation in service for an “early” candidate shall be no less in terms of both quantity and quality to that of a successful candidate coming up in the expected sixth year, a candidate who is showing “good progress” toward tenure is not appropriate to be considered for early tenure.  </p>
        <p> For tenure-track faculty candidates with years in faculty rank (or equivalent, NOT graduate or post-doctoral appointments) gained at other institutions, the resultant scholarly productivity and impact, teaching excellence and service participation completed prior to joining to UTSA as faculty shall be included as a part of the evaluation for tenure and promotion, including early tenure.</p>
        <p>Before initiating the process for an early tenure and promotion consideration, tenure-track faculty should consult with the cognizant Chair and Dean. If there is not unqualified support for an early application, it is most prudent to apply for promotion and tenure on the expected schedule.<br />
          <br />
          A final decision to “hold without prejudice” an early application for tenure is not a rejection or a denial, but simply a statement that the application is not yet ready for an affirmative decision.</p>
        <h2><em><strong>Principles for Promotion to Full Professor </strong></em></h2>
        <p>Promotion to full professor is predicated on the same core tenets articulated above for the promotion to Associate Professor – that, is standards for achievements in rank that constitute academic excellence of premier national (e.g., Carnegie “Research 1” or AAU and equivalent) research universities.  First and foremost, the university utilizes promotion to full professor to recognize those faculty who have made major, distinctive contributions to the mission of the university centered in outstanding research, scholarship or creative arts; and who are devoted, accomplished teachers who advance student success in and out of the classroom, recognized leaders of their respective discipline or area and citizens of our communities.  As such, candidates are expected to be substantive, intellectual leaders in the national / international communities of faculty in their respective disciplines or areas.  As in the case of the award of tenure, only candidates that receive consistent support for promotion to full professor at the departmental and college levels of the review process have a strong likelihood of success.</p>
        <p> Candidates are wise to consult with their Chair and Dean before pursuing promotion to Professor to gain assistance in the evaluation of readiness and success.  It is expected that seeking promotion to full professor is undertaken once a distinguished record of faculty leadership is established, and is done so with thought and respect for the evaluative process. </p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
