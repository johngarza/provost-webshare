<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">

    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->

        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!-- #include virtual="/home/includes/nav_PTGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>
        </div>

        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h2>Promotion and Tenure</h2>
         <h1>
           Review Process</h1>

        <p>The review process for promotion and tenure applications involves no fewer than seven levels of review, including the Departmental Faculty Review Advisory Committee (DFRAC), the Chair, the College Faculty Review Advisory Committee (CFRAC), the Dean, the University Faculty Review Advisory Committee (UFRAC), the Provost, and the President. This structure promotes a thorough, objective review of each case, and provides for input from all relevant perspectives, from the departmental through university-wide viewpoints.</p>

        <h2>Roles of Review Entities</h2>

        <p>All reports from the various levels are ultimately advisory to the President, who makes final decisions concerning the university&rsquo;s recommendations for promotion and tenure. The role of each entity in this review hierarchy can be summarized as follows:</p>

        <p><strong>DFRAC</strong>&mdash;a full and detailed review of the candidate’s performance during the probationary period or period since last promotion. The DFRAC provides a peer review by those members of the university community best qualified to judge the quality of the candidate’s activities. Accordingly, the DFRAC should provide a detailed written analysis of the candidate’s instructional, research/scholarly/creative, and service activities. The final recommendation of the DFRAC should be based upon that analysis and should report the final tally of any votes taken by the committee, including any abstentions or absences. All votes should be by secret ballot so that the votes of individuals are not divulged. Only DFRAC members physically present for the discussion should participate in the vote of the committee.</p>

        <p><strong>Department Chair</strong>&mdash; a full and detailed review of the candidate&rsquo;s performance during the probationary period or period since last promotion, from the perspective of the long-term needs of the department. The chair should independently evaluate the candidate&rsquo;s application packet, but consider the recommendations of the DFRAC in arriving at a recommendation. The chair&rsquo;s report should succinctly amplify points in the DFRAC report where there is agreement, and fully explain the reasons for any differences of opinion with the DFRAC report. The Chair&rsquo;s report should also briefly explain the process for selecting external reviewers, summarize the reviewers&rsquo; qualifications for evaluating the case, and highlight important and relevant elements from the external reviewer reports. When the case is forwarded to the college, the Chair shall notify the candidate(s) in writing about the nature of the DFRAC and Chair&rsquo;s recommendations. For example, the email or memo could state “your case has been forwarded to the Dean with a positive recommendation from the DFRAC and the Department Chair regarding your promotion and/or tenure case.”  If the DFRAC and Chair differ in their recommendations, the email or memo could state “your case has been forwarded to the Dean with a mixed recommendation regarding your promotion and/or tenure case.” Likewise, if the departmental recommendations are both negative, the email or memo should follow the earlier examples. Ideally, the Chair should provide verbal feedback to the DFRAC at the end of the departmental review and discuss areas of concurrence and disagreement. </p>

        <p><strong>CFRAC</strong>&mdash;a comprehensive review of the candidate’s packet, and DFRAC and Chair’s recom­menda­tions. The CFRAC provides a more general peer review from within the context of the college as a whole, ensuring that each department in the college is upholding equivalent standards for promotion and tenure. The CFRAC should provide a report outlining the justifica­tions for its final recommendations, and may cite the DFRAC report and Chair’s report liberally to highlight agreement or disagreement with previous recommendations. In cases where the CFRAC is in full agreement with the analysis of the DFRAC and Chair, it may provide a succinct report stating its agreement. The CFRAC should exercise all votes by secret ballot and report the numerical results of those votes, including abstentions and absences.  Only CFRAC members physically present for the discussion should participate in the vote of the committee.</p>

        <p><strong>Dean</strong>&mdash; an independent, comprehensive review of the candidate&rsquo;s packet, taking the DFRAC, Chair&rsquo;s, and CFRAC recommendations into consideration. The Dean is responsible for maintaining high standards for promotion and tenure within the college and for analyzing cases in the context of the long-term needs of the college.&nbsp; The Dean should provide a written analysis of each case, but may liberally cite opinions and analysis provided by earlier levels of review. In cases where the Dean is in agreement with all previous recommendations and feels that the case has been sufficiently analyzed in earlier reports, it is sufficient to provide a simple statement of agreement. When all cases from the college are transmitted to the Provost&rsquo;s Office for university-level review, the Dean shall notify all candidates about the nature of the CFRAC and Dean&rsquo;s recommendations. For example, the email or memo could state “your case has been forwarded to the Provost’s Office with a positive recommendation from the CFRAC and the Dean regarding your promotion and/or tenure case.” If the CFRAC and Dean differ in their recommendations, the email or memo could state “your case has been forwarded to the Provost’s Office with a mixed recommendation regarding your promotion and/or tenure case.” Likewise, if the college recommendations are both negative, the email or memo should follow the earlier examples. Ideally, the Dean should also provide verbal feedback to the CFRAC at the end of the college-level review and discuss areas of concurrence and disagreement.</p>

        <p><strong>UFRAC</strong>&mdash;a general analysis of the earlier reviews (DFRAC, Chair, CFRAC, Dean) of the candidate’s packet to ensure both uniformity of standards and that sufficient care has been taken in evaluating all relevant aspects of the candidate’s performance. The UFRAC should refrain from engaging in a detailed analysis of the candidate’s performance insofar as its membership is unlikely to have disciplinary expertise overlapping that of the candidate. The UFRAC should report all votes (once again, taken by secret ballot and including abstentions and absences), and need only provide a written analysis for those cases in which they disagree with the final recommendations of the earlier levels of review. In cases where there is disagreement among the earlier levels of review, the UFRAC should explain which parts of the earlier analyses are more compelling and thus led to the UFRAC’s ultimate recommendation.  Only UFRAC members physically present for the discussion should participate in the vote of the committee.</p>

        <p><strong>Provost</strong>&mdash;an independent review of the candidate&rsquo;s packet, and general analysis of the earlier reviews (DFRAC, Chair, CFRAC, Dean, UFRAC). The Provost is responsible for maintaining equivalent, and high, standards across the university and for analyzing each case in the context of the institution&rsquo;s long-term needs. The Provost shall compile a summary of the entire set of cases evaluated, including the recommendations made at each level of review, and make recommendations to the President. Following the President&rsquo;s deliberations and final decisions, the Provost should ideally meet with the UFRAC to discuss areas of concurrence and disagreement in individual cases.</p>

        <p><strong>President</strong>&mdash; an independent review of the candidate&rsquo;s packet, and general analysis of the earlier reviews (DFRAC, Chair, CFRAC, Dean, UFRAC, Provost). The President may consult with the Provost and other entities prior to reaching a final decision as to the institutional recommendation. Upon reaching a final decision in all cases, the President shall instruct the Provost to prepare appropriate written notification to all candidates for promotion and/or tenure concerning the outcome of the university&rsquo;s review process.</p>

        <h2>General Guidelines for the DFRAC and CFRAC</h2>

        <p>The DFRAC and CFRAC for each department and college, respectively, shall be constituted as indicated in the policies and procedures cited in the <em>Handbook of Operating Procedures</em> (HOP), Section 2.10.&nbsp; The roles and responsibilities of each of these committees is outlined in the previous section on &ldquo;Roles of Review Entities.&rdquo;</p>

        <p>In addition to the policies expressed under HOP 2.10, the DFRAC and CFRAC should adhere to the following guidelines. Careful adherence to these policies and guidelines is necessary to ensure a fair, objective, and consistent process throughout the review of each case.</p>

        <ul type="disc">
          <li>The DFRAC and CFRAC function exclusively to conduct internal peer evaluations for the purpose of making recommendations on<br />
          &nbsp;</li>

          <li style="list-style: none; display: inline">
            <ul type="circle">
              <li>faculty reappointment,</li>

              <li>tenure of an untenured associate professor,</li>

              <li>promotion to associate professor with tenure,</li>

              <li>promotion to professor or</li>

              <li>the initial appointment of a faculty member with tenure.&nbsp;</li>
            </ul>

            <p>The committees shall limit their recommendations to these actions, as appropriate.<br />
&nbsp;          </p>
          </li>

          <li>Faculty members serving on the DFRAC and CFRAC are responsible for reading <strong><u>all</u></strong> tenure and promotion materials, reviewing the applicant’s performance in each of the performance criteria thoroughly and participating in committee discussions and formulating committee recommendations.  Voting by proxy, email, absentee, etc., to recommend denial of tenure and/or promotion or to recommend promotion and/or tenure is not permitted.
<br />
          &nbsp;</li>

          <li>The analysis of the DFRAC and CFRAC should indicate the factors that contributed to each committee&rsquo;s recommendations <strong><u>and</u></strong> illuminate any factors that were prominently cited during the deliberations that would have been supportive of a contrary recommendation.<br />
          &nbsp;</li>

          <li>Recommendations should be based on consistently applied criteria appropriate for the faculty candidate&rsquo;s academic discipline.<br />
          &nbsp;</li>

          <li>Faculty serving on the DFRAC and CFRAC should focus on factual information and guard against inaccuracies caused by either emphasis or omission of information.<br />
          &nbsp;</li>

          <li>At each stage in the review process, all previous recommendations and analyses in the current review cycle are to be taken into account by the reviewing entity and noted in the written analysis.<br />
          &nbsp;</li>

          <li>Minority opinions and analyses from      members of FRACs may optionally be included as part of the FRAC report      along with other committee analyses and recommendations (see third bullet      above).  These “minority reports,”      if utilized, may be segregated in a separate section of the report, but      should not be submitted separately, and should be made available for      review by the entire FRAC to ensure consistency.  No other information or correspondence      may be placed in the applicant’s file for transmittal to a department      chair or onward to the provost.</li>
        </ul>
        <ul>
          <li>Regardless of whether a person is a member of more than one review committee for a case (e.g., DFRAC, CFRAC, and UFRAC), that person may only vote on the case once, and that vote occurs at the department level.  If that person is on the DFRAC and CFRAC and/or UFRAC that person may only vote on the case at the department level and may not vote on this case at the CFRAC or UFRAC levels.  Even if that person did not serve on the DFRAC and serves only on the CFRAC and/or UFRAC, they may not vote on their department’s cases at either the CFRAC or UFRAC level.      </li>
         <li>In the CFRAC and/or UFRAC deliberations, each committee member shall present a balanced description of the decisions rendered at lower levels for cases originating in his/her department or college.  It is expected that members of CFRAC will not advocate an outcome not supported by DFRAC (i.e., a member of DFRAC and CFRAC will not advocate at CFRAC an outcome inconsistent with the DFRAC recommendation).  In addition, faculty members on the CFRAC may not write the committee report for promotion and tenure cases from their department.  Likewise, members of UFRAC will not advocate an outcome not supported by one of the lower levels. </li>
          <li>Only full professors on the DFRAC and CFRAC may consider applicants for promotion to full professor.  If those committees, when constituted in accordance with the <em>Handbook</em> and College Bylaws, have fewer than three tenured full professors, the Dean shall appoint additional full professors until there are three on the committee.</li>


          <li><strong>The FRAC report should be signed by all participating members of the review committees. On the signature page, the report should include a header that reads: &ldquo;We, the undersigned members of the [DFRAC/CFRAC], have reviewed this report for completeness and accuracy, and attest that we have reached our recommendations through a thorough review and discussion of the available documentary evidence.&rdquo;</strong></li>
        </ul>

        <p>Each Dean is responsible for reviewing policies and procedures and these guidelines with Department Chairs and for assuring that these policies, procedures and instructions are followed.</p>

        <h2>General Guidelines for the UFRAC</h2>

        <p>The UFRAC shall be constituted as indicated in the policies and procedures cited in <u>Handbook of Operating Procedures</u>, Section 2.10.&nbsp; The roles and responsibilities of this committee is outlined in the section on &ldquo;Roles of Review Entities&rdquo; above.</p>

        <p>In addition to the policies expressed under HOP 2.10, the UFRAC should adhere to the following guidelines. Careful adherence to these policies and guidelines is necessary to ensure a fair, objective, and consistent process throughout the review of each case.</p>

        <ul type="disc">
          <li>The UFRAC shall function exclusively to conduct internal peer evaluations for the purpose of making recommendations on</li>

          <li style="list-style: none; display: inline">
            <ul type="circle">
              <li>tenure of an untenured associate professor,</li>

              <li>promotion to associate professor with tenure, or</li>

              <li>promotion to professor.</li>
            </ul>
          </li>

          <li>Faculty members serving on the UFRAC have the responsibility to read <u><strong>all</strong></u> applicants’ packets, to review all earlier evaluations and recommendations, and to participate in committee discussions and formulation of committee recommendations.  Voting by proxy, email, absentee, etc., to recommend denial of tenure and/or promotion or to recommend promotion and/or tenure is not permitted.<br />
          &nbsp;</li>

          <li>The analysis of the UFRAC should focus on the factors that caused each earlier review entity to make its recommendations and describe any factors that were prominently cited during the deliberations and which would be supportive of a contrary recommendation. The UFRAC should analyze these reviews to ensure that consistency and objectivity have been exercised at each stage of the earlier reviews in achieving earlier recommendations.<br />
          &nbsp;</li>

          <li>The UFRAC should evaluate whether or not the earlier recommendations are based on consistently applied criteria appropriate for the faculty candidate&rsquo;s academic discipline.<br />
          &nbsp;</li>

          <li>Minority opinions and analyses from members of UFRAC may optionally be included as part of the UFRAC report along with other committee analyses and recommendations (see third bullet above).&nbsp; These &ldquo;minority reports,&rdquo; if utilized, may be segregated in a separate section of the report, but should not be submitted separately, and should be made available for review by the entire UFRAC to ensure consistency. No other information or correspondence may be placed in the applicant&rsquo;s file for transmittal to the Provost.<br />
          &nbsp;</li>

          <li>When a person is a member of more than one review committee for a case (e.g., DFRAC and UFRAC), that person may only vote on the case once, and that vote occurs at the department level.  Even if that person did not serve on the DFRAC and serves only on the CFRAC or UFRAC, they may not vote on their cases at the CFRAC or UFRAC level.  In the UFRAC deliberations, each committee member shall present a balanced description rendered at lower levels for cases originating in his/her department or college.  It is expected that members of the UFRAC will not advocate an outcome not supported by one of the lower levels.</li>

          <li><strong>The UFRAC report should be signed by all participating members of the review committees. On the signature page, the report should include a header that reads: &ldquo;We, the undersigned members of the UFRAC, have reviewed this report for completeness and accuracy, and attest that we have reached our recommendations through a thorough review and discussion of the available documentary evidence.&rdquo;</strong><br />
          &nbsp;</li>
        </ul>

        <p>The Provost is responsible for reviewing policies and procedures and these guidelines with the members of the UFRAC and for assuring that these policies, procedures and instructions are followed.</p>

        <h2>Guidelines for Department Chairs</h2>

        <p>One of the most important elements in the review process is the recommendation of the Department Chair because the Chair has the greatest competence to review the quality of a candidate&rsquo;s performance within the academic discipline, balanced by the context of the department&rsquo;s long-term needs and aspirations. The Chair&rsquo;s report should contain the following essential elements:</p>

        <ul type="disc">
          <li>a summary of the review process followed by the department and DFRAC, including the selection of the external reviewers and the recommendations of the DFRAC;</li>

          <li>a brief description of the qualifications of the external reviewers used to evaluate the impact of the candidate&rsquo;s research/scholarly/creative products;</li>

          <li>an analysis of the candidate&rsquo;s contributions in each of the areas of teaching, research/scholarly/creative activity, and service within the context of the department&rsquo;s long-term needs;</li>

          <li>an explanation of aspects of the case that may be unfamiliar to reviewers at subsequent levels of evaluation&mdash; for example,</li>

          <li style="list-style: none; display: inline">
            <ul type="circle">
              <li>accepted standards for publishing scholarly work, including multiple author protocols,</li>

              <li>the importance of the candidate&rsquo;s contribution in any collaborative activities,</li>

              <li>the quality and impact of the publication outlets, performance venues, or exhibition galleries (as appropriate) utilized by the applicant,</li>

              <li>clarification of usual citation numbers for researchers in the applicant&rsquo;s field,</li>

              <li>the significance of the teaching activities undertaken by the candidate relative to disciplinary norms,</li>

              <li>departmental expectations for student mentoring at both undergraduate and graduate levels,</li>

              <li>the significance of any professional service contributions made by the candidate,</li>

              <li>the willingness of the candidate to participate on departmental committees, <em>etc</em>.</li>
            </ul>
          </li>

          <li>a succinct statement of the Chair&rsquo;s recommendation, with explanation of the factors leading to this recommendation.</li>
        </ul>

        <p>The Chair should strive to compose the report as objectively as possible, using factual data to support conclusions, and expressing the evaluation in terms of departmental expectations and aspirations. In doing so, the Chair understands that the recommendation is most likely to be upheld if there is a clear rationale for how the candidate&rsquo;s promotion and/or tenure will recognize outstanding contributions to the department and help the department improve its overall performance.</p>

        <h2>Guidelines for Deans</h2>

        <p>The role of the Dean&rsquo;s recommendation is to uphold high standards across the college and ensure that promotion and/or tenure decisions are made to support the long-term quality and productivity of the college. In all cases, the Dean&rsquo;s report should provide the following essential information:</p>

        <ul type="disc">
          <li>the nature of the department&rsquo;s recommendations through the DFRAC and Chair,</li>

          <li>the recommendation of the CFRAC and the numerical results of the CFRAC vote, and</li>

          <li>a succinct statement of the Dean&rsquo;s recommendation for the case.</li>
        </ul>

        <p>If the DFRAC, Chair, and UFRAC have all provided consistent analyses and recommendations with which the Dean is in agreement, then the Dean may provide a succinct statement of concurrence with the earlier recommendations.</p>

        <p>If earlier recommendations express diverse outcomes, or if the Dean disagrees with their conclusions, then a more comprehensive recommendation should be provided.&nbsp; That report should contain an explanation of the reasons supporting the Dean&rsquo;s recommendation, citing decisive arguments included in the DFRAC, Chair&rsquo;s, CFRAC, and external reviewer&rsquo;s reports.&nbsp; The Dean should take care to express conclusions within the context of the college&rsquo;s expectations and aspirations for long-term quality among its faculty.</p>
        <h2>Appeal of Promotion and Tenure Decisions</h2>
          <p>The promotion and tenure review process is a comprehensive one requiring several layers of thorough review. In general, appeals should be made only in cases where new, compelling information relevant to a promotion decision has become available since the completion of the college-level review. Such information might include, for example, a new major publication of research results, a prominent public review of the faculty applicant’s scholarly work, major external competitive funding awarded for research, or receiving a significant award for scholarly achievements.</p>
       <p> To file an appeal, see <a href="http://www.utsa.edu/hop/chapter2/2-10.html">HOP 2.10</a> section F. Appeal of Promotion and Tenure Decisions</p>
       <h2>Retention of Promotion and Tenure Documents</h2>
			 <p>
        Promotion and tenure documents are retained in accordance with the university’s official retention schedule, as follows:
       <ul>
         <li>Record series 3.1.143 Faculty Promotion and Tenure Review Records (Promotion File) – Evaluations, recommendations
					 memos, etc. are retained for 2 years following the date granting/denying tenure.</li>
				 <li>Record series 3.1.137 Employee Recognition Records – A copy of the letter granting/denying tenure and any final
					 determination letter regarding an appeal is retained for 5 years after employment ends.</strong></li></ul>
        </p>
				<p>
					The university’s full records retention schedule can be accessed through the following link:
					<a href="https://www.utsa.edu/openrecords/retention.html">https://www.utsa.edu/openrecords/retention.html</a>
				</p>
                <!-- InstanceEndEditable -->
        </div><!-- end col-main-->
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
