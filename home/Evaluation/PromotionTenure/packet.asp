<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Packet | Evaluation | Promotion/Tenure | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style>

#noborder table {border:none;}

#noborder table th {border:none;}

#noborder table tr {border:none;}

#noborder table td {border:none;}

</style>


<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
          <!-- #include virtual="/home/includes/nav_PTGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h2>Promotion and Tenure</h2>
        <h1> Preparation of the Application Packet</h1>

        <p>The application packet contains the materials that form the basis for the review at all levels of evaluation. It is important that faculty members under consideration for promotion and/or tenure make every effort to ensure that the material contained in the packet is complete, accurate, and professionally presented.</p>

        <p>The contents of an application packet should include the following elements:</p>

        <ol type="1">
          <li><a href="docs/PT-Coversheet-and-Checklist.pdf">cover sheet and checklist</a></li>

          <li>a statement of self-evaluation</li>

          <li>a professional vitae</li>

          <li>evaluations and recommendations by the various levels of review</li>

          <li>documentation of teaching effectiveness</li>
          <ul type="circle"><li>summary of course evaluations (complete template)</li>
<li>peer observer's report(s)</li>
<li>facullty member's report(s)</li>
          </ul>


          <li>documentation of research/scholarly/creative activities</li>

          <li>summary of service activities and responsibilities</li>

          <li>optional supplementary materials</li>
          <li>late materials.</li>
        </ol>

        <p>The professional vitae should serve as a simple listing of professional activities, while each of the other components provides more in-depth information about those activities. The suggested contents of each of these elements should include, but are not limited, to those suggested below. The candidate is responsible for preparing items #1 –&nbsp;3 and #5 –&nbsp;9 above (except that the peer observer’s report(s) and the faculty member’s report(s) is uploaded by the chair); the faculty review advisory committees, chair, dean and provost are responsible for appending materials contained in #4. </p>
<p>Items #1&ndash;4 constitute the review materials utilized by each of the FRACs, the chair, the dean, the provost, and the president in reviewing the application. Items #5&ndash;9 are made available to reviewers at the department and college levels, but are not transmitted to the university level unless the UFRAC or the provost should specially request them. The self-evaluation and vitae (#2&ndash;3) plus the documentation of research/scholarly creative activities (#6) comprise the information sent to external reviews for their evaluation. This information is summarized in the table below.</p>

        <table cellspacing="0" cellpadding="0" border="1">
          <tbody>
            <!--<tr>
              <td valign="top" width="96">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="383" colspan="8">
                <p align="center"><em>Item Number</em></p>
              </td>
            </tr>-->

            <tr>
              <td valign="top" width="96">
                <p><em><strong>Review Activity</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #1</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #2</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #3</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #4</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #5</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #6</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #7</strong></em></p>
              </td>

              <td valign="top" width="48">
                <p align="center"><em><strong>Item #8</strong></em></p>
              </td>
              <td width="48" align="center" valign="top"><strong><em>Item #9</em></strong></td>
            </tr>

            <tr>
              <td valign="top" width="96">
                <p><em>External reviews</em></p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>
              <td width="48" align="center" valign="top">&nbsp;</td>
            </tr>

            <tr>
              <td valign="top" width="96">
                <p><em>Dept. review</em></p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>
              <td width="48" align="center" valign="top">X</td>
            </tr>

            <tr>
              <td valign="top" width="96">
                <p><em>College review</em></p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>
              <td width="48" align="center" valign="top">X</td>
            </tr>

            <tr>
              <td valign="top" width="96">
                <p><em>Univ. review</em></p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">X</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>

              <td valign="top" width="48">
                <p align="center">&nbsp;</p>
              </td>
              <td width="48" align="center" valign="top">&nbsp;</td>
            </tr>
          </tbody>
        </table>

        <p>One hard copy of the required materials (#1&nbsp;–&nbsp;3) should be made available in a secured location in the department office and an electronic copy of #1 -3 and #5-9 should be posted in the official Faculty Review folder in SharePoint for review by FRAC members. Item #4 should be added to the hard copy folder and uploaded to SharePoint when they are completed.  Each of these items is described more fully in the following sections.</p>
<h2><em>1. Cover Sheet</em></h2>

        <p>The <a href="docs/PT-Coversheet-and-Checklist.pdf">cover sheet form</a> is available on this website as a PDF, and should be filled in by the faculty candidate and provided with the other materials. The second and later pages of the cover sheet are a checklist of the essential contents of the application package, as well as a checklist of possible optional supplementary materials that may be submitted by the applicant.</p>
        <p>The checklist should be signed by the department chair and dean once they have reviewed the files. All should be actual signatures, not electronic or digital signatures.</p>
<h2><em>2. Statement of Self-Evaluation</em></h2>

        <p>The statement of self-evaluation should be organized in three sections, outlining the applicant's activities, experiences, and plans in the areas of teaching, research/scholarly/creative activities, and service, respectively. &nbsp;</p>

        <ul type="disc">
          <li>For the teaching section, the applicant may wish to include a teaching statement outlining her or his philosophy/approach to teaching, and describe any innovative approaches used in delivering instruction.&nbsp;</li>

          <li>In the research/scholarly/creative activity section, the applicant can provide a context for her or his scholarly work, indicating the relationship between different projects and plans for future scholarship and how those plans build on past accomplishments (if applicable).</li>

          <li>The service section should provide an overview of service activities and explain the applicant&rsquo;s participation in key service roles, including her or his philosophy of service and how it complements teaching and scholarly activities.</li>
        </ul>

        <p>The statement of self-evaluation should be no more than 8&ndash;10 pages long.</p>

        <h2><em>3. Professional Vitae</em></h2>



      
        <div id="noborder">
        <table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td><div align="right"><em>Name and Contact Information</em></div></td>
            <td>This should include UTSA address, phone number, and email address, as well as current academic rank (for example, Assistant Professor, Associate Professor).</td>
          </tr>
          <tr>
            <td><div align="right"><em>Educational Background</em></div></td>
            <td>List all institutions from which a degree was earned, including the degree received and the major field of study. Awards received while a student at an educational institution may also be listed here.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Professional Employment History</em></div></td>
            <td>List all positions held in sequential order, with applicable dates, since earning the baccalaureate degree, including the present position at UTSA. </td>
          </tr>
          <tr>
            <td><div align="right"><em>Awards and Honors</em></div></td>
            <td>List any awards, honors, prizes, competitions, or other recognition received related to professional activities.</td>
          </tr>
          <tr>
            <td><p align="right"><em>Research/Scholarly/<br />
            Creative</em> <em>Activities Summary</em></p></td>
            <td><p>Summarize all products of research/scholarly/creative   activities, including publications, exhibitions, perform­ances, architectural projects, reviews, or other documenta­tion of scholarly contributions.  List separately the different types of publications (e.g. journal articles, books, reviews, <em>etc</em>.), scholarly products, or creative activity outcomes, providing respective listings of invited contributions, refereed contributions, and non-refereed contributions. All contributions should include the date and title of publication/exhibition/performance, the venue, and where applicable, the inclusive page numbers or size of the scholarly contribution.</p></td>
          </tr>
        <tr>
          <td><div align="right"><em>Scholarly Presentations</em></div></td>
          <td>List all external oral or poster presentations at conferences, meetings, or other institutions/universities related to scholarly work, and provide the dates and locations of presentations. Use separate listings for invited presentations, refereed contributions, and non-refereed contributions.
            </td>
           
         </tr>
        <tr>
          <td><div align="right"><em>Granting Activities</em></div></td>
         <td> Provide a list of grants received, whether for research, instructional, or public service activities (indicate one of these for each grant), giving the name of the granting agency, the project dates, the project title, and the total amount awarded for each.</td></tr>
        <tr><td><div align="right"><em>Intellectual Property</em></div></td><td>Where applicable, provide a summary of any intellectual property generated and indicate any patent applications, copyright privileges, licensing, or other commercialization that has resulted. The summary should include dates, titles, and other suitable identifying information.</td></tr>
        <tr><td><div align="right"><em>Teaching Activities</em></div></td><td>List all formal courses taught, indicating the level of the course (undergraduate or graduate) and its title. Provide a list of students mentored in research/scholarly/creative activities and any theses or dissertations directed. Summarize any service on graduate committees and for student advising.</td></tr>
        <tr><td><div align="right"><em>Service Activities</em></div></td><td>Provide separate listings of all committee assignments, assigned administrative activities (for example, department chairmanship, center directorship, etc.),  and professional service activities (including leadership in disciplinary organizations, service as a journal editor, manuscript or grant proposal reviewer, meetings or symposia organized, etc.). Each activity should include the dates of participation, the organizational level of the activity (for example, department, college, etc.), and any leadership roles played.</td></tr>
        
        </table>
</div>
         <p>&nbsp;</p>

<h2><em>4. Evaluation and Recommendation Materials</em></h2>

        <p>As the application goes through the review process, each level of review should append its analysis and recommendation to the packet for consideration by the next level of review. Guidelines for these various levels of review are provided in the &ldquo;Review Process&rdquo; section of these guidelines. The materials should be arranged in the following order, with the responsibility and timing for appending each set of materials indicated below:</p>

        <table cellspacing="0" cellpadding="0" border="1">
          <tbody>
            <tr>
              <td valign="top" width="160">
                <p><em>Item</em></p>
              </td>

              <td valign="top" width="160">
                <p><em>Responsible Individual</em></p>
              </td>

              <td valign="top" width="160">
                <p><em>When</em></p>
              </td>
            </tr>

            <tr>
              <td valign="top" width="160">
                <p>External review letters</p>
                <p>Peer Observer’s Report(s)</p>
Faculty Member’s Report(s) </td>

              <td valign="top" width="160">
                <p>Department Chair</p>
              </td>

              <td valign="top" width="160">
                <p>Prior to DFRAC review</p>
              </td>
            </tr>

            <tr>
              <td valign="top" width="160">
                <p>DFRAC analysis</p>

                <p>Chair&rsquo;s recommendation</p>
              </td>

              <td valign="top" width="160">
                <p>Department Chair</p>
              </td>

              <td valign="top" width="160">
                <p>Upon completion </p>
                <p>Upon completion</p>
              </td>
            </tr>

            <tr>
              <td valign="top" width="160">
                <p>CFRAC analysis</p>

                <p>Dean&rsquo;s recommendation</p>
              </td>

              <td valign="top" width="160">
                <p>Dean</p>
              </td>

              <td valign="top" width="160">
                <p>Upon completion</p>
                <p>Upon completion</p>
              </td>
            </tr>

            <tr>
              <td valign="top" width="160">
                <p>UFRAC analysis*</p>
              </td>

              <td valign="top" width="160">
                <p>UFRAC Chair</p>
              </td>

              <td valign="top" width="160">
                <p>Upon completion of UFRAC review</p>
              </td>
            </tr>
          </tbody>
        </table>

        <p>*Only required if UFRAC conclusion differs from earlier recommendations, or there is disparity among the earlier reviews.</p>

        <h2><em>5. Documentation of Teaching Effectiveness</em></h2>
 <div id="noborder">
        <table width="650" border="0">
          <tr>
            <td width="138"><div align="right"><em>Listing of Courses with Teaching Evaluation Summaries</em></div></td>
            <td width="502">Provide a table of courses taught during the evaluation period (the probationary period for tenure-track faculty, the period since the last promotion, or last post-tenure review, for tenured faculty), using the template provided below (<a href="Summary-of-Student-Course-Evaluation-Template.docx">this template</a> may be downloaded from the website). Do not include copies of student evaluation surveys or comments among these materials. </td>
          </tr>
        </table></div>
        <table border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td width="90"><p align="center"><em>Semester</em></p></td>
            <td width="90"><p align="center"><em>Course No.</em></p></td>
            <td width="90"><p align="center"><em>Course Type</em></p></td>
            <td width="82"><p align="center"><em>New prep?</em></p></td>
            <td width="71"><p align="center"><em>Course Enrollmt.</em></p></td>
            <td width="82"><p align="center"><em>No. of Responses</em></p></td>
            <td width="64"><p align="center"><em>Course rating</em></p></td>
            <td width="78"><p align="center"><em>Instructor rating</em></p></td>
          </tr>
          <tr>
            <td width="90"><p align="center">SP2011</p></td>
            <td width="90"><p align="center">ABC   nnn3</p></td>
            <td width="90"><p align="center">LD,   UD, or GR</p></td>
            <td width="82"><p align="center">NEW</p></td>
            <td width="71"><p align="center">xxx</p></td>
            <td width="82"><p align="center">yyy</p></td>
            <td width="64"><p align="center">X.X</p></td>
            <td width="78"><p align="center">Y.Y</p></td>
          </tr>
          <!--<tr>
            <td width="428" valign="top"><p align="center"><em>Note:</em> LD = lower division,<br />
            UD = upper division,<br />GR = graduate-level</p></td>
           </tr>-->
        </table>
<p><em>Note:</em> LD = lower division, UD = upper division, GR = graduate-level</p>

<div id="noborder">
<table width="650" border="0" cellspacing="2" cellpadding="2">
  <tr>
    <td width="136"><div align="right"><em>Peer Observer’s Report</em></div></td>
    <td width="500">Please refer to the <a href="docs/Peer-Observation-Guidelines.pdf">Peer Observation Guidelines</a> and the HOP, Chapter 2.20 “Peer Observation of Teaching” for more information.  Provide the report to the department chair according to the Peer Observation Guidelines, but no later than Sept. 1st, or the first workday thereafter, so that the department chair may upload it to the official Faculty Review folder in SharePoint.</td>
  </tr>
  <tr>
    <td><div align="right"><em>Faculty Member’s Report </em></div></td>
    <td>Please refer to the <a href="docs/Peer-Observation-Guidelines.pdf">Peer Observation Guidelines</a> and the HOP policy, Chapter 2.20 “Peer Observation of Teaching” for more information.  Provide the report to the department chair according to the Peer Observation Guidelines, but no later than Sept. 1st, or the first workday thereafter, so that the department chair may upload it to the official Faculty Review folder in SharePoint.</td>
  </tr>
  <tr>
    <td><div align="right"><em>Teaching Portfolios</em></div></td>
    <td>For each course taught, provide a portfolio containing the course syllabus, exams, handouts, problem sets and other written assignments, and other course materials developed by the faculty candidate.</td>
  </tr>
  <tr>
    <td><div align="right"><em>Instructional Development </em></div></td>
    <td>List any workshops, seminars, or other related meetings attended (or organized) to increase pedagogical effectiveness. This information should include the dates, formats, locations, and names of organizers.</td>
  </tr>
  <tr>
    <td><div align="right"><em>Instructional Grants</em></div></td>
    <td>List all grants related to instructional activities. This may be taken directly from relevant grants listed on the professional vitae and should contain the information indicated above for &ldquo;Grant Activities.&rdquo; Provide electronic copies of all funded grants and, optionally, referee comments for those grant proposals.</td>
  </tr>
  <tr>
    <td><div align="right"><em>Teaching Awards</em></div></td>
    <td>List any awards received for excellence in university-level teaching. This may include both awards received at UTSA and at other institutions of higher education, and should indicate the date, award name, awarding unit (for example, college, university, <em>etc</em>.), and institution.</td>
  </tr>
  <tr>
    <td><div align="right"><em>Students Mentored</em></div></td>
    <td>Provide a list of all students mentored in scholarly activities, indicating those who have completed degree programs under your mentorship, those who are currently enrolled, and employment outcomes for mentored students who have graduated. For undergraduate course advisement, a summary of the number of students served is sufficient.</td>
  </tr>
</table></div>
<p>&nbsp;</p>
<h2><em>6. Documentation of Research/Scholarly/Creative Activities</em></h2>
<div id="noborder">
        <table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="136"><div align="right"><em>Scholarly Products </em></div></td>
            <td width="500">Provide an electronic copy of all research/scholarly/creative works produced during the evaluation period. This includes full copies of any journal articles, book chapters, papers in conference proceedings, architectural projects, digital images of artwork, recordings of musical performances or compositions, and other short-format works. These may include manuscripts under review or in preparation. Applicants should provide 3-6 hard copies of any full books authored or edited by the faculty member for use in external review of the application. Portions of books may also be scanned to create a digital image for use in the internal review— scanning services are available through the University Library. In cases where the amount of scholarly products is extensive, a representa­tive sample of scholarly products may be submitted, after consultation with the Department Chair. Note that citation indices of published work may be included among the optional supplementary materials.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Reviews</em></div></td>
            <td>Where appropriate, provide copies of any reviews of scholarly and creative activity, including reviews of books published, exhibitions, performances, compositions, architectural projects, and other creative endeavors.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Grant Proposals</em></div></td>
            <td>An electronic copy of all funded grant proposals, as well as any proposals under review, or in preparation, should be provided with an indication of the present status of the proposal. Referee comments from funded proposals may be submitted along with the proposals themselves. If the amount of funded proposals is extensive, a representative sample of proposals may be submitted, after consultation with the Department Chair.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Intellectual Property</em></div></td>
            <td>Provide documentation of any intellectual property produced, including patents, copyrights, licensing agreements or other commercialization activities. Faculty are not required to divulge sensitive information concerning the intellectual property, but may document its development and potential commercialization through letters and other communications.</td>
          </tr>
        </table></div>
        <p>&nbsp;</p>
<h2><em>7. Summary of Service Activities and Responsibilities</em></h2>
<div id="noborder">
        <table width="650" border="0" cellspacing="2" cellpadding="2">
          <tr>
            <td width="138"><div align="right"><em>Committee Assignments</em></div></td>
            <td width="498">Separately list committee assignments at the department, college, and university levels, indicating dates of service and the name of the committee chair. Applicants should also indicate the extent of their contributions to the work of each committee listed.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Professional Service Activities</em></div></td>
            <td>List any activities, other than leadership positions, in the service of professional and disciplinary organizations. These may include committee assignments, manuscript and proposal review, journal editorship, organization of meetings, and other assistive activities. In all cases, provide dates of service, organizations served, and time committed.</td>
          </tr>
          <tr>
            <td><div align="right"><em>Leadership Positions</em></div></td>
            <td>Provide a summary of any leadership positions held at the university or within a professional/disciplinary organization or society. List the dates for each applicable position, the responsibilities of the position, and the time commitment involved in executing the responsibilities of the position. Also, indicate any special accomplishments achieved while in the leadership position.</td>
          </tr>
        </table></div>
        <p>&nbsp;</p>
<h2><em>8. Optional Supplementary Materials</em></h2>

        <p>Applicants for promotion and tenure may submit optional supplementary materials to highlight or document achievement in the areas of teaching, research/scholarly/creative activities, and service activities. A checklist of possible items that might be included among the supplementary materials is included with the <a href="docs/PT-Coversheet-and-Checklist.pdf">Cover Sheet form</a> available from the Provost’s website. Applicants are asked to provide materials in a non-printable (optional) electronic/digital format uploaded to the official Faculty Review folder in SharePoint, or other secure university-supported web site as may be directed. In addition, each department may determine whether a hard copy of selected supplementary materials should be made available in the department office along with the required materials.</p>
<p>Upon completion of the external review of a case, all professional work products, including copies of publications, reviews, creative works, grant proposals, reviewer comments, and other primary works, will be included among the supplementary materials. Candidates for promotion and/or tenure may also submit other supplementary materials in support of the application, including full sets of student teaching surveys with comments, works in progress, a statement of future research goals and directions, and other items as allowed by the department and college.</p>

        <p>Hard copies of the professional work products and supplementary materials will not automatically be made available for college-level review (the non-printable electronic copies will still be accessible online). The CFRAC and dean may optionally request a hard copy of any of those materials, if needed. Once the college-level review is completed, the supplementary materials will remain posted online during subsequent review of the application. </p>
        <p> In extraordinary cases, where the supplementary materials might clarify a point in dispute from earlier reviews, the UFRAC may also request access to hard copies of these materials for a given case. Once the president's final decision concerning a candidate is made, the secure web site containing supplementary materials will ordinarily be emptied and any hard copies of work products returned to the faculty applicant. Digital supplementary materials may be retained by the university in tenure denial cases until all appeals are completed.</p>

        <p>Please note that collecting supplementary materials in digital format is the responsibility of the faculty applicant. Assistance in creating digital archives is available through the Office of Information Technology and the University Library. By storing these materials online, the intent is that they are made accessible for the convenience of the DFRAC, Department Chair, CFRAC, and Dean to assist in their separate deliberations.</p>

        <p><strong><em>Special Note to Faculty</em></strong></p>

        <p>These guidelines are intended to help you prepare the most compelling, well-documented case possible for promotion and/or tenure. With the exception of primary work products (items #5&ndash;7) which become a part of the supplementary materials, your application packet will be evaluated at all levels of the review. As you prepare your packet, please consider how readily a reader may access and absorb the material it contains. Repetition and verbosity will only serve to fatigue reviewers without adding substance to your packet&mdash;be as concise and succinct as possible in each section of the packet.</p>
        <!-- InstanceEndEditable -->
      </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
