<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->

<head>
	<link rel="shortcut icon" href="/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
	<meta name="copyright" content="The University of Texas at San Antonio" />
	<!-- InstanceBeginEditable name="meta" -->
	<meta name="Description" content="The University of Texas at San Antonio" />
	<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
	<!-- InstanceEndEditable -->
	<!-- InstanceBeginEditable name="doctitle" -->
	<title>External | Evaluation | Promotion/Tenure | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
	<!-- InstanceEndEditable -->
	<link rel="stylesheet" type="text/css" href="/css/master.css" />
	<link rel="stylesheet" type="text/css" href="/css/template.css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
	<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
	<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
	<link rel="stylesheet" type="text/css" href="/css/print.css" media="print" />
	<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
	<script type="text/javascript" src="/js/custom.js"></script>
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
	<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
	<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
	<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="branding-blue">
		<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
		<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
	</div><!-- end branding -->

	<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="content-white">
		<div id="content" class="clearfix">

			<!-- InstanceBeginEditable name="content-title" -->
			<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
			<!-- InstanceEndEditable -->

			<div id="col-navigation">
				<div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
					<!-- InstanceBeginEditable name="nav" -->
					<!-- #include virtual="/home/includes/nav_PTGuidelines.asp" -->
					<!-- InstanceEndEditable -->
				</div>
			</div>

			<div id="col-main"><a name="acces-content" class="screen-reader"></a>
				<!-- InstanceBeginEditable name="content" -->
				<h2>Promotion and Tenure</h2>
				<h1>Solicitation of External Reviews</h1>

				<p><a href="docs/18-19_Appendix_D-TemplateLtrExternal-required-04-24-19.docx">External Review Letter Template</a><br />
					<a href="docs/UpdatedWorksheetEvaluators-PT-11-28-18.pdf">Worksheet for Outside Evaluators</a></p>
				<p>The purpose of using external reviews as a part of the promotion and tenure process is to advise the university as to the broader impact and value of a faculty member’s research/scholarly/ creative productivity to the discipline. At UTSA,
					external reviews are used to confirm the significance of results arising from the scholarly efforts of a faculty member, but are not intended to be conclusive elements of a promotion and tenure review.</p>
				<strong><em>Guidelines for Selecting External Reviewers</em></strong></p>
				<ul>
					<li>There shall be no less than three (3), and typically no more than six (6), letters from independent external reviewers that evaluate the faculty candidate’s academic record in the dossier in order for the candidate to be considered for
						promotion or tenure.  </li>
					<li>
						External reviewers should themselves be experts in the faculty candidate’s
						discipline, sub-field or area, and should not be a past mentor, dissertation
						advisor or a frequent or current (within last 5 years) collaborator, nor
						have a personal relationship with the candidate. Collaborators also include
						individuals who have worked closely with a candidate, where questions may
						arise about whether an independent assessment of the candidate’s achievements
						can be offered.  Each reviewer should be asked to outline in the report any
						past professional and/or personal association that (s)he may have with the
						faculty candidate. Rather than submitting external review letters, collaborators
						can be invited to submit collaborator letters of support that outline the
						significance of the independent contributions of candidates.

					</li>
					<li>Reviewers should ideally hold full professor rank or equivalent and be currently active, productive researchers, scholars or artists. The only exceptions to this should be reviewers who are acknowledged emerging leaders in the field though
						not yet at the rank of full professor. Reviewers that are not affiliated with an academic institution (for example, a researcher at a national laboratory) should have rank and experience commensurate with that of a full professor.</li>
					<li>Reviewers should be affiliated with a department or institution that is an aspirant for the department of the faculty candidate, for example, from Carnegie R1 or AAU institutions (including medical school colleagues as warranted). One of
						the chief purposes of promotion and tenure is to ensure that the university is making progress towards its strategic goals and aspirations, which cannot occur unless advice is continually solicited from those who represent aspirant
						institutions.</li>
					<li>Department Chairs shall document all reviewer nominations and the selection process on the required <a href="docs/UpdatedWorksheetEvaluators-PT-11-28-18.pdf">Worksheet for Outside Evaluators</a>, which will be approved by the Dean and
						included in the candidate’s dossier.</li>
					<li>Responsibility for choosing external reviewers rests with the Department Chair, but should involve consultation with others, including the faculty candidate, senior faculty members and the DFRAC. </li>
					<li>The list should include sufficient in number of potential reviewers (that is typically six to nine) to yield at least the minimum number (3) of <u>independent</u> evaluation letters required to advance the dossier for consideration.
						Department Chairs are responsible for properly managing this process and ensuring at least the minimum number is achieved in order to advance the dossier, as required, and shall seek well in advance assistance from the Dean to remedy any
						challenges.</li>
					<li>Each department should develop specific written guidelines for soliciting suggestions for reviewers and work to ensure the integrity of the process. That process shall include:</li>
					<ul>
						<li>consideration of names suggested by the faculty candidate;</li>
						<li>consideration of names proposed by senior faculty and DFRAC members in the same general area as the faculty candidate; and,</li>
						<li>identification of those nominated reviewers identified by the faculty candidate who are unsuitable due to a real or perceived conflict of interest;</li>
						<li>construction of the total list of reviewers that includes both names suggested by the faculty candidate, as well as those that are not suggested by the candidate (no more than half are expected to be from the candidate).</li>
					</ul>
				</ul>
				<p><strong><em>Process of Soliciting External Reviews</em></strong></p>
				<ul>
					<li>During the spring prior to the promotion and tenure review, the Department Chair shall solicit suggestions of potential reviewers from the faculty candidate, DFRAC and senior faculty in the same general sub-field as the candidate, and
						record all reviewers nominated on the Required Worksheet.</li>
					<li>The Department Chair reviews the nominations codified on the Worksheet for obvious real or perceived conflicts of interest, and if noted, checks the “no” box in the “Independent Column”.  If the Department Chair elects to proceed with
						soliciting a review, it is considered a letter of support and not an independent evaluation.</li>
					<li> The Department Chair contacts prospective reviewers to ascertain their availability to provide a review, and notes the response on the Worksheet.</li>
					<li>If the list of potential reviewers solicited by the Department Chair is exhausted the approved list without yielding at least three independent reviewers, the Department Chair should consult with the Dean for assistance with remedy and
						request additional names from both the faculty candidate, DFRAC and the senior faculty in the same general sub-field as the candidate.</li>
					<li>Once the list of reviewers is finalized, the Department Chair shall forward the application packet, including relevant work products and summary of the candidate’s workload assignment, to the external reviewers for their evaluation.
						Materials should be sent to reviewers early enough to allow the reviewer adequate time to conduct the review as well as for the department to review the case.</li>
					<li>The Department Chair shall solicit the review using <a href="docs/18-19_Appendix_D-TemplateLtrExternal-required-04-24-19.docx">the required template</a>. Reviewers are focused on evaluation of the candidate’s record of accomplishments in
						research, scholarship or creative arts, and may optionally provide information about the impact of any professional (disciplinary) service rendered by the faculty candidate and any of the candidate’s instruction-related activities if the
						reviewer has specific, relevant observations by which to evaluate those activities (e.g., use of nationally disseminated textbook authored by the candidate or service with the candidate on a disciplinary society or agency review board).</li>
					<li>The Department Chair shall obtain a short vitae or biography for each reviewer for inclusion with the review letter. Such vitae should be no more than two pages in length.</li>
					<li>Upon receipt of the evaluation and vitae from the reviewer, the Department Chair reviews the letter to evaluate any professional or personal affiliations or relationships by the reviewer with the candidate.  The Chair makes the final
						determination of Independence, and so notes on the Worksheet.  Evaluations not determined to be independent by the Department Chair will be considered only as letters of support and not included in the evaluative process.   </li>
					<li>After receipt of letters, the Department Chair transmits the Worksheet codifying the external reviewer process to the Dean for review and approval, prior to inclusion in the candidate’s dossier.</li>
				</ul>

				&nbsp;</p>
				<!-- InstanceEndEditable -->
			</div><!-- end col-main-->
		</div>
	</div>
	<!--end contentWhite-->


	<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<!-- InstanceBeginEditable name="userFooter" -->
	<!--#include virtual="/inc/footer/footer-about.html" -->
	<!-- InstanceEndEditable -->

	<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
	<div id="footer-blue">
		<div id="footer">
			<!--#include virtual="/inc/master/footerWrap.html" -->
		</div>
	</div>
	<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd -->

</html>
