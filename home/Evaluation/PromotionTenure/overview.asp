<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Overview | Evaluation | Promotion/Tenure | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            <!-- #include virtual="/home/includes/nav_PTGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        <h2>Promotion and Tenure</h2>
        <h1>
          Overview of Process</h1>

        <p>The purpose of the promotion and tenure process is to perform an objective evaluation of each case at several levels of review. Therefore, each case goes through at least seven levels of independent review before a final recommendation is achieved: DFRAC, chair, CFRAC, dean, UFRAC, provost, and president. The president&rsquo;s recommendation in all cases is final, but is subject to approval by the UT System Board of Regents.</p>

        <p>The promotion and tenure review process is summarized in the table below, which outlines the rough timeline and actions of the procedure.</p>

        <table cellspacing="0" cellpadding="0" border="1">
          <tbody>
            <tr>
              <th valign="top" width="68">
                <p><em>When</em></p>
              </th>

              <th valign="top" width="113">
                <p><em>Who</em></p>
              </th>

              <th valign="top" width="425">
                <p><em>Responsibility</em></p>
              </th>
            </tr>

             <tr>
              <td valign="top" width="68">
                <p><strong>January</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost Office</em></p>

               
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Forward forms for early promotion and tenure and promotion to full professor to the colleges and departments</li>
                </ul>

              </td>
            </tr>
              <tr>
              <td valign="top" width="68">
                <p><strong>Feb. 1</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Dean's</em><em> Office</em></p></td>

              <td valign="top" width="425">
                <ul>
                  <li>Deadline to forward requests for early promotion and tenure review and requests for promotion to full professor to the Office of the Provost and Vice President for Academic Affairs.</li>
                </ul>
              </td>
            </tr><tr>
              <td valign="top" width="68">
                <p><strong>February–<br />
                March</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost Office</em></p>

                
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Notify deans and department chairs which faculty members are due to undergo a mandatory review in the coming fall semester</li>
                </ul>
              </td>
            </tr>
              <tr>
              <td valign="top" width="68">
                <p><strong>March 1</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost Office</em></p>
</td>

              <td valign="top" width="425">
                <ul>
                  <li>Notify tenure-track faculty members that their mandatory review will take place in the coming fall semester</li>
                </ul>
              </td>
            </tr>
              
              
            <tr>
              <td valign="top" width="68">
                <p><strong>March-April</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost Office</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Set up electronic boxes for applicants and forward links to applicants</li>
                </ul>
              </td>
            </tr>
             <tr>
              <td valign="top" width="68">
                <p><strong>Spring</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Applicant</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Begin assembling materials for application packet;</li>
                  <li>Submit names of suggested external reviewers to chair</li>
                </ul>

               </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Department Chair</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Solicit suggestions of external reviewers from applicant and faculty;</li>
                  <li>Begin contacting potential reviewers</li>
                </ul>

              </td>
            </tr>
 <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

            <td valign="top" width="113">
                <p><em>Dean</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>If the faculty member being reviewed for promotion to full professor is a department chair, the dean shall appoint another faculty member at or above the rank of the department chair to serve in the role of department chair for this process.  Provide the name to the Provost’s Office.</li>
                </ul>
            </td>
            </tr>
            <tr>
              <td valign="top" width="68">
                <p><strong>Early Summer</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Applicant</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Finish assembling application packet and submit to chair</li>
                </ul>

              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Department Chair</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Solicit external reviews; letters are due in August;</li>
                  <li> Upload external review letters, a short bio for each reviewer and a copy of the letter sent to the reviewer requesting his/her assistance with the P&amp;T cases no later than September 1st.*</li>
                  <li>Upload peer observer’s report(s) and faculty member’s report (s) (provided by each candidate).</li>
                </ul>
              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>September</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Dean's Office</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Provide DFRAC names to the Provost’s Office no later than Sept. 1 or the first workday thereafter.</li>
                  <li>Provide CFRAC names to the Provost’s Office no later than Sept. 10 or the first workday thereafter.</li>
                </ul>
              </td>
            </tr>
            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Applicant</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Upload promotion and tenure documents to the official Faculty Review folder in SharePoint no later than Sept. 1st or the first workday thereafter.  Also submit one manila folder to department chair with hard copies of your cover sheet, vita, and statement of evaluation.</li>
                </ul>

              </td>
            </tr>
            
            <tr>
              <td valign="top" width="68">
                <p><strong>September-October</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>DFRAC</em></p>

              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review application packet and external review letters;</li>
                  <li>Deliberate in closed meeting and vote on case;</li>
                  <li>Prepare a written summary of evaluation analysis</li>
                </ul>              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Department Chair</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review all application materials, including DFRAC recommendation;</li>
                  <li>Prepare a written recommendation for forwarding to college;</li>
                  <li>Provide a written notification to applicant of the Department’s recommendations;</li>
                  <li>Upload DFRAC memo and department chair memo to the official Faculty Review folder in SharePoint;</li>
                  <li> Include original signed copies of external review letters,  DFRAC memo, and department chair memo in the faculty folder and forward to the Dean’s Office.</li>
                </ul>              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Applicant</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Optional opportunity to update file by adding new materials before forwarding to College</li>
                </ul>

              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
               <p><strong>September-November</strong></p>
              </td>

              <td valign="top" width="113">
               <p><em>CFRAC</em></p>

                <p>&nbsp;</p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review application packet, external review letters, peer observer’s report(s), faculty member’s report(s), and department recommendations; </li>
                  <li> Deliberate in closed meeting and vote on case;</li>
                  <li> Prepare a written summary of evaluation analysis.</li>
                </ul>
              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>College Dean</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review application packet, external review letters, peer observer report(s) faculty member report(s), department recommendations, and CFRAC recommendation;</li>
                  <li> Prepare a written recommendation for forwarding to provost;<br />
                    Provide a written notification to applicant of the college’s recommendations;</li>
                  <li> Upload signed checklist to the official Faculty Review folder in SharePoint;</li>
                  <li> Upload CFRAC memo and dean’s memo to the official Faculty Review folder in SharePoint;</li>
                  <li>                  Include original signed copy of checklist, CFRAC memo and dean’s memo to faculty folder and forward to the Provost’s Office.</li>
              </ul></td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Applicant</em></p>

                <p>&nbsp;</p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Optional opportunity to update file by adding new materials before forwarding to the Provost Office</li>
                </ul>

              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>November</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>College Dean</em></p>
              </td>

              <td valign="top" width="425">
                
                <ul>
                  <li><strong>Application materials due in Provost Office by  November 15th, or the first work day thereafter</strong></li>
                </ul>
              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>November-December</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>UFRAC</em></p>

                <p>&nbsp;</p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review application packet, external review letters, department and college recommendations; </li>
                  <li> Deliberate in closed meetings and vote on cases;</li>
                  <li> Prepare a written recommendation for forwarding the Provost.  Include a summary of evaluation analysis of cases for which there has been division of opinion.</li>
                </ul>
              </td>
            </tr>

<tr>
              <td valign="top" width="68">
                <p>&nbsp;</p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost Office</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Upload UFRAC Memo to the official Faculty Review folder in SharePoint; Include original signed copy of the memo to the respective faculty folders</li>
                </ul>
            </td>
            </tr>
            
            <tr>
              <td valign="top" width="68">
                <p><strong>December–<br />
                January</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review all application materials, including UFRAC recommendation;</li>
                  <li>Consult with chairs and deans, as needed clarification of application materials;</li>
                  <li>Prepare recommendations on all cases for the president</li>
                </ul>

              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>January</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>President</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Review all application materials, including recommendations at each level;</li>
                  <li>Render final decisions concerning promotion and tenure recommendations</li>
                </ul>
              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>January</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>Provost</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Prepare written notification to all applicants concerning outcome of P/T review</li>
                </ul>

              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>May</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>President</em></p>
              </td>

              <td valign="top" width="425">
                <ul>
                  <li>Forward positive recommendations to UT System by May 1.</li>
                </ul>

              </td>
            </tr>

            <tr>
              <td valign="top" width="68">
                <p><strong>August</strong></p>
              </td>

              <td valign="top" width="113">
                <p><em>UT Board of Regents</em></p>
              </td>

              <td valign="top" width="425">
                 <ul>
                   <li>Approve promotion and tenure recommendations from all campuses in UT System</li>
                 </ul>
              </td>
            </tr>
          </tbody>
        </table>
        <p>*External Review letters should be printed on the respective reviewer’s university letterhead with a clear, legible signature. An electronic version is acceptable, provided that it is on university letterhead and contains a clear, legible signature. All external review letters (including electronic versions) should be uploaded to SharePoint and a hard copy should be placed in the faculty member’s review folder. Email letters are not acceptable.</p>
<!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
