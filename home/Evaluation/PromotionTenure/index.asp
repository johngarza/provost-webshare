<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Promotion/Tenure | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<style>

#noborder table {border:none;}

#noborder table th {border:none;}

#noborder table tr {border:none;}

#noborder table td {border:none;}

</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle">Office of the Provost and Vice President for Academic Affairs</div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
          <!-- #include virtual="/home/includes/nav_PTGuidelines.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
         <h1>Promotion and Tenure Application Guidelines </h1>

        <p>This set of guidelines provides information for faculty applicants for promotion and tenure as prescribed by the&nbsp;<em>Handbook of Operating Procedures (HOP), chapter 2.10 “Faculty Reappointment, Promotion, and Tenure</em>,” and for faculty review advisory committees (FRACs), department chairs, and deans involved in the review process. These guidelines are reviewed annually and updated as needed by the Provost and Academic Affairs staff.</p>
        <p> The process of promotion and tenure is one of the most important activities undertaken by the university each year as it is one means by which the university upholds high standards and expectations for its faculty. It is the incumbent responsibility of all who are involved in the review process to read all applicable materials, deliberate the strengths and weaknesses of each case in good faith, independence and with objectivity, and to observe confidentiality concerning the views of others, as revealed during review discussions. A respectful, thorough, and objective review of faculty accomplishments depends upon the conscientious efforts of all participants in the review process.</p>
        <p> UTSA’s process is intended to be as forthright and transparent as possible, and as such, guidance on the respective roles and responsibilities, processes, and criteria are provided here. Questions concerning the university’s procedures for promotion and tenure may be directed to Academic Affairs. </p>
        <p> These guidelines are divided into several sections with the following contents:<br />
          <strong><a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/overview.asp">Overview of Process</a></strong>&nbsp;— a brief description of the timeline for review and the roles and responsibilities of each party at each stage of the process.<br />
          <strong><a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/criteria.asp">Principles Guiding Promotion and Tenure</a></strong>&nbsp;— information about the criteria to be followed in reviewing promotion and tenure applications, including cases for early tenure.<br />
          <strong><a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/packet.asp">Preparation of the Promotion Packet</a></strong>&nbsp;— a listing of essential and optional elements to include in the promotion packet prepared by faculty applicants.<br />
          <strong><a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/external.asp">Solicitation of External Reviews&nbsp;</a></strong>— requirements for department chairs in obtaining external review letters. <br />
          <strong><a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/process.asp">Review Process</a></strong>&nbsp;— an outline of the roles and responsibilities of the FRACs, department chair, and dean in conducting the review.<br />
          <strong><a href="http://provost.utsa.edu/home/evaluation/PromotionTenure/docs/PT-Coversheet-and-Checklist.pdf">Cover Sheet and Checklist&nbsp;</a></strong>— a summary of required and optional materials to be submitted for consideration for promotion and tenure.<br />
          <strong><a href="http://provost.utsa.edu/home/Evaluation/Help_Support/index.asp">Help/Support</a></strong>&nbsp;— Scanning Service, SharePoint help, Library services, online tutorials, etc.</p>
        <br clear="all" />
<p>&nbsp;</p>

       <h4 align="center"><a href="docs/2019-2020 PT Guidelines.pdf">This information also is available in PDF format.</a></h4>
        <p>&nbsp;</p>
       <div id="noborder"> <table width="700" border="0">
          <tr>
            <td><a href="docs/Form-for-Early-Promotion-Assoc-Professor.pdf"><img src="../../images/pt-early-review.png" width="200" height="75" /></a></td>
            <td><a href="docs/Form-for-Early-Tenure-Assoc-Professor.pdf"><img src="../../images/tenure-early-review.png" alt="" width="200" height="75" /></a></td>
            <td><a href="docs/Form-for-Review-for-Promotion-Full-Professor.pdf"><img src="../../images/prof-review.png" alt="" width="200" height="75" /></a></td>
          </tr>
        </table></div>
        <p>&nbsp;</p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
