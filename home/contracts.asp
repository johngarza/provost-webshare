<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />


<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Contracts | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
      <div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
           
            <!-- #include virtual="/inc/nav/home-nav.html"-->
           
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
        
        
        
          <h1>VPAA Commonly Used Contract Forms and Templates</h1>

        <p>
        The <a href="docs/Contracts%20-%20Routing%20Academic%20Agreements_20090811.pdf">
        Routing Academic Agreements (8/11/2009)</a> document has been provided by 
        <a href="mailto:Diane.Cordova@utsa.edu">Diane Cordova</a> to help correctly route specific academic agreements. 
        <strong>NOTE:</strong> Effective <strong>9/1/2009</strong>, 
        faculty contracts and faculty Requests for Salary Supplementation forms 
        will no longer route through Dr. Johnson for approval.&nbsp; Please route to the Provost&rsquo;s Office.        
        </p>

        <p>For commonly used templates or forms use the web links below or contact:<br /></p>

		<p><strong>Contracts Office Administration, Office of Business Affairs</strong></p>
        <p>Richard A. Wollnet<br />
            Senior Contracts Specialist<br />
            Phone: (210) 458-4069<br />
            E-Mail: <a href="mailto:Richard.Wollney@utsa.edu">Richard.Wollney@utsa.edu</a>
            </p>
     
            <p>Jennifer L. Salyers<br />
            Contracts Specialist<br />
            Phone: (210) 458-4975<br />
            E-Mail: <a href="mailto:Jennifer.Salyers@utsa.edu">Jennifer.Salyers@utsa.edu</a>
          </p>

        <p><strong>Web Links:</strong></p>

           
                <p><a href="http://www.utsa.edu/avpa/contracts.html">Contracts Office</a></p>

                <p><a href="http://www.utsa.edu/avpa/forms.html">Contracts Office - Forms and Templates</a></p>
           
        <p>If any of the links on this page to templates/forms do not work please send the specifics in an e-mail to <a href="mailto:George.Jeffery@utsa.edu">George.Jeffery@utsa.edu</a>.</p>
        
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
