<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/utsa-page-gn-asp-2col.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="doctitle" -->
<title>Faculty Cluster Hiring | UTSA Provost | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/template.css"/>
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/print.css" media="print"/>
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>
<!-- InstanceBeginEditable name="head" --><style>
#noborder table {border:none;}
#noborder table th {border:none;}
#noborder table tr {border:none;}
#noborder table td {border:none;}
</style>
<!-- InstanceEndEditable -->
<!--#include virtual="/inc-share/analytics/analytics.js"-->
</head>

<body>
<!--#include virtual="/inc-share/analytics/clicktalestart.html" -->
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
<!--#include virtual="/inc-share/accessibility/screen-reader-skip-local.html" -->
<!--#include virtual="/inc/master/brandWrap-globalNav.html" -->
</div><!-- end branding -->

<!-- WHITE AREA /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="content-white">
	<div id="content" class="clearfix">
    
    <!-- InstanceBeginEditable name="content-title" -->
	<div class="pageTitle"><a href="/home/">Office of the Provost and Vice President for Academic Affairs</a></div>
	<!-- InstanceEndEditable -->
        
        <div id="col-navigation">
            <div class="nav-page"><a name="acces-localnav" class="screen-reader"></a>
            <!-- InstanceBeginEditable name="nav" -->
            	<!--#include virtual="/cluster-hiring/includes/nav_home.asp" -->
            <!-- InstanceEndEditable -->
            </div>       
        </div>
    
        <div id="col-main"><a name="acces-content" class="screen-reader"></a>
        <!-- InstanceBeginEditable name="content" -->
		<h2><img src="images/brainhealth_banner1.jpg" width="700" height="200" />Faculty Hiring Cluster</h2>
		<h1>BRAIN HEALTH</h1>

        <p>The 21st century has been called the “The New Century of the Brain” according Scientific American (March 2014), which stated, “<em>despite a century of sustained research, brain scientists remain ignorant of the workings of the three-pound organ that is the seat of all conscious human activity.</em>” </p>
        <p>Recognizing these challenges, the Obama administration announced that it was establishing a large-scale initiative: the <a href="http://www.braininitiative.nih.gov/">Brain Research through Advancing Innovative Neurotechnologies Initiative</a>, or the BRAIN Initiative, in 2013. Called “<em>the most visible big science effort of the president's second term</em>,&quot; it was subsequently funded for $200 million in 2015.  Concurrently, the European Union launched the Human Brain Project and has dedicated $1.6 billion to their efforts over a ten-year period.</p>
        <p>UTSA has long pursued excellence in the study of the brain through its existing <a href="http://neuroscience.utsa.edu/Home.html">Neurosciences Institute</a>, which has 25 active primary investigators and their research teams, focused on understanding the basic development and functions of the brain.  </p>
        <p>Other researchers have been studying the brain from a behavioral perspective. To these existing strengths we now plan to add a complementary strength in our ability to study and understand brain-related diseases such as Alzheimer’s and Parkinson’s. In addition, over 5.2 million Americans are estimated to suffer from PTSD any given year, particularly our returning military members and veterans. </p>
        <p>Additionally, UTSA plans to build depth in our ability to use data analytics or “big data” methods to study the epidemiology of these diseases in different minority populations and their link to other diseases, with partner UTHSCSA.</p>
        <h2>OPENING: <a href="http://jobs.sciencecareers.org/job/419681/professor-endowed-chair-in-brain-health-/">Professor/Endowed Chair in Brain Health</a></h2>
        <p>&nbsp;</p>
        <table width="700" border="1">
          <tr>
            <td style="background-color:#BDB1A6;"><h2><strong>A new approach to brain health research</strong></h2><br /><img src="images/George-Perry-210.png" width="125" height="137" align="left" style="padding-right:10px" />
            <p>As we shift into the 21st century, our life expectancy has increased dramatically due to a number of factors including better health habits, improved nutrition, quality improvements in public health, and life saving medical interventions. However, while we are living longer and our bodies may be in better condition, our mental acuity isn’t keeping in tandem.</p>
              <p>
              Despite our many discoveries, many brain functions are still a mystery. While we make progress along many research tracks, we need to shift our approach, “shake the trees”, to spark possible breakthroughs. So we are harnessing the power of collaboration: across our many departments, research centers and institutes, and colleges, while leveraging the resources of the UT System, mirroring the national and international research efforts to crack the mysteries of the brain.</p>
              <p>
              This type of holistic collaboration is happening right now among UTSA researchers in the departments of Biology, Chemistry, Electrical &amp; Computer Engineering, and Kinesiology, Health &amp; Nutrition. It is also influencing the way we approach faculty recruitment. </p>
              <p>
            At UTSA, we hope this new approach to brain health leads to new research inquiries. We want to discover, clarify, mitigate and rectify the circumstances that impede cognitive function so we can all maintain a clear and active mind regardless of age and condition.</p>
            <p><strong>D. George Perry, Ph.D</strong><br />
Dean of the College of Sciences  <br />                              
Professor of Biology</p>  
  </td>
          </tr>
        </table>
       <p>&nbsp;</p>
        <h2>RESEARCH AREAS</h2>
        <div style="float: left;"><h3> • Brain circuits &amp; electrical signaling<br />
        • Neurodegenerative disease<br />
        • Traumatic brain injury</h3></div>
         <div style="float: left;"><h3>&nbsp;&nbsp;&nbsp;&nbsp;• Regenerative medicine<br />
        &nbsp;&nbsp;&nbsp;&nbsp;• Stem cell therapies<br />
        &nbsp;&nbsp;&nbsp;&nbsp;• Medicinal chemistry</h3></div>
         <div style="float: left;"><h3> &nbsp;&nbsp;&nbsp;&nbsp;• Drug design<br />
        &nbsp;&nbsp;&nbsp;&nbsp;• Neuroinflamation</h3></div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
      <p>&nbsp;</p>
       <!-- <div id="noborder"><table width="700" border="1">
          <tr>
            <td width="249"><h4>• Brain circuits &amp; electrical signaling</h4></td>
            <td width="200"><h4>• Regenerative medicine</h4></td>
            <td width="195"><h4>• 
           Drug design</h4></td>
          </tr>
          <tr>
            <td><h4>• Neurodegenerative disease</h4></td>
            <td width="200"><h4>• Stem cell therapies</h4></td>
            <td width="195"><h4>• Neuroinflammation</h4></td>
          </tr>
          <tr>
            <td><h4>• Traumatic brain injury</h4></td>
            <td width="200"><h4>• Medicinal chemistry</h4></td>
            <td width="195"><h4>&nbsp;</h4></td>
          </tr>
        </table></div>-->
       
        <h2>UTSA RESEARCH CENTERS</h2>
        <ul type="disc">
          <li>
            <h3><a href="http://neuroscience.utsa.edu/Home.html">UTSA Neurosciences Institute (NI)</a></h3>
          </li>
          <li>
            <h3><a href="https://www.utsa.edu/sciences/research/centers.html">San Antonio Cellular Therapeutics      Institute  (SACTI)</a></h3>
          </li>
          <li>
            <h3><a href="http://www.stceid.utsa.edu/">South Texas Center for Emerging Infectious Diseases (STCEID)</a></h3>
          </li>
          <li>
            <h3><a href="http://utcidd.org/">Center      for Innovation and Drug Discovery (CIDD)</a></h3>
          </li>
          <li>
            <h3><a href="http://colfa.utsa.edu/IHDR/">Institute for Health Disparities Research (IHDR)</a>
            </h3>
          </li>
        </ul>
        <hr />
        <h2>New UTSA faculty for 2016–2017</h2>
        <div id="noborder">
        <table width="700" border="1">
  <tr>
    <td><img src="images/golob-210.jpg" width="210" height="232" /></td>
    <td><img src="images/Lee-210.jpg" width="210" height="232" /></td>
    <td><img src="images/maroof-210.png" width="210" height="232" /></td>
  </tr>
  <tr>
    <td> <h3><strong>Golob Lab | <em>Cognitive Neuroscience</em></strong></h3>
      <p> <strong>Dr. Edward Golob</strong>’s lab studies how auditory processing is affected by attention, memory, and the relations between perception and action.  He seeks to understand the cognitive and neurobiological differences that accompany normal aging, age-related neurological disorders such as Alzheimer's disease, and speech fluency disorders. In many studies, they monitor the brain's electrical activity using event-related potentials and EEG; in others, they use transcranial magnetic and electrical stimulation to transiently influence brain activity. The lab is expanding its work to include traumatic brain injury and risk of future cognitive impairments, as well as patient rehabilitation using advanced computing and brain-computer interface methods.</p>     </td>
    <td><h3><strong>Lee Lab |<em> Cell Cycle and Neurodegeneration </em> </strong></h3>
  
  <p> <strong>Dr. Hyoung-gon Lee</strong>’s research hypothesizes that cell cycle re-entry in the CNS is a key pathogenic mechanism in neurodegeneration.  He is using transgenic mouse models to dissect and understand what might trigger cell cycle activation and whether this event bears any causal relationship with neurodegeneration like that observed in Alzheimer’s disease. </p>
  <p>Neurodegenerative diseases cause neuronal death, but how?  Neurons are non-proliferative, meaning their cell-cycle is arrested; perhaps accidental activation of the cell-cycle sets them on a course to die. </p></td>
    <td><h3><strong>Maroof Lab | <em>Cortical Interneuron Fate and Function in Disease</em></strong></h3>
 
  <p> <strong>Dr. Asif Maroof</strong> is using cutting-edge transgenic technology and stem cells to study the differentiation of cortical interneurons.  He is determining their diversity, how they connect, and serve information flow in the brain.  His research is fundamental to building the next generation of cell-based therapies for a whole array of neurological disorders and diseases.</p> <p>Projection neurons transmit information between brain regions, but it’s the local circuit interneurons that shape the signals being transmitted.  The diversity of interneurons confers the powerful computational capacity of the CNS, and their dysfunction results in pathological states.  </p></td>
  </tr>
</table></div>
        <h2 align="center"><a href="brain-health-faculty.asp">Brain Health Faculty at UTSA </a></h2>
       <div id="noborder"> <table width="700" border="1">
  <tr>
    <td valign="middle"><h3 align="center"><a href="brain-health-faculty.asp#biology">Biology</a></h3></td>
    <td valign="middle"><h3 align="center"><a href="brain-health-faculty.asp#chemistry">Chemistry</a></h3></td>
    <td valign="middle"><h3 align="center"><a href="brain-health-faculty.asp#ece">Electrical & Computer <br />
      Engineering</a></h3></td>
	<td valign="middle"><h3 align="center"><a href="brain-health-faculty.asp#health">Kinesiology, Health <br />
	  & Nutrition</a></h3></td>
    <td colspan="2" valign="middle"><h3 align="center"><a href="brain-health-faculty.asp#psychology">Psychology</a></h3>      
      <h3><strong>
        </h2>
      </strong></h3></td>
  </tr>
  
</table></div>
        <hr />
        <h2>Brain Health Cluster Hiring Committee</h2>
<p>Chair: Bernard Arulanandam<br />
George Perry<br />
Doug Frantz<br />
John McCarrey<br />
Sandra Morissette<br />
Anson Ong<br />
Floyd Wormley<br />
Charles Wilson</p>

        
        <h2>Brain Health Updates</h2>
        <p> <a href="http://www.utsa.edu/spotlights/brainresearch/">http://www.utsa.edu/spotlights/brainresearch/</a></p>
        <!-- InstanceEndEditable -->
        </div><!-- end col-main-->      
    </div>
</div><!--end contentWhite-->


<!-- UserFooter /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<!-- InstanceBeginEditable name="userFooter" -->
<!--#include virtual="/inc/footer/footer-about.html" -->
<!-- InstanceEndEditable -->

<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue"><div id="footer">
	<!--#include virtual="/inc/master/footerWrap.html" -->
</div></div>
<!--#include virtual="/inc-share/analytics/clicktaleend.html" -->
</body><!-- InstanceEnd --></html>
